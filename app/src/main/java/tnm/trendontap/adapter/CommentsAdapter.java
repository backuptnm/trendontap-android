package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.CommentsItem;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

/**
 * Created by TNM on 12/13/2017.
 */

public class CommentsAdapter extends ArrayAdapter<CommentsItem> {
    private ArrayList<CommentsItem> commentsItemArrayList;
    Context context;
    private LayoutInflater inflater;
    String reactimg;

    public CommentsAdapter(@NonNull Context context, ArrayList<CommentsItem> commentsItemArrayList, String reactimg) {
        super(context, 0, commentsItemArrayList);
        this.context = context;
        this.reactimg = reactimg;
        this.commentsItemArrayList = commentsItemArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return commentsItemArrayList.size();
    }


    public void setReactimg(String reactimg) {
        this.reactimg = reactimg;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_comments, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.mediumFont(context));
            holder.userimg = (ImageView) convertView.findViewById(R.id.userimg);
            holder.reactionimg = (ImageView) convertView.findViewById(R.id.reactionimg);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.description.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.name.setText(commentsItemArrayList.get(position).getName());
        holder.description.setText(commentsItemArrayList.get(position).getMessage());

        if (!TextUtils.isEmpty(commentsItemArrayList.get(position).getPhotourl())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(commentsItemArrayList.get(position).getPhotourl())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }

        if (!TextUtils.isEmpty(reactimg)) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerInside();
            requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());
            // requestOptions.placeholder(R.mipmap.userplaceholder);
            //requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(reactimg)
                    .apply(requestOptions)
                    .into(holder.reactionimg);
        }
        return convertView;
    }


    class Holder {
        TextView name, description;
        ImageView userimg, reactionimg;
    }

    public void update(ArrayList<CommentsItem> commentsItemArrayList) {
        Collections.sort(commentsItemArrayList, new Comparator<CommentsItem>() {
            @Override
            public int compare(CommentsItem o1, CommentsItem o2) {
                return Long.valueOf(o1.getTimeStamp()).compareTo(o2.getTimeStamp());
            }
        });
        this.commentsItemArrayList = commentsItemArrayList;
        notifyDataSetChanged();
    }
}
