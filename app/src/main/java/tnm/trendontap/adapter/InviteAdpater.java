package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendNotification;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.BuyerInvitations;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

public class InviteAdpater extends ArrayAdapter<Users> {
    private Context context;
    private ArrayList<Users> inviteItemArrayList;
    private LayoutInflater inflater;
    private List<Users> arraylist;

    private Users current_user;
    Preference preference;

    public InviteAdpater(@NonNull Context context, ArrayList<Users> inviteItemArrayList) {
        super(context, 0, inviteItemArrayList);
        this.context = context;
        this.inviteItemArrayList = inviteItemArrayList;
        this.arraylist = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        preference = new Preference(context, Login_Pref);
        current_user = new Gson().fromJson(preference.getString("userdetail"), Users.class);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_invite_contact, parent, false);
            holder.profilepic = (CircleImageView) convertView.findViewById(R.id.profilepic);
            holder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            holder.textViewName.setTypeface(RobotoFont.regularFont(context));
            holder.connect = (TextView) convertView.findViewById(R.id.connect);
            holder.connect.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final Users users = inviteItemArrayList.get(position);
        holder.textViewName.setText(getName(users.getFirstName(), users.getLastName()));

        if (!TextUtils.isEmpty(users.getProfilePic())) {

            Picasso.with(context)
                    .load(users.getProfilePic())
                    .placeholder(R.mipmap.userplaceholder)
                    .error(R.mipmap.userplaceholder)
                    .into(holder.profilepic);
        }
        holder.connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                        child(users.getUserID()).setValue(true);
             /*   FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                        child(users.getUserID()).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);*/
                holder.connect.setText("Requested");


                ActivityItem activityItem = new ActivityItem();
                activityItem.setProfilePic(current_user.getProfilePic());
                activityItem.setLongMessage("started");
                activityItem.setMessage(current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG);
                activityItem.setPostID("");
                activityItem.setPostImageURL("");
                activityItem.setReactionID("");
                activityItem.setShortMessage("following you");
                activityItem.setTiggerBy(current_user.getFirstName() + " " + current_user.getLastName());
                activityItem.setActivityType(BuyerFollower);
                activityItem.setTimeStamp(System.currentTimeMillis());
                activityItem.setUserID(current_user.getUserID());


                final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
                final String key = databaseReference.push().getKey();
                activityItem.setId(key);
                databaseReference.child(key).setValue(activityItem);

                FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                        child(users.getUserID()).
                        child(key).setValue(true);

                sendNotification(users.getUserID(), current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG, activityItem);

            }
        });

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query query = rootRef.child(BuyerInvitations).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(users.getUserID())) {
                    holder.connect.setText("Requested");
                } else {
                    holder.connect.setText("Connect");
                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return convertView;
    }

    class Holder {
        TextView textViewName, connect;
        CircleImageView profilepic;
    }

    @Override
    public int getCount() {
        return inviteItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addList(ArrayList<Users> list) {
        inviteItemArrayList = list;
        this.arraylist.addAll(list);
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        inviteItemArrayList.clear();
        if (charText.length() == 0) {
            inviteItemArrayList.addAll(arraylist);
        } else {
            filterData(charText);
        }

        notifyDataSetChanged();
    }

    private void filterData(String charText) {

        for (Users users : arraylist) {
            if (users.getFirstName().toLowerCase(Locale.getDefault()).contains(charText) || users.getLastName().toLowerCase(Locale.getDefault()).contains(charText)) {
                inviteItemArrayList.add(users);
            }
        }
       /* if (inviteItemArrayList.size() == 0) {
             nosupllierdata.setVisibility(View.VISIBLE);
        } else {
             nosupllierdata.setVisibility(View.GONE);
        }*/
    }
}
