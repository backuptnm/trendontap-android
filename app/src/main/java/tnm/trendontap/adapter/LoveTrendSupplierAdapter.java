package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import tnm.trendontap.R;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;



public class LoveTrendSupplierAdapter extends ArrayAdapter<Users> implements StickyListHeadersAdapter {
    private ArrayList<Users> supplierItemArrayList;
    private Context mContext;
    private LayoutInflater inflater;
    private List<Users> arraylist;
    public SparseBooleanArray mSelectedItemsIds;
    ArrayList<Users> selecteduser;



    public LoveTrendSupplierAdapter(@NonNull Context mContext, ArrayList<Users> supplierItemArrayList) {
        super(mContext, 0);
        this.mContext = mContext;
        this.supplierItemArrayList = supplierItemArrayList;
        selecteduser = new ArrayList<>();
        this.arraylist = new ArrayList<>();
        mSelectedItemsIds = new SparseBooleanArray();
        // this.nosupllierdata = nosupllierdata;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_love_trend_supplier_item, parent, false);
            holder.othersupplier = (LinearLayout) convertView.findViewById(R.id.othersupplier);
            holder.allsupliers = (RelativeLayout) convertView.findViewById(R.id.allsupliers);
            holder.supplierimg = (CircleImageView) convertView.findViewById(R.id.supplierimg);
            holder.imageSelect = (ImageView) convertView.findViewById(R.id.imageSelect);
            holder.imageSelectAll = (ImageView) convertView.findViewById(R.id.imageSelectAll);
            holder.suppliername = (TextView) convertView.findViewById(R.id.suppliername);
            holder.suppliername.setTypeface(RobotoFont.mediumFont(mContext));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.allsupliers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imageSelectAll.setVisibility(View.VISIBLE);
                clearallsection();
            }
        });

        if (getSelectedCount() > 0) {
            holder.imageSelectAll.setVisibility(View.GONE);
        } else {
            holder.imageSelectAll.setVisibility(View.VISIBLE);
        }


        if (position == 0) {
            holder.allsupliers.setVisibility(View.VISIBLE);
            holder.othersupplier.setVisibility(View.GONE);
        } else {
            holder.allsupliers.setVisibility(View.GONE);
            holder.othersupplier.setVisibility(View.VISIBLE);

            Users supplierItem = supplierItemArrayList.get(position - 1);

            holder.suppliername.setText(supplierItem.getFirstName() + " " + supplierItem.getLastName());

            holder.type.setText(supplierItem.getJobTitle());

            if (!TextUtils.isEmpty(supplierItem.getProfilePic())) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.mipmap.userplaceholder);
                requestOptions.error(R.mipmap.userplaceholder);

                Glide.with(mContext)
                        .load(supplierItem.getProfilePic())
                        .apply(requestOptions)
                        .into(holder.supplierimg);
            }

            if (mSelectedItemsIds.get(position)) {
                holder.imageSelect.setVisibility(View.VISIBLE);
            } else {
                holder.imageSelect.setVisibility(View.GONE);
            }
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return supplierItemArrayList.size() + 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
            selecteduser.add(supplierItemArrayList.get(position-1));
        } else {
            mSelectedItemsIds.delete(position);
            selecteduser.remove(supplierItemArrayList.get(position-1));
        }
        notifyDataSetChanged();
    }

    public void clearallsection() {
        mSelectedItemsIds.clear();
        selecteduser.clear();
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public ArrayList<Users> getSupplierItemArrayList() {
        return selecteduser;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_supplier, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.text.setTypeface(RobotoFont.mediumFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        if (position == 0) {
            holder.text.setText("Send post to all");
        } else if (position == 1) {
            holder.text.setText("Or select individual");
        }

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0)
            return 0;
        else
            return 1;

    }

    class Holder {
        TextView suppliername, type;
        CircleImageView supplierimg;
        ImageView imageSelect,imageSelectAll;
        LinearLayout othersupplier;
        RelativeLayout allsupliers;
    }

    class HeaderViewHolder {
        TextView text;
    }

    public void addList(ArrayList<Users> list) {
        supplierItemArrayList = list;
        this.arraylist.addAll(list);
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        supplierItemArrayList.clear();
        if (charText.length() == 0) {
            supplierItemArrayList.addAll(arraylist);
            if (supplierItemArrayList.size() == 0) {
                //    nosupllierdata.setVisibility(View.VISIBLE);
            } else {
                //  nosupllierdata.setVisibility(View.GONE);
            }
        } else {
            filterData(charText);
        }

        notifyDataSetChanged();
    }

    private void filterData(String charText) {

        for (Users supplierItem : arraylist) {
            if ((supplierItem.getFirstName() + " " + supplierItem.getLastName()).toLowerCase(Locale.getDefault()).contains(charText)) {
                supplierItemArrayList.add(supplierItem);
            } else if (supplierItem.getJobTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                supplierItemArrayList.add(supplierItem);
            }
        }
        if (supplierItemArrayList.size() == 0) {
            // nosupllierdata.setVisibility(View.VISIBLE);
        } else {
            //  nosupllierdata.setVisibility(View.GONE);
        }
    }

}
