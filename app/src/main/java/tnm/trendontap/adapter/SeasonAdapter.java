package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.modal.SeasonItem;
import tnm.trendontap.utility.RobotoFont;


public class SeasonAdapter extends ArrayAdapter<SeasonItem> {
    private ArrayList<SeasonItem> seasonItemArrayList;
    Context context;
    LinearLayout noseasondata;

    public SeasonAdapter(@NonNull Context context, ArrayList<SeasonItem> seasonItemArrayList, LinearLayout noseasondata) {
        super(context, 0, seasonItemArrayList);
        this.seasonItemArrayList = seasonItemArrayList;
        this.noseasondata = noseasondata;
        this.context = context;
    }

    @Override
    public int getCount() {
        return seasonItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 3;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context,
                    R.layout.item_list_app, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        SeasonItem item = seasonItemArrayList.get(position);
        holder.name.setText(item.getName());
        return convertView;
    }

    class ViewHolder {
        TextView name;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.name);
            name.setTypeface(RobotoFont.mediumFont(context));
            view.setTag(this);
        }
    }

    public void addList(ArrayList<SeasonItem> list) {
        seasonItemArrayList = list;
        notifyDataSetChanged();
    }




}
