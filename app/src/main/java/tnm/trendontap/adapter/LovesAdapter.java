package tnm.trendontap.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Reactions_Loves_Fragment;
import tnm.trendontap.fragment.Reactions_Trends_Fragment;
import tnm.trendontap.fragment.UploadLoveFragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.Position;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;
import static tnm.trendontap.utility.Utils.setDialogwidth;


public class LovesAdapter extends ArrayAdapter<LovesItem> {
    LayoutInflater inflater;
    Context mContext;
    private ArrayList<LovesItem> lovesItemArrayList;
    int selectedPos = -1;
    ProgressDialog progressDialog;
    LoveImagesAdapter myPagerAdapter;

    public LovesAdapter(@NonNull Context mContext, ArrayList<LovesItem> lovesItemArrayList) {
        super(mContext, 0, lovesItemArrayList);
        this.mContext = mContext;
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Please wait..");
        this.lovesItemArrayList = lovesItemArrayList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lovesItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_loves_item, parent, false);
            holder.rlTagLayout = (RelativeLayout) convertView.findViewById(R.id.rlTagLayout);
            holder.locationname = (TextView) convertView.findViewById(R.id.locationname);
            holder.locationname.setTypeface(RobotoFont.mediumFont(mContext));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.interesttrend = (TextView) convertView.findViewById(R.id.interesttrend);
            holder.interesttrend.setTypeface(RobotoFont.mediumFont(mContext)); //regular normal text
            holder.deliverydatetxt = (TextView) convertView.findViewById(R.id.deliverydatetxt);
            holder.deliverydatetxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.deliverydate = (TextView) convertView.findViewById(R.id.deliverydate);
            holder.deliverydate.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactioncnt = (TextView) convertView.findViewById(R.id.reactioncnt);
            holder.reactioncnt.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactionbtn = (LinearLayout) convertView.findViewById(R.id.reactionbtn);
            holder.doticon = (ImageView) convertView.findViewById(R.id.doticon);
            holder.item_tagged = (ImageView) convertView.findViewById(R.id.item_tagged);
            holder.loveicon = (ImageView) convertView.findViewById(R.id.loveicon);
            holder.optionicon = (LinearLayout) convertView.findViewById(R.id.optionicon);
            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final LovesItem lovesItem = lovesItemArrayList.get(position);

        if (lovesItem.isNotify())
            holder.doticon.setVisibility(View.VISIBLE);
        else
            holder.doticon.setVisibility(View.INVISIBLE);

        holder.abouttrend.setText(lovesItem.getAbouttrend());
        holder.locationname.setVisibility(View.VISIBLE);
        try {
            if (lovesItem.getLocationItemArrayList() != null) {
                if (!TextUtils.isEmpty(lovesItem.getLocationItemArrayList().title) && lovesItem.getLocationItemArrayList().latitude != 0.0) {
                    holder.locationname.setText(lovesItem.getLocationItemArrayList().getTitle() + " >");
                } else {
                    holder.locationname.setVisibility(View.GONE);
                }
            } else {
                holder.locationname.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            holder.locationname.setVisibility(View.GONE);
        }

        holder.locationname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("location", lovesItem.getLocationItemArrayList());
                MainActivity.loadContentFragment(new tnm.trendontap.fragment.MapFragment(), false, bundle);
            }
        });

        if (lovesItem.getLikecnt() == 0) {
            holder.loveicon.setImageResource(R.mipmap.loves_icon);
            holder.interesttrend.setText("No interest in your love yet");
            holder.interesttrend.setTextColor(Color.parseColor("#929292"));
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
        } else {
            holder.loveicon.setImageResource(R.mipmap.loves_icon_active);
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
            holder.interesttrend.setTextColor(Color.parseColor("#81BBFF"));
            String likertxt = "";

            if (lovesItem.getLikecnt() == 1) {
                likertxt = lovesItem.getLiker1() + " shows interest in your love";
                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1(), "");
            } else if (lovesItem.getLikecnt() == 2) {
                likertxt = lovesItem.getLiker1() + " and " + lovesItem.getLiker2() + " show interest in your love";
                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1(), lovesItem.getLiker2());
            } else {
                int otherlikers = (int) lovesItem.getLikecnt() - 2;
                String likeretxt = "";
                if (otherlikers == 1) {
                    likeretxt = String.valueOf(otherlikers) + " other";
                } else
                    likeretxt = String.valueOf(otherlikers) + " others";

                likertxt = lovesItem.getLiker1() + ", " + lovesItem.getLiker2() + " and " + likeretxt + "  show interest in your love";

                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1() + ", " + lovesItem.getLiker2(), likeretxt);

            }
        }

        int reactcnt = Integer.parseInt(lovesItem.getReactioncnt());
        holder.reactiontxt.setText("Reactions");
        if (reactcnt > 0) {
            if (reactcnt == 1)
                holder.reactiontxt.setText("Reaction");

            holder.reactiontxt.setBackgroundResource(R.drawable.rectangle_blue_bg);

        } else {
            holder.reactiontxt.setBackgroundResource(R.drawable.rectangle_grey_bg);
        }

        holder.optionicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(mContext, lovesItem, position);
            }
        });
        holder.reactionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(lovesItem.getReactioncnt()) == 0) {
                    return;
                }
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts);
                databaseReference.child(lovesItem.getId()).child(UnreadReactionCount).setValue(false);
                holder.doticon.setVisibility(View.INVISIBLE);
                Bundle bundle = new Bundle();
                bundle.putString("title", lovesItem.getName());
                bundle.putString("from", "love");
                bundle.putString("category", lovesItem.getCategory());
                bundle.putStringArrayList("photourls", lovesItem.getPhotoUrlsList());
                bundle.putString("postid", lovesItem.getId());
                MainActivity.loadContentFragment(new Reactions_Loves_Fragment(), false, bundle);
            }
        });


        if (lovesItem.getDeliverydate().trim().equalsIgnoreCase("")) {
            holder.deliverydate.setVisibility(View.GONE);
            holder.deliverydatetxt.setVisibility(View.GONE);
        } else {
            holder.deliverydate.setVisibility(View.VISIBLE);
            holder.deliverydatetxt.setVisibility(View.VISIBLE);
            holder.deliverydate.setText(lovesItem.getDeliverydate());
        }

        holder.daysago.setText(Utils.getDay(lovesItem.getDaysago()));
        holder.reactioncnt.setText(lovesItem.getReactioncnt());

       /* int reactioncnt = Integer.parseInt(lovesItem.getReactioncnt());
        if (reactioncnt == 0) {
            holder.reactiontxt.setText("Reactions");
        } else if (reactioncnt == 1) {
            holder.reactiontxt.setText("Reaction");
        } else {
            holder.reactiontxt.setText("Reactions");
        }*/

        myPagerAdapter = new LoveImagesAdapter(mContext, lovesItem.getPhotoUrls());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (lovesItem.getPhotoUrls().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                ArrayList<String> array = lovesItemArrayList.get(position).getPhotoUrls();
                intent.putStringArrayListExtra("photoUrlsList", array);
                mContext.startActivity(intent);
            }
        });

        holder.item_tagged.setTag(position);
        holder.item_tagged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                LovesItem lovesItem = lovesItemArrayList.get(pos);
                if (lovesItem.getTag_itemsArrayList().size() == 0) {
                    return;
                }
                if (selectedPos == pos) {
                    holder.rlTagLayout.removeAllViews();
                    selectedPos = -1;
                    return;
                }
                selectedPos = pos;
                holder.rlTagLayout.removeAllViews();
                ArrayList<Tag_Items> tag_itemsArrayList = lovesItem.getTag_itemsArrayList();
                for (int i = 0; i < tag_itemsArrayList.size(); i++) {
                    Tag_Items tag_items = tag_itemsArrayList.get(i);
                    if (tag_items != null) {
                        try {
                            String tag = tag_items.name;
                            Position position = tag_items.position;
                            setTagName(mContext, holder.rlTagLayout, i, position.xPos, position.yPos, tag);
                        } catch (NullPointerException e) {
                        }

                    }
                }
            }
        });

        if (lovesItem.getTag_itemsArrayList().size() > 0) {
            holder.item_tagged.setVisibility(View.VISIBLE);
        } else {
            holder.item_tagged.setVisibility(View.GONE);
        }
        holder.rlTagLayout.removeAllViews();
        return convertView;
    }


    private void setTagName(Context context, RelativeLayout rlImageTag, int itemCount, final float x, final float y, final String tag) {
        Log.d("Nzm", "x=" + x + "y=" + y);
        final FrameLayout mFrame = new FrameLayout(context);
        RelativeLayout mInnerFrame = new RelativeLayout(context);

        if (x != 0 && y != 0) {
            FrameLayout.LayoutParams mParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mFrame.setLayoutParams(mParams);
            mFrame.setBackgroundColor(Color.parseColor("#929292"));
            //  mFrame.setPadding(x, y, 0, 0);
            mFrame.setX(x);
            mFrame.setY(y);
            RelativeLayout.LayoutParams mParams1 = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mInnerFrame.setLayoutParams(mParams1);
            mInnerFrame.setPadding(10, 10, 10, 10);
            //mFrame1.setBackground(getDrawable(R.mipmap.item_tagged_text_container));
            mInnerFrame.setBackgroundColor(Color.parseColor("#000000"));


            TextView tv = new TextView(context);
            tv.setLayoutParams(mParams);
            tv.setText(tag);
            tv.setTextSize(16);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            mInnerFrame.addView(tv);

            FrameLayout.LayoutParams imageParam = new FrameLayout.LayoutParams(45, 45);
            // imageParam.setMargins(0,-20,-20,0);
            ImageView image = new ImageView(context);
            image.setId(itemCount);
            image.setTag(itemCount);
            image.setLayoutParams(imageParam);
            image.setPadding(10, 10, 10, 10);
            image.setMaxHeight(25);
            image.setVisibility(View.GONE);
            image.setImageResource(R.mipmap.close);
            image.setMaxWidth(25);

            mInnerFrame.setTag(itemCount);

            mInnerFrame.addView(image);
            mInnerFrame.setTag(itemCount);
            mFrame.addView(mInnerFrame);
            rlImageTag.addView(mFrame);

        }
    }

    class Holder {
        TextView abouttrend, interesttrend, deliverydatetxt, deliverydate, daysago, reactioncnt, reactiontxt, locationname;
        ImageView doticon, item_tagged, loveicon;
        LinearLayout optionicon, reactionbtn;
        ClickableViewPager pager;
        CirclePageIndicator indicator;
        RelativeLayout rlTagLayout;
    }

    public void addList(ArrayList<LovesItem> list) {
        lovesItemArrayList = list;

        Collections.sort(lovesItemArrayList, new Comparator<LovesItem>() {
            @Override
            public int compare(LovesItem o1, LovesItem o2) {
                return Long.valueOf(o2.getDaysago()).compareTo(o1.getDaysago());
            }
        });

        notifyDataSetChanged();
    }

    public ArrayList<LovesItem> getArrayList() {
        return lovesItemArrayList;
    }

    private void setFont(TextView textView, String str, String str1, String str2) {

        int part1 = str.indexOf(str1);
        SpannableStringBuilder SS = new SpannableStringBuilder(str);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part1, part1 + str1.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part1, part1 + str1.length(), 0);


        if (!str2.equalsIgnoreCase("")) {
            int part2 = str.indexOf(str2);
            SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part2, part2 + str2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part2, part2 + str2.length(), 0);
        }

        textView.setText(SS);
    }


    private void onOptionsDialog(final Context activity, final LovesItem lovesItem, final int position) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options);
        dialog.setCancelable(false);

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };
        View.OnClickListener editClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putParcelable("lovesItem", lovesItem);
                bundle.putString("action", "edit");
                MainActivity.loadContentFragment(new UploadLoveFragment(), false, bundle);
            }
        };


        View.OnClickListener deleteClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeLoveItem(lovesItem, position, dialog);
            }
        };

        TextView txt_edit = ((TextView) dialog.findViewById(R.id.txt_edit));
        txt_edit.setTypeface(RobotoFont.regularFont(activity));
        txt_edit.setOnClickListener(editClick);
        final TextView txt_share = ((TextView) dialog.findViewById(R.id.txt_share));
        txt_share.setTypeface(RobotoFont.regularFont(activity));
        final TextView txt_send = ((TextView) dialog.findViewById(R.id.txt_send));
        txt_send.setTypeface(RobotoFont.regularFont(activity));
        TextView txt_delete = ((TextView) dialog.findViewById(R.id.txt_delete));
        txt_delete.setTypeface(RobotoFont.regularFont(activity));
        txt_delete.setOnClickListener(deleteClick);
        TextView txt_cancel = ((TextView) dialog.findViewById(R.id.txt_cancel));
        txt_cancel.setTypeface(RobotoFont.regularFont(activity));
        txt_cancel.setOnClickListener(cancelClick);
        dialog.show();

        FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren())
                            return;
                        final ArrayList<String> supplierlist = new ArrayList<>();
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            supplierlist.add(dataSnapshot1.getKey());
                        }

                        txt_share.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                        txt_share.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                sentToDialog(mContext, lovesItem.getId(), supplierlist, "share");
                            }
                        });
                        txt_send.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                        txt_send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                sentToDialog(mContext, lovesItem.getId(), supplierlist, "sendto");
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void removeLoveItem(final LovesItem lovesItem, final int position, final Dialog dialog) {
        progressDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);
        DatabaseReference databaseReference2 = FirebaseDatabase.getInstance().getReference(UserPosts);
        databaseReference2.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves).child(lovesItem.getId()).removeValue();
        databaseReference.child(lovesItem.getId()).removeValue();
        lovesItemArrayList.remove(position);
        //   databaseReference1.child(lovesItem.getId()).removeValue();
        databaseReference2.child(lovesItem.getId()).removeValue();

        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
        databaseReference1.orderByKey().equalTo(lovesItem.getId());
        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    databaseReference1.child(dataSnapshot1.getKey()).child(Loves).child(lovesItem.getId()).removeValue();
                    dialog.dismiss();
                }
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void sentToDialog(Context context, String postid, ArrayList<String> supplierlist, String from) {
        final Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.shareoptiondialog);
        dialog.setCancelable(false);

        RecyclerView horizontal_recycler_view = (RecyclerView) dialog.findViewById(R.id.horizontal_recycler_view);
        ArrayList<Users> horizontalList = new ArrayList<>();

        TextView txt_cancel = ((TextView) dialog.findViewById(R.id.txt_cancel));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.txt_cancel)).setTypeface(RobotoFont.mediumFont(context));

        ShareContactAdpater shareContactAdpater = new ShareContactAdpater(context, horizontalList, dialog);
        shareContactAdpater.setFrom(from);
        shareContactAdpater.setPostid(postid);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
        horizontal_recycler_view.setAdapter(shareContactAdpater);

        if (from.equalsIgnoreCase("share"))
            processShare(postid, horizontalList, shareContactAdpater, dialog, supplierlist, horizontal_recycler_view);
        else
            processSend(postid, horizontalList, shareContactAdpater, dialog, supplierlist, horizontal_recycler_view);

    }

    int cnt = 0;

    public void processSend(final String postid, final ArrayList<Users> horizontalList,
                            final ShareContactAdpater shareContactAdpater,
                            final Dialog dialog,
                            final ArrayList<String> supplierlist, final RecyclerView horizontal_recycler_view) {
        cnt = 0;

        for (int j = 0; j < supplierlist.size(); j++) {
            final int finalJ = j;
            FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).
                    child(supplierlist.get(finalJ)).child(Loves).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            if (dataSnapshot.hasChildren() && dataSnapshot.hasChild(postid)) {
                                cnt++;
                                FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                        child(supplierlist.get(finalJ)).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot usersval) {
                                                horizontal_recycler_view.setVisibility(View.VISIBLE);
                                                horizontalList.add(usersval.getValue(Users.class));
                                                shareContactAdpater.notifyDataSetChanged();
                                                if (!dialog.isShowing()) {
                                                    dialog.show();
                                                    setDialogwidth(dialog, mContext, 60);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }

                            if ((finalJ + 1) == supplierlist.size() && cnt == 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(mContext, "You have not shared with your any current suppliers", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(mContext, "You have not shared with your any current suppliers", Toast.LENGTH_LONG).show();
                        }
                    });
        }

    }


    int sharecnt = 0;

    public void processShare(final String postid, final ArrayList<Users> horizontalList,
                             final ShareContactAdpater shareContactAdpater,
                             final Dialog dialog,
                             final ArrayList<String> supplierlist, final RecyclerView horizontal_recycler_view) {
        sharecnt = 0;
        for (int j = 0; j < supplierlist.size(); j++) {
            final int finalJ = j;
            FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).
                    child(supplierlist.get(finalJ)).child(Loves).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            if (!dataSnapshot.hasChild(postid)) {
                                sharecnt++;
                                FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                        child(supplierlist.get(finalJ)).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot usersval) {
                                                horizontal_recycler_view.setVisibility(View.VISIBLE);
                                                horizontalList.add(usersval.getValue(Users.class));
                                                shareContactAdpater.notifyDataSetChanged();
                                                if (!dialog.isShowing()) {
                                                    dialog.show();
                                                    setDialogwidth(dialog, mContext, 60);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }

                            if ((finalJ + 1) == supplierlist.size() && sharecnt == 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(mContext, "You have already shared with your all current suppliers", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(mContext, "You have already shared with your all current suppliers", Toast.LENGTH_LONG).show();
                        }
                    });
        }

    }

}
