package tnm.trendontap.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Reactions_Trends_Fragment;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.utility.ScaledImageView;


public class GridItemAdapter extends ArrayAdapter<PhotoTimeStamp> {
    private ArrayList<PhotoTimeStamp> photoTimeStampArrayList;
    Context context;

    public GridItemAdapter(@NonNull Context context, ArrayList<PhotoTimeStamp> photoTimeStampArrayList) {
        super(context, 0, photoTimeStampArrayList);
        this.photoTimeStampArrayList = photoTimeStampArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return photoTimeStampArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context,
                    R.layout.data_item, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        final PhotoTimeStamp photoTimeStamp = photoTimeStampArrayList.get(position);

        String item = photoTimeStamp.getImage();
        if (!TextUtils.isEmpty(item)) {
            RequestOptions requestOptions = new RequestOptions();
            // requestOptions.placeholder(R.mipmap.userplaceholder);
            // requestOptions.error(R.mipmap.ic_launcher);

            Glide.with(context)
                    .load(item)
                    .apply(requestOptions)
                    .into(holder.data_item_image);
        }
        //holder.data_item_image.setTag(photoTimeStampArrayList.get(position));
        holder.data_item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (photoTimeStamp.getReactionsCount() == 0) {
                    Toast.makeText(context, "You currently have no reaction for this love", Toast.LENGTH_LONG).show();
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("title", "");
                bundle.putString("category", "");
                bundle.putString("postimg", photoTimeStamp.getImage());
                bundle.putString("postid", photoTimeStamp.getPostid());
                bundle.putString("from", "love");
                MainActivity.loadContentFragment(new Reactions_Trends_Fragment(), false, bundle);
            }
        });
        return convertView;
    }

    class ViewHolder {
        ScaledImageView data_item_image;

        public ViewHolder(View view) {
            data_item_image = (ScaledImageView) view.findViewById(R.id.data_item_image);
            view.setTag(this);
        }
    }

    public void addList(ArrayList<PhotoTimeStamp> list) {
        photoTimeStampArrayList = list;
        notifyDataSetChanged();
    }


}
