package tnm.trendontap.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.ConnectUser_Fragment;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;


public class ActiveContactAdpater extends RecyclerView.Adapter<ActiveContactAdpater.MyViewHolder> {

    private ArrayList<Users> dataSet;
    Context context;

    public ActiveContactAdpater(Context context, ArrayList<Users> data) {
        this.dataSet = data;
        this.context = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.textViewName.setTypeface(RobotoFont.regularFont(context));
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (getAdapterPosition()) {
                        case 0:
                            break;
                        case 1:
                            break;
                    }
                }
            });
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_active_contact, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textViewName.setText(dataSet.get(listPosition).getFirstName());

        if (!TextUtils.isEmpty(dataSet.get(listPosition).getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(dataSet.get(listPosition).getProfilePic())
                    .apply(requestOptions)
                    .into(holder.imageViewIcon);
        }

        holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("UserID", dataSet.get(listPosition).getUserID());
                //    bundle.putString("name", inviteItemArrayList.get(i).getFirstName() + " " + inviteItemArrayList.get(i).getLastName());
                MainActivity.loadContentFragment(new ConnectUser_Fragment(), false, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
