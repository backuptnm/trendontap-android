package tnm.trendontap.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Profile_Fragment;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;


public class ShareContactAdpater extends RecyclerView.Adapter<ShareContactAdpater.MyViewHolder> {

    private ArrayList<Users> dataSet;
    Context context;
    Dialog dialog;

    String from = "", postid = "";

    public ShareContactAdpater(Context context, ArrayList<Users> data, Dialog dialog) {
        this.dataSet = data;
        this.context = context;
        this.dialog = dialog;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;

        ImageView userimg;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.textViewName.setTypeface(RobotoFont.regularFont(context));
            this.userimg = (ImageView) itemView.findViewById(R.id.userimg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    if (getFrom().equalsIgnoreCase("sendto")) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("loggeduser", false);
                        bundle.putString("userid", dataSet.get(getAdapterPosition()).getUserID());
                        MainActivity.loadContentFragment(new Profile_Fragment(), false, bundle);
                    } else {
                        shareWithuser(getPostid(), dataSet.get(getAdapterPosition()).getUserID());
                    }
                }
            });
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_share_contact, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textViewName.setText(dataSet.get(listPosition).getFirstName());

        if (!TextUtils.isEmpty(dataSet.get(listPosition).getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(dataSet.get(listPosition).getProfilePic())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }


    public void shareWithuser(String postid, String userid) {
        DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
        userpostsfeed.child(userid).child(Loves).child(postid).setValue(true);
        Toast.makeText(context, "Shared successfully", Toast.LENGTH_LONG).show();
    }
}
