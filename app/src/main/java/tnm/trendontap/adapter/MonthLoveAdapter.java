package tnm.trendontap.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;


public class MonthLoveAdapter extends ArrayAdapter<PhotoTimeStamp> {
    private Context context;
    private LayoutInflater inflater;

    ArrayList<PhotoTimeStamp> photoTimeStampArrayList;

    public MonthLoveAdapter(@NonNull Context context, ArrayList<PhotoTimeStamp> photoTimeStampArrayList) {
        super(context, 0, photoTimeStampArrayList);
        this.photoTimeStampArrayList = photoTimeStampArrayList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return photoTimeStampArrayList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final PhotoTimeStamp photoTimeStamp = photoTimeStampArrayList.get(position);



        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_activity_list_item, parent, false);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }



        return convertView;
    }

    private class Holder {
        TextView txt, daysago, connect;
        ImageView doticon, postimg;
        CircleImageView avtarurl;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
