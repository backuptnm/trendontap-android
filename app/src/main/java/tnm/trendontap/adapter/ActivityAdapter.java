package tnm.trendontap.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.BuyerConnections;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.BuyerInvitations;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;


public class ActivityAdapter extends ArrayAdapter<ActivityItem> {
    private ArrayList<ActivityItem> activityItemArrayList;
    private Context context;
    private LayoutInflater inflater;

    public ActivityAdapter(@NonNull Context context, ArrayList<ActivityItem> activityItemArrayList) {
        super(context, 0, activityItemArrayList);
        this.activityItemArrayList = activityItemArrayList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return activityItemArrayList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_activity_list_item, parent, false);
            holder.avtarurl = (CircleImageView) convertView.findViewById(R.id.avtarurl);
            holder.postimg = (SquareImageView) convertView.findViewById(R.id.postimg);
            holder.doticon = (ImageView) convertView.findViewById(R.id.doticon);
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(context));
            holder.txt = (TextView) convertView.findViewById(R.id.txt);
            holder.txt.setTypeface(RobotoFont.regularFont(context));
            holder.connect = (TextView) convertView.findViewById(R.id.connect);
            holder.connect.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final ActivityItem activityItem = activityItemArrayList.get(position);
        String fullstr = activityItem.getTiggerBy() + " " + activityItem.getLongMessage() + " " + activityItem.getShortMessage();
        try {
            setFont(holder.txt, fullstr, activityItem.getTiggerBy(), activityItem.getShortMessage());
        } catch (Exception e) {

        }

        try {
            List<String> stringArrayList = activityItemArrayList.get(position).getIsReadUsers();
            if (stringArrayList.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                holder.doticon.setVisibility(View.INVISIBLE);
            } else
                holder.doticon.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            holder.doticon.setVisibility(View.VISIBLE);
        }
        holder.daysago.setText(Utils.getDay(activityItem.getTimeStamp()));
        if (!TextUtils.isEmpty(activityItem.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);
            Glide.with(context)
                    .load(activityItem.getProfilePic())
                    .apply(requestOptions)
                    .into(holder.avtarurl);
        }

        if (!TextUtils.isEmpty(activityItem.getPostImageURL())) {
            RequestOptions requestOptions = new RequestOptions();


            Glide.with(context)
                    .load(activityItem.getPostImageURL())
                    .apply(requestOptions)
                    .into(holder.postimg);
        }


        if (!activityItem.getActivityType().equalsIgnoreCase(BuyerFollower)) {
            holder.postimg.setVisibility(View.VISIBLE);
        } else {
            holder.postimg.setVisibility(View.GONE);
        }


        holder.connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                        child(activityItem.getUserID()).setValue(true);
                FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                        child(activityItem.getUserID()).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);

                FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                        child(activityItem.getUserID()).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
                FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                        child(activityItem.getUserID()).removeValue();

                holder.connect.setVisibility(View.GONE);

            }
        });

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query query = rootRef.child(BuyerInvitations).child(activityItem.getUserID());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChildren()) {

                    for (DataSnapshot requested : dataSnapshot.getChildren()) {
                        if (activityItem.getActivityType().equals(BuyerFollower) && FirebaseAuth.getInstance().getCurrentUser().getUid().equalsIgnoreCase(requested.getKey())) {
                            holder.connect.setVisibility(View.VISIBLE);
                        } else {
                            holder.connect.setVisibility(View.GONE);
                        }
                    }
                } else {
                    holder.connect.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                holder.connect.setVisibility(View.GONE);
            }
        });

        return convertView;
    }

    private class Holder {
        TextView txt, daysago, connect;
        ImageView doticon;
        CircleImageView avtarurl;
        SquareImageView postimg;
    }

    public void setList(ArrayList<ActivityItem> activityItemArrayList) {
        this.activityItemArrayList = activityItemArrayList;
        Collections.sort(activityItemArrayList, new Comparator<ActivityItem>() {
            @Override
            public int compare(ActivityItem o1, ActivityItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }

    private void setFont(TextView textView, String str, String str1, String str2) {

        int part1 = str.indexOf(str1);
        int part2 = str.indexOf(str2);

        SpannableStringBuilder SS = new SpannableStringBuilder(str);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(context)), part1, part1 + str1.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(context)), part2, part2 + str2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#000000")), part1, part1 + str1.length(), 0);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#70B1FF")), part2, part2 + str2.length(), 0);
        textView.setText(SS);
    }

}
