package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;

import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;



public class SupplierAdapter extends ArrayAdapter<Users> {
    private ArrayList<Users> supplierItemArrayList;
    private Context mContext;
    private LayoutInflater inflater;
    private List<Users> arraylist;

    private LinearLayout nosupllierdata;


    public SupplierAdapter(@NonNull Context mContext, ArrayList<Users> supplierItemArrayList, LinearLayout nosupllierdata) {
        super(mContext, 0, supplierItemArrayList);
        this.mContext = mContext;
        this.supplierItemArrayList = supplierItemArrayList;
        this.arraylist = new ArrayList<>();
        this.nosupllierdata = nosupllierdata;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_supplier_item, parent, false);
            holder.supplierimg = (CircleImageView) convertView.findViewById(R.id.supplierimg);
            holder.suppliername = (TextView) convertView.findViewById(R.id.suppliername);
            holder.suppliername.setTypeface(RobotoFont.mediumFont(mContext));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Users supplierItem = supplierItemArrayList.get(position);

        String fname = supplierItem.getFirstName().substring(0, 1).toUpperCase() + supplierItem.getFirstName().substring(1);
        String lname = supplierItem.getLastName().substring(0, 1).toUpperCase() + supplierItem.getLastName().substring(1);
        holder.suppliername.setText(fname + " " + lname);
        // holder.type.setText(supplierItem.getType());
        holder.type.setText(supplierItem.getJobTitle());

        //if (!TextUtils.isEmpty(supplierItem.getPhotourl())) {
        if (!TextUtils.isEmpty(supplierItem.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(mContext)
                    //.load(supplierItem.getPhotourl())
                    .load(supplierItem.getProfilePic())
                    .apply(requestOptions)
                    .into(holder.supplierimg);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return supplierItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class Holder {
        TextView suppliername, type;
        CircleImageView supplierimg;
    }


    public void addList(ArrayList<Users> list) {
        supplierItemArrayList = list;
        Collections.sort(supplierItemArrayList, new Comparator<Users>() {
            public int compare(Users o1, Users o2) {
                return (o1.getFirstName() + " " + o1.getLastName()).toLowerCase().compareTo((o2.getFirstName() + " " + o2.getLastName()).toLowerCase());
            }
        });
        this.arraylist.addAll(supplierItemArrayList);
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        supplierItemArrayList.clear();
        if (charText.length() == 0) {
            supplierItemArrayList.addAll(arraylist);
            if (supplierItemArrayList.size() == 0) {
                nosupllierdata.setVisibility(View.VISIBLE);
            } else {
                nosupllierdata.setVisibility(View.GONE);
            }
        } else {
            filterData(charText);
        }

        notifyDataSetChanged();
    }

    private void filterData(String charText) {


        for (Users supplierItem : arraylist) {
            if ((supplierItem.getFirstName() + " " + supplierItem.getLastName()).toLowerCase(Locale.getDefault()).contains(charText)) {
                supplierItemArrayList.add(supplierItem);
            } else if (supplierItem.getJobTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                supplierItemArrayList.add(supplierItem);
            }
        }
        if (supplierItemArrayList.size() == 0) {
            nosupllierdata.setVisibility(View.VISIBLE);
        } else {
            nosupllierdata.setVisibility(View.GONE);
        }
    }

}
