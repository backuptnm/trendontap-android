package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.GoogleDriveModel;
import tnm.trendontap.utility.RobotoFont;

/**
 * Created by TNM on 2/20/2018.
 */

public class GoogleDriveAdapter extends ArrayAdapter<GoogleDriveModel> {

    ArrayList<GoogleDriveModel> googleDriveModelArrayList;
    private LayoutInflater inflater;
    private Context context;

    public GoogleDriveAdapter(@NonNull Context context, ArrayList<GoogleDriveModel> googleDriveModelArrayList) {
        super(context, 0, googleDriveModelArrayList);
        this.context = context;
        this.googleDriveModelArrayList = googleDriveModelArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return googleDriveModelArrayList.size();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.text_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.text.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        GoogleDriveModel googleDriveModel = googleDriveModelArrayList.get(position);
        holder.text.setText(googleDriveModel.getName());
        return convertView;
    }

    private class Holder {
        TextView text;
    }


}
