package tnm.trendontap.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.GalleryFragment;
import tnm.trendontap.fragment.MyReactionFragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.Position;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.fragment.CameraFragment.camera_Pref;
import static tnm.trendontap.utility.EventClass.COMMENT;
import static tnm.trendontap.utility.EventClass.COMMENTMSG;
import static tnm.trendontap.utility.EventClass.LIKE_LOVE;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class BuyerLovesAdapter extends ArrayAdapter<BuyerLovesItem> {
    LayoutInflater inflater;
    private Activity mContext;
    private ArrayList<BuyerLovesItem> buyerLovesItemArrayList;
    private DatabaseReference databaseReference, databaseReference1;
    private int selectedPos = -1;
    View view;
    Preference preference;
    Users users;
    private boolean isBuyerList = false;

    public BuyerLovesAdapter(@NonNull Activity mContext, ArrayList<BuyerLovesItem> buyerLovesItemArrayList) {
        super(mContext, 0, buyerLovesItemArrayList);
        this.mContext = mContext;
        this.buyerLovesItemArrayList = buyerLovesItemArrayList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preference = new Preference(mContext, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
    }

    @Override
    public int getCount() {
        return buyerLovesItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    LoveImagesAdapter myPagerAdapter;

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_buyer_loves_items, parent, false);
            holder.userdetaillay = (LinearLayout) convertView.findViewById(R.id.userdetaillay);
            holder.rlTagLayout = (RelativeLayout) convertView.findViewById(R.id.rlTagLayout);
            holder.userimg = (CircleImageView) convertView.findViewById(R.id.userimg);
            holder.lovesicon = (ImageView) convertView.findViewById(R.id.lovesicon);
            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.username.setTypeface(RobotoFont.mediumFont(mContext));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(mContext));
            holder.locationtxt = (TextView) convertView.findViewById(R.id.locationtxt);
            holder.locationtxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.deliverydatetxt = (TextView) convertView.findViewById(R.id.deliverydatetxt);
            holder.deliverydatetxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.deliverydate = (TextView) convertView.findViewById(R.id.deliverydate);
            holder.deliverydate.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactionbtn = (LinearLayout) convertView.findViewById(R.id.reactionbtn);
            holder.reactbtn = (LinearLayout) convertView.findViewById(R.id.reactbtn);
            holder.optionicon = (LinearLayout) convertView.findViewById(R.id.optionicon);
            holder.item_tagged = (ImageView) convertView.findViewById(R.id.item_tagged);
            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final BuyerLovesItem buyerLovesItem = buyerLovesItemArrayList.get(position);

        if (!TextUtils.isEmpty(buyerLovesItem.getUserimg())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(mContext)
                    .load(buyerLovesItem.getUserimg())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }

        holder.username.setText(buyerLovesItem.getUsername());
        holder.type.setText(buyerLovesItem.getUsercompany());
        holder.abouttrend.setText(buyerLovesItem.getAbouttrend());

        if (buyerLovesItem.getDeliverydate().trim().equalsIgnoreCase("")) {
            holder.deliverydate.setVisibility(View.GONE);
            holder.deliverydatetxt.setVisibility(View.GONE);
        } else {
            holder.deliverydate.setVisibility(View.VISIBLE);
            holder.deliverydatetxt.setVisibility(View.VISIBLE);
            holder.deliverydate.setText(buyerLovesItem.getDeliverydate());
        }

        holder.daysago.setText(Utils.getDay(buyerLovesItem.getDaysago()));

        holder.locationtxt.setVisibility(View.VISIBLE);
        try {
            if (buyerLovesItem.getLocationItemArrayList() != null) {
                if (!TextUtils.isEmpty(buyerLovesItem.getLocationItemArrayList().title) && buyerLovesItem.getLocationItemArrayList().latitude != 0.0) {
                    holder.locationtxt.setText(buyerLovesItem.getLocationItemArrayList().getTitle() + " >");
                } else {
                    holder.locationtxt.setVisibility(View.GONE);
                }
            } else {
                holder.locationtxt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            holder.locationtxt.setVisibility(View.GONE);
        }

        holder.locationtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("location", buyerLovesItem.getLocationItemArrayList());
                MainActivity.loadContentFragment(new tnm.trendontap.fragment.MapFragment(), false, bundle);
            }
        });


        holder.optionicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(mContext, buyerLovesItem);
            }
        });

        if (isBuyerList)
            holder.userdetaillay.setVisibility(View.GONE);
        else
            holder.userdetaillay.setVisibility(View.VISIBLE);

        holder.lovesicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.lovesicon.getTag().toString().equalsIgnoreCase("like")) {
                    setLike(false, holder.lovesicon, buyerLovesItem.getPostid(), buyerLovesItem);
                } else {
                    setLike(true, holder.lovesicon, buyerLovesItem.getPostid(), buyerLovesItem);
                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(users.getProfilePic());
                    activityItem.setLongMessage(LIKE_MSG);
                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + LIKE_MSG);
                    activityItem.setPostID(buyerLovesItem.getPostid());
                    activityItem.setPostImageURL(buyerLovesItem.getPhotoUrlsList().get(0));
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("love");
                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                    activityItem.setActivityType(LIKE_LOVE);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(buyerLovesItem.getUserpostid());

                    // processActivity(activityItem);
                    String msg = users.getFirstName() + " " + users.getLastName() + LIKE_MSG + "love";
                    sendActivityToUser(buyerLovesItem.getUserpostid(), activityItem, msg);

                }
            }
        });

        holder.reactionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("from", "love");
                bundle.putString("imgurl", buyerLovesItem.getPhotoUrlsList().get(0));
                bundle.putString("postid", buyerLovesItem.getPostid());
                bundle.putString("postuserid", buyerLovesItem.getUserpostid());
                MainActivity.loadContentFragment(new MyReactionFragment(), false, bundle);
            }
        });
        holder.reactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences mPreferences = mContext.getSharedPreferences(camera_Pref, 0);
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", "");
                editor.apply();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", false);
                bundle.putString("from", "love");
                bundle.putString("postid", buyerLovesItem.getPostid());
                bundle.putString("postuserid", buyerLovesItem.getUserpostid());
                bundle.putString("postimg", buyerLovesItem.getPhotoUrlsList().get(0));
                MainActivity.loadContentFragment(new GalleryFragment(), false, bundle);
            }
        });
        if (buyerLovesItem.hasReaction()) {
            holder.reactionbtn.setVisibility(View.VISIBLE);
            holder.reactbtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
        } else {
            holder.reactionbtn.setVisibility(View.INVISIBLE);
            holder.reactbtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
        }

        myPagerAdapter = new LoveImagesAdapter(mContext, buyerLovesItem.getPhotoUrlsList());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (buyerLovesItem.getPhotoUrlsList().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                List<String> array = buyerLovesItemArrayList.get(position).getPhotoUrlsList();
                intent.putStringArrayListExtra("photoUrlsList", (ArrayList<String>) array);
                mContext.startActivity(intent);
            }
        });


        if (buyerLovesItem.getTag_itemsArrayList() != null && buyerLovesItem.getTag_itemsArrayList().size() > 0) {
            holder.item_tagged.setVisibility(View.VISIBLE);
        } else {
            holder.item_tagged.setVisibility(View.GONE);
        }

        holder.item_tagged.setTag(position);
        holder.item_tagged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                BuyerLovesItem buyerLovesItem = buyerLovesItemArrayList.get(pos);
                if (buyerLovesItem.getTag_itemsArrayList() == null) {
                    return;
                }
                if (selectedPos == pos) {
                    holder.rlTagLayout.removeAllViews();
                    selectedPos = -1;
                    return;
                }
                selectedPos = pos;
                holder.rlTagLayout.removeAllViews();
                ArrayList<Tag_Items> tag_itemsArrayList = buyerLovesItem.getTag_itemsArrayList();
                for (int i = 0; i < tag_itemsArrayList.size(); i++) {
                    Tag_Items tag_items = tag_itemsArrayList.get(i);
                    if (tag_items != null) {
                        try {
                            String tag = tag_items.name;
                            Position position = tag_items.position;
                            setTagName(mContext, holder.rlTagLayout, i, position.xPos, position.yPos, tag);
                        } catch (NullPointerException e) {
                        }

                    }
                }

            }
        });


        makePostLikes(holder.lovesicon, buyerLovesItem);

        return convertView;
    }

    private void setTagName(Context context, RelativeLayout rlImageTag, int itemCount, final float x, final float y, final String tag) {
        Log.d("Nzm", "x=" + x + "y=" + y);
        final FrameLayout mFrame = new FrameLayout(context);
        RelativeLayout mInnerFrame = new RelativeLayout(context);

        if (x != 0 && y != 0) {
            FrameLayout.LayoutParams mParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mFrame.setLayoutParams(mParams);
            mFrame.setBackgroundColor(Color.parseColor("#929292"));
            //  mFrame.setPadding(x, y, 0, 0);
            mFrame.setX(x);
            mFrame.setY(y);
            RelativeLayout.LayoutParams mParams1 = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mInnerFrame.setLayoutParams(mParams1);
            mInnerFrame.setPadding(10, 10, 10, 10);
            //mFrame1.setBackground(getDrawable(R.mipmap.item_tagged_text_container));
            mInnerFrame.setBackgroundColor(Color.parseColor("#000000"));


            TextView tv = new TextView(context);
            tv.setLayoutParams(mParams);
            tv.setText(tag);
            tv.setTextSize(16);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            mInnerFrame.addView(tv);

            FrameLayout.LayoutParams imageParam = new FrameLayout.LayoutParams(45, 45);
            // imageParam.setMargins(0,-20,-20,0);
            ImageView image = new ImageView(context);
            image.setId(itemCount);
            image.setTag(itemCount);
            image.setLayoutParams(imageParam);
            image.setPadding(10, 10, 10, 10);
            image.setMaxHeight(25);
            image.setVisibility(View.GONE);
            image.setImageResource(R.mipmap.close);
            image.setMaxWidth(25);

            mInnerFrame.setTag(itemCount);

            mInnerFrame.addView(image);
            mInnerFrame.setTag(itemCount);
            mFrame.addView(mInnerFrame);
            rlImageTag.addView(mFrame);

        }
    }

    public void setIsBuyerList(boolean isBuyerList) {
        this.isBuyerList = isBuyerList;
    }

    class Holder {
        TextView abouttrend, deliverydatetxt, deliverydate, daysago, reactiontxt, username, type, locationtxt;
        ImageView lovesicon, item_tagged;
        ClickableViewPager pager;
        CirclePageIndicator indicator;
        LinearLayout reactionbtn, reactbtn, optionicon, userdetaillay;
        CircleImageView userimg;
        RelativeLayout rlTagLayout;
    }


    public void addList(ArrayList<BuyerLovesItem> list) {
        buyerLovesItemArrayList = list;
        Collections.sort(buyerLovesItemArrayList, new Comparator<BuyerLovesItem>() {
            @Override
            public int compare(BuyerLovesItem o1, BuyerLovesItem o2) {
                return Long.valueOf(o2.getDaysago()).compareTo(o1.getDaysago());
            }
        });
        notifyDataSetChanged();
    }

    private void setLike(boolean islike, ImageView img, String id, BuyerLovesItem buyerLovesItem) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(id).child(Likes);
        databaseReference1 = FirebaseDatabase.getInstance().getReference().
                child(FirebaseTrendOnTapPosts).child(id);
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
            databaseReference1.child(LikesCount).setValue(buyerLovesItem.getLikesCount() + 1);
            buyerLovesItem.setLikesCount(buyerLovesItem.getLikesCount() + 1);

        } else {
            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
            databaseReference1.child(LikesCount).setValue(buyerLovesItem.getLikesCount() - 1);
            buyerLovesItem.setLikesCount(buyerLovesItem.getLikesCount() - 1);
        }
    }


    private void makePostLikes(final ImageView img, final BuyerLovesItem buyerLovesItem) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(buyerLovesItem.getPostid()).child(Likes);
        databaseReference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        //   databaseReference.orderByChild(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               /* if (dataSnapshot.getChildrenCount() == 0) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                } else {
                    img.setImageResource(R.mipmap.loves_icon_active);
                    img.setTag("like");
                }*/

                if (!dataSnapshot.hasChildren()) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                    return;
                }

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (dataSnapshot1.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        img.setImageResource(R.mipmap.loves_icon_active);
                        img.setTag("like");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    protected void onOptionsDialog(Context activity, final BuyerLovesItem buyerLovesItem) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options_supplier);
        dialog.setCancelable(false);

        LinearLayout txt_hide_lay = (LinearLayout) dialog.findViewById(R.id.txt_hide_lay);
        txt_hide_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts)
                        .child(buyerLovesItem.getPostid());
                databaseReference.child(HideSuppliers).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
                buyerLovesItemArrayList.remove(buyerLovesItem);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        LinearLayout txt_cancel_lay = (LinearLayout) dialog.findViewById(R.id.txt_cancel_lay);
        txt_cancel_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        TextView txt_hide = (TextView) dialog.findViewById(R.id.txt_hide);
        txt_hide.setTypeface(RobotoFont.regularFont(activity));


        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));

        dialog.show();
    }


}
