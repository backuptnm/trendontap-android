package tnm.trendontap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import tnm.trendontap.R;


/**
 * Created by TNM on 3/13/2018.
 */

public class DropboxAdpater extends BaseAdapter {
    Context context;
    ArrayList<JSONObject> dropboxItemArrayList;
    LayoutInflater inflater;
    private Picasso picasso;

    public DropboxAdpater(Picasso picasso, Context context, ArrayList<JSONObject> dropboxItemArrayList) {
        this.context = context;
        this.picasso = picasso;
        this.dropboxItemArrayList = dropboxItemArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dropboxItemArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Holder viewHolder;
        if (convertView == null) {
            viewHolder = new Holder();
            convertView = inflater.inflate(R.layout.files_item, viewGroup, false);
            viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.mTextView = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (Holder) convertView.getTag();
        }
        try {
            viewHolder.mTextView.setText(dropboxItemArrayList.get(i).getString("name"));
            if (dropboxItemArrayList.get(i).getString(".tag").equalsIgnoreCase("file")) {
                picasso.load(R.mipmap.ic_insert_drive_file_blue_36dp)
                        .noFade()
                        .into(viewHolder.mImageView);
            } else {
                picasso.load(R.mipmap.ic_folder_blue_36dp)
                        .noFade()
                        .into(viewHolder.mImageView);
            }
        } catch (Exception e) {

        }
        return convertView;
    }

    public class Holder {
        ImageView mImageView;
        TextView mTextView;
    }
}
