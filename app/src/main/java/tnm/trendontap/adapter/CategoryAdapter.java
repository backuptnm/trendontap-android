package tnm.trendontap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.modal.LovesCategoryItem;
import tnm.trendontap.utility.RobotoFont;


/**
 * Created by Desap on 15-11-2017.
 */

public class CategoryAdapter extends ArrayAdapter<LovesCategoryItem> {

    private ArrayList<LovesCategoryItem> lovesCategoryItemArrayList;
    Context context;

    public CategoryAdapter(Context context, ArrayList<LovesCategoryItem> lovesCategoryItemArrayList) {
        super(context, 0, lovesCategoryItemArrayList);
        this.context = context;
        this.lovesCategoryItemArrayList = lovesCategoryItemArrayList;
    }

    @Override
    public int getCount() {
        return lovesCategoryItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_category_list, parent, false);
            holder.txt_categoty = (TextView) convertView.findViewById(R.id.txt_categoty);
            holder.txt_categoty.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txt_categoty.setText(lovesCategoryItemArrayList.get(position).getName());
        return convertView;
    }

    class Holder {
        TextView txt_categoty;
    }


    public void addList(ArrayList<LovesCategoryItem> lovesCategoryItemArrayList) {
        this.lovesCategoryItemArrayList = lovesCategoryItemArrayList;
        notifyDataSetChanged();
    }
}
