package tnm.trendontap.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Reaction_Fragment;
import tnm.trendontap.fragment.Reactions_Trends_Fragment;
import tnm.trendontap.modal.PhotoConnectStamp;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.utility.ScaledImageView;


public class ConnectGridItemAdapter extends ArrayAdapter<PhotoConnectStamp> {
    private ArrayList<PhotoConnectStamp> photoTimeStampArrayList;
    Context context;

    public ConnectGridItemAdapter(@NonNull Context context, ArrayList<PhotoConnectStamp> photoConnectStamps) {
        super(context, 0, photoConnectStamps);
        this.photoTimeStampArrayList = photoConnectStamps;
        this.context = context;
    }

    @Override
    public int getCount() {
        return photoTimeStampArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context,
                    R.layout.data_item, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        final PhotoConnectStamp photoTimeStamp = photoTimeStampArrayList.get(position);

        String item = photoTimeStamp.getPhotoUrlsList().get(0);
        if (!TextUtils.isEmpty(item)) {
            RequestOptions requestOptions = new RequestOptions();
            // requestOptions.placeholder(R.mipmap.userplaceholder);
            // requestOptions.error(R.mipmap.ic_launcher);

            Glide.with(context)
                    .load(item)
                    .apply(requestOptions)
                    .into(holder.data_item_image);
        }
        //holder.data_item_image.setTag(photoTimeStampArrayList.get(position));
        holder.data_item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoConnectStamp photoConnectStamp = photoTimeStamp;
                if (photoConnectStamp.getFrom().equals("trend")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("title", photoConnectStamp.getTitle());
                    bundle.putString("category", photoConnectStamp.getCategory());
                    bundle.putString("postimg", photoConnectStamp.getPostimg());
                    bundle.putString("postid", photoConnectStamp.getPostid());
                    bundle.putString("from", "trend");
                    MainActivity.loadContentFragment(new Reaction_Fragment(), false, bundle);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("name", "Reactions");
                    bundle.putString("from", "love");
                    bundle.putString("postid", photoConnectStamp.getPostid());
                    bundle.putStringArrayList("photourls", photoConnectStamp.getPhotoUrlsList());
                    MainActivity.loadContentFragment(new Reaction_Fragment(), false, bundle);
                }


            }
        });
        return convertView;
    }

    class ViewHolder {
        ScaledImageView data_item_image;

        public ViewHolder(View view) {
            data_item_image = (ScaledImageView) view.findViewById(R.id.data_item_image);
            view.setTag(this);
        }
    }

    public void addList(ArrayList<PhotoConnectStamp> list) {
        photoTimeStampArrayList = list;
        notifyDataSetChanged();
    }


}
