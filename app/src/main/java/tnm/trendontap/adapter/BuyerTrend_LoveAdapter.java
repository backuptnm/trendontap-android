package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.activity.CameraActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.GalleryFragment;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.MainActivity.loadContentFragment;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;


public class BuyerTrend_LoveAdapter extends ArrayAdapter<BuyerTrendLoveItem> {
    LayoutInflater inflater;
    private Activity mContext;
    private ArrayList<BuyerTrendLoveItem> buyerTrendLoveItemArrayList;
    String from;

    public BuyerTrend_LoveAdapter(@NonNull Activity mContext,
                                  ArrayList<BuyerTrendLoveItem> buyerTrendLoveItemArrayList, String from) {
        super(mContext, 0, buyerTrendLoveItemArrayList);
        this.mContext = mContext;
        this.from = from;
        this.buyerTrendLoveItemArrayList = buyerTrendLoveItemArrayList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return buyerTrendLoveItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_buyer_trend_love_item, parent, false);
            holder.blackbg = (RelativeLayout) convertView.findViewById(R.id.blackbg);
            holder.trendimg = (SquareImageView) convertView.findViewById(R.id.trendimg);
            holder.lovesicon = (ImageView) convertView.findViewById(R.id.lovesicon);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.boldFont(mContext));
            holder.category = (TextView) convertView.findViewById(R.id.category);
            holder.category.setTypeface(RobotoFont.boldFont(mContext));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.deliverydatetxt = (TextView) convertView.findViewById(R.id.deliverydatetxt);
            holder.deliverydatetxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.deliverydate = (TextView) convertView.findViewById(R.id.deliverydate);
            holder.deliverydate.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactbtn = (LinearLayout) convertView.findViewById(R.id.reactbtn);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.reactbtn.setTag(position);

        final BuyerTrendLoveItem buyerTrendLoveItem = buyerTrendLoveItemArrayList.get(position);

        if (from.equalsIgnoreCase("loves"))
            holder.blackbg.setVisibility(View.GONE);
        else
            holder.blackbg.setVisibility(View.VISIBLE);

        holder.name.setText(buyerTrendLoveItem.getName());
        holder.category.setText(buyerTrendLoveItem.getCategory());
        holder.abouttrend.setText(buyerTrendLoveItem.getAbouttrend());

        if (buyerTrendLoveItem.getDeliverydate().trim().equalsIgnoreCase("")) {
            holder.deliverydate.setVisibility(View.GONE);
            holder.deliverydatetxt.setVisibility(View.GONE);
        } else {
            holder.deliverydate.setVisibility(View.VISIBLE);
            holder.deliverydatetxt.setVisibility(View.VISIBLE);
            holder.deliverydate.setText(buyerTrendLoveItem.getDeliverydate());
        }

        holder.daysago.setText(Utils.getDay(buyerTrendLoveItem.getDaysago()));

        holder.lovesicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.lovesicon.getTag().toString().equalsIgnoreCase("like")) {
                    setLike(false, holder.lovesicon, buyerTrendLoveItem.getPostid());
                } else {
                    setLike(true, holder.lovesicon, buyerTrendLoveItem.getPostid());
                }
            }
        });

        holder.reactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(mContext, "test", Toast.LENGTH_LONG).show();

                int pos = (int) view.getTag();
                BuyerTrendLoveItem buyerTrendLoveItem = buyerTrendLoveItemArrayList.get(pos);
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", false);
                bundle.putString("from", from);
                bundle.putString("postid", buyerTrendLoveItem.getPostid());
                bundle.putString("postuserid", buyerTrendLoveItem.getUserid());
                bundle.putString("postimg", buyerTrendLoveItem.getImgurl());
                MainActivity.loadContentFragment(new GalleryFragment(), false, bundle);

            }
        });


        if (!TextUtils.isEmpty(buyerTrendLoveItem.getImgurl())) {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerInside();
            requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());

            Glide.with(mContext)
                    .load(buyerTrendLoveItem.getImgurl())
                    .apply(requestOptions)
                    .into(holder.trendimg);
        }

        makePostLikes(holder.lovesicon, buyerTrendLoveItem);
        return convertView;
    }

    class Holder {
        TextView name, category, abouttrend, deliverydatetxt, deliverydate, daysago;
        ImageView lovesicon;
        SquareImageView trendimg;
        LinearLayout reactbtn;
        RelativeLayout blackbg;
    }

    public void addList(ArrayList<BuyerTrendLoveItem> list) {
        buyerTrendLoveItemArrayList = list;
        Collections.sort(buyerTrendLoveItemArrayList, new Comparator<BuyerTrendLoveItem>() {
            @Override
            public int compare(BuyerTrendLoveItem o1, BuyerTrendLoveItem o2) {
                return Long.valueOf(o2.getDaysago()).compareTo(o1.getDaysago());
            }
        });
        notifyDataSetChanged();
    }


    public void setLike(boolean islike, ImageView img, String id) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(id).child(Likes);
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
        } else {
            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
        }

    }


    public void makePostLikes(final ImageView img, BuyerTrendLoveItem buyerTrendLoveItem) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(buyerTrendLoveItem.getPostid()).child(Likes);
        databaseReference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       /* if (dataSnapshot.getChildrenCount() == 0) {
                            img.setImageResource(R.mipmap.loves_icon);
                            img.setTag("dislike");
                        } else {
                            img.setImageResource(R.mipmap.loves_icon_active);
                            img.setTag("like");
                        }*/

                        if (!dataSnapshot.hasChildren()) {
                            img.setImageResource(R.mipmap.loves_icon);
                            img.setTag("dislike");
                            return;
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            if (dataSnapshot1.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                img.setImageResource(R.mipmap.loves_icon_active);
                                img.setTag("like");
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


}
