package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.activity.CommentActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Reaction_Fragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.EventClass.LIKE_LOVE;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getCounttxt;


public class ConnectionTrendAdapter extends ArrayAdapter<TrendsItem> {
    LayoutInflater inflater;
    private Context mContext;
    private ArrayList<TrendsItem> connectionTrendsItemArrayList;
    private String userId;
    Users users;
    Preference preference;

    public ConnectionTrendAdapter(@NonNull Context mContext, ArrayList<TrendsItem> connectionTrendsItemArrayList, String userId) {
        super(mContext, 0, connectionTrendsItemArrayList);
        this.mContext = mContext;
        this.userId = userId;
        this.connectionTrendsItemArrayList = connectionTrendsItemArrayList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preference = new Preference(mContext, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
    }

    @Override
    public int getCount() {
        return connectionTrendsItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_connect_user_trend_item, parent, false);
            holder.trendimg = (ScaledImageView) convertView.findViewById(R.id.trendimg);
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.interesttrend = (TextView) convertView.findViewById(R.id.interesttrend);
            holder.interesttrend.setTypeface(RobotoFont.mediumFont(mContext)); //regular normal text
            holder.comment = (TextView) convertView.findViewById(R.id.comment);
            holder.comment.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.commenttxt = (TextView) convertView.findViewById(R.id.commenttxt);
            holder.commenttxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.interested = (ImageView) convertView.findViewById(R.id.interested);
            holder.loveimg = (ImageView) convertView.findViewById(R.id.loveimg);
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final TrendsItem trendsItem = connectionTrendsItemArrayList.get(position);

        // holder.abouttrend.setVisibility(View.GONE);

        holder.abouttrend.setText(trendsItem.getContent());

        holder.comment.setText(getCounttxt(String.valueOf(trendsItem.getComment_count()), "Comment"));


        if (trendsItem.getLikecnt() == 0) {
            holder.interesttrend.setText("No interest in your trend yet");
            holder.interesttrend.setTextColor(Color.parseColor("#929292"));
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
        } else {
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
            holder.interesttrend.setTextColor(Color.parseColor("#81BBFF"));
            String likertxt = "";

            if (trendsItem.getLikecnt() == 1) {
                likertxt = trendsItem.getLiker1() + " shows interest in your trend";
                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1(), "");
            } else if (trendsItem.getLikecnt() == 2) {
                likertxt = trendsItem.getLiker1() + " and " + trendsItem.getLiker2() + " show interest in your trend";
                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1(), trendsItem.getLiker2());
            } else {
                int otherlikers = (int) trendsItem.getLikecnt() - 2;
                String likeretxt = "";
                if (otherlikers == 1) {
                    likeretxt = String.valueOf(otherlikers) + " other";
                } else
                    likeretxt = String.valueOf(otherlikers) + " others";

                likertxt = trendsItem.getLiker1() + ", " + trendsItem.getLiker2() + " and " + likeretxt + "  show interest in your trend";

                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1() + ", " + trendsItem.getLiker2(), likeretxt);

            }
        }

        holder.daysago.setText(Utils.getDay(trendsItem.getTimeStamp()));

        holder.reactiontxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Integer.parseInt(trendsItem.getReactionsCount()) == 0) {
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("title", trendsItem.getPostTitle());
                bundle.putString("category", trendsItem.getCategoryName());
                bundle.putString("postimg", trendsItem.getPhotoUrlsList().get(0));
                bundle.putString("postid", trendsItem.getId());
                bundle.putString("from", "trend");
                MainActivity.loadContentFragment(new Reaction_Fragment(), false, bundle);
            }
        });


        int reactioncnt = Integer.parseInt(trendsItem.getReactionsCount());
        if (reactioncnt == 0) {
            holder.reactiontxt.setText("Reactions");
        } else if (reactioncnt == 1) {
            holder.reactiontxt.setText("Reaction");
        } else {
            holder.reactiontxt.setText("Reactions");
        }


      /*  if (trendsItem.getLikecnt() == 0) {
            holder.loveimg.setImageResource(R.mipmap.loves_icon);
        } else {
            holder.loveimg.setImageResource(R.mipmap.loves_icon_active);
        }*/

        holder.loveimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /* if (holder.loveimg.getTag().toString().equalsIgnoreCase("like")) {
                    setLike(false, holder.loveimg, trendsItem.getId(), trendsItem.getLikecnt());
                } else {
                    setLike(true, holder.loveimg, trendsItem.getId(), trendsItem.getLikecnt());
                }*/

                if (holder.loveimg.getTag().toString().equalsIgnoreCase("like")) {
                    //setLike(false, holder.loveimg, trendsItem.getId(), trendsItem.getLikecnt());
                    setLike(false, holder.loveimg, trendsItem, holder.interested);
                } else {
                    setLike(true, holder.loveimg, trendsItem, holder.interested);
                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(users.getProfilePic());
                    activityItem.setLongMessage(LIKE_MSG);
                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + LIKE_MSG);
                    activityItem.setPostID(trendsItem.getId());
                    activityItem.setPostImageURL(trendsItem.getPhotoUrlsList().get(0));
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("trend");
                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                    activityItem.setActivityType(LIKE_TREND);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(trendsItem.getUserid());

                    // processActivity(activityItem);
                    String msg = users.getFirstName() + " " + users.getLastName() + LIKE_MSG + "trend";
                    sendActivityToUser(trendsItem.getUserid(), activityItem, msg);

                }
            }
        });

        holder.commenttxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).startActivityForResult(new Intent(mContext, CommentActivity.class).
                        putExtra("post_id", trendsItem.getId()).
                        putExtra("is_from_post", true).
                        putExtra("sento", userId).
                        putExtra("from", "trend").
                        putExtra("position", position).
                        putExtra("post_img", trendsItem.getPhotoUrlsList().get(0)), 123);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).startActivityForResult(new Intent(mContext, CommentActivity.class).
                        putExtra("post_id", trendsItem.getId()).
                        putExtra("is_from_post", true).
                        putExtra("sento", userId).
                        putExtra("from", "trend").
                        putExtra("position", position).
                        putExtra("post_img", trendsItem.getPhotoUrlsList().get(0)), 123);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        if (!TextUtils.isEmpty(trendsItem.getPhotoUrlsList().get(0))) {

            Picasso.with(mContext)
                    .load(trendsItem.getPhotoUrlsList().get(0))
                    .resize(Utils.getScreenWidth(), Utils.getScreenWidth())
                    .centerInside()
                    .into(holder.trendimg);
        }

        makePostLikes(holder.loveimg, trendsItem, holder.interested);

        return convertView;
    }

    class Holder {
        TextView name, abouttrend, interesttrend, comment, daysago, commenttxt, reactiontxt;
        ScaledImageView trendimg;
        ImageView loveimg, interested;
    }

    public void addList(ArrayList<TrendsItem> list) {
        connectionTrendsItemArrayList = list;
        Collections.sort(connectionTrendsItemArrayList, new Comparator<TrendsItem>() {
            @Override
            public int compare(TrendsItem o1, TrendsItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


    public void setFont(TextView textView, String str, String str1, String str2) {

        int part1 = str.indexOf(str1);
        int part2 = str.indexOf(str2);

        SpannableStringBuilder SS = new SpannableStringBuilder(str);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part1, part1 + str1.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part2, part2 + str2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part1, part1 + str1.length(), 0);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part2, part2 + str2.length(), 0);
        textView.setText(SS);
    }


    private void  makePostLikes(final ImageView img, final TrendsItem lovesItem, final ImageView interested) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(lovesItem.getId()).child(Likes);
        databaseReference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                    if (lovesItem.getLikecnt() > 0) {
                        interested.setImageResource(R.mipmap.loves_icon_active);
                    } else {
                        interested.setImageResource(R.mipmap.loves_icon);
                    }
                    return;
                }

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (dataSnapshot1.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        img.setImageResource(R.mipmap.loves_icon_active);
                        img.setTag("like");
                        interested.setImageResource(R.mipmap.loves_icon_active);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


   /* private void setLike(boolean islike, ImageView img, String id, final long buyer_likes_count) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("buyer-post-like").child(id).child("likes");
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");
            databaseReference.child(userId).setValue(true);

            DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference("buyer-user-like").child(userId);
            databaseReference1.child(id).setValue(true);

            DatabaseReference databaseReference2 = FirebaseDatabase.getInstance().getReference("posts").child(id);
            databaseReference2.child("likesCount").setValue(buyer_likes_count + 1);
        } else {
            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            //databaseReference.child(userId).removeValue();
            databaseReference.removeValue();

            DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference("buyer-user-like").child(userId);
            databaseReference1.child(id).removeValue();

            DatabaseReference databaseReference2 = FirebaseDatabase.getInstance().getReference("posts").child(id);
            if (buyer_likes_count > 0)
                databaseReference2.child("likesCount").setValue(buyer_likes_count - 1);
            else
                databaseReference2.child("likesCount").setValue(0);
        }

        notifyDataSetChanged();
    }*/


    public void setLike(boolean islike, ImageView img, TrendsItem buyerTrendsItem, ImageView interested) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(buyerTrendsItem.getId()).child(Likes);
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().
                child(FirebaseTrendOnTapPosts).child(buyerTrendsItem.getId());
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");

            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
            databaseReference1.child(LikesCount).setValue(buyerTrendsItem.getLikecnt() + 1);
            buyerTrendsItem.setLikecnt(buyerTrendsItem.getLikecnt() + 1);

            interested.setImageResource(R.mipmap.loves_icon_active);
        } else {

            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
            databaseReference1.child(LikesCount).setValue(buyerTrendsItem.getLikecnt() - 1);
            if (buyerTrendsItem.getLikecnt() > 0)
                buyerTrendsItem.setLikecnt(buyerTrendsItem.getLikecnt() - 1);

            if (buyerTrendsItem.getLikecnt() > 0) {
                interested.setImageResource(R.mipmap.loves_icon_active);
            } else {
                interested.setImageResource(R.mipmap.loves_icon);
            }
        }

    }


}
