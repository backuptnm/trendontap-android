package tnm.trendontap.adapter;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import tnm.trendontap.fragment.Buyer_Love_Pager_Fragment;
import tnm.trendontap.fragment.Buyer_Trend_Pager_Fragment;
import tnm.trendontap.fragment.Connection_Loves_Fragment;
import tnm.trendontap.fragment.Connection_Trends_Fragment;


public class ConnectionUserAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private String userdetail;

    public ConnectionUserAdapter(FragmentManager fm, int mNumOfTabs, String userdetail) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.userdetail = userdetail;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Connection_Trends_Fragment.newInstance(userdetail, "");
            case 1:
                return Connection_Loves_Fragment.newInstance(userdetail, "");
            default:
                return null;
        }
    }


    @Override
    public int getItemPosition(@NonNull Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }


}
