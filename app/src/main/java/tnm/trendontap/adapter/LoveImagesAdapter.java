package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

/**
 * Created by admin on 02-Apr-18.
 */

public class LoveImagesAdapter extends PagerAdapter {

    private Context context;
    ArrayList<String> imageList;
    private LayoutInflater inflater;

    public LoveImagesAdapter(Context context, ArrayList<String> imageList) {
        this.context = context;
        this.imageList = imageList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.row_love_images_layout, view, false);

        assert imageLayout != null;
        final ScaledImageView lovesimg = (ScaledImageView) imageLayout.findViewById(R.id.loveimg);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerInside();
        requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());
        int imageSize = imageList.size();
        for (int i = 0; i < imageSize; i++) {
            String url = imageList.get(position);
            Glide.with(context)
                    .load(url)
                    .apply(requestOptions)
                    .into(lovesimg);
        }
        view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }


}
