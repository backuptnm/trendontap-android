package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;


public class ReactionpageAdapter extends ArrayAdapter<String> {
    private ArrayList<String> reactionItemArray;
    LayoutInflater inflater;
    private Context context;

    public ReactionpageAdapter(@NonNull Context context, ArrayList<String> reactionItemArray) {
        super(context, 0, reactionItemArray);
        this.context = context;
        this.reactionItemArray = reactionItemArray;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_reactionpage_item, parent, false);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (!TextUtils.isEmpty(reactionItemArray.get(position))) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerCrop();
            requestOptions.override(300, 300);
            Glide.with(context)
                    .load(reactionItemArray.get(position))
                    .apply(requestOptions)
                    .into(holder.image);
        }

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return reactionItemArray.size();
    }

    private class Holder {
        ImageView image;
    }

}
