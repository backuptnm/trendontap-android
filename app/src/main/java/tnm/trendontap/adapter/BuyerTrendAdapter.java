package tnm.trendontap.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.GalleryFragment;
import tnm.trendontap.fragment.MyReactionFragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.fragment.CameraFragment.camera_Pref;
import static tnm.trendontap.utility.EventClass.LIKE_LOVE;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class BuyerTrendAdapter extends ArrayAdapter<BuyerTrendsItem> {
    LayoutInflater inflater;
    private Context mContext;
    private ArrayList<BuyerTrendsItem> trendsItemList;
    Preference preference;
    Users users;
    private DatabaseReference databaseReference, databaseReference1;

    boolean isBuyerList = false;


    public BuyerTrendAdapter(@NonNull Activity activity, ArrayList<BuyerTrendsItem> trendsItemList) {
        super(activity, 0, trendsItemList);
        this.mContext = activity;
        this.trendsItemList = trendsItemList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preference = new Preference(mContext, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
    }

    @Override
    public int getCount() {
        return trendsItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_buyer_trend_item, parent, false);
            holder.userimg = (CircleImageView) convertView.findViewById(R.id.userimg);
            holder.trendimg = (ScaledImageView) convertView.findViewById(R.id.trendimg);
            holder.lovesicon = (ImageView) convertView.findViewById(R.id.lovesicon);
            holder.locationtxt = (TextView) convertView.findViewById(R.id.locationtxt);
            holder.locationtxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.username.setTypeface(RobotoFont.mediumFont(mContext));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(mContext));
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.boldFont(mContext));
            holder.category = (TextView) convertView.findViewById(R.id.category);
            holder.category.setTypeface(RobotoFont.boldFont(mContext));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.deliverydatetxt = (TextView) convertView.findViewById(R.id.deliverydatetxt);
            holder.deliverydatetxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.deliverydate = (TextView) convertView.findViewById(R.id.deliverydate);
            holder.deliverydate.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.userdetaillay = (LinearLayout) convertView.findViewById(R.id.userdetaillay);
            holder.reactionbtn = (LinearLayout) convertView.findViewById(R.id.reactionbtn);
            holder.reactbtn = (LinearLayout) convertView.findViewById(R.id.reactbtn);
            holder.optionicon = (LinearLayout) convertView.findViewById(R.id.optionicon);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.reactionbtn.setTag(position);
        holder.reactbtn.setTag(position);

        final BuyerTrendsItem trendsItem = trendsItemList.get(position);

        holder.username.setText(trendsItem.getUsername());
        holder.name.setText(trendsItem.getName());
        holder.type.setText(trendsItem.getUsercompany());
        holder.category.setText(trendsItem.getCategory());
        holder.abouttrend.setText(trendsItem.getAbouttrend());

        if (trendsItem.getDeliverydate().trim().equalsIgnoreCase("")) {
            holder.deliverydate.setVisibility(View.GONE);
            holder.deliverydatetxt.setVisibility(View.GONE);
        } else {
            holder.deliverydate.setVisibility(View.VISIBLE);
            holder.deliverydatetxt.setVisibility(View.VISIBLE);
            holder.deliverydate.setText(trendsItem.getDeliverydate());
        }

        holder.daysago.setText(Utils.getDay(Long.parseLong(trendsItem.getDaysago())));
        holder.reactionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("from", "trend");
                bundle.putString("location", trendsItem.getName());
                bundle.putString("category", trendsItem.getCategory());
                bundle.putString("imgurl", trendsItem.getImgurl());
                bundle.putString("postid", trendsItem.getPostid());
                bundle.putString("postuserid", trendsItem.getUserpostid());
                MainActivity.loadContentFragment(new MyReactionFragment(), false, bundle);
            }
        });

        holder.locationtxt.setVisibility(View.VISIBLE);
        try {
            if (trendsItem.getLocationItemArrayList() != null) {
                if (!TextUtils.isEmpty(trendsItem.getLocationItemArrayList().title) && trendsItem.getLocationItemArrayList().latitude != 0.0) {
                    holder.locationtxt.setText(trendsItem.getLocationItemArrayList().getTitle() + " >");
                } else {
                    holder.locationtxt.setVisibility(View.GONE);
                }
            } else {
                holder.locationtxt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            holder.locationtxt.setVisibility(View.GONE);
        }

        holder.locationtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("location", trendsItem.getLocationItemArrayList());
                MainActivity.loadContentFragment(new tnm.trendontap.fragment.MapFragment(), false, bundle);
            }
        });


        holder.lovesicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.lovesicon.getTag().toString().equalsIgnoreCase("like")) {
                    setLike(false, holder.lovesicon, trendsItem.getPostid(), trendsItem);
                } else {
                    setLike(true, holder.lovesicon, trendsItem.getPostid(), trendsItem);
                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(users.getProfilePic());
                    activityItem.setLongMessage(LIKE_MSG);
                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + LIKE_MSG);
                    activityItem.setPostID(trendsItem.getPostid());
                    activityItem.setPostImageURL(trendsItem.getImgurl());
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("trend");
                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                    activityItem.setActivityType(LIKE_TREND);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(trendsItem.getUserpostid());

                    // processActivity(activityItem);
                    String msg = users.getFirstName() + " " + users.getLastName() + LIKE_MSG + "trend";
                    sendActivityToUser(trendsItem.getUserpostid(), activityItem, msg);
                }
            }
        });

        holder.reactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences mPreferences = mContext.getSharedPreferences(camera_Pref, 0);
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", "");
                editor.apply();

                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", false);
                bundle.putString("from", "trend");
                bundle.putString("postid", trendsItem.getPostid());
                bundle.putString("postuserid", trendsItem.getUserpostid());
                bundle.putString("postimg", trendsItem.getImgurl());
                MainActivity.loadContentFragment(new GalleryFragment(), false, bundle);
            }
        });

        if (trendsItem.hasReaction()) {
            holder.reactionbtn.setVisibility(View.VISIBLE);
            holder.reactbtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
        } else {
            holder.reactionbtn.setVisibility(View.INVISIBLE);
            holder.reactbtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
        }


        holder.optionicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(mContext, trendsItem);
            }
        });

        if (isBuyerList)
            holder.userdetaillay.setVisibility(View.GONE);
        else
            holder.userdetaillay.setVisibility(View.VISIBLE);


        if (!TextUtils.isEmpty(trendsItem.getUserimg())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(mContext)
                    .load(trendsItem.getUserimg())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }

        if (!TextUtils.isEmpty(trendsItem.getImgurl())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerInside();
            requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());

            Glide.with(mContext)
                    .load(trendsItem.getImgurl())
                    .apply(requestOptions)
                    .into(holder.trendimg);
        }
        makePostLikes(holder.lovesicon, trendsItem);
        downloadPDF(trendsItem.getPdfUrl(), holder.progressBar);
        return convertView;
    }

    public void setLike(boolean islike, ImageView img, String id, BuyerTrendsItem buyerTrendsItem) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(id).child(Likes);
        databaseReference1 = FirebaseDatabase.getInstance().getReference().
                child(FirebaseTrendOnTapPosts).child(id);
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
            databaseReference1.child(LikesCount).setValue(buyerTrendsItem.getLikesCount() + 1);
            buyerTrendsItem.setLikesCount(buyerTrendsItem.getLikesCount() + 1);
        } else {
            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
            databaseReference1.child(LikesCount).setValue(buyerTrendsItem.getLikesCount() - 1);
            buyerTrendsItem.setLikesCount(buyerTrendsItem.getLikesCount() - 1);
        }

    }


    public void setIsBuyerList(boolean buyerList) {
        isBuyerList = buyerList;
    }

    public void makePostLikes(final ImageView img, BuyerTrendsItem buyerTrendsItem) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(buyerTrendsItem.getPostid()).child(Likes);
        databaseReference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              /*  if (dataSnapshot.getChildrenCount() == 0) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                } else {
                    img.setImageResource(R.mipmap.loves_icon_active);
                    img.setTag("like");
                }*/

                if (!dataSnapshot.hasChildren()) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                    return;
                }

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (dataSnapshot1.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        img.setImageResource(R.mipmap.loves_icon_active);
                        img.setTag("like");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    class Holder {
        TextView name, category, abouttrend, deliverydatetxt, deliverydate, daysago, reactiontxt, locationtxt, username, type;
        ImageView lovesicon;
        ScaledImageView trendimg;
        LinearLayout reactionbtn, reactbtn, optionicon, userdetaillay;
        CircleImageView userimg;
        ProgressBar progressBar;
    }

    public void addList(ArrayList<BuyerTrendsItem> list) {
        trendsItemList = list;
        Collections.sort(trendsItemList, new Comparator<BuyerTrendsItem>() {
            @Override
            public int compare(BuyerTrendsItem o1, BuyerTrendsItem o2) {
                long last = Long.parseLong(o2.getDaysago());
                long first = Long.parseLong(o1.getDaysago());
                return Long.valueOf(last).compareTo(first);
            }
        });
        // Collections.reverse(trendsItemList);
        notifyDataSetChanged();
    }

    private void onOptionsDialog(Context activity, final BuyerTrendsItem buyerTrendsItem) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options_supplier);
        dialog.setCancelable(false);


        LinearLayout txt_hide_lay = (LinearLayout) dialog.findViewById(R.id.txt_hide_lay);
        txt_hide_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts).child(buyerTrendsItem.getPostid());
                databaseReference.child(HideSuppliers).
                        child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
                trendsItemList.remove(buyerTrendsItem);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        LinearLayout txt_cancel_lay = (LinearLayout) dialog.findViewById(R.id.txt_cancel_lay);
        txt_cancel_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        TextView txt_hide = (TextView) dialog.findViewById(R.id.txt_hide);
        txt_hide.setTypeface(RobotoFont.regularFont(activity));

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));

        dialog.show();
    }


    public void downloadPDF(String url, ProgressBar progressBar) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf
        if (!pdfFile.exists()) {
            new DownloadFile(progressBar).execute(url, name);
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        ProgressBar progressBar;

        public DownloadFile(ProgressBar progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "TOT DOWNLOAD");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // FileDownloader.downloadFile(fileUrl, pdfFile);

            int MEGABYTE = 1024 * 1024;
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);

                byte[] buffer = new byte[MEGABYTE];


                int fileLength = urlConnection.getContentLength();
                long total = 0;
                int count;
                while ((count = inputStream.read(buffer)) != -1) {
                    total += count;
                    // publishing the progress....
                    Bundle resultData = new Bundle();
                    resultData.putInt("progress", (int) (total * 100 / fileLength));
                    //receiver.send(UPDATE_PROGRESS, resultData);
                    publishProgress((int) (total * 100 / fileLength));
                    fileOutputStream.write(buffer, 0, count);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }

        private void publishProgress(int progress) {
            progressBar.setProgress(progress);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            // Toast.makeText(getApplicationContext(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
            Log.d("Download complete", "----------");
        }
    }

}
