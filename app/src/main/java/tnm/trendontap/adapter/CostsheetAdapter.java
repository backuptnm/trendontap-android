package tnm.trendontap.adapter;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import tnm.trendontap.R;
import tnm.trendontap.modal.CostSheet_Model;
import tnm.trendontap.utility.RobotoFont;

public class CostsheetAdapter extends ArrayAdapter<CostSheet_Model> {
    private LayoutInflater inflater;
    private Context mContext;
    private ArrayList<CostSheet_Model> costsheetItemArrayList;
    private List<CostSheet_Model> arraylist;
    private RelativeLayout nocostsheetdata;

    public CostsheetAdapter(@NonNull Context mContext, ArrayList<CostSheet_Model> costsheetItemArrayList, RelativeLayout nocostsheetdata) {
        super(mContext, 0, costsheetItemArrayList);
        this.mContext = mContext;
        this.arraylist = new ArrayList<>();
        this.costsheetItemArrayList = costsheetItemArrayList;
        this.nocostsheetdata = nocostsheetdata;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return costsheetItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_costsheet_item, parent, false);
            holder.stylenotxt = (TextView) convertView.findViewById(R.id.stylenotxt);
            holder.stylenotxt.setTypeface(RobotoFont.mediumFont(mContext));
            holder.styleno = (TextView) convertView.findViewById(R.id.styleno);
            holder.styleno.setTypeface(RobotoFont.mediumFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        CostSheet_Model costsheetItem = costsheetItemArrayList.get(position);
        holder.styleno.setText(costsheetItem.getStyleNumber());
        return convertView;
    }

    class Holder {
        TextView stylenotxt, styleno;
    }

    public void addList(ArrayList<CostSheet_Model> list) {
        costsheetItemArrayList = list;
        this.arraylist.addAll(list);
        notifyDataSetChanged();
    }

    public void filter(String charText, ListView listView) {
        charText = charText.toLowerCase(Locale.getDefault());
        costsheetItemArrayList.clear();
        if (charText.length() == 0) {
            costsheetItemArrayList.addAll(arraylist);
            if (costsheetItemArrayList.size() == 0) {
                nocostsheetdata.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            } else {
                nocostsheetdata.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }
        } else {
            filterData(charText,listView);
        }
        notifyDataSetChanged();
    }

    private void filterData(String charText, ListView listView) {

        for (CostSheet_Model supplierItem : arraylist) {
            if (supplierItem.getStyleNumber().toLowerCase(Locale.getDefault()).contains(charText)) {
                costsheetItemArrayList.add(supplierItem);
            }
        }
        if (costsheetItemArrayList.size() == 0) {
            nocostsheetdata.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        } else {
            nocostsheetdata.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }


}
