package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.SuppliersInvitations;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Buyers;
import static tnm.trendontap.utility.FireStoreUtils.BuyersOfSupplier;
import static tnm.trendontap.utility.FireStoreUtils.IsInviationAccepted;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.SupplierInvitations;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;


public class InvitationAdapter extends ArrayAdapter<SuppliersInvitations> {
    private ArrayList<SuppliersInvitations> invitationItemArrayList;
    private Context mContext;
    private Activity mActivity;
    private LayoutInflater inflater;
    private List<SuppliersInvitations> arraylist;
    private LinearLayout nosupllierdata;
    ListView invitationlist;

    public InvitationAdapter(@NonNull Activity activity, Context mContext, ArrayList<SuppliersInvitations> invitationItemArrayList, LinearLayout nosupllierdata, ListView invitationlist) {
        super(mContext, 0, invitationItemArrayList);
        this.mActivity = activity;
        this.mContext = mContext;
        this.invitationItemArrayList = invitationItemArrayList;
        this.arraylist = new ArrayList<>();
        this.nosupllierdata = nosupllierdata;
        this.invitationlist = invitationlist;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_invitations_item, parent, false);
            holder.supplierimg = (CircleImageView) convertView.findViewById(R.id.supplierimg);
            holder.suppliername = (TextView) convertView.findViewById(R.id.suppliername);
            holder.suppliername.setTypeface(RobotoFont.mediumFont(mContext));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(mContext));
            holder.imageCancel = (ImageView) convertView.findViewById(R.id.imageCancel);
            holder.imageAccept = (ImageView) convertView.findViewById(R.id.imageAccept);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.imageCancel.setTag(position);
        holder.imageAccept.setTag(position);

        SuppliersInvitations invitationItem = invitationItemArrayList.get(position);
        holder.suppliername.setText(invitationItem.getBuyerName());
        holder.type.setText(invitationItem.getCompanyName());

        if (!TextUtils.isEmpty(invitationItem.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(mContext)
                    .load(invitationItem.getProfilePic())
                    .apply(requestOptions)
                    .into(holder.supplierimg);
        }
        holder.imageCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                SuppliersInvitations invitationItem = invitationItemArrayList.get(pos);
                FirebaseDatabase.getInstance().getReference().child(SupplierInvitations).child(invitationItem.getId()).setValue(null);
                mActivity.onBackPressed();
            }
        });
        holder.imageAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                SuppliersInvitations invitationItem = invitationItemArrayList.get(pos);
                final String userId = invitationItem.getUserID();
                final String id = FirebaseAuth.getInstance().getCurrentUser().getUid();

                FirebaseDatabase.getInstance().getReference().child(SupplierInvitations).child(invitationItem.getId()).child(IsInviationAccepted).setValue(true);

                if (!userId.equals(id)) {
                    FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).child(userId).child(Suppliers + "/" + id).setValue(true)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        FirebaseDatabase.getInstance().getReference().child(BuyersOfSupplier).child(id).
                                                child(Buyers + "/" + userId).setValue(true)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            mActivity.onBackPressed();
                                                        }
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                    }
                                                });
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                }
                            });


                    FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(userId).child(Loves).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> hashMap = new HashMap<>();
                            for (DataSnapshot lovesitem : dataSnapshot.getChildren()) {
                                hashMap.put(lovesitem.getKey(), lovesitem.getValue());
                            }

                            //  DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
                            // userposts.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves).updateChildren(hashMap);

                            DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
                            userpostsfeed.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves).updateChildren(hashMap);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(userId).child(Trends).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            HashMap<String, Object> hashMap = new HashMap<>();
                            for (DataSnapshot trenditem : dataSnapshot.getChildren()) {
                                hashMap.put(trenditem.getKey(), trenditem.getValue());
                            }

                            //   DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
                            //   userposts.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Trends).updateChildren(hashMap);

                            DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
                            userpostsfeed.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Trends).updateChildren(hashMap);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }


            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return invitationItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class Holder {
        TextView suppliername, type;
        CircleImageView supplierimg;
        ImageView imageCancel, imageAccept;
    }

    public void addList(ArrayList<SuppliersInvitations> list) {
        invitationItemArrayList = list;
        Collections.sort(invitationItemArrayList, new Comparator<SuppliersInvitations>() {
            @Override
            public int compare(SuppliersInvitations o1, SuppliersInvitations o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        this.arraylist.clear();
        this.arraylist.addAll(invitationItemArrayList);
        if (invitationItemArrayList.size() == 0) {
            nosupllierdata.setVisibility(View.VISIBLE);
            invitationlist.setVisibility(View.GONE);
        } else {
            nosupllierdata.setVisibility(View.GONE);
            invitationlist.setVisibility(View.VISIBLE);
        }
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        invitationItemArrayList.clear();
        if (charText.length() == 0) {
            invitationItemArrayList.addAll(arraylist);
            if (invitationItemArrayList.size() == 0) {
                nosupllierdata.setVisibility(View.VISIBLE);
                invitationlist.setVisibility(View.GONE);
            } else {
                nosupllierdata.setVisibility(View.GONE);
                invitationlist.setVisibility(View.VISIBLE);
            }
        } else {
            filterData(charText);
        }

        notifyDataSetChanged();
    }

    private void filterData(String charText) {

        for (SuppliersInvitations invitationItem : arraylist) {
            if ((invitationItem.getBuyerName()).toLowerCase(Locale.getDefault()).contains(charText)) {
                invitationItemArrayList.add(invitationItem);
            }
        }
        if (invitationItemArrayList.size() == 0) {
            nosupllierdata.setVisibility(View.VISIBLE);
            invitationlist.setVisibility(View.GONE);
        } else {
            nosupllierdata.setVisibility(View.GONE);
            invitationlist.setVisibility(View.VISIBLE);
        }
    }

}
