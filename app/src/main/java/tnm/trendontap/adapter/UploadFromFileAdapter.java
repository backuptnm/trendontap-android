package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import tnm.trendontap.R;

import tnm.trendontap.utility.RobotoFont;

/**
 * Created by TNM on 2/20/2018.
 */

public class UploadFromFileAdapter extends ArrayAdapter<File> {

    ArrayList<File> uploadFileItemArrayList;
    private LayoutInflater inflater;
    private Context context;

    public UploadFromFileAdapter(@NonNull Context context, ArrayList<File> uploadFileItemArrayList) {
        super(context, 0, uploadFileItemArrayList);
        this.context = context;
        this.uploadFileItemArrayList = uploadFileItemArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return uploadFileItemArrayList.size();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.text_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.text.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        File uploadFileItem = uploadFileItemArrayList.get(position);
        holder.text.setText(uploadFileItem.getName());
        holder.text.setTypeface(RobotoFont.regularFont(context));
        return convertView;
    }

    private class Holder {
        TextView text;
    }


}
