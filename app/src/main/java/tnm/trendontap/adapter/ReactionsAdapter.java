package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.CostingSheet_Activity;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;


public class ReactionsAdapter extends ArrayAdapter<ReactionTrendItem> {

    Context context;
    LayoutInflater inflater;

    private ArrayList<ReactionTrendItem> reactionItemArrayList;

    public ReactionsAdapter(Context context, ArrayList<ReactionTrendItem> reactionItemArrayList) {
        super(context, 0, reactionItemArrayList);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.reactionItemArrayList = reactionItemArrayList;
    }

    @Override
    public int getCount() {
        return reactionItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_reactions_items, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.regularFont(context));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(context));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.regularFont(context));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(context));
            holder.costtxt = (TextView) convertView.findViewById(R.id.costtxt);
            holder.costtxt.setTypeface(RobotoFont.regularFont(context));
            holder.userimg = (CircleImageView) convertView.findViewById(R.id.userimg);
            holder.costbtn = (LinearLayout) convertView.findViewById(R.id.costbtn);
            holder.namebg = (LinearLayout) convertView.findViewById(R.id.namebg);
            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final ReactionTrendItem reactionTrendItem = reactionItemArrayList.get(position);
        holder.name.setText(reactionTrendItem.getUsername());
        holder.type.setText(reactionTrendItem.getUsercompany());
        holder.abouttrend.setText(reactionTrendItem.getContent());
        holder.daysago.setText(Utils.getDay(reactionTrendItem.getTimeStamp()));

        LoveImagesAdapter myPagerAdapter = new LoveImagesAdapter(context, reactionTrendItem.getPhotoUrls());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (reactionItemArrayList.get(position).getPhotoUrls().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(context, FullScreenImageActivity.class);
                ArrayList<String> array = reactionTrendItem.getPhotoUrls();
                intent.putStringArrayListExtra("photoUrlsList", array);
                context.startActivity(intent);
            }
        });

        try {
            if (reactionTrendItem.getCostsheetID().equalsIgnoreCase("") || !TextUtils.isEmpty(reactionTrendItem.getCostsheetID())) {
                holder.costbtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
            } else {
                holder.costbtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
            }
        } catch (Exception e) {
            holder.costbtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
        }

        holder.costbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (reactionTrendItem.getCostsheetID().equalsIgnoreCase("") || !TextUtils.isEmpty(reactionTrendItem.getCostsheetID())) {
                        return;
                    }
                } catch (Exception e) {
                    return;
                }


                Intent costingsheet = new Intent(context, CostingSheet_Activity.class);

                costingsheet.putExtra("post_id", "").
                        putExtra("Reaction_Id", "").
                        putExtra("postimg", "").
                        putExtra("sentto", "").
                        putExtra("costsheet_id", reactionTrendItem.getCostsheetID());
                context.startActivity(costingsheet);
                ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        return convertView;
    }

    class Holder {
        CircleImageView userimg;
        ClickableViewPager pager;
        CirclePageIndicator indicator;
        TextView name, type, abouttrend, daysago, costtxt;
        LinearLayout costbtn, namebg;
    }


    public void addList(ArrayList<ReactionTrendItem> list) {
        reactionItemArrayList = list;
        Collections.sort(reactionItemArrayList, new Comparator<ReactionTrendItem>() {
            @Override
            public int compare(ReactionTrendItem o1, ReactionTrendItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


}
