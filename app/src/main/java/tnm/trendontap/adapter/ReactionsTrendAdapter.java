package tnm.trendontap.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.CostingSheet_Activity;
import tnm.trendontap.activity.CommentActivity;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.SeasonActivity;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.MyReaction;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.SampleRequest;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;


import static tnm.trendontap.utility.EventClass.COSTINSHEET;

import static tnm.trendontap.utility.EventClass.REQUESTMSG;
import static tnm.trendontap.utility.EventClass.SAMPLE;

import static tnm.trendontap.utility.EventClass.sendActivityToUser;

import static tnm.trendontap.utility.FireStoreUtils.CostsheetRequest;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ReactionID;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.Sample;
import static tnm.trendontap.utility.FireStoreUtils.StyleNumber;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentBuyer;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.confiramtionDialog;


public class ReactionsTrendAdapter extends ArrayAdapter<ReactionTrendItem> {

    Context context;
    LayoutInflater inflater;
    DatabaseReference databaseReference;
    Preference preference;
    Users users;
    private ArrayList<ReactionTrendItem> reactionTrendItemArrayList;
    String posturl, from;

    public ReactionsTrendAdapter(Context context, ArrayList<ReactionTrendItem> reactionTrendItemArrayList,
                                 String posturl, String from) {
        super(context, 0, reactionTrendItemArrayList);

        preference = new Preference(context, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.reactionTrendItemArrayList = reactionTrendItemArrayList;
        this.posturl = posturl;
        this.from = from;
    }

    @Override
    public int getCount() {
        return reactionTrendItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setPosturl(String posturl) {
        this.posturl = posturl;

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_reactions_trend_items, parent, false);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.regularFont(context));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(context));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.regularFont(context));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(context));
            holder.sampletxt = (TextView) convertView.findViewById(R.id.sampletxt);
            holder.sampletxt.setTypeface(RobotoFont.regularFont(context));
            holder.costtxt = (TextView) convertView.findViewById(R.id.costtxt);
            holder.costtxt.setTypeface(RobotoFont.regularFont(context));
            holder.bintxt = (TextView) convertView.findViewById(R.id.bintxt);
            holder.bintxt.setTypeface(RobotoFont.regularFont(context));
            holder.commentcnt = (TextView) convertView.findViewById(R.id.commentcnt);
            holder.commentcnt.setTypeface(RobotoFont.regularFont(context));
            holder.commenttxt = (TextView) convertView.findViewById(R.id.commenttxt);
            holder.commenttxt.setTypeface(RobotoFont.regularFont(context));
            holder.userimg = (CircleImageView) convertView.findViewById(R.id.userimg);
            holder.sampleicon = (ImageView) convertView.findViewById(R.id.sampleicon);
            holder.doticon = (ImageView) convertView.findViewById(R.id.doticon);
            holder.costicon = (ImageView) convertView.findViewById(R.id.costicon);
            holder.samplebtn = (LinearLayout) convertView.findViewById(R.id.samplebtn);
            holder.costbtn = (LinearLayout) convertView.findViewById(R.id.costbtn);
            holder.binbtn = (LinearLayout) convertView.findViewById(R.id.binbtn);
            holder.commentbtn = (LinearLayout) convertView.findViewById(R.id.commentbtn);
            holder.options_icon = (LinearLayout) convertView.findViewById(R.id.options_icon);
            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final ReactionTrendItem reactionTrendItem = reactionTrendItemArrayList.get(position);
        holder.name.setText(reactionTrendItem.getUsername());
        holder.type.setText(reactionTrendItem.getUsercompany());
        holder.daysago.setText(Utils.getDay(reactionTrendItem.getTimeStamp()));
        holder.abouttrend.setText(reactionTrendItem.getContent());
        holder.commentcnt.setText(String.valueOf(reactionTrendItem.getCommentCount()));
        LoveImagesAdapter myPagerAdapter = new LoveImagesAdapter(context, reactionTrendItemArrayList.get(position).getPhotoUrls());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (reactionTrendItemArrayList.get(position).getPhotoUrls().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(context, FullScreenImageActivity.class);
                ArrayList<String> array = reactionTrendItemArrayList.get(position).getPhotoUrls();
                intent.putStringArrayListExtra("photoUrlsList", array);
                context.startActivity(intent);
            }
        });

        holder.options_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(context, reactionTrendItem);
            }
        });
        holder.commenttxt.setText("Comments");
        if (reactionTrendItem.getCommentCount() > 0) {
            if (reactionTrendItem.getCommentCount() == 1)
                holder.commenttxt.setText("Comment");
            holder.commenttxt.setBackgroundResource(R.drawable.rectangle_blue_bg);
            holder.commenttxt.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.commenttxt.setBackgroundResource(R.drawable.rectangle_grey_bg);
            holder.commenttxt.setTextColor(Color.parseColor("#929292"));
        }

        if (reactionTrendItem.isUnreadCommentBuyer()) {
            holder.doticon.setVisibility(View.VISIBLE);
        } else {
            holder.doticon.setVisibility(View.INVISIBLE);
        }

        if (reactionTrendItem.isSampleRequest()) {
            holder.samplebtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
            holder.sampletxt.setTextColor(Color.parseColor("#FFFFFF"));
            holder.sampleicon.setColorFilter(Color.parseColor("#FFFFFF"));
        } else {
            holder.samplebtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
            holder.sampletxt.setTextColor(Color.parseColor("#929292"));
            holder.sampleicon.setColorFilter(Color.parseColor("#929292"));
        }

        if (reactionTrendItem.isCostsheetRequest()) {
            holder.costbtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
            holder.costtxt.setTextColor(Color.parseColor("#FFFFFF"));
            holder.costicon.setColorFilter(Color.parseColor("#FFFFFF"));
        } else {
            holder.costbtn.setBackgroundResource(R.drawable.rectangle_grey_bg);
            holder.costtxt.setTextColor(Color.parseColor("#929292"));
            holder.costicon.setColorFilter(Color.parseColor("#929292"));
        }

        if (!TextUtils.isEmpty(reactionTrendItem.getUserphoto())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(reactionTrendItem.getUserphoto())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }
        String postid = "";
        try {
            postid = reactionTrendItemArrayList.get(position).getPostID();
        } catch (Exception e) {

        }
        final String finalPostid = postid;
        holder.commentbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CommentActivity.class).
                        putExtra("post_id", finalPostid).
                        putExtra("reaction_id", reactionTrendItemArrayList.get(position).getId()).
                        putExtra("sento", reactionTrendItemArrayList.get(position).getUserID()).
                        putExtra("from", from).
                        putExtra("reaction_img", reactionTrendItemArrayList.get(position).getPhotoUrls().get(0)));
                ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);

                DatabaseReference reactionread = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).
                        child(reactionTrendItemArrayList.get(position).getId());
                reactionread.child(tReactionUnreadCommentBuyer).setValue(false);
                // reactionread.child("unread_comment_buyer").setValue(false);
            }
        });

        holder.samplebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reactionTrendItem.isSampleRequest()) {
                    return;
                }

                DatabaseReference sampleReference = FirebaseDatabase.getInstance().getReference().child(Sample);

                databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reactionTrendItem.getId());
                databaseReference.child(FireStoreUtils.SampleRequest).setValue(true);
                reactionTrendItem.setSampleRequest(true);

                holder.samplebtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
                holder.sampletxt.setTextColor(Color.parseColor("#FFFFFF"));
                holder.sampleicon.setColorFilter(Color.parseColor("#FFFFFF"));


                ActivityItem activityItem = new ActivityItem();
                activityItem.setProfilePic(users.getProfilePic());
                activityItem.setLongMessage(REQUESTMSG);
                activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + REQUESTMSG);
                activityItem.setPostID(reactionTrendItem.getPostID());
                activityItem.setPostImageURL(posturl);
                activityItem.setReactionID(reactionTrendItem.getId());
                activityItem.setShortMessage(SAMPLE);
                activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                activityItem.setActivityType(SAMPLE);
                activityItem.setTimeStamp(System.currentTimeMillis());
                activityItem.setUserID(users.getUserID());
                String msg = users.getFirstName() + " " + users.getLastName() + REQUESTMSG + SAMPLE;
                sendActivityToUser(reactionTrendItem.getUserID(), activityItem, msg);

                confiramtionDialog(context, "Sample requested");


            }
        });

        holder.costbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reactionTrendItem.isCostsheetRequest()) {
                    return;
                }
                databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reactionTrendItem.getId());
                databaseReference.child(CostsheetRequest).setValue(true);
                reactionTrendItem.setCostsheetRequest(true);
                holder.costbtn.setBackgroundResource(R.drawable.rectangle_blue_bg);
                holder.costtxt.setTextColor(Color.parseColor("#FFFFFF"));
                holder.costicon.setColorFilter(Color.parseColor("#FFFFFF"));

                ActivityItem activityItem = new ActivityItem();
                activityItem.setProfilePic(users.getProfilePic());
                activityItem.setLongMessage(REQUESTMSG);
                activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + REQUESTMSG);
                activityItem.setPostID(reactionTrendItem.getPostID());
                activityItem.setPostImageURL(posturl);
                activityItem.setReactionID(reactionTrendItem.getId());
                activityItem.setShortMessage(COSTINSHEET);
                activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                activityItem.setActivityType(COSTINSHEET);
                activityItem.setTimeStamp(System.currentTimeMillis());
                activityItem.setUserID(users.getUserID());

                String msg = users.getFirstName() + " " + users.getLastName() + REQUESTMSG + COSTINSHEET;
                sendActivityToUser(reactionTrendItem.getUserID(), activityItem, msg);

                confiramtionDialog(context, "Costing Sheet requested");
            }
        });
        holder.binbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**************************/
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(false);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.setContentView(R.layout.dialog_bin);

                TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
                tvTitle.setTypeface(RobotoFont.boldFont(context));


                TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
                done_action.setText("Yes");
                done_action.setTypeface(RobotoFont.boldFont(context));
                done_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                        if (reactionTrendItem.isRecycleBin()) {
                            return;
                        }
                        databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reactionTrendItem.getId());
                        databaseReference.child(RecycleBin).setValue(true);
                        reactionTrendItem.setRecycleBin(true);
                        reactionTrendItemArrayList.remove(reactionTrendItem);
                        notifyDataSetChanged();

                        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(reactionTrendItem.getPostID()).
                                addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        DatabaseReference reactioncnt = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);
                                        try {
                                            if (!dataSnapshot.hasChild(ReactionsCount)) {
                                                reactioncnt.child(reactionTrendItem.getPostID()).child(ReactionsCount).setValue(0);
                                                return;
                                            }
                                            long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                                            if (reactionsCount > 0) {
                                                reactioncnt.child(reactionTrendItem.getPostID()).child(ReactionsCount).setValue(reactionsCount - 1);
                                            }
                                        } catch (Exception e) {

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }
                });
                TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
                cancel_action.setTypeface(RobotoFont.boldFont(context));
                cancel_action.setText("No");
                cancel_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                /**************************/


            }
        });

        return convertView;
    }


    public void addSampleDialog(final Context activity, final ReactionTrendItem myReaction) {
        final Dialog dialog = new Dialog(activity);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_view_sample);

        final TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(activity));

        final TextView edtname = (TextView) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(activity));

        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(activity));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        FirebaseDatabase.getInstance().getReference().child(Sample).orderByChild(ReactionID).equalTo(myReaction.getId()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            edtname.setText(dataSnapshot1.child(StyleNumber).getValue().toString());
                            break;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }

    class Holder {
        CircleImageView userimg;
        ImageView sampleicon, costicon, doticon;
        TextView name, type, abouttrend, daysago, sampletxt, costtxt, bintxt, commentcnt, commenttxt;
        LinearLayout samplebtn, costbtn, binbtn, commentbtn, options_icon;

        ClickableViewPager pager;
        CirclePageIndicator indicator;
    }

    public void update(ArrayList<ReactionTrendItem> reactionTrendItemArrayList) {
        this.reactionTrendItemArrayList = reactionTrendItemArrayList;
        Collections.sort(reactionTrendItemArrayList, new Comparator<ReactionTrendItem>() {
            @Override
            public int compare(ReactionTrendItem o1, ReactionTrendItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


    private void onOptionsDialog(final Context activity, final ReactionTrendItem reactionTrendItem) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options_buyer_reaction);
        dialog.setCancelable(false);

        LinearLayout view_cost_lay = (LinearLayout) dialog.findViewById(R.id.view_cost_lay);

        LinearLayout txt_cancel_lay = (LinearLayout) dialog.findViewById(R.id.txt_cancel_lay);
        txt_cancel_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        LinearLayout view_sample_lay = (LinearLayout) dialog.findViewById(R.id.view_sample_lay);
        TextView view_sample_txt = (TextView) dialog.findViewById(R.id.view_sample_txt);
        view_sample_txt.setTypeface(RobotoFont.regularFont(context));
        TextView view_cost_txt = (TextView) dialog.findViewById(R.id.view_cost_txt);
        view_cost_txt.setTypeface(RobotoFont.regularFont(activity));

        try {
            if (reactionTrendItem.isSampleSent()) {
                view_sample_txt.setTextColor(Color.parseColor("#0A6CD9"));
                view_sample_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addSampleDialog(activity, reactionTrendItem);
                    }
                });
            } else {
                view_sample_txt.setTextColor(Color.parseColor("#929292"));
            }
        } catch (Exception e) {
            view_sample_txt.setTextColor(Color.parseColor("#929292"));
        }

        if (!TextUtils.isEmpty(reactionTrendItem.getCostsheetID()) && reactionTrendItem.isCostsheetSent()) {
            view_cost_txt.setTextColor(Color.parseColor("#0A6CD9"));
            view_cost_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent costsheetintent = new Intent(context, CostingSheet_Activity.class);
                    costsheetintent.putExtra("Reaction_Id", reactionTrendItem.getId());
                    costsheetintent.putExtra("isSend", reactionTrendItem.isCostsheetSent());
                    costsheetintent.putExtra("costsheet_id", reactionTrendItem.getCostsheetID());
                    costsheetintent.putExtra("sentto", reactionTrendItem.getUserID());
                    costsheetintent.putExtra("isdisable", true);
                    dialog.dismiss();
                    context.startActivity(costsheetintent);
                    ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);

                }
            });
        } else
            view_cost_txt.setTextColor(Color.parseColor("#929292"));


        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));

        dialog.show();
    }


    public void addList(ArrayList<ReactionTrendItem> list) {
        reactionTrendItemArrayList = list;
        Collections.sort(reactionTrendItemArrayList, new Comparator<ReactionTrendItem>() {
            @Override
            public int compare(ReactionTrendItem o1, ReactionTrendItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


}
