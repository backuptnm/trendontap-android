package tnm.trendontap.adapter;

/**
 * Created by TNM on 3/22/2018.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import tnm.trendontap.R;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.utility.ExpandableHeightGridView;

public class CustomAdapter extends BaseAdapter {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private ArrayList<String> photoTimeStampArrayList;
    private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

    private LayoutInflater mInflater;


    Map<String, ArrayList<PhotoTimeStamp>> hashMapArrayList;
    Context context;
    int imageWidth;

    public CustomAdapter(Context context, ArrayList<String> photoTimeStampArrayList) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.photoTimeStampArrayList = photoTimeStampArrayList;
        this.context = context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int gridWidth = displayMetrics.widthPixels;
        imageWidth = gridWidth / 4;

    }

   /* public CustomAdapter(Context context, ArrayList<HashMap<String, ArrayList<PhotoTimeStamp>>> hashMapArrayList) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.hashMapArrayList = hashMapArrayList;
        this.context = context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int gridWidth = displayMetrics.widthPixels;
        imageWidth = gridWidth / 4;

    }*/

    @Override
    public int getCount() {
        // return mData.size();
        return photoTimeStampArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.snippet_item1, null);
            holder.textView = (TextView) convertView.findViewById(R.id.text);
            holder.data_grid = (ExpandableHeightGridView) convertView.findViewById(R.id.data_grid);
            holder.data_grid.setColumnWidth(imageWidth);
            holder.data_grid.setExpanded(true);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String month = photoTimeStampArrayList.get(position);
        holder.textView.setText(month);

        ArrayList<PhotoTimeStamp> value = (ArrayList<PhotoTimeStamp>) hashMapArrayList.get(month);
        holder.data_grid.setAdapter(new GridItemAdapter(context, value));

        return convertView;
    }

    public class ViewHolder {
        public TextView textView;
        public ExpandableHeightGridView data_grid;
    }


    public void addList(ArrayList<String> photoTimeStampArrayList, Map<String, ArrayList<PhotoTimeStamp>> imagesArray) {
        this.photoTimeStampArrayList = photoTimeStampArrayList;
        this.hashMapArrayList = imagesArray;
        notifyDataSetChanged();
    }




}
