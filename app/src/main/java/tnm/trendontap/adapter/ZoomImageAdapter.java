package tnm.trendontap.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.utility.TouchImageView;

/**
 * Created by admin on 02-Apr-18.
 */

public class ZoomImageAdapter extends PagerAdapter {

    private Context context;
    List<String> imageList;
    private LayoutInflater inflater;

    public ZoomImageAdapter(Context context, List<String> imageList) {
        this.context = context;
        this.imageList = imageList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @SuppressLint("CheckResult")
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.zoom_images_layout, view, false);

        assert imageLayout != null;
        final TouchImageView mImageView = (TouchImageView) imageLayout.findViewById(R.id.imageView);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.override(300, 300);
        int imageSize = imageList.size();

        String url = imageList.get(position);
       /* Glide.with(context)
                .load(new File(url))
                .apply(requestOptions)
                .into(mImageView);*/

        Glide.with(context).asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>(){
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {


                        mImageView.setImageBitmap(resource);
                    }
                });




        view.addView(imageLayout);

        return imageLayout;
    }



    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }


}
