package tnm.trendontap.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import tnm.trendontap.fragment.Costsheet_Fragment;
import tnm.trendontap.fragment.Reaction_Page_Fragment;
import tnm.trendontap.fragment.SampleRequest_Fragment;


public class ProfilePagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private String userid;
    private String userrole;

    public ProfilePagerAdapter(FragmentManager fm, int mNumOfTabs, String userid, String userrole) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.userid = userid;
        this.userrole = userrole;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Reaction_Page_Fragment.newInstance(userid, userrole);
            case 1:
                return Costsheet_Fragment.newInstance(userid, userrole);
            case 2:
                return SampleRequest_Fragment.newInstance(userid, userrole);
            default:
                return null;
        }
    }


    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }


}
