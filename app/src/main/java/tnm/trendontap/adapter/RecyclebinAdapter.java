package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.RecycleImageActivity;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.utility.SquareImageView;

import static tnm.trendontap.activity.RecycleBinActivity.selectxt;


public class RecyclebinAdapter extends ArrayAdapter<ReactionTrendItem> {
    private ArrayList<ReactionTrendItem> reactionItemArray;
    LayoutInflater inflater;
    private Context context;
    LinearLayout bottom_layout;
    public ArrayList<ReactionTrendItem> selectedid;

    public RecyclebinAdapter(@NonNull Context context, ArrayList<ReactionTrendItem> reactionItemArray, LinearLayout bottom_layout) {
        super(context, 0, reactionItemArray);
        this.context = context;
        this.reactionItemArray = reactionItemArray;
        selectedid = new ArrayList<>();
        this.bottom_layout = bottom_layout;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_recyclebin_item, parent, false);
            holder.image = (SquareImageView) convertView.findViewById(R.id.image);
            holder.selecticon = (ImageView) convertView.findViewById(R.id.selecticon);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (selectxt.getText().toString().equalsIgnoreCase("Select")) {
            holder.selecticon.setVisibility(View.GONE);
            holder.selecticon.setTag("hide");
            bottom_layout.setVisibility(View.GONE);
        }

        final ReactionTrendItem recycleBinItem = reactionItemArray.get(position);

        if (!TextUtils.isEmpty(String.valueOf(recycleBinItem.getPhotoUrls().get(0)))) {
            RequestOptions requestOptions = new RequestOptions();

            Glide.with(context)
                    .load(recycleBinItem.getPhotoUrls().get(0))
                    .apply(requestOptions)
                    .into(holder.image);
        }


        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selectxt.getTag().toString().equalsIgnoreCase("selectxt")) {
                    if (holder.selecticon.getTag().toString().equalsIgnoreCase("hide")) {
                        holder.selecticon.setVisibility(View.VISIBLE);
                        holder.selecticon.setTag("show");
                        selectedid.add(recycleBinItem);
                        bottom_layout.setVisibility(View.VISIBLE);
                    } else {
                        holder.selecticon.setVisibility(View.GONE);
                        holder.selecticon.setTag("hide");
                        selectedid.remove(recycleBinItem);
                        if (selectedid.size() > 0)
                            bottom_layout.setVisibility(View.VISIBLE);
                        else
                            bottom_layout.setVisibility(View.GONE);
                    }
                    //Toast.makeText(context, recycleBinItem.getId(), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, RecycleImageActivity.class);
                    intent.putExtra("reactionTrendItem", reactionItemArray.get(position));
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            }
        });
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return reactionItemArray.size();
    }

    private class Holder {
        SquareImageView image;
        ImageView selecticon;

    }


}
