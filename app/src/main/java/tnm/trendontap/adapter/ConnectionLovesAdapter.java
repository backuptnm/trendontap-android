package tnm.trendontap.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.activity.CommentActivity;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Connection_Loves_Fragment;
import tnm.trendontap.fragment.Reaction_Fragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.EventClass.LIKE_LOVE;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getCounttxt;


public class ConnectionLovesAdapter extends ArrayAdapter<LovesItem> {
    LayoutInflater inflater;
    private Context mContext;
    private ArrayList<LovesItem> connectionLovesItemArrayList;
    String userId;
    LoveImagesAdapter myPagerAdapter;
    Preference preference;
    Users users;

    public ConnectionLovesAdapter(@NonNull Context mContext, ArrayList<LovesItem> connectionLovesItemArrayList, String userId) {
        super(mContext, 0, connectionLovesItemArrayList);
        this.mContext = mContext;
        this.userId = userId;
        this.connectionLovesItemArrayList = connectionLovesItemArrayList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preference = new Preference(mContext, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
    }

    @Override
    public int getCount() {
        return connectionLovesItemArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_connect_user_loves_item, parent, false);
            holder.interested = (ImageView) convertView.findViewById(R.id.interested);
            holder.loveimg = (ImageView) convertView.findViewById(R.id.loveimg);
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.interesttrend = (TextView) convertView.findViewById(R.id.interesttrend);
            holder.interesttrend.setTypeface(RobotoFont.mediumFont(mContext)); //regular normal text
            holder.comment = (TextView) convertView.findViewById(R.id.comment);
            holder.comment.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.commenttxt = (TextView) convertView.findViewById(R.id.commenttxt);
            holder.commenttxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));

            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final LovesItem lovesItem = connectionLovesItemArrayList.get(position);

        myPagerAdapter = new LoveImagesAdapter(mContext, lovesItem.getPhotoUrls());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (lovesItem.getPhotoUrls().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                ArrayList<String> array = connectionLovesItemArrayList.get(position).getPhotoUrls();
                intent.putStringArrayListExtra("photoUrlsList", array);
                mContext.startActivity(intent);
            }
        });

        holder.abouttrend.setText(lovesItem.getAbouttrend());

        holder.comment.setText(getCounttxt(String.valueOf(lovesItem.getComment_count()), "Comment"));

        if (lovesItem.getLikecnt() == 0) {
            holder.interesttrend.setText("No interest in your love yet");
            holder.interesttrend.setTextColor(Color.parseColor("#929292"));
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
        } else {
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
            holder.interesttrend.setTextColor(Color.parseColor("#81BBFF"));
            String likertxt = "";

            if (lovesItem.getLikecnt() == 1) {
                likertxt = lovesItem.getLiker1() + " shows interest in your love";
                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1(), "");
            } else if (lovesItem.getLikecnt() == 2) {
                likertxt = lovesItem.getLiker1() + " and " + lovesItem.getLiker2() + " show interest in your love";
                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1(), lovesItem.getLiker2());
            } else {
                int otherlikers = (int) lovesItem.getLikecnt() - 2;
                String likeretxt = "";
                if (otherlikers == 1) {
                    likeretxt = String.valueOf(otherlikers) + " other";
                } else
                    likeretxt = String.valueOf(otherlikers) + " others";

                likertxt = lovesItem.getLiker1() + ", " + lovesItem.getLiker2() + " and " + likeretxt + "  show interest in your love";

                setFont(holder.interesttrend, likertxt, lovesItem.getLiker1() + ", " + lovesItem.getLiker2(), likeretxt);
            }
        }

        holder.daysago.setText(Utils.getDay(lovesItem.getDaysago()));

        holder.reactiontxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("name", "Reactions");
                bundle.putString("from", "love");
                bundle.putString("postid", lovesItem.getId());
                bundle.putStringArrayList("photourls", lovesItem.getPhotoUrlsList());
                MainActivity.loadContentFragment(new Reaction_Fragment(), false, bundle);
            }
        });
       /* if (lovesItem.getLikecnt() == 0) {
            holder.loveimg.setImageResource(R.mipmap.loves_icon);
        } else {
            holder.loveimg.setImageResource(R.mipmap.loves_icon_active);
        }*/

        int reactioncnt = Integer.parseInt(lovesItem.getReactioncnt());
        if (reactioncnt == 0) {
            holder.reactiontxt.setText("Reactions");
        } else if (reactioncnt == 1) {
            holder.reactiontxt.setText("Reaction");
        } else {
            holder.reactiontxt.setText("Reactions");
        }

        holder.loveimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.loveimg.getTag().toString().equalsIgnoreCase("like")) {
                    setLike(false, holder.loveimg, lovesItem, holder.interested);
                } else {
                    setLike(true, holder.loveimg, lovesItem, holder.interested);
                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(users.getProfilePic());
                    activityItem.setLongMessage(LIKE_MSG);
                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + LIKE_MSG);
                    activityItem.setPostID(lovesItem.getId());
                    activityItem.setPostImageURL(lovesItem.getPhotoUrlsList().get(0));
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("love");
                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                    activityItem.setActivityType(LIKE_LOVE);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(lovesItem.getUserid());

                    // processActivity(activityItem);
                    String msg = users.getFirstName() + " " + users.getLastName() + LIKE_MSG + "love";
                    sendActivityToUser(lovesItem.getUserid(), activityItem, msg);

                }
            }
        });


        holder.commenttxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).startActivityForResult(new Intent(mContext, CommentActivity.class).
                        putExtra("post_id", lovesItem.getId()).
                        putExtra("is_from_post", true).
                        putExtra("sento", userId).
                        putExtra("from", "love").
                        putExtra("position", position).
                        putExtra("post_img", lovesItem.getPhotoUrlsList().get(0)), 123);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).startActivityForResult(new Intent(mContext, CommentActivity.class).
                        putExtra("post_id", lovesItem.getId()).
                        putExtra("is_from_post", true).
                        putExtra("sento", userId).
                        putExtra("from", "love").
                        putExtra("position", position).
                        putExtra("post_img", lovesItem.getPhotoUrlsList().get(0)), 123);
                ((Activity) mContext).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        makePostLikes(holder.loveimg, lovesItem, holder.interested);
        return convertView;
    }

    class Holder {

        TextView name, abouttrend, interesttrend, comment, daysago, commenttxt, reactiontxt;
        ClickableViewPager pager;
        CirclePageIndicator indicator;
        ImageView loveimg, interested;
    }

    public void addList(ArrayList<LovesItem> list) {
        connectionLovesItemArrayList = list;
        Collections.sort(connectionLovesItemArrayList, new Comparator<LovesItem>() {
            @Override
            public int compare(LovesItem o1, LovesItem o2) {
                return Long.valueOf(o2.getDaysago()).compareTo(o1.getDaysago());
            }
        });
        notifyDataSetChanged();
    }


    public void setFont(TextView textView, String str, String str1, String str2) {

        int part1 = str.indexOf(str1);
        int part2 = str.indexOf(str2);

        SpannableStringBuilder SS = new SpannableStringBuilder(str);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part1, part1 + str1.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part2, part2 + str2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part1, part1 + str1.length(), 0);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part2, part2 + str2.length(), 0);
        textView.setText(SS);
    }

    private void makePostLikes(final ImageView img, final LovesItem lovesItem, final ImageView interested) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(lovesItem.getId()).child(Likes);
        databaseReference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                    if (lovesItem.getLikecnt() > 0) {
                        interested.setImageResource(R.mipmap.loves_icon_active);
                    } else {
                        interested.setImageResource(R.mipmap.loves_icon);
                    }
                    return;
                }

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if (dataSnapshot1.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        img.setImageResource(R.mipmap.loves_icon_active);
                        img.setTag("like");
                        interested.setImageResource(R.mipmap.loves_icon_active);
                    }
                }
              /*  if (!dataSnapshot.exists()) {
                    img.setImageResource(R.mipmap.loves_icon);
                    img.setTag("dislike");
                    if (lovesItem.getLikecnt() > 0) {
                        interested.setImageResource(R.mipmap.loves_icon_active);
                    } else {
                        interested.setImageResource(R.mipmap.loves_icon);
                    }
                } else {
                    img.setImageResource(R.mipmap.loves_icon_active);
                    img.setTag("like");
                    interested.setImageResource(R.mipmap.loves_icon_active);
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setLike(boolean islike, ImageView img, LovesItem buyerLovesItem, ImageView interested) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(PostLikes).child(buyerLovesItem.getId()).child(Likes);
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().
                child(FirebaseTrendOnTapPosts).child(buyerLovesItem.getId());
        if (islike) {
            img.setImageResource(R.mipmap.loves_icon_active);
            img.setTag("like");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
            databaseReference1.child(LikesCount).setValue(buyerLovesItem.getLikecnt() + 1);
            buyerLovesItem.setLikecnt(buyerLovesItem.getLikecnt() + 1);
            interested.setImageResource(R.mipmap.loves_icon_active);
        } else {
            img.setImageResource(R.mipmap.loves_icon);
            img.setTag("dislike");
            databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
            databaseReference1.child(LikesCount).setValue(buyerLovesItem.getLikecnt() - 1);

            if (buyerLovesItem.getLikecnt() > 0)
                buyerLovesItem.setLikecnt(buyerLovesItem.getLikecnt() - 1);

            if (buyerLovesItem.getLikecnt() > 0) {
                interested.setImageResource(R.mipmap.loves_icon_active);
            } else {
                interested.setImageResource(R.mipmap.loves_icon);
            }
        }
        notifyDataSetChanged();
    }

}
