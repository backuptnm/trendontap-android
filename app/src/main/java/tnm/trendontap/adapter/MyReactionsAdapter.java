package tnm.trendontap.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.CommentActivity;
import tnm.trendontap.activity.CostingSheet_Activity;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.UploadLoveFragment;
import tnm.trendontap.fragment.UploadReactionFragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.MyReaction;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.SampleItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import tnm.trendontap.utility.Utils;


import static tnm.trendontap.activity.CameraActivity.loadContentFragment;
import static tnm.trendontap.utility.EventClass.COSTINSHEET;
import static tnm.trendontap.utility.EventClass.SAMPLE;
import static tnm.trendontap.utility.EventClass.SENTMSG;

import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.ReactionID;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.Sample;
import static tnm.trendontap.utility.FireStoreUtils.SampleID;
import static tnm.trendontap.utility.FireStoreUtils.SampleSent;
import static tnm.trendontap.utility.FireStoreUtils.StyleNumber;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentSupplier;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.confiramtionDialog;


public class MyReactionsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MyReaction> mMyReactionItemArrayList;
    LayoutInflater inflater;
    String postimg = "", from = "";
    LoveImagesAdapter myPagerAdapter;

    public MyReactionsAdapter(Context context, ArrayList<MyReaction> myReactionItemArrayList, String postimg, String from) {
        this.context = context;
        this.postimg = postimg;
        this.from = from;
        this.mMyReactionItemArrayList = myReactionItemArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mMyReactionItemArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_my_reactions_item, parent, false);
            holder.content = (TextView) convertView.findViewById(R.id.content);
            holder.content.setTypeface(RobotoFont.mediumFont(context));
            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.username.setTypeface(RobotoFont.regularFont(context));
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.type.setTypeface(RobotoFont.regularFont(context));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(context));
            holder.commentcnt = (TextView) convertView.findViewById(R.id.commentcnt);
            holder.commentcnt.setTypeface(RobotoFont.regularFont(context));
            holder.commenttxt = (TextView) convertView.findViewById(R.id.commenttxt);
            holder.commenttxt.setTypeface(RobotoFont.regularFont(context));
            holder.userimg = (CircleImageView) convertView.findViewById(R.id.userimg);
            // holder.trendimg = (ScaledImageView) convertView.findViewById(R.id.trendimg);
            holder.doticon = (ImageView) convertView.findViewById(R.id.doticon);
            holder.optionicon = (LinearLayout) convertView.findViewById(R.id.optionicon);
            holder.pager = (ClickableViewPager) convertView.findViewById(R.id.loveimgpager);
            holder.indicator = (CirclePageIndicator) convertView.findViewById(R.id.indicator);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        final MyReaction myReaction = mMyReactionItemArrayList.get(position);
        holder.username.setText(myReaction.getName());
        holder.type.setText(myReaction.getType());
        holder.content.setText(myReaction.getAbouttrend());
        holder.commentcnt.setText(String.valueOf(myReaction.getCommentcount()));

        if (myReaction.isUnread_comment()) {
            holder.doticon.setVisibility(View.VISIBLE);
        } else {
            holder.doticon.setVisibility(View.INVISIBLE);
        }

        holder.commenttxt.setText("Comments");
        if (myReaction.getCommentcount() > 0) {
            if (myReaction.getCommentcount() == 1)
                holder.commenttxt.setText("Comment");
            holder.commenttxt.setBackgroundResource(R.drawable.rectangle_blue_bg);
            holder.commenttxt.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.commenttxt.setBackgroundResource(R.drawable.rectangle_grey_bg);
            holder.commenttxt.setTextColor(Color.parseColor("#929292"));
        }

        holder.commenttxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CommentActivity.class).
                        putExtra("reaction_id", myReaction.getReactionid()).
                        putExtra("post_id", myReaction.getPost_id()).
                        putExtra("postimg", postimg).
                        putExtra("from", from).
                        putExtra("sento", myReaction.getBuyer_user_id()).
                        putExtra("reaction_img", myReaction.getImgurl()));
                ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
                DatabaseReference reactionread = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).
                        child(myReaction.getReactionid());
                reactionread.child(tReactionUnreadCommentSupplier).setValue(false);
            }
        });

        holder.daysago.setText(Utils.getDay(myReaction.getTimeStamp()));

        if (!TextUtils.isEmpty(myReaction.getAvtarurl())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(context)
                    .load(myReaction.getAvtarurl())
                    .apply(requestOptions)
                    .into(holder.userimg);
        }

        myPagerAdapter = new LoveImagesAdapter(context, myReaction.getPhotoUrls());
        holder.pager.setAdapter(myPagerAdapter);
        holder.indicator.setViewPager(holder.pager);
        if (myReaction.getPhotoUrls().size() > 1) {
            holder.indicator.setVisibility(View.VISIBLE);
        } else {
            holder.indicator.setVisibility(View.GONE);
        }
        holder.pager.setCurrentItem(0);
        holder.pager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(context, FullScreenImageActivity.class);
                ArrayList<String> array = mMyReactionItemArrayList.get(position).getPhotoUrls();
                intent.putStringArrayListExtra("photoUrlsList", array);
                context.startActivity(intent);
            }
        });

        holder.optionicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(context, myReaction);
            }
        });
        return convertView;
    }

    public void setPosturl(String postimg) {
        this.postimg = postimg;
    }

    class Holder {
        CircleImageView userimg;
        ImageView doticon;
        //ScaledImageView trendimg;
        TextView username, type, commentcnt, commenttxt, daysago, content;
        LinearLayout optionicon;
        ClickableViewPager pager;
        CirclePageIndicator indicator;
    }

    public void update(ArrayList<MyReaction> myReactionItemArrayList) {
        this.mMyReactionItemArrayList = myReactionItemArrayList;
        Collections.sort(mMyReactionItemArrayList, new Comparator<MyReaction>() {
            @Override
            public int compare(MyReaction o1, MyReaction o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


    private void onOptionsDialog(Context activity, final MyReaction myReaction) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options_supplier_reaction);
        dialog.setCancelable(false);

        TextView txt_edit = ((TextView) dialog.findViewById(R.id.txt_edit));
        txt_edit.setTypeface(RobotoFont.regularFont(activity));
        TextView send_sample = ((TextView) dialog.findViewById(R.id.send_sample));
        send_sample.setTypeface(RobotoFont.regularFont(activity));
        TextView sendcostsheet = ((TextView) dialog.findViewById(R.id.sendcostsheet));
        sendcostsheet.setTypeface(RobotoFont.regularFont(activity));
        TextView txt_delete = ((TextView) dialog.findViewById(R.id.txt_delete));
        txt_delete.setTypeface(RobotoFont.regularFont(activity));
        TextView txt_cancel = ((TextView) dialog.findViewById(R.id.txt_cancel));
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if (myReaction.isSample_request()) {
            send_sample.setTextColor(Color.parseColor("#0A6CD9"));
            send_sample.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addSampleDialog(context, myReaction);
                    dialog.dismiss();
                }
            });
        } else {
            send_sample.setTextColor(Color.parseColor("#929292"));
        }

        if (myReaction.isSample_sent()) {
            send_sample.setText("View Sample");
        } else {
            send_sample.setText("Send Sample");
        }

        if (myReaction.isCost_request()) {
            sendcostsheet.setTextColor(Color.parseColor("#0A6CD9"));
            sendcostsheet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // addSampleDialog(context, myReaction);
                    Intent costsheetintent = new Intent(context, CostingSheet_Activity.class);
                    costsheetintent.putExtra("Reaction_Id", myReaction.getReactionid());
                    costsheetintent.putExtra("isSend", myReaction.isCost_sent());
                    costsheetintent.putExtra("costsheet_id", myReaction.getCostsheet_id());
                    costsheetintent.putExtra("isdisable", false);
                    costsheetintent.putExtra("postimg", postimg);
                    costsheetintent.putExtra("sentto", myReaction.getBuyer_user_id());
                    costsheetintent.putExtra("post_id", myReaction.getPost_id());
                    dialog.dismiss();
                    context.startActivity(costsheetintent);
                    ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
                }
            });
        } else {
            sendcostsheet.setTextColor(Color.parseColor("#929292"));
        }
        if (!myReaction.getCostsheet_id().equalsIgnoreCase("") && !myReaction.getCostsheet_id().equalsIgnoreCase("null")) {
            sendcostsheet.setText("View Costing Sheet");
        } else {
            sendcostsheet.setText("Send Costing Sheet");
        }

        txt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReaction(myReaction, dialog);
            }
        });

        txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editReaction(myReaction, dialog);
            }
        });

        dialog.show();
    }

    public void editReaction(MyReaction myReaction, Dialog dialog) {
        dialog.dismiss();
        Bundle bundle = new Bundle();
        bundle.putString("postid", myReaction.getPost_id());
        bundle.putString("selectedImage", myReaction.getPhotoUrls().get(0));
        bundle.putString("postuserid", myReaction.getBuyer_user_id());
        bundle.putString("postimg", postimg);
        bundle.putString("reactionid", myReaction.getReactionid());
        bundle.putString("content", myReaction.getAbouttrend());
        bundle.putBoolean("isEdit", true);
        MainActivity.loadContentFragment(new UploadReactionFragment(), false, bundle);
    }


    public void deleteReaction(MyReaction myReaction, Dialog dialog) {
        dialog.dismiss();
        DatabaseReference reaction = FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction);
        reaction.child(myReaction.getReactionid()).removeValue();

        final DatabaseReference postref = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts)
                .child(myReaction.getPost_id());
        postref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                if (reactionsCount > 0)
                    postref.child(ReactionsCount).setValue(reactionsCount - 1);

                ArrayList<String> suppliers = new ArrayList<>();
                if (dataSnapshot.hasChild(ReactedSuppiers) && dataSnapshot.child(ReactedSuppiers).getValue() != null) {
                    suppliers = (ArrayList<String>) dataSnapshot.child(ReactedSuppiers).getValue();
                    if (suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        suppliers.remove(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        postref.child(ReactedSuppiers).setValue(suppliers);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void addSampleDialog(final Context activity, final MyReaction myReaction) {
        final Dialog dialog = new Dialog(activity);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_add_sample);

        final TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(activity));

        final EditText edtname = (EditText) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(activity));
        edtname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) ;
                {
                    String code = edtname.getText().toString();
                    Utils.hidekeyboard(activity, edtname);
                    sendSample(activity, myReaction, code, tvTitle);
                    dialog.dismiss();
                }
                return false;
            }
        });

        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setTypeface(RobotoFont.boldFont(activity));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = edtname.getText().toString();
                Utils.hidekeyboard(activity, edtname);
                sendSample(activity, myReaction, code, tvTitle);
                dialog.dismiss();
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(activity));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (myReaction.isSample_sent()) {
            tvTitle.setText("View Sample");

            FirebaseDatabase.getInstance().getReference().child(Sample).orderByChild(ReactionID).equalTo(myReaction.getReactionid()).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                edtname.setText(dataSnapshot1.child(StyleNumber).getValue().toString());
                                break;
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        } else {
            tvTitle.setText("Send Sample");
        }


    }

    public void sendSample(Context activity, final MyReaction myReaction, final String code, TextView tvTitle) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Sample);

        if (tvTitle.getText().toString().equals("View Sample")) {
            databaseReference.orderByChild(ReactionID).equalTo(myReaction.getReactionid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String sampleid = dataSnapshot1.child(ID).getValue().toString();
                        databaseReference.child(sampleid).child(StyleNumber).setValue(code);
                        break;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            String id = databaseReference.push().getKey();
            Preference preference = new Preference(activity, Login_Pref);
            Users users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
            long tsLong = System.currentTimeMillis();
            String userName = users.getFirstName() + " " + users.getLastName();
            String userId = users.getUserID();
            String buyer_user_id = myReaction.getBuyer_user_id();
            SampleItem sampleItem = new SampleItem(buyer_user_id, id, myReaction.getReactionid(),
                    code, userName, userId, tsLong);

            databaseReference.child(id).setValue(sampleItem);
            final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).child(myReaction.getReactionid());
            databaseReference1.child(SampleID).setValue(id);
            DatabaseReference database = FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                    child(myReaction.getReactionid());
            database.child(SampleSent).setValue(true);
            tvTitle.setText("View Sample");

            int pos = matchValue(mMyReactionItemArrayList, myReaction.getReactionid());//.indexOf(myReaction);
            if (pos != -1) {
                mMyReactionItemArrayList.get(pos).setSample_sent(true);
            }


            ActivityItem activityItem = new ActivityItem();
            activityItem.setProfilePic(users.getProfilePic());
            activityItem.setLongMessage(SENTMSG);
            activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + SENTMSG);
            activityItem.setPostID(myReaction.getPost_id());
            activityItem.setPostImageURL(postimg);
            activityItem.setReactionID(myReaction.getReactionid());
            activityItem.setShortMessage(SAMPLE);
            activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
            activityItem.setActivityType(SAMPLE);
            activityItem.setTimeStamp(System.currentTimeMillis());
            activityItem.setUserID(users.getUserID());


            String msg = users.getFirstName() + " " + users.getLastName() + SENTMSG + SAMPLE;
            sendActivityToUser(buyer_user_id, activityItem, msg);

            confiramtionDialog(context, "Sent");
        }

    }


    public int matchValue(ArrayList<MyReaction> mMyReactionItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < mMyReactionItemArrayList.size(); j++) {
            if (mMyReactionItemArrayList.get(j).getReactionid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }
}
