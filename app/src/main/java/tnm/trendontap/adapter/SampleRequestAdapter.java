package tnm.trendontap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import tnm.trendontap.R;
import tnm.trendontap.modal.SampleRequest;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;


public class SampleRequestAdapter extends ArrayAdapter<SampleRequest> {
    private LayoutInflater inflater;
    private Context mContext;
    private ArrayList<SampleRequest> sampleRequestlist;
    private List<SampleRequest> arraylist;
    private LinearLayout nocostsheetdata;


    public SampleRequestAdapter(@NonNull Context mContext, ArrayList<SampleRequest> sampleRequestlist, LinearLayout nocostsheetdata) {
        super(mContext, 0, sampleRequestlist);
        this.mContext = mContext;
        this.arraylist = new ArrayList<>();
        this.sampleRequestlist = sampleRequestlist;
        this.nocostsheetdata = nocostsheetdata;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sampleRequestlist.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_samplerequest_item, parent, false);
            holder.samplesenttxt = (TextView) convertView.findViewById(R.id.samplesenttxt);
            holder.samplesenttxt.setTypeface(RobotoFont.mediumFont(mContext));
            holder.samplesent = (TextView) convertView.findViewById(R.id.samplesent);
            holder.samplesent.setTypeface(RobotoFont.mediumFont(mContext));
            holder.trackingnotxt = (TextView) convertView.findViewById(R.id.trackingnotxt);
            holder.trackingnotxt.setTypeface(RobotoFont.mediumFont(mContext));
            holder.trackingno = (TextView) convertView.findViewById(R.id.trackingno);
            holder.trackingno.setTypeface(RobotoFont.mediumFont(mContext));
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        SampleRequest sampleRequest = sampleRequestlist.get(position);
        holder.samplesent.setText(Utils.getDateFromTimeStamp(sampleRequest.getTimestamp()));
        holder.trackingno.setText(sampleRequest.getTrackingno());
        return convertView;
    }

    class Holder {
        TextView samplesenttxt, samplesent, trackingnotxt, trackingno;
    }

    public void addList(ArrayList<SampleRequest> list) {
        sampleRequestlist = list;
        this.arraylist.addAll(list);
        notifyDataSetChanged();
    }

    public void filter(String charText, ListView listview) {
        charText = charText.toLowerCase(Locale.getDefault());
        sampleRequestlist.clear();
        if (charText.length() == 0) {
            sampleRequestlist.addAll(arraylist);
            if (sampleRequestlist.size() == 0) {
                nocostsheetdata.setVisibility(View.VISIBLE);
                listview.setVisibility(View.GONE);
            } else {
                nocostsheetdata.setVisibility(View.GONE);
                listview.setVisibility(View.VISIBLE);
            }
        } else {
            filterData(charText,listview);
        }
        notifyDataSetChanged();
    }

    private void filterData(String charText, ListView listview) {

        for (SampleRequest supplierItem : arraylist) {
            if (Utils.getDateFromTimeStamp(supplierItem.getTimestamp()).toString().toLowerCase(Locale.getDefault()).contains(charText)) {
                sampleRequestlist.add(supplierItem);
            } else if (supplierItem.getTrackingno().toLowerCase(Locale.getDefault()).contains(charText)) {
                sampleRequestlist.add(supplierItem);
            }
        }
        if (sampleRequestlist.size() == 0) {
            nocostsheetdata.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        } else {
            nocostsheetdata.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
        }
    }


}
