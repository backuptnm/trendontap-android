package tnm.trendontap.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import tnm.trendontap.R;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.Place;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;


/**
 * Created by Vivek on 02-02-2018.
 */

public class NearbyAdapter extends ArrayAdapter<LocationItem> {

    private ArrayList<LocationItem> mPlaceList;
    private Context context;
    private LayoutInflater inflater;
    ArrayList<LocationItem> arraylist;

    public NearbyAdapter(@NonNull Context context, ArrayList<LocationItem> mPlaceList) {
        super(context, 0, mPlaceList);
        this.mPlaceList = mPlaceList;
        this.context = context;
        this.arraylist = new ArrayList<>();
     //   this.arraylist.addAll(mPlaceList);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mPlaceList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_location, parent, false);
            viewHolder.loc_name = (TextView) convertView.findViewById(R.id.loc_name);
            viewHolder.loc_name.setTypeface(RobotoFont.mediumFont(context));
            viewHolder.loc_address = (TextView) convertView.findViewById(R.id.loc_address);
            viewHolder.loc_address.setTypeface(RobotoFont.regularFont(context));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        LocationItem placesList = mPlaceList.get(position);
        viewHolder.loc_name.setText(placesList.getTitle());
        viewHolder.loc_address.setText(placesList.getAddress());

        return convertView;
    }

    class ViewHolder {
        TextView loc_name, loc_address;
    }


   /* public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mPlaceList.clear();
       // Frag_JobAvailList.swipe_refresh_layout.setVisibility(View.VISIBLE);
       // Frag_JobAvailList.nojobs.setVisibility(View.GONE);
        if (charText.length() == 0) {
            mPlaceList.addAll(arraylist);
        } else {
                filterData(charText);
        }

        notifyDataSetChanged();
    }

    public void filterData(String charText) {
        for (LocationItem jr : arraylist) {

            if (jr.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                mPlaceList.add(jr);
            }
        }
        if (mPlaceList.size() == 0) {
           // Frag_JobAvailList.swipe_refresh_layout.setVisibility(View.GONE);
            //Frag_JobAvailList.nojobs.setVisibility(View.VISIBLE);
        }
    }*/

}
