package tnm.trendontap.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.Reactions_Trends_Fragment;
import tnm.trendontap.fragment.UploadTrendFragment;
import tnm.trendontap.fragment.Viewpager_Fragment;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.CustomTypefaceSpan;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;
import static tnm.trendontap.utility.Utils.setDialogwidth;

public class TrendAdapter extends ArrayAdapter<TrendsItem> {
    LayoutInflater inflater;
    Context mContext;
    private ArrayList<TrendsItem> trendsItemList;
    private ProgressDialog progressDialog;
    Drawable drawable;

    public TrendAdapter(@NonNull Context mContext, ArrayList<TrendsItem> trendsItemList) {
        super(mContext, 0, trendsItemList);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Please wait..");
        this.mContext = mContext;
        this.trendsItemList = trendsItemList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resources res = mContext.getResources();
        drawable = res.getDrawable(R.drawable.circular);
    }

    @Override
    public int getCount() {
        return trendsItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.row_trend_item, parent, false);
            holder.trendimg = (ScaledImageView) convertView.findViewById(R.id.trendimg);
            holder.locationname = (TextView) convertView.findViewById(R.id.locationname);
            holder.locationname.setTypeface(RobotoFont.boldFont(mContext));
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.name.setTypeface(RobotoFont.boldFont(mContext));
            holder.category = (TextView) convertView.findViewById(R.id.category);
            holder.category.setTypeface(RobotoFont.boldFont(mContext));
            holder.abouttrend = (TextView) convertView.findViewById(R.id.abouttrend);
            holder.abouttrend.setTypeface(RobotoFont.mediumFont(mContext));
            holder.interesttrend = (TextView) convertView.findViewById(R.id.interesttrend);
            holder.interesttrend.setTypeface(RobotoFont.mediumFont(mContext)); //regular normal text
            holder.deliverydatetxt = (TextView) convertView.findViewById(R.id.deliverydatetxt);
            holder.deliverydatetxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.deliverydate = (TextView) convertView.findViewById(R.id.deliverydate);
            holder.deliverydate.setTypeface(RobotoFont.regularFont(mContext));
            holder.daysago = (TextView) convertView.findViewById(R.id.daysago);
            holder.daysago.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactioncnt = (TextView) convertView.findViewById(R.id.reactioncnt);
            holder.reactioncnt.setTypeface(RobotoFont.regularFont(mContext));
            holder.reactiontxt = (TextView) convertView.findViewById(R.id.reactiontxt);
            holder.reactiontxt.setTypeface(RobotoFont.regularFont(mContext));
            holder.doticon = (ImageView) convertView.findViewById(R.id.doticon);
            holder.loveicon = (ImageView) convertView.findViewById(R.id.loveicon);
            holder.reactionbtn = (LinearLayout) convertView.findViewById(R.id.reactionbtn);
            holder.optionicon = (LinearLayout) convertView.findViewById(R.id.optionicon);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.progressBar.setProgress(0);   // Main Progress
        holder.progressBar.setSecondaryProgress(100); // Secondary Progress
        holder.progressBar.setMax(100);
        holder.progressBar.setProgressDrawable(drawable);

        final TrendsItem trendsItem = trendsItemList.get(position);

        if (trendsItem.isUnreadReactionCount()) {
            holder.doticon.setVisibility(View.VISIBLE);
        } else
            holder.doticon.setVisibility(View.INVISIBLE);
        holder.name.setText(trendsItem.getPostTitle());
        holder.category.setText(trendsItem.getCategoryName());
        holder.abouttrend.setText(trendsItem.getContent());


        if (trendsItem.getLikecnt() == 0) {
            holder.loveicon.setImageResource(R.mipmap.loves_icon);
            holder.interesttrend.setText("No interest in your trend yet");
            holder.interesttrend.setTextColor(Color.parseColor("#929292"));
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
        } else {
            holder.loveicon.setImageResource(R.mipmap.loves_icon_active);
            holder.interesttrend.setTypeface(RobotoFont.regularFont(mContext));
            holder.interesttrend.setTextColor(Color.parseColor("#81BBFF"));
            String likertxt = "";

            if (trendsItem.getLikecnt() == 1) {
                likertxt = trendsItem.getLiker1() + " shows interest in your trend";
                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1(), "");
            } else if (trendsItem.getLikecnt() == 2) {
                likertxt = trendsItem.getLiker1() + " and " + trendsItem.getLiker2() + " show interest in your trend";
                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1(), trendsItem.getLiker2());
            } else {
                int otherlikers = (int) trendsItem.getLikecnt() - 2;
                String likeretxt = "";
                if (otherlikers == 1) {
                    likeretxt = String.valueOf(otherlikers) + " other";
                } else
                    likeretxt = String.valueOf(otherlikers) + " others";

                likertxt = trendsItem.getLiker1() + ", " + trendsItem.getLiker2() + " and " + likeretxt + "  show interest in your trend";

                setFont(holder.interesttrend, likertxt, trendsItem.getLiker1() + ", " + trendsItem.getLiker2(), likeretxt);

            }

        }
        holder.locationname.setVisibility(View.VISIBLE);
        try {
            if (trendsItem.getLocationItemArrayList() != null) {
                if (!TextUtils.isEmpty(trendsItem.getLocationItemArrayList().title) && trendsItem.getLocationItemArrayList().latitude != 0.0) {
                    holder.locationname.setText(trendsItem.getLocationItemArrayList().getTitle() + " >");
                } else {
                    holder.locationname.setVisibility(View.GONE);
                }
            } else {
                holder.locationname.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            holder.locationname.setVisibility(View.GONE);
        }

        holder.locationname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("location", trendsItem.getLocationItemArrayList());
                MainActivity.loadContentFragment(new tnm.trendontap.fragment.MapFragment(), false, bundle);
            }
        });

        holder.optionicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsDialog(mContext, trendsItem, position);
            }
        });


        if (trendsItem.getDeliveryDate().equals("")) {
            holder.deliverydatetxt.setVisibility(View.GONE);
            holder.deliverydate.setVisibility(View.GONE);
        } else {
            holder.deliverydatetxt.setVisibility(View.VISIBLE);
            holder.deliverydate.setVisibility(View.VISIBLE);
            holder.deliverydate.setText(trendsItem.getDeliveryDate());
        }


        holder.daysago.setText(Utils.getDay(trendsItem.getTimeStamp()));
        holder.reactioncnt.setText(trendsItem.getReactionsCount());
        int reactcnt = Integer.parseInt(trendsItem.getReactionsCount());
        holder.reactiontxt.setText("Reactions");
        if (reactcnt > 0) {
            if (reactcnt == 1)
                holder.reactiontxt.setText("Reaction");
            holder.reactiontxt.setBackgroundResource(R.drawable.rectangle_blue_bg);
        } else {
            holder.reactiontxt.setBackgroundResource(R.drawable.rectangle_grey_bg);
        }
        holder.reactionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(trendsItem.getReactionsCount()) == 0) {
                    return;
                }
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts);
                databaseReference.child(trendsItem.getId()).child(UnreadReactionCount).setValue(false);
                holder.doticon.setVisibility(View.INVISIBLE);
                Bundle bundle = new Bundle();
                bundle.putString("from", "trend");
                bundle.putString("title", trendsItem.getPostTitle());
                bundle.putString("category", trendsItem.getCategoryName());
                bundle.putString("postimg", trendsItem.getPhotoUrlsList().get(0));
                bundle.putString("postid", trendsItem.getId());
                MainActivity.loadContentFragment(new Reactions_Trends_Fragment(), false, bundle);
            }
        });

        if (!TextUtils.isEmpty(trendsItem.getPhotoUrlsList().get(0))) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerInside();
            requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());

           /* requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);*/

            Glide.with(mContext)
                    .load(trendsItem.getPhotoUrlsList().get(0))
                    .apply(requestOptions)
                    .into(holder.trendimg);
        }

        if (!TextUtils.isEmpty(trendsItem.getPdfUrl())) {
            downloadPDF(trendsItem.getPdfUrl(), holder.progressBar);
        }
       /* holder.trendimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trendsItemList == null && trendsItemList.size() == 0)
                    return;

                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 13);
                } else {
                    String pdfUrl = trendsItemList.get(position).getPdfUrl();
                    downloadPDF(pdfUrl, holder.progressBar);
                }
            }
        });*/

        return convertView;
    }

    class Holder {

        TextView name, category, abouttrend, interesttrend, deliverydatetxt, deliverydate, daysago, reactioncnt,
                reactiontxt, locationname;
        ImageView doticon, loveicon;
        ScaledImageView trendimg;
        LinearLayout reactionbtn, optionicon;
        ProgressBar progressBar;
    }

    public void addList(ArrayList<TrendsItem> list) {
        trendsItemList = list;
        Collections.sort(trendsItemList, new Comparator<TrendsItem>() {
            @Override
            public int compare(TrendsItem o1, TrendsItem o2) {
                return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
            }
        });
        notifyDataSetChanged();
    }


    public void setFont(TextView textView, String str, String str1, String str2) {

        int part1 = str.indexOf(str1);
        int part2 = str.indexOf(str2);

        SpannableStringBuilder SS = new SpannableStringBuilder(str);
        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part1, part1 + str1.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new CustomTypefaceSpan("", RobotoFont.mediumFont(mContext)), part2, part2 + str2.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part1, part1 + str1.length(), 0);
        SS.setSpan(new ForegroundColorSpan(Color.parseColor("#0B7AFF")), part2, part2 + str2.length(), 0);
        textView.setText(SS);
    }

    protected void onOptionsDialog(final Context activity, final TrendsItem trendsItem, final int pos) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_options);
        dialog.setCancelable(false);

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        };
        View.OnClickListener editClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putParcelable("trendsItem", trendsItem);
                bundle.putString("action", "edit");
                MainActivity.loadContentFragment(new UploadTrendFragment(), false, bundle);
            }
        };

        View.OnClickListener deleteClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeTrendItem(trendsItem, dialog, pos);
                dialog.dismiss();
            }
        };

        ((TextView) dialog.findViewById(R.id.txt_edit)).setOnClickListener(editClick);
        ((TextView) dialog.findViewById(R.id.txt_edit)).setTypeface(RobotoFont.regularFont(activity));
        final TextView txt_share = ((TextView) dialog.findViewById(R.id.txt_share));
        txt_share.setTypeface(RobotoFont.regularFont(activity));
        final TextView txt_send = ((TextView) dialog.findViewById(R.id.txt_send));
        txt_send.setTypeface(RobotoFont.regularFont(activity));
        ((TextView) dialog.findViewById(R.id.txt_delete)).setOnClickListener(deleteClick);
        ((TextView) dialog.findViewById(R.id.txt_delete)).setTypeface(RobotoFont.regularFont(activity));

        ((TextView) dialog.findViewById(R.id.txt_cancel)).setOnClickListener(cancelClick);
        ((TextView) dialog.findViewById(R.id.txt_cancel)).setTypeface(RobotoFont.mediumFont(activity));

        dialog.show();

        FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren())
                            return;
                        final ArrayList<String> supplierlist = new ArrayList<>();
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            supplierlist.add(dataSnapshot1.getKey());
                        }

                        txt_share.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                        txt_share.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                sentToDialog(mContext, trendsItem.getId(), supplierlist, "share");
                            }
                        });
                        txt_send.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                        txt_send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                sentToDialog(mContext, trendsItem.getId(), supplierlist, "sendto");
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void removeTrendItem(final TrendsItem trendsItem, final Dialog dialog, int pos) {
        progressDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);

        DatabaseReference databaseReference2 = FirebaseDatabase.getInstance().getReference(UserPosts);
        databaseReference2.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Trends).
                child(trendsItem.getId()).removeValue();
        databaseReference.child(trendsItem.getId()).removeValue();
        trendsItemList.remove(trendsItem);

        databaseReference2.child(trendsItem.getId()).removeValue();

        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
        databaseReference1.orderByKey().equalTo(trendsItem.getId());
        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    databaseReference1.child(dataSnapshot1.getKey()).child(Trends).child(trendsItem.getId()).removeValue();
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    private void sentToDialog(Context context, String postid, ArrayList<String> supplierlist, String from) {
        final Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.shareoptiondialog);
        dialog.setCancelable(false);

        RecyclerView horizontal_recycler_view = (RecyclerView) dialog.findViewById(R.id.horizontal_recycler_view);
        ArrayList<Users> horizontalList = new ArrayList<>();

        TextView txt_cancel = ((TextView) dialog.findViewById(R.id.txt_cancel));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.txt_cancel)).setTypeface(RobotoFont.mediumFont(context));

        ShareContactAdpater shareContactAdpater = new ShareContactAdpater(context, horizontalList, dialog);
        shareContactAdpater.setFrom(from);
        shareContactAdpater.setPostid(postid);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
        horizontal_recycler_view.setAdapter(shareContactAdpater);

        if (from.equalsIgnoreCase("share"))
            processShare(postid, horizontalList, shareContactAdpater, dialog, supplierlist, horizontal_recycler_view);
        else
            processSend(postid, horizontalList, shareContactAdpater, dialog, supplierlist, horizontal_recycler_view);

    }

    int cnt = 0;

    public void processSend(final String postid, final ArrayList<Users> horizontalList,
                            final ShareContactAdpater shareContactAdpater,
                            final Dialog dialog,
                            final ArrayList<String> supplierlist, final RecyclerView horizontal_recycler_view) {
        cnt = 0;

        for (int j = 0; j < supplierlist.size(); j++) {
            final int finalJ = j;
            FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).
                    child(supplierlist.get(finalJ)).child(Trends).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            if (dataSnapshot.hasChildren() && dataSnapshot.hasChild(postid)) {
                                cnt++;
                                FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                        child(supplierlist.get(finalJ)).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot usersval) {
                                                horizontal_recycler_view.setVisibility(View.VISIBLE);
                                                horizontalList.add(usersval.getValue(Users.class));
                                                shareContactAdpater.notifyDataSetChanged();
                                                if (!dialog.isShowing()) {
                                                    dialog.show();
                                                    setDialogwidth(dialog, mContext, 60);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }

                            if ((finalJ + 1) == supplierlist.size() && cnt == 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(mContext, "You have not shared with your any current suppliers", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(mContext, "You have not shared with your any current suppliers", Toast.LENGTH_LONG).show();
                        }
                    });
        }

    }

    int sharecnt = 0;

    public void processShare(final String postid, final ArrayList<Users> horizontalList,
                             final ShareContactAdpater shareContactAdpater,
                             final Dialog dialog,
                             final ArrayList<String> supplierlist, final RecyclerView horizontal_recycler_view) {
        sharecnt = 0;
        for (int j = 0; j < supplierlist.size(); j++) {
            final int finalJ = j;
            FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).
                    child(supplierlist.get(finalJ)).child(Trends).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {

                            if (!dataSnapshot.hasChild(postid)) {
                                sharecnt++;
                                FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                        child(supplierlist.get(finalJ)).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot usersval) {
                                                horizontal_recycler_view.setVisibility(View.VISIBLE);
                                                horizontalList.add(usersval.getValue(Users.class));
                                                shareContactAdpater.notifyDataSetChanged();
                                                if (!dialog.isShowing()) {
                                                    dialog.show();
                                                    setDialogwidth(dialog, mContext, 60);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }

                            if ((finalJ + 1) == supplierlist.size() && sharecnt == 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(mContext, "You have already shared with your all current suppliers", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(mContext, "You have already shared with your all current suppliers", Toast.LENGTH_LONG).show();
                        }
                    });
        }

    }


    public void downloadPDF(String url, ProgressBar progressBar) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf
        if (!pdfFile.exists()) {
            new DownloadFile(progressBar).execute(url, name);
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        ProgressBar progressBar;

        public DownloadFile(ProgressBar progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "TOT DOWNLOAD");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // FileDownloader.downloadFile(fileUrl, pdfFile);

            int MEGABYTE = 1024 * 1024;
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);

                byte[] buffer = new byte[MEGABYTE];


                int fileLength = urlConnection.getContentLength();
                long total = 0;
                int count;
                while ((count = inputStream.read(buffer)) != -1) {
                    total += count;
                    // publishing the progress....
                    Bundle resultData = new Bundle();
                    resultData.putInt("progress", (int) (total * 100 / fileLength));
                    //receiver.send(UPDATE_PROGRESS, resultData);
                    publishProgress((int) (total * 100 / fileLength));
                    fileOutputStream.write(buffer, 0, count);
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }

        private void publishProgress(int progress) {
            progressBar.setProgress(progress);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            // Toast.makeText(getApplicationContext(), "Download PDf successfully", Toast.LENGTH_SHORT).show();
            Log.d("Download complete", "----------");
        }
    }

}
