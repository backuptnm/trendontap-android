package tnm.trendontap.imagecrop.util;

/**
 * Created by admin on 04-May-18.
 */

public interface Easing {

    double easeOut(double time, double start, double end, double duration);

    double easeIn(double time, double start, double end, double duration);

    double easeInOut(double time, double start, double end, double duration);
}

