package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TNM on 3/9/2018.
 */

public class UserReactionFeeds implements Parcelable {
    private int costsheetCount, reactionCount, sampleCount;

    public UserReactionFeeds() {

    }

    public UserReactionFeeds(int costsheetCount, int reactionCount, int sampleCount) {
        this.costsheetCount = costsheetCount;
        this.reactionCount = reactionCount;
        this.sampleCount = sampleCount;
    }


    public int getCostsheetCount() {
        return costsheetCount;
    }

    public int getReactionCount() {
        return reactionCount;
    }

    public int getSampleCount() {
        return sampleCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.costsheetCount);
        dest.writeInt(this.reactionCount);
        dest.writeInt(this.sampleCount);
    }

    private UserReactionFeeds(Parcel in) {
        this.costsheetCount = in.readInt();
        this.reactionCount = in.readInt();
        this.sampleCount = in.readInt();
    }

    public static final Parcelable.Creator<UserReactionFeeds> CREATOR = new Parcelable.Creator<UserReactionFeeds>() {
        @Override
        public UserReactionFeeds createFromParcel(Parcel source) {
            return new UserReactionFeeds(source);
        }

        @Override
        public UserReactionFeeds[] newArray(int size) {
            return new UserReactionFeeds[size];
        }
    };
}
