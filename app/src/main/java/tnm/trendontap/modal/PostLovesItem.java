package tnm.trendontap.modal;


import java.util.ArrayList;

public class PostLovesItem {
    public String catagory, catagoryName, content, deliveryDate, pdfURL, postType, id, userID, postTitle;
    public long timeStamp, likesCount, reactionsCount;
    public boolean unreadReactionCount;
    public ArrayList<String> photoURLs;
    public ArrayList<Tag_Items> tagItems;
    public LocationItem location;

    public PostLovesItem(ArrayList<String> photoURLs, String catagory, String catagoryName, String content,
                         long likesCount,
                         String deliveryDate, LocationItem location, String pdfURL,
                         long timeStamp, long reactionsCount, boolean unreadReactionCount, String postType,
                         String id, String userID, String postTitle,ArrayList<Tag_Items> tagItems) {
        this.photoURLs = photoURLs;
        this.catagory = catagory;
        this.catagoryName = catagoryName;
        this.content = content;
        this.likesCount = likesCount;
        this.deliveryDate = deliveryDate;
        this.location = location;
        this.pdfURL = pdfURL;
        this.timeStamp = timeStamp;
        this.reactionsCount = reactionsCount;
        this.unreadReactionCount = unreadReactionCount;
        this.postType = postType;
        this.id = id;
        this.userID = userID;
        this.postTitle = postTitle;
        this.tagItems=tagItems;
    }

    public void setCatagory(String catagory) {
        this.catagory = catagory;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setCatagoryName(String catagoryName) {
        this.catagoryName = catagoryName;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public void setLocation(LocationItem location) {
        this.location = location;
    }

    public void setpdfURL(String pdfURL) {
        this.pdfURL = pdfURL;
    }

    public void setPhotoURLs(ArrayList<String> photoURLs) {
        this.photoURLs = photoURLs;
    }

    public ArrayList<String> getPhotoURLs() {
        return photoURLs;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public void setReactionsCount(long reactionsCount) {
        this.reactionsCount = reactionsCount;
    }

    public void setTag_items(ArrayList<Tag_Items> tagItems) {
        this.tagItems = tagItems;
    }

    public void setUnreadReactionCount(boolean unreadReactionCount) {
        this.unreadReactionCount = unreadReactionCount;
    }



}
