package tnm.trendontap.modal;


import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ActivityItem {

    private String profilePic, id, longMessage, message, postID, postImageURL, reactionID, shortMessage,
            tiggerBy, activityType, userID;

    long timeStamp;

    private ArrayList<String> isReadUsers = null;

    public ActivityItem() {

    }

    public ActivityItem(String profilePic, String id, String longMessage,
                        String message, String postID, String postImageURL, String reactionID,
                        String shortMessage, String tiggerBy, long timeStamp, String activityType,
                        String userID, ArrayList<String> isReadUsers) {
        this.profilePic = profilePic;
        this.id = id;
        this.longMessage = longMessage;
        this.message = message;
        this.postID = postID;
        this.postImageURL = postImageURL;
        this.reactionID = reactionID;
        this.shortMessage = shortMessage;
        this.tiggerBy = tiggerBy;
        this.timeStamp = timeStamp;
        this.activityType = activityType;
        this.userID = userID;
        this.isReadUsers = isReadUsers;
    }


    public ArrayList<String> getIsReadUsers() {
        return isReadUsers;
    }

    public void setIsReadUsers(ArrayList<String> isReadUsers) {
        this.isReadUsers = isReadUsers;
    }


    public String getId() {
        return id;
    }

    public String getPostID() {
        return postID;
    }

    public String getUserID() {
        return userID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getActivityType() {
        return activityType;
    }

    public String getLongMessage() {
        return longMessage;
    }

    public String getPostImageURL() {
        return postImageURL;
    }


    public String getReactionID() {
        return reactionID;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public String getTiggerBy() {
        return tiggerBy;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public void setPostImageURL(String postImageURL) {
        this.postImageURL = postImageURL;
    }

    public void setReactionID(String reactionID) {
        this.reactionID = reactionID;
    }


    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public void setTiggerBy(String tiggerBy) {
        this.tiggerBy = tiggerBy;
    }
}


