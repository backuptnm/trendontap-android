package tnm.trendontap.modal;

/**
 * Created by TNM on 2/20/2018.
 */

public class GoogleDriveModel {
    String name, id;
    long size;

    public GoogleDriveModel(String name, String id, long size) {
        this.name = name;
        this.id = id;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public long getSize() {
        return size;
    }

}
