package tnm.trendontap.modal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CostSheet_Model {

    String buyerUserID;
    public ArrayList<HashMap<String,Object>> fabrics;

    double garmentCost;
    String id;
    double overhead;
    double packagingCost;
    String reactionID;
    String styleNumber;
    String supplierName;
    long timeStamp;
    double totalCost;
    double totalOverhead;
    String userID;

    public CostSheet_Model() {
    }

    public CostSheet_Model(String buyerUserID,  ArrayList<HashMap<String,Object>> fabrics, double garmentCost, String id, double overhead, double packagingCost, String reactionID, String styleNumber, String supplierName, long timeStamp, double totalCost, double totalOverhead, String userID) {
        this.buyerUserID = buyerUserID;
        this.fabrics = fabrics;
        this.garmentCost = garmentCost;
        this.id = id;
        this.overhead = overhead;
        this.packagingCost = packagingCost;
        this.reactionID = reactionID;
        this.styleNumber = styleNumber;
        this.supplierName = supplierName;
        this.timeStamp = timeStamp;
        this.totalCost = totalCost;
        this.totalOverhead = totalOverhead;
        this.userID = userID;
    }

    public String getBuyerUserID() {
        return buyerUserID;
    }

    public void setBuyerUserID(String buyerUserID) {
        this.buyerUserID = buyerUserID;
    }

    public ArrayList<HashMap<String,Object>> getFabrics() {
        return fabrics;
    }

    public void setFabrics( ArrayList<HashMap<String,Object>> fabrics) {
        this.fabrics = fabrics;
    }

    public double getGarmentCost() {
        return garmentCost;
    }

    public void setGarmentCost(double garmentCost) {
        this.garmentCost = garmentCost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getOverhead() {
        return overhead;
    }

    public void setOverhead(double overhead) {
        this.overhead = overhead;
    }

    public double getPackagingCost() {
        return packagingCost;
    }

    public void setPackagingCost(double packagingCost) {
        this.packagingCost = packagingCost;
    }

    public String getReactionID() {
        return reactionID;
    }

    public void setReactionID(String reactionID) {
        this.reactionID = reactionID;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public double getTotalOverhead() {
        return totalOverhead;
    }

    public void setTotalOverhead(double totalOverhead) {
        this.totalOverhead = totalOverhead;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
