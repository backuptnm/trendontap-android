package tnm.trendontap.modal;

import java.util.ArrayList;

public class PostReaction {
    public String content, id, postByUserID, postID, userID;
    public long commentCount, timeStamp;
    public boolean isDeleted, costsheetRequest, costsheetSent, recycleBin, sampleRequest, sampleSent, unreadCommentBuyer, unreadCommentSupplier;
    public ArrayList<String> photoURLs;


    public PostReaction(long commentCount, String content, boolean costsheetRequest,
                        boolean costsheetSent, String id, String postByUserID, String postID,
                        boolean recycleBin, boolean sampleRequest, boolean sampleSent, long timeStamp,
                        boolean unreadCommentBuyer, boolean unreadCommentSupplier, String userID,
                        ArrayList<String> photoURLs, boolean isDeleted) {
        this.commentCount = commentCount;
        this.content = content;
        this.costsheetRequest = costsheetRequest;
        this.costsheetSent = costsheetSent;
        this.id = id;
        this.postByUserID = postByUserID;
        this.postID = postID;
        this.recycleBin = recycleBin;
        this.sampleRequest = sampleRequest;
        this.sampleSent = sampleSent;
        this.timeStamp = timeStamp;
        this.unreadCommentBuyer = unreadCommentBuyer;
        this.unreadCommentSupplier = unreadCommentSupplier;
        this.userID = userID;
        this.photoURLs = photoURLs;
        this.isDeleted = isDeleted;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public String getContent() {
        return content;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getuserID() {
        return userID;
    }

    public String getId() {
        return id;
    }

    public String getpostID() {
        return postID;
    }

    public ArrayList<String> getphotoURLs() {
        return photoURLs;
    }

    public long getcommentCount() {
        return commentCount;
    }

    public String getpostByUserID() {
        return postByUserID;
    }
}

