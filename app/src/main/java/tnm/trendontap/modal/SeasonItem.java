package tnm.trendontap.modal;

public class SeasonItem {

    private String name, id, userID;

    private long timeStamp, dateCreated, dateUpdated;

    private boolean isActive, isDeleted;

    public SeasonItem() {

    }

    public SeasonItem(String id, String name, long timeStamp, String userID, long dateCreated, long dateUpdated, boolean isActive, boolean isDeleted) {
        this.id = id;
        this.name = name;
        this.timeStamp = timeStamp;
        this.userID = userID;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
    }

    public SeasonItem(String id, String name, long timeStamp, String userID) {
        this.id = id;
        this.name = name;
        this.timeStamp = timeStamp;
        this.userID = userID;
    }

    public void setDateUpdated(long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public long getDateUpdated() {
        return dateUpdated;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public String getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
