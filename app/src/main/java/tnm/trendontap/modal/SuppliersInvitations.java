package tnm.trendontap.modal;

/**
 * Created by admin on 24-Jan-18.
 */

public class SuppliersInvitations {
    private String profilePic;
    private String buyerName;
    private String companyName;
    private String id;
    private String invitedSupplierEmailID;
    private long timeStamp;
    private String userID;
    private String verificationCode;
    private boolean isInviationAccepted;

    public SuppliersInvitations() {
    }

   

    public String getProfilePic() {
        return profilePic;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getUserID() {
        return userID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getId() {
        return id;
    }

    public String getInvitedSupplierEmailID() {
        return invitedSupplierEmailID;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public boolean isIsInviationAccepted() {
        return isInviationAccepted;
    }
}


