package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TNM on 2/6/2018.
 */

public class Position implements Parcelable {
    public float height, width, xPos, yPos;

    public Position() {

    }

    public Position(float height, float width, float xPos, float yPos) {
        this.height = height;
        this.width = width;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.height);
        dest.writeFloat(this.width);
        dest.writeFloat(this.xPos);
        dest.writeFloat(this.yPos);
    }

    protected Position(Parcel in) {
        this.height = in.readFloat();
        this.width = in.readFloat();
        this.xPos = in.readFloat();
        this.yPos = in.readFloat();
    }

    public static final Parcelable.Creator<Position> CREATOR = new Parcelable.Creator<Position>() {
        @Override
        public Position createFromParcel(Parcel source) {
            return new Position(source);
        }

        @Override
        public Position[] newArray(int size) {
            return new Position[size];
        }
    };

    public float getheight() {
        return height;
    }

    public float getwidth() {
        return width;
    }

    public float getxPos() {
        return xPos;
    }

    public float getyPos() {
        return yPos;
    }
}
