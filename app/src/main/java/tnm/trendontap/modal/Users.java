package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TNM on 12/26/2017.
 */

public class Users implements Parcelable {

    private String userID, firstName, lastName, jobTitle, userType, emailID,
            profilePic, companyName, city, phone, deviceID, appVersion, password, deviceType,
            oneSingleUserID;

    private long badgeCount, dateCreated, dateUpdated;

    private boolean isActive, isDeleted, isAuthenticated, isNotificationsRequired;

    private tnm.trendontap.modal.UserReactionFeeds UserReactionFeeds;

    public Users() {
    }

    public Users(String appVersion, String userID, String companyName, String deviceID, String deviceType,
                 String emailID, String firstName, String lastName, String jobTitle, String password,
                 String userType, String oneSingleUserID, tnm.trendontap.modal.UserReactionFeeds UserReactionFeeds,
                 long badgeCount, String phone, long dateCreated, long dateUpdated,
                 boolean isActive, boolean isDeleted, boolean isAuthenticated, boolean isNotificationsRequired) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.userType = userType;
        this.emailID = emailID;
        this.deviceType = deviceType;
        this.companyName = companyName;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.password = password;
        this.oneSingleUserID = oneSingleUserID;
        this.UserReactionFeeds = UserReactionFeeds;
        this.badgeCount = badgeCount;
        this.phone = phone;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.isAuthenticated = isAuthenticated;
        this.isNotificationsRequired = isNotificationsRequired;

    }

    public long getDateCreated() {
        return dateCreated;
    }


    public String getUserID() {
        return userID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public boolean isIsAuthenticated() {
        return isAuthenticated;
    }

    public boolean isIsNotificationsRequired() {
        return isNotificationsRequired;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public long getBadgeCount() {
        return badgeCount;
    }

    public long getDateUpdated() {
        return dateUpdated;
    }


    public void setBadgeCount(long badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getOneSingleUserID() {
        return oneSingleUserID;
    }

    public void setOne_single_userId(String oneSingleUserID) {
        this.oneSingleUserID = oneSingleUserID;
    }

    public String getAppVersion() {
        return appVersion;
    }


    public String getDeviceID() {
        return deviceID;
    }


    public String getDeviceType() {
        return deviceType;
    }

    public String getPassword() {
        return password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getCity() {
        return city;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getUserType() {
        return userType;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAvatarUrl(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public tnm.trendontap.modal.UserReactionFeeds getUserReactionFeeds() {
        return UserReactionFeeds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userID);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.jobTitle);
        dest.writeString(this.userType);
        dest.writeString(this.emailID);
        dest.writeString(this.profilePic);
        dest.writeString(this.companyName);
        dest.writeString(this.city);
        dest.writeString(this.phone);
        dest.writeString(this.deviceID);
        dest.writeString(this.appVersion);
        dest.writeString(this.password);
        dest.writeString(this.deviceType);
        dest.writeString(this.oneSingleUserID);
        dest.writeLong(this.dateCreated);
        dest.writeLong(this.dateUpdated);
        dest.writeLong(badgeCount);
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isDeleted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isAuthenticated ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isNotificationsRequired ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.UserReactionFeeds, flags);


    }

    protected Users(Parcel in) {
        this.userID = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.jobTitle = in.readString();
        this.userType = in.readString();
        this.emailID = in.readString();
        this.profilePic = in.readString();
        this.companyName = in.readString();
        this.city = in.readString();
        this.phone = in.readString();
        this.deviceID = in.readString();
        this.appVersion = in.readString();
        this.password = in.readString();
        this.deviceType = in.readString();
        this.oneSingleUserID = in.readString();
        this.dateCreated = in.readLong();
        this.dateUpdated = in.readLong();
        this.badgeCount = in.readLong();
        this.isActive = in.readByte() != 0;
        this.isDeleted = in.readByte() != 0;
        this.isAuthenticated = in.readByte() != 0;
        this.isNotificationsRequired = in.readByte() != 0;
        this.UserReactionFeeds = in.readParcelable(tnm.trendontap.modal.UserReactionFeeds.class.getClassLoader());
    }

    public static final Parcelable.Creator<Users> CREATOR = new Parcelable.Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel source) {
            return new Users(source);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };
}


