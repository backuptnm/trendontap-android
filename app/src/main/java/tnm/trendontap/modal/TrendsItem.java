package tnm.trendontap.modal;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TrendsItem implements Parcelable {
    private String postTitle, categoryName, content, interesttrend, deliveryDate,
            reactionsCount, id, liker1, liker2, pdfUrl, userid;
    private boolean unreadReactionCount;
    private long timeStamp, likecnt, comment_count;

    private LocationItem locationItemArrayList;


    ArrayList<String> photoUrls;


    public TrendsItem(String postTitle, String categoryName, String content, String interesttrend,
                      String deliveryDate, long timeStamp, String reactionsCount,
                      boolean unreadReactionCount, ArrayList<String> photoUrls, String id, String liker1,
                      String liker2, long likecnt, LocationItem locationItemArrayList, String pdfUrl, String userid,
                      long comment_count) {
        this.postTitle = postTitle;
        this.categoryName = categoryName;
        this.content = content;
        this.interesttrend = interesttrend;
        this.deliveryDate = deliveryDate;
        this.timeStamp = timeStamp;
        this.likecnt = likecnt;
        this.reactionsCount = reactionsCount;
        this.unreadReactionCount = unreadReactionCount;
        this.photoUrls = photoUrls;
        this.id = id;
        this.liker1 = liker1;
        this.liker2 = liker2;
        this.locationItemArrayList = locationItemArrayList;
        this.pdfUrl = pdfUrl;
        this.userid = userid;
        this.comment_count = comment_count;

    }


    public TrendsItem(String postTitle, String categoryName, String content, String interesttrend,
                      String deliveryDate, long timeStamp, String reactionsCount,
                      boolean unreadReactionCount, ArrayList<String> photoUrls, String id, String liker1,
                      String liker2, long likecnt, LocationItem locationItemArrayList, String pdfUrl,
                      String userid) {
        this.postTitle = postTitle;
        this.categoryName = categoryName;
        this.content = content;
        this.interesttrend = interesttrend;
        this.deliveryDate = deliveryDate;
        this.timeStamp = timeStamp;
        this.likecnt = likecnt;
        this.reactionsCount = reactionsCount;
        this.unreadReactionCount = unreadReactionCount;
        this.id = id;
        this.liker1 = liker1;
        this.liker2 = liker2;
        this.locationItemArrayList = locationItemArrayList;
        this.pdfUrl = pdfUrl;
        this.userid = userid;
        this.photoUrls = photoUrls;
    }


    public ArrayList<String> getPhotoUrlsList() {
        return photoUrls;
    }

    public long getComment_count() {
        return comment_count;
    }

    public String getUserid() {
        return userid;
    }

    public LocationItem getLocationItemArrayList() {
        return locationItemArrayList;
    }

    public long getLikecnt() {
        return likecnt;
    }

    public void setLikecnt(long likecnt) {
        this.likecnt = likecnt;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public String getContent() {
        return content;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getInteresttrend() {
        return interesttrend;
    }

    public String getReactionsCount() {
        return reactionsCount;
    }

    public boolean isUnreadReactionCount() {
        return unreadReactionCount;
    }

    public String getId() {
        return id;
    }

    public void setInteresttrend(String interesttrend) {
        this.interesttrend = interesttrend;
    }

    public String getLiker2() {
        return liker2;
    }

    public String getLiker1() {
        return liker1;
    }

    public void setLiker2(String liker2) {
        this.liker2 = liker2;
    }

    public void setLiker1(String liker1) {
        this.liker1 = liker1;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.postTitle);
        dest.writeString(this.categoryName);
        dest.writeString(this.content);
        dest.writeString(this.interesttrend);
        dest.writeString(this.deliveryDate);
        dest.writeString(this.reactionsCount);
        dest.writeStringList(this.photoUrls);
        dest.writeString(this.id);
        dest.writeString(this.liker1);
        dest.writeString(this.liker2);
        dest.writeByte(this.unreadReactionCount ? (byte) 1 : (byte) 0);
        dest.writeLong(this.timeStamp);
        dest.writeLong(this.likecnt);
        dest.writeParcelable(this.locationItemArrayList, flags);
        dest.writeString(this.pdfUrl);
        dest.writeString(this.userid);
    }

    protected TrendsItem(Parcel in) {
        this.postTitle = in.readString();
        this.categoryName = in.readString();
        this.content = in.readString();
        this.interesttrend = in.readString();
        this.deliveryDate = in.readString();
        this.reactionsCount = in.readString();
        this.photoUrls = in.createStringArrayList();
        this.id = in.readString();
        this.liker1 = in.readString();
        this.liker2 = in.readString();
        this.unreadReactionCount = in.readByte() != 0;
        this.timeStamp = in.readLong();
        this.likecnt = in.readLong();
        this.locationItemArrayList = in.readParcelable(LocationItem.class.getClassLoader());
        this.pdfUrl = in.readString();
        this.userid = in.readString();
    }

    public static final Creator<TrendsItem> CREATOR = new Creator<TrendsItem>() {
        @Override
        public TrendsItem createFromParcel(Parcel source) {
            return new TrendsItem(source);
        }

        @Override
        public TrendsItem[] newArray(int size) {
            return new TrendsItem[size];
        }
    };
}
