package tnm.trendontap.modal;

import java.util.ArrayList;

/**
 * Created by admin on 20-Dec-17.
 */

public class MyReaction {
    private String post_id, name, type, abouttrend, avtarurl, imgurl, reactionid, buyer_user_id, costsheet_id = "";
    private long timeStamp, commentcount;
    private boolean unread_comment, sample_request, sample_sent, cost_request, cost_sent;
    ArrayList<String> photoUrlsList;

    public MyReaction() {

    }

    public MyReaction(String name, String type, String abouttrend, long timeStamp, String avtarurl,
                      String imgurl, long commentcount, boolean unread_comment, String reactionid,
                      String buyer_user_id, boolean sample_request, boolean sample_sent, boolean cost_request,
                      boolean cost_sent, String costsheet_id, String post_id, ArrayList<String> photoUrlsList) {
        this.name = name;
        this.type = type;
        this.abouttrend = abouttrend;
        this.timeStamp = timeStamp;
        this.avtarurl = avtarurl;
        this.imgurl = imgurl;
        this.commentcount = commentcount;
        this.unread_comment = unread_comment;
        this.reactionid = reactionid;
        this.buyer_user_id = buyer_user_id;
        this.sample_request = sample_request;
        this.sample_sent = sample_sent;
        this.cost_request = cost_request;
        this.cost_sent = cost_sent;
        this.costsheet_id = costsheet_id;
        this.post_id = post_id;
        this.photoUrlsList = photoUrlsList;
    }

    public String getCostsheet_id() {
        try {
            return costsheet_id;
        } catch (Exception e) {
            return "";
        }
    }

    public ArrayList<String> getPhotoUrls() {
        return photoUrlsList;
    }

    public void setPhotoUrls(ArrayList<String> photoUrls) {
        this.photoUrlsList = photoUrls;
    }
    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setCostsheet_id(String costsheet_id) {
        this.costsheet_id = costsheet_id;
    }

    public String getBuyer_user_id() {
        return buyer_user_id;
    }

    public void setBuyer_user_id(String buyer_user_id) {
        this.buyer_user_id = buyer_user_id;
    }

    public boolean isSample_request() {
        return sample_request;
    }

    public void setSample_request(boolean sample_request) {
        this.sample_request = sample_request;
    }

    public boolean isSample_sent() {
        return sample_sent;
    }

    public void setSample_sent(boolean sample_sent) {
        this.sample_sent = sample_sent;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAbouttrend(String abouttrend) {
        this.abouttrend = abouttrend;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAbouttrend() {
        return abouttrend;
    }

    public String getAvtarurl() {
        return avtarurl;
    }

    public void setAvtarurl(String avtarurl) {
        this.avtarurl = avtarurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getImgurl() {
        return imgurl;
    }

    public long getCommentcount() {
        return commentcount;
    }

    public void setCommentcount(long commentcount) {
        this.commentcount = commentcount;
    }

    public boolean isUnread_comment() {
        return unread_comment;
    }

    public void setUnread_comment(boolean unread_comment) {
        this.unread_comment = unread_comment;
    }

    public String getReactionid() {
        return reactionid;
    }

    public void setReactionid(String reactionid) {
        this.reactionid = reactionid;
    }


    public boolean isCost_request() {
        return cost_request;
    }

    public boolean isCost_sent() {
        return cost_sent;
    }

    public void setCost_request(boolean cost_request) {
        this.cost_request = cost_request;
    }

    public void setCost_sent(boolean cost_sent) {
        this.cost_sent = cost_sent;
    }
}
