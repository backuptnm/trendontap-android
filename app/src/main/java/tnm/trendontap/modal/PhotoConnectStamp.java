package tnm.trendontap.modal;

import java.util.ArrayList;

/**
 * Created by TNM on 3/21/2018.
 */

public class PhotoConnectStamp {
    String name, from, postid;
    ArrayList<String> photoUrlsList;
    String title, category, postimg;

    public PhotoConnectStamp(String name, String from, String postid, ArrayList<String> photoUrlsList) {
        this.name = name;
        this.from = from;
        this.postid = postid;
        this.photoUrlsList = photoUrlsList;
    }

    public PhotoConnectStamp(String title, String category, String postimg, String postid, String from, ArrayList<String> photoUrlsList) {
        this.title = title;
        this.category = category;
        this.postimg = postimg;
        this.from = from;
        this.postid = postid;
        this.photoUrlsList = photoUrlsList;
    }


    public String getName() {
        return name;
    }

    public String getFrom() {
        return from;
    }

    public String getPostid() {
        return postid;
    }

    public ArrayList<String> getPhotoUrlsList() {
        return photoUrlsList;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getPostimg() {
        return postimg;
    }
}
