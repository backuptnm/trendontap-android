package tnm.trendontap.modal;


public class BuyerTrendLoveItem {
    private String imgurl, name, category, abouttrend, deliverydate, postid, userid;
    private long daysago;
    private long reactionsCount;

    public BuyerTrendLoveItem(String imgurl, String name, String category, String abouttrend, String deliverydate,
                              long daysago, long reactionsCount, String postid, String userid) {
        this.imgurl = imgurl;
        this.name = name;
        this.category = category;
        this.abouttrend = abouttrend;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.postid = postid;
        this.reactionsCount = reactionsCount;
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public long getReactionsCount() {
        return reactionsCount;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getName() {
        return name;
    }

    public String getAbouttrend() {
        return abouttrend;
    }

    public String getCategory() {
        return category;
    }

    public long getDaysago() {
        return daysago;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public String getPostid() {
        return postid;
    }
}
