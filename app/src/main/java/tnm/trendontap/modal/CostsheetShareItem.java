package tnm.trendontap.modal;

/**
 * Created by TNM on 4/9/2018.
 */

public class CostsheetShareItem {
    String item1, item2, item3;

    public CostsheetShareItem(String item1, String item2, String item3) {
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
    }

    public String getItem1() {
        return item1;
    }

    public String getItem2() {
        return item2;
    }

    public String getItem3() {
        return item3;
    }
}
