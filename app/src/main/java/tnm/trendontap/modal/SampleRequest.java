package tnm.trendontap.modal;

public class SampleRequest {

    private String trackingno;
    private long timestamp;

    public SampleRequest(long timestamp, String trackingno) {
        this.timestamp = timestamp;
        this.trackingno = trackingno;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTrackingno() {
        return trackingno;
    }
}
