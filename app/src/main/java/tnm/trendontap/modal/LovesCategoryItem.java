package tnm.trendontap.modal;

/**
 * Created by TNM on 1/5/2018.
 */

public class LovesCategoryItem {

    private String id, name, userID;
    private long timeStamp;

    public LovesCategoryItem() {

    }

    public LovesCategoryItem(String id, String name, long timeStamp, String userID) {
        this.id = id;
        this.name = name;
        this.timeStamp = timeStamp;
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
