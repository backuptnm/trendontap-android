package tnm.trendontap.modal;

/**
 * Created by TNM on 12/13/2017.
 */

public class CommentsItem {
    private String id, message, postID, userID, name, photourl;
    private long timeStamp;


    public CommentsItem(String id, String message, String postID, long timeStamp, String userID, String name, String photourl) {
        this.id = id;
        this.message = message;
        this.postID = postID;
        this.timeStamp = timeStamp;
        this.userID = userID;
        this.name = name;
        this.photourl = photourl;
    }

    public CommentsItem(String id, String message, String postID, long timeStamp, String userID) {
        this.id = id;
        this.message = message;
        this.postID = postID;
        this.timeStamp = timeStamp;
        this.userID = userID;
    }

    public String getPostID() {
        return postID;
    }

    public String getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return message;
    }


    public String getName() {
        return name;
    }

    public String getPhotourl() {
        return photourl;
    }


}
