package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TNM on 1/30/2018.
 */

public class LocationItem implements Parcelable {

    public String address, title;
    public double latitude, logitude;

    public LocationItem(String title, String address, double latitude, double logitude) {
        this.address = address;
        this.title = title;
        this.latitude = latitude;
        this.logitude = logitude;
    }

    public LocationItem() {

    }

    public double getLatitude() {
        return latitude;
    }

    public double getLogitude() {
        return logitude;
    }

    public String getAddress() {
        return address;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeString(this.title);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.logitude);
    }

    protected LocationItem(Parcel in) {
        this.address = in.readString();
        this.title = in.readString();
        this.latitude = in.readDouble();
        this.logitude = in.readDouble();
    }

    public static final Parcelable.Creator<LocationItem> CREATOR = new Parcelable.Creator<LocationItem>() {
        @Override
        public LocationItem createFromParcel(Parcel source) {
            return new LocationItem(source);
        }

        @Override
        public LocationItem[] newArray(int size) {
            return new LocationItem[size];
        }
    };
}
