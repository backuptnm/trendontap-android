package tnm.trendontap.modal;


import android.os.Parcel;
import android.os.Parcelable;

public class BuyerTrendsItem implements Parcelable {
    private String imgurl, name, catagory, abouttrend, type, deliverydate, daysago, username, postid, userimg,
            userpostid, pdfUrl, usercompany;
    private boolean hasreaction;
    long likesCount;

    LocationItem locationItemArrayList;

    public BuyerTrendsItem(String imgurl, String name, String catagory, String abouttrend, String deliverydate,
                           String daysago, String username, String type, boolean hasreaction, String postid,
                           String userimg, String userpostid, long likesCount, String pdfUrl, String usercompany,
                           LocationItem locationItemArrayList) {
        this.imgurl = imgurl;
        this.name = name;
        this.catagory = catagory;
        this.abouttrend = abouttrend;
        this.type = type;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.username = username;
        this.hasreaction = hasreaction;
        this.postid = postid;
        this.userimg = userimg;
        this.userpostid = userpostid;
        this.likesCount = likesCount;
        this.pdfUrl = pdfUrl;
        this.usercompany = usercompany;
        this.locationItemArrayList = locationItemArrayList;
    }

    public LocationItem getLocationItemArrayList() {
        return locationItemArrayList;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public String getUserimg() {
        return userimg;
    }

    public void setUserimg(String userimg) {
        this.userimg = userimg;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getName() {
        return name;
    }

    public String getAbouttrend() {
        return abouttrend;
    }

    public String getCategory() {
        return catagory;
    }

    public String getDaysago() {
        return daysago;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public String getType() {
        return type;
    }

    public String getUsername() {
        return username;
    }

    public boolean hasReaction() {
        return hasreaction;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getPostid() {
        return postid;
    }

    public String getUserpostid() {
        return userpostid;
    }

    public void setUserpostid(String userpostid) {
        this.userpostid = userpostid;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getUsercompany() {
        return usercompany;
    }

    public void setUsercompany(String usercompany) {
        this.usercompany = usercompany;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imgurl);
        dest.writeString(this.name);
        dest.writeString(this.catagory);
        dest.writeString(this.abouttrend);
        dest.writeString(this.type);
        dest.writeString(this.usercompany);
        dest.writeString(this.deliverydate);
        dest.writeString(this.daysago);
        dest.writeString(this.username);
        dest.writeString(this.postid);
        dest.writeString(this.userimg);
        dest.writeString(this.userpostid);
        dest.writeByte(this.hasreaction ? (byte) 1 : (byte) 0);
    }

    protected BuyerTrendsItem(Parcel in) {
        this.imgurl = in.readString();
        this.name = in.readString();
        this.catagory = in.readString();
        this.abouttrend = in.readString();
        this.type = in.readString();
        this.deliverydate = in.readString();
        this.daysago = in.readString();
        this.username = in.readString();
        this.usercompany = in.readString();
        this.postid = in.readString();
        this.userimg = in.readString();
        this.userpostid = in.readString();
        this.hasreaction = in.readByte() != 0;
    }

    public static final Parcelable.Creator<BuyerTrendsItem> CREATOR = new Parcelable.Creator<BuyerTrendsItem>() {
        @Override
        public BuyerTrendsItem createFromParcel(Parcel source) {
            return new BuyerTrendsItem(source);
        }

        @Override
        public BuyerTrendsItem[] newArray(int size) {
            return new BuyerTrendsItem[size];
        }
    };
}
