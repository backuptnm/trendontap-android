package tnm.trendontap.modal;

/**
 * Created by TNM on 3/21/2018.
 */

public class PhotoTimeStamp {
    String image, postid;
    long timestamp,reactionsCount;


    public PhotoTimeStamp(String image, long timestamp, String postid, long reactionsCount) {
        this.image = image;
        this.timestamp = timestamp;
        this.reactionsCount = reactionsCount;
        this.postid = postid;
    }

    public String getImage() {
        return image;
    }

    public String getPostid() {
        return postid;
    }

    public long getReactionsCount() {
        return reactionsCount;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
