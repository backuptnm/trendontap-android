package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.ArrayList;

public class ReactionTrendItem implements Parcelable {

    private String username, userrole, usercompany, userphoto, content, id, userID, postID, costsheetID,
            postByUserID, sampleID;
    private int commentCount;
    private long timeStamp;
    private boolean costsheetRequest, sampleRequest, recycleBin, unreadCommentBuyer,
            costsheetSent, unreadCommentSupplier, sampleSent, isDeleted;

    private ArrayList<String> photoURLs;

    public ReactionTrendItem() {

    }

   /* public ReactionTrendItem(String content, String id, ArrayList<String> photoURLs,
                             String userID, int commentCount, long timeStamp,
                             boolean costsheetRequest, boolean sampleRequest,
                             boolean recycleBin, boolean unreadCommentBuyer,
                             String postID, String username, String userrole, String userphoto,
                             String costsheetID, boolean costsheetSent, boolean unreadCommentSupplier,
                             String postByUserID, String usercompany, boolean sampleSent, String sampleID) {
        this.commentCount = commentCount;
        this.content = content;
        this.photoURLs = photoURLs;
        this.userID = userID;
        this.timeStamp = timeStamp;
        this.costsheetRequest = costsheetRequest;
        this.sampleRequest = sampleRequest;
        this.recycleBin = recycleBin;
        this.unreadCommentBuyer = unreadCommentBuyer;
        this.id = id;
        this.postID = postID;
        this.username = username;
        this.userrole = userrole;
        this.userphoto = userphoto;
        this.costsheetID = costsheetID;
        this.costsheetSent = costsheetSent;
        this.unreadCommentSupplier = unreadCommentSupplier;
        this.postByUserID = postByUserID;
        this.usercompany = usercompany;
        this.sampleSent = sampleSent;
        this.sampleID = sampleID;
    }*/


    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getSampleID() {
        return sampleID;
    }

    public boolean isSampleSent() {
        return sampleSent;
    }

    public String getPostByUserID() {
        return postByUserID;
    }


    public String getUsercompany() {
        return usercompany;
    }

    public void setUsercompany(String usercompany) {
        this.usercompany = usercompany;
    }

    public boolean isCostsheetSent() {
        return costsheetSent;
    }

    @Nullable
    public String getCostsheetID() {
        return costsheetID;
    }

    public String getPostID() {
        return postID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getId() {
        return id;
    }

    public ArrayList<String> getPhotoUrls() {
        return photoURLs;
    }

    public String getContent() {
        return content;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public String getUserID() {
        return userID;
    }


    public boolean isCostsheetRequest() {
        return costsheetRequest;
    }

    public boolean isRecycleBin() {
        return recycleBin;
    }

    public boolean isSampleRequest() {
        return sampleRequest;
    }

    public boolean isUnreadCommentBuyer() {
        return unreadCommentBuyer;
    }

    public boolean isUnreadCommentSupplier() {
        return unreadCommentSupplier;
    }

    public void setPhotoUrls(ArrayList<String> photoURLs) {
        this.photoURLs = photoURLs;
    }

    public String getUserphoto() {
        return userphoto;
    }

    public String getUserrole() {
        return userrole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }

    public void setSampleRequest(boolean sampleRequest) {
        this.sampleRequest = sampleRequest;
    }

    public void setCostsheetRequest(boolean costsheetRequest) {
        this.costsheetRequest = costsheetRequest;
    }

    public void setRecycleBin(boolean recycleBin) {
        this.recycleBin = recycleBin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.userrole);
        dest.writeString(this.userphoto);
        dest.writeString(this.content);
        dest.writeString(this.id);
        dest.writeString(this.userID);
        dest.writeString(this.postByUserID);
        dest.writeString(this.postID);
        dest.writeString(this.costsheetID);
        dest.writeInt(this.commentCount);
        dest.writeLong(this.timeStamp);
        dest.writeByte(this.costsheetRequest ? (byte) 1 : (byte) 0);
        dest.writeByte(this.sampleRequest ? (byte) 1 : (byte) 0);
        dest.writeByte(this.recycleBin ? (byte) 1 : (byte) 0);
        dest.writeByte(this.unreadCommentBuyer ? (byte) 1 : (byte) 0);
        dest.writeByte(this.costsheetSent ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.photoURLs);
        dest.writeByte(this.unreadCommentSupplier ? (byte) 1 : (byte) 0);
    }

    protected ReactionTrendItem(Parcel in) {
        this.username = in.readString();
        this.userrole = in.readString();
        this.userphoto = in.readString();
        this.content = in.readString();
        this.id = in.readString();
        this.userID = in.readString();
        this.postByUserID = in.readString();
        this.postID = in.readString();
        this.costsheetID = in.readString();
        this.commentCount = in.readInt();
        this.timeStamp = in.readLong();
        this.costsheetRequest = in.readByte() != 0;
        this.sampleRequest = in.readByte() != 0;
        this.recycleBin = in.readByte() != 0;
        this.unreadCommentBuyer = in.readByte() != 0;
        this.costsheetSent = in.readByte() != 0;
        this.photoURLs = in.createStringArrayList();
        this.unreadCommentSupplier = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ReactionTrendItem> CREATOR = new Parcelable.Creator<ReactionTrendItem>() {
        @Override
        public ReactionTrendItem createFromParcel(Parcel source) {
            return new ReactionTrendItem(source);
        }

        @Override
        public ReactionTrendItem[] newArray(int size) {
            return new ReactionTrendItem[size];
        }
    };
}
