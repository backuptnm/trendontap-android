package tnm.trendontap.modal;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class BuyerLovesItem implements Parcelable {
    private String imgurl, name, category, abouttrend, type, deliverydate, username, postid, userimg, userpostid,
            usercompany;
    private boolean hasreaction;
    private long daysago, likesCount;
    private ArrayList<Tag_Items> tag_itemsArrayList;
    ArrayList<String> photoUrlsList;
    LocationItem locationItemArrayList;

    public BuyerLovesItem(String imgurl, String name, String category, String abouttrend, String deliverydate,
                          long daysago, String username, String type, boolean hasreaction, String postid,
                          String userimg, String userpostid, ArrayList<Tag_Items> tag_itemsArrayList) {
        this.imgurl = imgurl;
        this.name = name;
        this.category = category;
        this.abouttrend = abouttrend;
        this.type = type;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.username = username;
        this.hasreaction = hasreaction;
        this.postid = postid;
        this.userimg = userimg;
        this.userpostid = userpostid;
        this.tag_itemsArrayList = tag_itemsArrayList;
    }

    public BuyerLovesItem(ArrayList<String> photoUrls, String name, String category, String abouttrend,
                          String deliverydate,
                          long daysago, String username, String type, boolean hasreaction, String postid,
                          String userimg, String userpostid, ArrayList<Tag_Items> tag_itemsArrayList,
                          long likesCount, String usercompany,LocationItem locationItemArrayList) {
        this.photoUrlsList = photoUrls;
        this.name = name;
        this.category = category;
        this.abouttrend = abouttrend;
        this.type = type;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.username = username;
        this.hasreaction = hasreaction;
        this.postid = postid;
        this.userimg = userimg;
        this.userpostid = userpostid;
        this.tag_itemsArrayList = tag_itemsArrayList;
        this.likesCount = likesCount;
        this.usercompany = usercompany;
        this.locationItemArrayList=locationItemArrayList;
    }

    public LocationItem getLocationItemArrayList() {
        return locationItemArrayList;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public ArrayList<Tag_Items> getTag_itemsArrayList() {
        return tag_itemsArrayList;
    }

    public void setUserpostid(String userpostid) {
        this.userpostid = userpostid;
    }


    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setAbouttrend(String abouttrend) {
        this.abouttrend = abouttrend;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUsercompany(String usercompany) {
        this.usercompany = usercompany;
    }

    public String getUsercompany() {
        return usercompany;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setHasreaction(boolean hasreaction) {
        this.hasreaction = hasreaction;
    }

    public void setDaysago(long daysago) {
        this.daysago = daysago;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getName() {
        return name;
    }

    public String getAbouttrend() {
        return abouttrend;
    }

    public String getCategory() {
        return category;
    }

    public long getDaysago() {
        return daysago;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public String getType() {
        return type;
    }

    public String getUsername() {
        return username;
    }

    public boolean hasReaction() {
        return hasreaction;
    }

    public String getPostid() {
        return postid;
    }

    public String getUserimg() {
        return userimg;
    }

    public void setUserimg(String userimg) {
        this.userimg = userimg;
    }

    public String getUserpostid() {
        return userpostid;
    }

    public ArrayList<String> getPhotoUrlsList() {
        return photoUrlsList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imgurl);
        dest.writeString(this.name);
        dest.writeString(this.category);
        dest.writeString(this.abouttrend);
        dest.writeString(this.type);
        dest.writeString(this.deliverydate);
        dest.writeString(this.username);
        dest.writeString(this.postid);
        dest.writeString(this.userimg);
        dest.writeString(this.userpostid);
        dest.writeString(this.usercompany);
        dest.writeByte(this.hasreaction ? (byte) 1 : (byte) 0);
        dest.writeLong(this.daysago);
        dest.writeTypedList(this.tag_itemsArrayList);
        dest.writeStringList(this.photoUrlsList);
    }

    public BuyerLovesItem(Parcel in) {
        this.imgurl = in.readString();
        this.name = in.readString();
        this.category = in.readString();
        this.abouttrend = in.readString();
        this.type = in.readString();
        this.deliverydate = in.readString();
        this.username = in.readString();
        this.usercompany = in.readString();
        this.postid = in.readString();
        this.userimg = in.readString();
        this.userpostid = in.readString();
        this.hasreaction = in.readByte() != 0;
        this.daysago = in.readLong();
        this.tag_itemsArrayList = in.createTypedArrayList(Tag_Items.CREATOR);
        this.photoUrlsList = in.createStringArrayList();
    }

    public static final Creator<BuyerLovesItem> CREATOR = new Creator<BuyerLovesItem>() {
        @Override
        public BuyerLovesItem createFromParcel(Parcel source) {
            return new BuyerLovesItem(source);
        }

        @Override
        public BuyerLovesItem[] newArray(int size) {
            return new BuyerLovesItem[size];
        }
    };
}
