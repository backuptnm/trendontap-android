package tnm.trendontap.modal;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class LovesItem implements Parcelable {
    private String name, category, abouttrend, deliverydate, reactioncnt, liker1, liker2, id, userid;
    private long daysago, likecnt, comment_count, buyer_likes_count;
    private boolean notify, tagged;
    private ArrayList<Tag_Items> tag_itemsArrayList;
    private LocationItem locationItemArrayList;


    ArrayList<String> photoUrlsList;


    public LovesItem(ArrayList<String> photoUrlsList, String name, String category, String abouttrend, long likecnt,
                     String deliverydate,
                     long daysago, String reactioncnt, boolean notify, boolean tagged,
                     String liker1, String liker2,
                     String id, ArrayList<Tag_Items> tag_itemsArrayList,
                     LocationItem locationItemArrayList, long comment_count, String userid) {
        this.photoUrlsList = photoUrlsList;
        this.name = name;
        this.category = category;
        this.abouttrend = abouttrend;
        this.likecnt = likecnt;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.reactioncnt = reactioncnt;
        this.notify = notify;
        this.tagged = tagged;
        this.liker1 = liker1;
        this.liker2 = liker2;
        this.id = id;
        this.tag_itemsArrayList = tag_itemsArrayList;
        this.locationItemArrayList = locationItemArrayList;
        this.comment_count = comment_count;
        this.userid = userid;
    }

    public LovesItem(ArrayList<String> photoUrlsList, String name, String category, String abouttrend, long likecnt,
                     String deliverydate,
                     long daysago, String reactioncnt, boolean notify, boolean tagged,
                     String liker1, String liker2,
                     String id, ArrayList<Tag_Items> tag_itemsArrayList, LocationItem locationItemArrayList, String userid) {
        this.photoUrlsList = photoUrlsList;
        this.name = name;
        this.category = category;
        this.abouttrend = abouttrend;
        this.likecnt = likecnt;
        this.deliverydate = deliverydate;
        this.daysago = daysago;
        this.reactioncnt = reactioncnt;
        this.notify = notify;
        this.tagged = tagged;
        this.liker1 = liker1;
        this.liker2 = liker2;
        this.id = id;
        this.tag_itemsArrayList = tag_itemsArrayList;
        this.locationItemArrayList = locationItemArrayList;
        this.userid = userid;
    }


    public String getUserid() {
        return userid;
    }

    public ArrayList<String> getPhotoUrlsList() {
        return photoUrlsList;
    }

    public ArrayList<Tag_Items> getTag_itemsArrayList() {
        return tag_itemsArrayList;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAbouttrend() {
        return abouttrend;
    }

    public String getCategory() {
        return category;
    }

    public long getDaysago() {
        return daysago;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public long getLikecnt() {
        return likecnt;
    }

    public void setLikecnt(long likecnt) {
        this.likecnt = likecnt;
    }

    public String getReactioncnt() {
        return reactioncnt;
    }

    public boolean isNotify() {
        return notify;
    }

    public boolean isTagged() {
        return tagged;
    }

    public String getLiker1() {
        return liker1;
    }

    public String getLiker2() {
        return liker2;
    }

    public void setLiker1(String liker1) {
        this.liker1 = liker1;
    }

    public void setLiker2(String liker2) {
        this.liker2 = liker2;
    }

    public ArrayList<String> getPhotoUrls() {
        return photoUrlsList;
    }

    public void setPhotoUrls(ArrayList<String> photoUrls) {
        this.photoUrlsList = photoUrls;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public LocationItem getLocationItemArrayList() {
        return locationItemArrayList;
    }

    public void setLocationItemArrayList(LocationItem locationItemArrayList) {
        this.locationItemArrayList = locationItemArrayList;
    }

    public long getComment_count() {
        return comment_count;
    }

    public long getBuyer_likes_count() {
        return buyer_likes_count;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.category);
        dest.writeString(this.abouttrend);
        dest.writeString(this.deliverydate);
        dest.writeString(this.reactioncnt);
        dest.writeString(this.liker1);
        dest.writeString(this.liker2);
        dest.writeString(this.id);
        dest.writeLong(this.daysago);
        dest.writeLong(this.likecnt);
        dest.writeByte(this.notify ? (byte) 1 : (byte) 0);
        dest.writeByte(this.tagged ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.photoUrlsList);
    }

    protected LovesItem(Parcel in) {
        this.name = in.readString();
        this.category = in.readString();
        this.abouttrend = in.readString();
        this.deliverydate = in.readString();
        this.reactioncnt = in.readString();
        this.liker1 = in.readString();
        this.liker2 = in.readString();
        this.id = in.readString();
        this.daysago = in.readLong();
        this.likecnt = in.readLong();
        this.notify = in.readByte() != 0;
        this.tagged = in.readByte() != 0;
        this.photoUrlsList = in.createStringArrayList();
    }

    public static final Creator<LovesItem> CREATOR = new Creator<LovesItem>() {
        @Override
        public LovesItem createFromParcel(Parcel source) {
            return new LovesItem(source);
        }

        @Override
        public LovesItem[] newArray(int size) {
            return new LovesItem[size];
        }
    };
}
