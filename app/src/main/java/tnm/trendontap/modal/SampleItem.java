package tnm.trendontap.modal;

/**
 * Created by admin on 09-Feb-18.
 */

public class SampleItem {

    private String buyerUserID, id, reactionID, styleNumber, supplierName, userID;
    private long timeStamp;


    public SampleItem(String buyerUserID, String id, String reactionID, String styleNumber, String supplierName, String userID, long timeStamp) {
        this.buyerUserID = buyerUserID;
        this.id = id;
        this.reactionID = reactionID;
        this.styleNumber = styleNumber;
        this.supplierName = supplierName;
        this.userID = userID;
        this.timeStamp = timeStamp;
    }


    public String getBuyerUserID() {
        return buyerUserID;
    }

    public void setBuyerUserID(String buyerUserID) {
        this.buyerUserID = buyerUserID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReactionID() {
        return reactionID;
    }

    public void setReactionID(String reactionID) {
        this.reactionID = reactionID;
    }

    public String getStyleNumber() {
        return styleNumber;
    }

    public void setStyleNumber(String styleNumber) {
        this.styleNumber = styleNumber;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
