package tnm.trendontap.modal;

import android.os.Parcel;
import android.os.Parcelable;

public class Tag_Items implements Parcelable {
    public Position position;
    public String name;


    public Tag_Items() {

    }

    public Tag_Items(String name, Position position) {
        this.name = name;
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.position, flags);
        dest.writeString(this.name);
    }

    protected Tag_Items(Parcel in) {
        this.position = in.readParcelable(Position.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Tag_Items> CREATOR = new Parcelable.Creator<Tag_Items>() {
        @Override
        public Tag_Items createFromParcel(Parcel source) {
            return new Tag_Items(source);
        }

        @Override
        public Tag_Items[] newArray(int size) {
            return new Tag_Items[size];
        }
    };

    public String getname() {
        return name;
    }

    public Position getPosition() {
        return position;
    }
}
