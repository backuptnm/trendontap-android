package tnm.trendontap.utility;

import android.content.Context;
import android.graphics.Typeface;


public class RobotoFont {

    public static Typeface blackFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Black.ttf");
    }

    public static Typeface blackItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_BlackItalic.ttf");
    }

    public static Typeface boldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Bold.ttf");
    }

    public static Typeface boldItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_BoldItalic.ttf");
    }

    public static Typeface italicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Italic.ttf");
    }

    public static Typeface lightFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Light.ttf");
    }

    public static Typeface lightItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_LightItalic.ttf");
    }

    public static Typeface mediumFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Medium.ttf");
    }

    public static Typeface mediumItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_MediumItalic.ttf");
    }


    public static Typeface regularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Regular.ttf");
    }

    public static Typeface thinFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_Thin.ttf");
    }

    public static Typeface thinItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto/Roboto_ThinItalic.ttf");
    }


}
