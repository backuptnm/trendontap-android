package tnm.trendontap.utility;


import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;

import static tnm.trendontap.utility.FireStoreUtils.BadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.InvitedSuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.OneSingleUserID;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;


public class EventClass {
    public static String LIKE_LOVE = "like_love";
    public static String LIKE_TREND = "like_trend";
    public static String LOVE = "love";
    public static String TREND = "trend";
    public static String REACTION = "reaction";
    public static String SAMPLE = "sample";
    public static String COMMENT = "comment";
    public static String PostComment = "PostComment";
    public static String COSTINSHEET = "costing sheet";
    public static String POSTMSG = " has posted a new ";
    public static String REACTMSG = " has reacted to your ";
    public static String REQUESTMSG = " has requested a ";
    public static String SENTMSG = " has sent you a ";
    public static String COMMENTMSG = " left a comment on a ";
    public static String INVITEMSG = " like to connect with you on Trend On Tap ";
    public static String BUYER_INVITE_MSG = " started following you";
    public static String LIKE_MSG = " likes your ";


    public static void processActivity(final ActivityItem activityItem) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
        final String key = databaseReference.push().getKey();
        activityItem.setId(key);
        databaseReference.child(key).setValue(activityItem);

        FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            DatabaseReference useractivity = FirebaseDatabase.getInstance()
                                    .getReference().child(FirebasePathUserActivities);
                            useractivity.child(child.getKey()).child(key).setValue(true);
                            String message = activityItem.getMessage();
                            try {
                                if (!activityItem.getShortMessage().trim().equalsIgnoreCase("")) {
                                    message = message.trim() + " " + activityItem.getShortMessage();
                                }
                            } catch (Exception e) {

                            }
                            sendNotification(child.getKey(), message, activityItem);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public static void sendActivityToUser(final String sentto, ActivityItem activityItem, final String message) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
        final String key = databaseReference.push().getKey();
        activityItem.setId(key);
        databaseReference.child(key).setValue(activityItem);

        DatabaseReference useractivity = FirebaseDatabase.getInstance()
                .getReference().child(FirebasePathUserActivities);

        useractivity.child(sentto).child(key).setValue(true);
        sendNotification(sentto, message, activityItem);
    }

    public static void sendNotification(final String userid, final String message, final ActivityItem activityItem) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathUsers).child(userid);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.getValue(Users.class);
                int bageCount = 0;
                try {
                    bageCount = (int) users.getBadgeCount();
                } catch (Exception e) {

                }

                databaseReference.child(BadgeCount).setValue(bageCount + 1);

                String one_single_userId = "";
                one_single_userId = String.valueOf(dataSnapshot.child(OneSingleUserID).getValue());
                if (one_single_userId.equalsIgnoreCase(""))
                    return;

                try {
                    String object = "{ \"Content-Type\":\"application/json\",\"contents\": {\"en\":\"" + message + "\"}," +
                            "\"include_player_ids\": [\"" + one_single_userId + "\"]," +
                            "\"data\":{\"type\":\"" + activityItem.getActivityType() + "\"," +
                            "\"activity_id\":\"" + activityItem.getId() + "\"," +
                            "\"reactionId\":\"" + activityItem.getReactionID() + "\"" +
                            ",\"userId\":\"" + activityItem.getUserID() + "\"," +
                            "\"postid\":\"" + activityItem.getPostID() + "\"," +
                            "\"post_img\":\"" + activityItem.getPostImageURL() + "\"," +
                            "\"short_message\":\"" + activityItem.getShortMessage() + "\"}}";

                    final String finalOne_single_userId = one_single_userId;
                    OneSignal.postNotification(new JSONObject(object),
                            new OneSignal.PostNotificationResponseHandler() {
                                @Override
                                public void onSuccess(JSONObject response) {
                                    Log.i("OneSignalExample-" + finalOne_single_userId, "postNotification Success: " + response.toString());
                                }

                                @Override
                                public void onFailure(JSONObject response) {
                                    Log.e("OneSignalExample-" + finalOne_single_userId, "postNotification Failure: " + response.toString());
                                }
                            });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
