package tnm.trendontap.utility;


import tnm.trendontap.modal.SwipeMenu;

/**
 * Created by TNM on 2/23/2018.
 */

public interface SwipeMenuCreator {
    void create(SwipeMenu menu);
}
