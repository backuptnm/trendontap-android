package tnm.trendontap.utility;

import android.text.TextUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by admin on 14-Dec-17.
 */

public class FileSearch {

    /**
     * Search a directory and return a list of all **directories** contained inside
     *
     * @param directory
     * @return
     */
    public static ArrayList<String> getDirectoryPaths(String directory) {
        ArrayList<String> pathArray = new ArrayList<>();
        File file = new File(directory);
        File[] listfiles = file.listFiles();
        if (listfiles == null)
            return pathArray;

        pathArray.add("/All Photos");
        for (int i = 0; i < listfiles.length; i++) {
            if (listfiles[i].isDirectory()) {
                ArrayList<String> imgURLs = getFilePaths(listfiles[i].getAbsolutePath());
                if (imgURLs.size() > 1) {
                    pathArray.add(listfiles[i].getAbsolutePath());
                }
            }
        }

        return pathArray;
    }

    /**
     * Search a directory and return a list of all **files** contained inside
     *
     * @param directory
     * @return
     */
    public static ArrayList<String> getFilePaths(String directory) {
        ArrayList<String> pathArray = new ArrayList<>();
        File file = new File(directory);
        File[] listfiles = file.listFiles();
        pathArray.add("");
        if (listfiles == null)
            return pathArray;

        for (int i = 1; i < listfiles.length; i++) {
            if (listfiles[i].isFile()) {
                String path = listfiles[i].getAbsolutePath();
                if (!pathArray.contains(path) && !TextUtils.isEmpty(path) && (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png")))
                    pathArray.add(path);
            }
        }
        return pathArray;
    }

    public static ArrayList<String> getAllFilePaths(ArrayList<String> allDirectory) {
        ArrayList<String> pathArray = new ArrayList<>();
        pathArray.add("");
        for (int k = 0; k < allDirectory.size(); k++) {
            String directory = allDirectory.get(k);
            File file = new File(directory);
            File[] listfiles = file.listFiles();
            if (listfiles == null)
                continue;

            for (int i = 1; i < listfiles.length; i++) {
                if (listfiles[i].isFile()) {
                    String path = listfiles[i].getAbsolutePath();
                    if (!pathArray.contains(path) && !TextUtils.isEmpty(path) && (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png")))
                        pathArray.add(path);
                }
            }
        }

        return pathArray;
    }
}
