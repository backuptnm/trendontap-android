package tnm.trendontap.utility;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Picture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import tnm.trendontap.R;
import tnm.trendontap.activity.CameraActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.fragment.UseImageFragment;

import static tnm.trendontap.activity.CameraActivity.loadContentFragment;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.calculateInSampleSize;
import static tnm.trendontap.utility.Utils.getResizedBitmap;

public class CustomCameraView extends SurfaceView {
    public Camera camera;
    public int camaraval = 0;
    SurfaceHolder previewHolder;
    Context ctx;
    Preference preference;
    boolean isTakingPicture = false;
    int val = 0;
    boolean isFromBuyers;
    Uri selectedImage;

    String activity_calling_from;

    private Parameters params;

    JSONArray imageCaptureArray;
    SharedPreferences mPreferences;
    public static final String camera_Pref = "camera_pref";
    String postid = "", postuserid, postimg, from;


    public CustomCameraView(Context ctx, boolean isFromBuyers, String activity_calling_from, String postid,
                            String postuserid, String postimg, String from) {
        super(ctx);
        this.ctx = ctx;
        this.postid = postid;
        this.postuserid = postuserid;
        this.postimg = postimg;
        this.from = from;
        preference = new Preference(ctx, Login_Pref);
        this.isFromBuyers = isFromBuyers;
        this.activity_calling_from = activity_calling_from;

        imageCaptureArray = new JSONArray();
        mPreferences = ctx.getSharedPreferences(camera_Pref, 0);
        try {
            imageCaptureArray = new JSONArray(mPreferences.getString("imagePath", "")); //String.valueOf(new JSONArray())
        } catch (JSONException e) {
            e.printStackTrace();
            imageCaptureArray = new JSONArray();
        }

    }

    public void startCamera() {

        previewHolder = this.getHolder();
        previewHolder.addCallback(surfaceHolderListener);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    SurfaceHolder.Callback surfaceHolderListener = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            Log.i("flip", "5");
            camera = Camera.open();
            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Exception e) {
            }
            // enableZoom();
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {

            int rotation = ((Activity) ctx).getWindowManager()
                    .getDefaultDisplay().getRotation();
            Log.i("jdhgj", rotation + "");

            Parameters params = camera.getParameters();
            Display display = ((WindowManager) ctx
                    .getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();

            List<Camera.Size> previewSizes = params.getSupportedPreviewSizes();
            Camera.Size previewSize = previewSizes.get(0);

            if (display.getRotation() == Surface.ROTATION_0) {
                params.setPreviewSize(previewSize.width, previewSize.height);
                camera.setDisplayOrientation(90);
            }

            if (display.getRotation() == Surface.ROTATION_90) {
                params.setPreviewSize(previewSize.width, previewSize.height);
            }

            if (display.getRotation() == Surface.ROTATION_180) {
                params.setPreviewSize(previewSize.width, previewSize.height);
            }

            if (display.getRotation() == Surface.ROTATION_270) {
                params.setPreviewSize(previewSize.width, previewSize.height);
                camera.setDisplayOrientation(180);
            }

            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            camera.setParameters(params);
            camera.startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder arg0) {
            camera.stopPreview();
            camera.release();

        }
    };

    public void takePictureFocus() {
        try {
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean arg0, Camera arg1) {
                    //TODO Is there anything we have to do after autofocus?
                    takePicture();
                }
            });
        } catch (Exception e) {
            takePicture();
        }
    }

    public void takePicture() {

        if (!isTakingPicture) {

            val = 0;
            //	Sender.outer.setVisibility(View.VISIBLE);

            final Handler h1 = new Handler();
            h1.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (val == 0 || val == 2) {

                        val++;
                        h1.postDelayed(this, 200);
                    } else if (val == 1) {

                        val++;
                        h1.postDelayed(this, 200);
                    } else {

                        h1.removeCallbacksAndMessages(null);
                    }
                }
            }, 200);


            if (preference.getBoolean("isflash", false)) {
                Camera.Parameters params = camera.getParameters();
                params.setFlashMode(Parameters.FLASH_MODE_TORCH);
                camera.setParameters(params);
                camera.startPreview();
            }

            camera.takePicture(myShutterCallback, myPictureCallback_RAW,
                    myPictureCallback_JPG);
            isTakingPicture = true;

        }
    }

    public Bitmap RotateBitmap(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                source.getHeight(), matrix, false);
    }

    public void zoomCamera(boolean zoomInOrOut) {
        if (camera != null) {
            Parameters parameter = camera.getParameters();

            if (parameter.isZoomSupported()) {
                int MAX_ZOOM = parameter.getMaxZoom();
                int currnetZoom = parameter.getZoom();
                if (zoomInOrOut && (currnetZoom < MAX_ZOOM && currnetZoom >= 0)) {
                    parameter.setZoom(++currnetZoom);
                } else if (!zoomInOrOut
                        && (currnetZoom <= MAX_ZOOM && currnetZoom > 0)) {
                    parameter.setZoom(--currnetZoom);
                }
            } else
                Toast.makeText(ctx, "Zoom not available", Toast.LENGTH_LONG)
                        .show();

            camera.setParameters(parameter);
        }
    }

    ShutterCallback myShutterCallback = new ShutterCallback() {

        @Override
        public void onShutter() {
            // TODO Auto-generated method stub


        }
    };

    PictureCallback myPictureCallback_RAW = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {
            // TODO Auto-generated method stub

        }
    };

    PictureCallback myPictureCallback_JPG = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {


            // TODO Auto-generated method stub
            isTakingPicture = false;
            camera.startPreview();

            BitmapFactory.Options options = new BitmapFactory.Options();

            // First decode with inJustDecodeBounds=true to check dimensions
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeByteArray(arg0, 0, arg0.length,
                    options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, Utils.getScreenWidth(), Utils.getScreenHeight()); //My device pixel resolution
            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            Bitmap bitmap = BitmapFactory.decodeByteArray(arg0, 0, arg0.length,
                    options);


            Display display = ((WindowManager) ctx
                    .getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();

            int orientation = display.getRotation();

            if (orientation == Surface.ROTATION_90) {
                bitmap = getResizedBitmap(bitmap, 900);
            } else if (orientation == Surface.ROTATION_270) {
                bitmap = RotateBitmap(bitmap, 180);
                bitmap = getResizedBitmap(bitmap, 900);
            } else if (orientation == Surface.ROTATION_180) {
                bitmap = getResizedBitmap(bitmap, 900);
            } else if (orientation == Surface.ROTATION_0) {
                if (camaraval == 0)
                    bitmap = RotateBitmap(bitmap, 90);
                else
                    bitmap = RotateBitmap(bitmap, 270); // for selfi

                bitmap = getResizedBitmap(bitmap, 900);
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = timeStamp + "_";

            String name = "/pic" + imageFileName + ".jpg";

            final File file = new File(Environment.getExternalStorageDirectory() + name);

            FileOutputStream outStream = null;
            try {

                outStream = new FileOutputStream(file);
                bitmap.compress(CompressFormat.JPEG, 100, outStream);
                outStream.close();
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                    bitmap = null;
                }


                String path = file.getAbsolutePath();

                if (imageCaptureArray.length() < 4)
                    imageCaptureArray.put(path);
                else
                    try {
                        imageCaptureArray.put(3, path);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", imageCaptureArray.toString());
                editor.apply();

                UseImageFragment fragment = new UseImageFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", isFromBuyers);
                bundle.putString("from", from);
                bundle.putString("postid", postid);
                try {
                    bundle.putString("selectedImage", (String) imageCaptureArray.get(imageCaptureArray.length() - 1));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (isFromBuyers) {
                    fragment.setArguments(bundle);
                    loadContentFragment(fragment, false);
                } else {
                    bundle.putString("postuserid", postuserid);
                    bundle.putString("postimg", postimg);
                    bundle.putString("from", from);

                    MainActivity.loadContentFragment(fragment, false, bundle);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("Log", "onPictureTaken - jpeg");
        }
    };


    public void flipCamera() {

        camera.stopPreview();

//NB: if you don't release the current camera before switching, you app will crash
        camera.release();

//swap the id of the camera to be used
        if (camaraval == Camera.CameraInfo.CAMERA_FACING_BACK) {
            camaraval = Camera.CameraInfo.CAMERA_FACING_FRONT;
        } else {
            camaraval = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        camera = Camera.open(camaraval);

        setCameraDisplayOrientation(ctx, camaraval, camera);
        try {

            camera.setPreviewDisplay(previewHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();
    }


    public static void setCameraDisplayOrientation(Context ctx,
                                                   int cameraId, Camera camera) {
        Camera.CameraInfo info =
                new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = ((Activity) ctx).getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }


}
