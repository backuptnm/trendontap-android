package tnm.trendontap.utility;

public class FireStoreUtils {

    public static final String FirebasePathUsers = "Users";
    //Field


    public static final String ID = "id";
    public static final String TimeStamp = "timeStamp";
    public static final String ProfilePic = "profilePic";
    public static final String BadgeCount = "badgeCount";
    public static final String FirstName = "firstName";
    public static final String LastName = "lastName";
    public static final String JobTitle = "jobTitle";
    public static final String CompanyName = "companyName";
    public static final String EmailID = "emailID";
    public static final String UserType = "userType";
    public static final String Phone = "phone";
    public static final String OneSingleUserID = "oneSingleUserID";
    public static final String DeviceType = "deviceType";
    public static final String AppVersion = "appVersion";
    public static final String DateUpdated = "dateUpdated";


    /********Firebase Post & Field*************/
    public static final String FirebaseTrendOnTapPosts = "Posts";
    public static final String Loves = "Loves";
    public static final String Trends = "Trends";
    public static final String UserPosts = "UserPosts";
    public static final String UserPostFeeds = "UserPostFeeds";


    /*********Location Dicationary*************/
    public static final String Latitude = "latitude";
    public static final String Logitude = "logitude";
    public static final String Title = "title";
    public static final String Address = "address";


    /********Firebase Reactions & Field*************/
    public static final String FirebasePathPostReaction = "Reactions";
    public static final String PostID = "postID";
    public static final String SampleRequest = "sampleRequest";
    public static final String CostsheetRequest = "costsheetRequest";
    public static final String SampleSent = "sampleSent";
    public static final String CostsheetSent = "costsheetSent";
    public static final String CostsheetID = "costsheetID";
    public static final String SampleID = "sampleID";
    public static final String tReactionUnreadCommentSupplier = "unreadCommentSupplier";
    public static final String tReactionUnreadCommentBuyer = "unreadCommentBuyer";
    public static final String tReactionCommentCount = "commentCount";
    public static final String PostByUserID = "postByUserID";
    public static final String RecycleBin = "recycleBin";
    public static final String isDeleted = "isDeleted";


    /********User Reactions & Field*************/
    public static final String FirebaseUserReactionFeeds = "UserReactionFeeds";
    public static final String tUserReactionFeedsCostSheetCount = "costsheetCount";
    public static final String tUserReactionFeedsSampleCount = "sampleCount";

    /********Love & Trend Category & Field*************/
    public static final String TrendsSeason = "TrendsSeason";
    public static final String TrendsUserSeason = "TrendsUserSeason";
    public static final String FirebasePathLovesCategory = "LovesCategory";
    public static final String FirebasePathUserLovesCategory = "LovesUserCategory";
    public static final String Name = "name";

    /********Firebase Comments & Field*************/
    public static final String FirebasePathcComments = "Comments";
    public static final String FirebasePathcReactionComments = "ReactionComments";
    public static final String FirebasePathcPostComments = "PostComments";
    public static final String FirebasePathcUserComments = "UserComments";
    public static final String Message = "message";

    /********Firebase Love Likes*************/
    public static final String Likes = "Likes";
    public static final String PostLikes = "PostLikes";
    public static final String FirebasePathBuyerPostLike = "BuyerPostLikes";
    public static final String FirebasePathUserLike = "UserLikes";
    public static final String FirebasePathBuyerUserLike = "BuyerUserLikes";

    //Post TableField
    public static final String Content = "content";
    public static final String PostTitle = "postTitle";
    public static final String PdfURL = "pdfURL";
    public static final String tPostSendToSupplier = "SendToSupplier";
    public static final String DeliveryDate = "deliveryDate";
    public static final String Catagory = "catagory";
    public static final String CatagoryName = "catagoryName";
    public static final String tPostPostType = "postType";
    public static final String tPostLikers = "likers";
    public static final String LikesCount = "likesCount";
    public static final String BuyerLikesCount = "BuyerLikesCount";
    public static final String ReactionsCount = "reactionsCount";
    public static final String UnreadReactionCount = "unreadReactionCount";
    public static final String ReactedSuppiers = "reactedSuppiers";
    public static final String Location = "location";
    public static final String TagItems = "tagItems";
   // public static final String TagItems = "tag_items";
    public static final String HideSuppliers = "hideSuppliers";
    public static final String UserID = "userID";
    public static final String tPostPhotoId = "photoURLs";


    /********Firebase User Profile & Field*************/
    public static final String FirebasePathUserProfile = "UserProfile";
    public static final String FirebasePathUserProfileBio = "Bio";
    public static final String FirebasePathUserProfileUserName = "Username";

    public static final String FirebasePathUserProfileFollowingCount = "SuppliersCount";
    public static final String FirebasePathUserProfileFollowerCount = "BuyersCount";

    //Field
    public static final String tUserProfileLoveCount = "LoveCount";


    /********Firebase Buyers and Supplies & Field*************/
    public static final String SuppliersOfBuyer = "SuppliersOfBuyer";   //user-follower
    public static final String BuyersOfSupplier = "BuyersOfSupplier";
    public static final String Buyers = "Buyers";
    public static final String Suppliers = "Suppliers";


    /********Firebase Followers & Field*************/
    public static final String FirebasePathActivities = "Activities";
    public static final String tActivitiesPostId = "postID";
    public static final String ReactionID = "reactionID";
    public static final String ActivityType = "ActivityType";
    public static final String tActivitiesMessage = "Message";
    public static final String tActivitiesShortMessage = "ShortMessage";
    public static final String tActivitiesLongMessage = "LongMessage";
    public static final String tActivitiesTriggerByUser = "TiggerBy";
    public static final String tActivityPostImageUrl = "PostImageURL";
    public static final String tActivityisRead = "isReadUsers";


    //Activity Type Constant
    public static final String kActivityTypeLove = "Love";
    public static final String kActivityTypeTrend = "Trend";
    public static final String kActivityTypeFollow = "Follow";
    public static final String kActivityTypeReaction = "Reaction";
    public static final String kActivityTypeInvitation = "Invitation";
    public static final String kActivityTypeComment = "Comment";
    public static final String kActivityTypePostComment = "PostComment";
    public static final String Sample = "Samples";
    public static final String kActivityTypeSampleSent = "SampleSent";
    public static final String kActivityTypeCostingSheet = "CostingSheet";
    public static final String CostingSheetSent = "CostingSheetSent";
    public static final String BuyerFollower = "BuyerFollower";
    public static final String kActivityTypeLikeLove = "LikeLove";
    public static final String kActivityTypeLikeTrend = "LikeTrend";

    //Field
    public static final String FirebasePathUserActivities = "UserActivity";
    public static final String FirebasePathFollowActivities = "ActivityFollow";

    public static final String FirebasePathUserActivitieLike = "ActivityLike";
    public static final String FirebasePathActivityPost = "ActivityPost";


    /********Firebase Costsheet & Field*************/

    public static final String FirebasePathCostSheet = "Costsheets";
    public static final String tSheetReactionId = "ReactionID";
    public static final String tSheetSupplierName = "SupplierName";
    public static final String BuyerUserID = "buyerUserID";
    public static final String StyleNumber = "styleNumber";
    public static final String PackagingCost = "packagingCost";
    public static final String Overhead = "overhead";
    public static final String GarmentCost = "garmentCost";
    public static final String TotalOverhead = "totalOverhead";
    public static final String TotalCost = "totalCost";



    /********Firebase Invite Suppliers & Field*************/
    public static final String SupplierInvitations = "SupplierInvitations";
    public static final String InvitedSuppliersOfBuyer = "InvitedSuppliersOfBuyer";

    //Field
    public static final String BuyerName = "buyerName";
    public static final String VerificationCode = "verificationCode";
    public static final String InvitedSupplierEmailID = "invitedSupplierEmailID";
    public static final String IsInviationAccepted = "isInviationAccepted";


    /********Buyer Connetion & Field*************/
    public static final String BuyerConnections = "BuyerConnections";

    /********Buyer Invitation & Field*************/
    public static final String BuyerInvitations = "BuyerInvitations";




}
