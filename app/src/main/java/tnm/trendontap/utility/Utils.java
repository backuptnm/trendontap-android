package tnm.trendontap.utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import tnm.trendontap.R;
import tnm.trendontap.activity.HomeActivity;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.OneSingleUserID;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;

public class Utils {

    public static final String Login_Pref = "loadie_login_pref";

    public static final String Buyer = "Buyer";
    public static final double defaultradius = 0.10;
    public static final String Supplier = "Supplier";
    public static final String senderemail = "smtp@trendontap.com";
    public static final String senderpwd = "234delbd";
    // public static final String uploadpath = "gs://trendontap-c1948.appspot.com"; //tnm6
    //public static final String uploadpath = "gs://trendontap-144611.appspot.com/"; //jeevantnm
    //  public static final String uploadpath = "gs://trandontap-sandbox-android.appspot.com/"; //new sandox android
    public static final String uploadpath = "gs://trandontaptest.appspot.com/"; //totqatest android
    // public static final String uploadpath = "gs://trandontaptest.appspot.com/"; //new sandox testing android

    public static final String googlekey = "AIzaSyCvYxDB9rfKF_jc7fRgrHlK8hcwGCU49M4";


    public static String getVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "1.0";
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }


    public static double milestoMeter() {
        return defaultradius * 1609.34;
    }

    public static void logout(Context context, String message) {
       /* NotificationManager nmg = (NotificationManager) context.getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            nmg.cancelAll();
        } catch (Exception e) {

        }*/
        Preference preference = new Preference(context, Login_Pref);
        SharedPreferences.Editor editor = preference.edit();
        editor.clear();
        editor.apply();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers);
        databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(OneSingleUserID).setValue("");

        FirebaseAuth.getInstance().signOut();
        BadgeUtils.clearBadge(context);
        Intent newIntent = new Intent(context, HomeActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.putExtra("message", message);
        context.startActivity(newIntent);
        ((Activity) context).finish();
        ((Activity) context).overridePendingTransition(R.anim.left_slide_in, R.anim.left_slide_out);
    }


    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void sendMail(Context context, String message, String subject, String emailtxt) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("mailto:" + emailtxt));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        // intent.putExtra(Intent.EXTRA_EMAIL, emailtxt);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        Intent mailer = Intent.createChooser(intent, "Send mail using");
        context.startActivity(mailer);
    }


    public static void hidekeyboard(Context context, EditText editText) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    public static String getName(String fname, String lname) {
        String fnametxt = "", lnametxt = "";
        if (fname.length() > 0)
            fnametxt = fname.substring(0, 1).toUpperCase() + fname.substring(1);

        if (lname.length() > 0)
            lnametxt = lname.substring(0, 1).toUpperCase() + lname.substring(1);

        return fnametxt + " " + lnametxt;
    }

    /*public static String getDay(long timestamp) {

        long difference = (System.currentTimeMillis() / 1000) - timestamp;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(difference);

        int dayNum = c.get(Calendar.DAY_OF_WEEK);
        if (dayNum == 0) {
            return "Today";
        } else if (dayNum == 1) {
            return "Yesterday";
        }
        return dayNum + " days ago";
    }*/

    public static String getDay(long timestamp) {

        long difference = System.currentTimeMillis() - timestamp;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(difference);
        // int dayNum = c.get(Calendar.DAY_OF_WEEK);


        if (difference > 0) {
            int seconds = (int) (difference / 1000) % 60;
            int minutes = (int) ((difference / (1000 * 60)) % 60);
            int hours = (int) ((difference / (1000 * 60 * 60)) % 24);
            int dayNum = (int) ((difference / (24 * 1000 * 60 * 60)));
            int week = (int) (dayNum / 4);
            int month = (int) (dayNum / 30);
            int year = (int) (dayNum / 365);
            // long days =   difference /  DAY_MILLIS;

            if (year >= 2) {
                return year + " years ago";
            } else if (year == 1) {
                return "Last year";
            } else {
                if (month >= 2) {
                    return month + " months ago";
                } else if (month == 1) {
                    return "Last month";
                } else {
                    if (week >= 2) {
                        return week + " weeks ago";
                    } else if (week == 1) {
                        return "Last week";
                    } else {
                        if (dayNum >= 2) {
                            return dayNum + " days ago";
                        } else if (dayNum == 1) {
                            return "Yesterday";
                        } else {
                            if (hours >= 2) {
                                return hours + " hours ago";
                            } else if (hours == 1) {
                                return "An hour ago";
                            } else {
                                if (minutes >= 2) {
                                    return minutes + " minutes ago";
                                } else if (minutes == 1) {
                                    return "A minute ago";
                                } else {
                                    if (seconds >= 3) {
                                        return seconds + " seconds ago";
                                    } else {
                                        return "Just now";
                                    }
                                }
                            }
                        }
                    }

                }

            }

        } else
            return "Just now";
    }


    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public static String getDateFromTimeStamp(long timestamp) {
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(timestamp);
        String date = DateFormat.format("dd/MM/yyyy", calendar).toString();
        return date;
    }


    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //There are changes after apk lollipop while detecting internet connection:
        //if user's os newer then lollipop:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;

            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);

                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        }
        //if user's os older then lollipop:
        else {
            //getAllNetworkInfo method work for only before API 19:
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static double parseDouble(String string, int defaultValue) {
        try {
            return Double.parseDouble(string);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static void setDialogwidth(Dialog dialog, Context context, int margin) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        dialog.getWindow().setLayout(displayMetrics.widthPixels - margin, WindowManager.LayoutParams.WRAP_CONTENT);
    }


    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static void hideSoftKeyboard(Activity mActivity) {
        try {
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static boolean isDestroy(Activity activity) {
        if (activity == null || activity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed())) {
            return true;
        } else {
            return false;
        }
    }

    public static String getCounttxt(String countval, String text) {
        int count = Integer.parseInt(countval);
        if (count == 1) {
            return count + " " + text;
        }
        return count + " " + text + "s";
    }


    public static boolean isLessEqualFifty(java.io.File file) {
        double fileSizeInBytes = file.length();

        double fileSizeInKB = fileSizeInBytes / 1024;

        double fileSizeInMB = fileSizeInKB / 1024;


        if (fileSizeInMB < 50) {
            return true;
        }
        return false;

    }

    public static boolean isLessEqualFifty(long fileSizeInBytes) {

        long fileSizeInKB = fileSizeInBytes / 1024;

        long fileSizeInMB = fileSizeInKB / 1024;

        if (fileSizeInMB < 50) {
            return true;
        }
        return false;
    }


    public static void confiramtionDialog(Context context, String value) {
        final Dialog agreeDialog = new Dialog(context);
        agreeDialog.setCancelable(false);
        agreeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        agreeDialog.setContentView(R.layout.dialog_confirmation);

        TextView message = (TextView) agreeDialog.findViewById(R.id.message);
        message.setTypeface(RobotoFont.regularFont(context));
        message.setText(value);
        agreeDialog.show();
        agreeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                agreeDialog.dismiss();
            }
        }, 1300);

    }


    public static boolean shouldShow(DataSnapshot dataSnapshot, String userrole) {
        if (userrole.equalsIgnoreCase(Utils.Supplier))
            return true;
        else if (dataSnapshot.hasChild(RecycleBin) && ((boolean) dataSnapshot.child(RecycleBin).getValue()))
            return false;
        else if (dataSnapshot.hasChild(isDeleted) && (boolean) dataSnapshot.child(isDeleted).getValue())
            return false;
        else
            return true;


    }


}
