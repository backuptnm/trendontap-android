package tnm.trendontap.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preference {

    SharedPreferences preferences;

    public Preference(Context c, String prefname) {
        preferences = c.getSharedPreferences(prefname, Context.MODE_PRIVATE);
    }

    public Editor edit() {
        return preferences.edit();
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public int getInt(String key, int def) {

        return preferences.getInt(key, 0);
    }

    public long getLong(String key, long def) {
        return preferences.getLong(key, 0);
    }


    public boolean getBoolean(String key, Boolean def) {
        return preferences.getBoolean(key, def);
    }


    public boolean haskey(String key) {
        return preferences.contains(key);
    }
}
