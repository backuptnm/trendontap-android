package tnm.trendontap.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.UploadFromFileAdapter;
import tnm.trendontap.fragment.GoogleDriveListFragment;
import tnm.trendontap.fragment.UploadTrendFragment;
import tnm.trendontap.modal.GoogleDriveModel;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.Utils.isLessEqualFifty;

/**
 * Created by TNM on 4/3/2018.
 */

public class UploadFromFileManager extends AppCompatActivity {

    ListView files_list;
    public static ArrayList<File> fileList = new ArrayList<File>();
    UploadFromFileAdapter uploadFromFileAdapter;
    public static int REQUEST_PERMISSIONS = 1;
    boolean boolean_permission;
    TextView norecords, labeltxt;
    ImageView close;
    File dir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        init();

    }

    private void init() {
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        files_list = (ListView) findViewById(R.id.files_list);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.regularFont(this));
        labeltxt.setText("File Manager");
        norecords = (TextView) findViewById(R.id.norecords);
        norecords.setTypeface(RobotoFont.regularFont(this));
        dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        fn_permission();
        files_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (!isLessEqualFifty(fileList.get(i))) {
                    Toast.makeText(UploadFromFileManager.this, "Your Trend exceeds our maximum size limit of 50MB, please reduce the size and try again", Toast.LENGTH_SHORT).show();
                    return;
                }
                addTrendNameDialog(fileList.get(i));
            }
        });
    }


    public void addTrendNameDialog(final File file) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_add_season);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(this));
        tvTitle.setText("Give your Trend a Title");
        final EditText edtname = (EditText) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(this));
        edtname.setHint("Title");
        edtname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) ;
                {
                    if (edtname.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(UploadFromFileManager.this, "Please enter title", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    dialog.dismiss();
                    InputMethodManager imm = (InputMethodManager) UploadFromFileManager.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                    Bundle bundle = new Bundle();
                    bundle.putString("trendimg", file.getAbsolutePath());
                    bundle.putString("trendname", edtname.getText().toString().trim());


                    MainActivity.loadContentFragment(new UploadTrendFragment(), false, bundle);
                    finish();

                }
                return false;
            }
        });


        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setText("OK");
        done_action.setTypeface(RobotoFont.boldFont(this));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtname.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(UploadFromFileManager.this, "Please enter title", Toast.LENGTH_LONG).show();
                    return;
                }
                dialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString("trendimg", file.getAbsolutePath());
                bundle.putString("trendname", edtname.getText().toString().trim());
                MainActivity.loadContentFragment(new UploadTrendFragment(), false, bundle);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                onBackPressed();
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(this));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);

    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    getfile(listFile[i]);

                } else {

                    boolean booleanpdf = false;
                    if (listFile[i].getName().endsWith(".pdf")) {

                        for (int j = 0; j < fileList.size(); j++) {
                            if (fileList.get(j).getName().equals(listFile[i].getName())) {
                                booleanpdf = true;
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false;
                        } else {
                            fileList.add(listFile[i]);

                        }
                    }
                }
            }
        }
        return fileList;
    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE))) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);

            }
        } else {
            boolean_permission = true;

            fileList = getfile(dir);
            processList(fileList);
        }
    }


    public void processList(ArrayList<File> fileList) {
        if (fileList.size() == 0) {
            norecords.setVisibility(View.VISIBLE);
            files_list.setVisibility(View.GONE);
            return;
        }

        norecords.setVisibility(View.GONE);
        files_list.setVisibility(View.VISIBLE);

        uploadFromFileAdapter = new UploadFromFileAdapter(UploadFromFileManager.this, fileList);
        files_list.setAdapter(uploadFromFileAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                boolean_permission = true;
                fileList = getfile(dir);
                processList(fileList);

            } else {
                Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();

            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

}
