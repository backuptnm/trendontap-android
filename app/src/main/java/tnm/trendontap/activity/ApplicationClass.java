package tnm.trendontap.activity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.BadgeUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.MainActivity.activitycnt;
import static tnm.trendontap.utility.FireStoreUtils.BadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.tActivityisRead;
import static tnm.trendontap.utility.Utils.Login_Pref;

/**
 * Created by TNM on 2/28/2018.
 */

public class ApplicationClass extends Application {
    Preference preference;
    Users users;

    public ApplicationClass() {
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        preference = new Preference(this, Login_Pref);
       // FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new ExampleNotificationReceivedHandler())
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    private class ExampleNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {

           /* preference = new Preference(getApplicationContext(), Login_Pref);
            users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
            long badgeCount = 0;
            try {
                badgeCount = users.getBadgeCount();
            } catch (Exception e) {

            }*/

            if (!Utils.isAppIsInBackground(ApplicationClass.this)) {

                MainActivity.getBadgeCount(MainActivity.activitycnt, getApplicationContext());
               /* try {
                    Intent intent = new Intent();
                    intent.setAction("com.onNotificationReceived");
                    sendBroadcast(intent);

                } catch (Exception e) {

                }*/
            }

          /*  DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            databaseReference1.child(BadgeCount).setValue(badgeCount + 1);
            users.setBadgeCount(badgeCount + 1);
            Log.i("users", users + "");
            SharedPreferences.Editor editor = preference.edit();
            String json = new Gson().toJson(users);
            editor.putString("userid", users.getUserID());
            editor.putString("userrole", users.getUserType());
            editor.putString("userdetail", json);
            editor.apply();

            if (!Utils.isAppIsInBackground(ApplicationClass.this)) {
                try {
                    Intent intent = new Intent();
                    intent.putExtra("badgecnt", badgeCount + 1);
                    intent.setAction("com.onNotificationReceived");
                    sendBroadcast(intent);

                } catch (Exception e) {

                }
            }*/

            // BadgeUtils.setBadge(getApplicationContext(), (int) badgeCount + 1);

        }
    }

    public void getBadgeCount() {
        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().
                getReference().child(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long badgeCount = (long) dataSnapshot.child(BadgeCount).getValue();
                if (badgeCount == 0)
                    return;
                databaseReference1.child(BadgeCount).setValue(badgeCount - 1);
              /*  users.setBadgeCount(badgeCount - 1);
                Log.i("users", users + "");
                SharedPreferences.Editor editor = preference.edit();
                String json = new Gson().toJson(users);
                editor.putString("userid", users.getUserID());
                editor.putString("userdetail", json).apply();
                editor.apply();*/
                activitycnt.setText(String.valueOf(badgeCount - 1));
                BadgeUtils.setBadge(getApplicationContext(), (int) badgeCount - 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

           /* preference = new Preference(getApplicationContext(), Login_Pref);
            users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
            long badge_cnt = 0;
            try {
                badge_cnt = users.getBadgeCount();
            } catch (Exception e) {

            }
            if (badge_cnt > 0) {
                badge_cnt = badge_cnt - 1;
            }
            BadgeUtils.setBadge(getApplicationContext(), (int) badge_cnt);

            DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            databaseReference1.child(BadgeCount).setValue(badge_cnt);
            users.setBadgeCount(badge_cnt);
            Log.i("users", users + "");
            SharedPreferences.Editor editor = preference.edit();
            String json = new Gson().toJson(users);
            editor.putString("userid", users.getUserID());
            editor.putString("userrole", users.getUserType());
            editor.putString("userdetail", json);
            editor.apply();
            BadgeUtils.setBadge(getApplicationContext(), (int) badge_cnt);*/

            getBadgeCount();

            OSNotificationAction.ActionType actionType = result.action.type;

            JSONObject data = result.notification.payload.additionalData;
            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

            String type = "", activity_id = "", reactionId = "", post_img = "", userId = "", postid = "", from = "";
            String openURL = null;
            Object activityToLaunch = MainActivity.class;

            if (data != null) {
                type = data.optString("type", "");
                activity_id = data.optString("activity_id", "");
                reactionId = data.optString("reactionId", "");
                postid = data.optString("postid", "");
                post_img = data.optString("post_img", "");
                userId = data.optString("userId", "");
                from = data.optString("short_message", "");
            }


            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities).child(activity_id);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ActivityItem activityItem = dataSnapshot.getValue(ActivityItem.class);
                    ArrayList<String> stringList = new ArrayList<>();
                    try {
                        stringList = activityItem.getIsReadUsers();
                    } catch (Exception e) {

                    }

                    try {
                        if (!stringList.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            stringList.add(String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                            activityItem.setIsReadUsers(stringList);
                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities).child(activityItem.getId());
                            databaseReference.child(tActivityisRead).setValue(stringList);
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            Intent intent = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("issentNotification", "true");
            intent.putExtra("type", type);
            intent.putExtra("activity_id", activity_id);
            intent.putExtra("reactionId", reactionId);
            intent.putExtra("postid", postid);
            intent.putExtra("userId", userId);
            intent.putExtra("post_img", post_img);
            intent.putExtra("from", from);
            Log.i("OneSignalExample", "openURL = " + openURL);

            // startActivity(intent);
            startActivity(intent);


        }
    }

}
