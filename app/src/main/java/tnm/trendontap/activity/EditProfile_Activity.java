package tnm.trendontap.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.fragment.Season_Fragment;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.CompanyName;
import static tnm.trendontap.utility.FireStoreUtils.DateUpdated;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirstName;
import static tnm.trendontap.utility.FireStoreUtils.JobTitle;
import static tnm.trendontap.utility.FireStoreUtils.LastName;
import static tnm.trendontap.utility.FireStoreUtils.ProfilePic;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.uploadpath;


public class EditProfile_Activity extends AppCompatActivity {

    CoordinatorLayout mainwrapper;
    TextView txt_save, txt_cancel, labeltxt, changeprofile, companyname, edit_email;
    CircleImageView profilepic;
    EditText edt_first_name, edt_last_name, edit_jobtitle, edt_companyname;
    ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private TextView tvBrowseGallery, tvTakeNewPicture;
    private RelativeLayout cancel_btn_add_images;
    private static final int SELECT_FILE = 1;
    private static final int CAMERA_REQUEST = 1888;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    Bitmap photo = null;

    Preference preference;
    Users users;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        mainwrapper = (CoordinatorLayout) findViewById(R.id.mainwrapper);
        profilepic = (CircleImageView) findViewById(R.id.profilepic);

        edt_first_name = (EditText) findViewById(R.id.edt_first_name);
        edt_first_name.setTypeface(RobotoFont.regularFont(this));
        edt_first_name.setText(users.getFirstName());

        edt_last_name = (EditText) findViewById(R.id.edt_last_name);
        edt_last_name.setTypeface(RobotoFont.regularFont(this));
        edt_last_name.setText(users.getLastName());

        companyname = (TextView) findViewById(R.id.companyname);
        companyname.setTypeface(RobotoFont.regularFont(this));
        companyname.setText(users.getCompanyName());

        edt_companyname = (EditText) findViewById(R.id.edt_companyname);
        edt_companyname.setTypeface(RobotoFont.regularFont(this));
        edt_companyname.setText(users.getCompanyName());

        if (users.getUserType().equalsIgnoreCase(Utils.Supplier)) {
            edt_companyname.setVisibility(View.VISIBLE);
            companyname.setVisibility(View.GONE);
        } else {
            edt_companyname.setVisibility(View.GONE);
            companyname.setVisibility(View.VISIBLE);
        }

        edit_jobtitle = (EditText) findViewById(R.id.edit_jobtitle);
        edit_jobtitle.setTypeface(RobotoFont.regularFont(this));
        edit_jobtitle.setText(users.getJobTitle());

        edit_email = (TextView) findViewById(R.id.edit_email);
        edit_email.setTypeface(RobotoFont.regularFont(this));
        edit_email.setText(users.getEmailID());

        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.regularFont(this));
        txt_save = (TextView) findViewById(R.id.txt_save);
        txt_save.setTypeface(RobotoFont.regularFont(this));
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.mediumFont(this));
        changeprofile = (TextView) findViewById(R.id.changeprofile);
        changeprofile.setTypeface(RobotoFont.regularFont(this));

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        if (!TextUtils.isEmpty(users.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(this)
                    .load(users.getProfilePic())
                    .apply(requestOptions)
                    .into(profilepic);
        }

        changeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectOptionsDialog();
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }

        }
    }

    private void galleryIntent() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, ""), SELECT_FILE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == SELECT_FILE) {
                try {
                    Uri selectedImage = data.getData();
                    InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                    photo = BitmapFactory.decodeStream(imageStream);
                    photo = Utils.getResizedBitmap(photo, 500);
                    // profilepic.setImageURI(selectedImage);
                    profilepic.setImageBitmap(photo);
                } catch (Exception e) {

                }

            } else if (requestCode == CAMERA_REQUEST) {
                photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                photo = Utils.getResizedBitmap(photo, 500);
                profilepic.setImageBitmap(photo);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }


    public void saveProfile() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        String edt_first_name_txt = edt_first_name.getText().toString().trim();
        String edt_last_name_txt = edt_last_name.getText().toString().trim();
        String edit_jobtitle_txt = edit_jobtitle.getText().toString().trim();

        String edt_companyname_txt = edt_companyname.getText().toString().trim();


        if (TextUtils.isEmpty(edt_first_name_txt)) {
            Snackbar.make(mainwrapper, "Please enter first name", Snackbar.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(edt_last_name_txt)) {
            Snackbar.make(mainwrapper, "Please enter last name", Snackbar.LENGTH_SHORT).show();
            return;
        } else if (users.getUserType().equalsIgnoreCase(Utils.Supplier) && TextUtils.isEmpty(edt_companyname_txt)) {
            Snackbar.make(mainwrapper, "Please enter company name", Snackbar.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(edit_jobtitle_txt)) {
            Snackbar.make(mainwrapper, "Please enter job title", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(FirstName).setValue(edt_first_name_txt);
        databaseReference.child(LastName).setValue(edt_last_name_txt);

        if (users.getUserType().equalsIgnoreCase(Utils.Supplier)) {
            databaseReference.child(CompanyName).setValue(edt_companyname_txt);
            users.setCompanyName(edt_companyname_txt);
        }

        databaseReference.child(JobTitle).setValue(edit_jobtitle_txt);
        databaseReference.child(DateUpdated).setValue(System.currentTimeMillis());
        users.setFirstName(edt_first_name_txt);
        users.setLastName(edt_last_name_txt);
        users.setJobTitle(edit_jobtitle_txt);

        if (photo != null) {
            uploadFile(photo, databaseReference);
        } else {
            SharedPreferences.Editor editor = preference.edit();
            String json = new Gson().toJson(users);
            editor.putString("userdetail", json).apply();
            editor.apply();
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            onBackPressed();
        }

    }


    private void uploadFile(Bitmap bitmap, final DatabaseReference databaseReference) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(uploadpath);
        StorageReference imagesRef = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "/profileimg.jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Toast.makeText(EditProfile_Activity.this, "Failed to change profile photo", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = preference.edit();
                String json = new Gson().toJson(users);
                editor.putString("userdetail", json).apply();
                editor.apply();
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                onBackPressed();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl-->", "" + downloadUrl);
                databaseReference.child(ProfilePic).setValue(String.valueOf(downloadUrl));
                users.setAvatarUrl(String.valueOf(downloadUrl));
                SharedPreferences.Editor editor = preference.edit();
                String json = new Gson().toJson(users);
                editor.putString("userdetail", json).apply();
                editor.apply();
                Toast.makeText(EditProfile_Activity.this, "Successfully saved", Toast.LENGTH_SHORT).show();
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                onBackPressed();
            }
        });

    }


    protected void selectOptionsDialog() {
        final Dialog dialog = new Dialog(this, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_upload_option);
        dialog.setCancelable(false);

        TextView select_option = (TextView) dialog.findViewById(R.id.select_option);
        select_option.setTypeface(RobotoFont.regularFont(this));

        TextView take_new_picture_add_images = (TextView) dialog.findViewById(R.id.take_new_picture_add_images);
        take_new_picture_add_images.setTypeface(RobotoFont.regularFont(this));

        take_new_picture_add_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                int permissionCheck = ContextCompat.checkSelfPermission(EditProfile_Activity.this, android.Manifest.permission.CAMERA);

                if (permissionCheck == 0) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(EditProfile_Activity.this, new String[]{android.Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
        });

        TextView browse_gallery_add_images = (TextView) dialog.findViewById(R.id.browse_gallery_add_images);
        browse_gallery_add_images.setTypeface(RobotoFont.regularFont(this));
        browse_gallery_add_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                galleryIntent();
            }
        });


        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(this));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
