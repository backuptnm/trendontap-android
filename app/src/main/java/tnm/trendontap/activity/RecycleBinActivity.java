package tnm.trendontap.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.RecyclebinAdapter;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.Utils.confiramtionDialog;

/**
 * Created by TNM on 2/16/2018.
 */

public class RecycleBinActivity extends AppCompatActivity {
    GridView recyclebingrid;
    TextView norecordstxt, labeltxt, delete, restore;
    LinearLayout bottom_layout;
    public static TextView selectxt;
    RecyclebinAdapter recyclebinAdapter;
    ArrayList<ReactionTrendItem> reactionItemArray;
    RelativeLayout norecordslayout;
    LinearLayout back_arrow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclebin_activity);

        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        reactionItemArray = new ArrayList<>();
        recyclebinAdapter = new RecyclebinAdapter(RecycleBinActivity.this, reactionItemArray, bottom_layout);
        recyclebingrid = (GridView) findViewById(R.id.recyclebingrid);
        recyclebingrid.setAdapter(recyclebinAdapter);

        delete = (TextView) findViewById(R.id.delete);
        delete.setTypeface(RobotoFont.regularFont(this));
        restore = (TextView) findViewById(R.id.restore);
        restore.setTypeface(RobotoFont.regularFont(this));
        norecordstxt = (TextView) findViewById(R.id.norecordstxt);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        selectxt = (TextView) findViewById(R.id.selectxt);
        norecordslayout = (RelativeLayout) findViewById(R.id.norecordslayout);
        back_arrow = (LinearLayout) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        labeltxt.setTypeface(RobotoFont.mediumFont(this));
        selectxt.setTypeface(RobotoFont.regularFont(this));
        selectxt.setTag("selectxt");
        selectxt.setText("Select");
        bottom_layout.setVisibility(View.GONE);
        selectxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectxt.getTag().toString().equalsIgnoreCase("selectxt")) {
                    selectxt.setText("Cancel");
                    selectxt.setTag("canceltxt");

                } else {
                    selectxt.setText("Select");
                    selectxt.setTag("selectxt");
                    recyclebinAdapter.notifyDataSetChanged();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReaction(recyclebinAdapter.selectedid);
            }
        });

        restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restoreReaction(recyclebinAdapter.selectedid);
            }
        });

        setList();
    }

    public void deleteReaction(final ArrayList<ReactionTrendItem> reactionidlist) {

        for (int j = 0; j < reactionidlist.size(); j++) {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction);
            databaseReference.child(reactionidlist.get(j).getId()).child(isDeleted).setValue(true);
            //databaseReference.child(reactionidlist.get(j).getId()).removeValue();

            final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts).child(reactionidlist.get(j).getPostID());
            databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                    if (reactionsCount > 0)
                        databaseReference1.child(ReactionsCount).setValue(reactionsCount - 1);
                    confiramtionDialog(RecycleBinActivity.this, "Deleted");

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    public void restoreReaction(final ArrayList<ReactionTrendItem> reactionidlist) {
        for (int j = 0; j < reactionidlist.size(); j++) {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reactionidlist.get(j).getId());
            databaseReference.child(RecycleBin).setValue(false);

            final int finalJ = j;
            FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).
                    child(reactionidlist.get(j).getPostID()).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                            DatabaseReference reactioncnt = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);
                            reactioncnt.child(reactionidlist.get(finalJ).getPostID()).child(ReactionsCount).setValue(reactionsCount + 1);
                            if (finalJ + 1 == reactionidlist.size()) {
                                confiramtionDialog(RecycleBinActivity.this, "Restored");
                                //Toast.makeText(RecycleBinActivity.this, "Restored", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            onBackPressed();
                        }
                    });
        }


    }


    public void setList() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(RecycleBin).equalTo(true).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        reactionItemArray.clear();
                        if (!dataSnapshot.hasChildren()) {
                            norecordslayout.setVisibility(View.VISIBLE);
                            recyclebingrid.setVisibility(View.GONE);
                            bottom_layout.setVisibility(View.GONE);
                            selectxt.setVisibility(View.GONE);
                            // recyclebinAdapter.notifyDataSetChanged();
                            return;
                        }
                        norecordslayout.setVisibility(View.GONE);
                        recyclebingrid.setVisibility(View.VISIBLE);
                        selectxt.setVisibility(View.VISIBLE);

                        for (DataSnapshot recyclebinitem : dataSnapshot.getChildren()) {
                            ReactionTrendItem reactionTrendItem = recyclebinitem.getValue(ReactionTrendItem.class);
                            if (!reactionTrendItem.isIsDeleted())
                                reactionItemArray.add(reactionTrendItem);
                        }
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int gridWidth = displayMetrics.widthPixels;
                        int imageWidth = gridWidth / 4;
                        recyclebingrid.setColumnWidth(imageWidth);
                        recyclebinAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        norecordstxt.setVisibility(View.VISIBLE);
                        recyclebingrid.setVisibility(View.GONE);
                    }
                });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }
}
