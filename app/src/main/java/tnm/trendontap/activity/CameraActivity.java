package tnm.trendontap.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;

import tnm.trendontap.R;

import tnm.trendontap.fragment.GalleryFragment;
import tnm.trendontap.utility.UniversalImageLoader;

import static tnm.trendontap.fragment.CameraFragment.camera_Pref;

public class CameraActivity extends FragmentActivity {
    public static FragmentManager manager;
    FrameLayout content_frame;
    boolean isFromBuyers;
    SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mPreferences = getSharedPreferences(camera_Pref, 0);

        isFromBuyers = getIntent().getBooleanExtra("buyers", true);

        manager = getSupportFragmentManager();
        content_frame = (FrameLayout) findViewById(R.id.content_frame_camera_activity);

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("imagePath", "");
        editor.apply();

       /* if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 105);
        }*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 105);
        } else {
            GalleryFragment fragment = new GalleryFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("buyers", isFromBuyers);
            fragment.setArguments(bundle);
            if (isFromBuyers)
                loadContentFragment(fragment, true);
            else
                MainActivity.loadContentFragment(fragment, false);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 105) {
            int access_camera = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int access_write = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_camera != PackageManager.PERMISSION_GRANTED && access_write != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(CameraActivity.this, "Sorry!!!, you can't open camera without granting permission", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 105);
            } else {
                GalleryFragment fragment = new GalleryFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", isFromBuyers);
                fragment.setArguments(bundle);
                if (isFromBuyers)
                    loadContentFragment(fragment, true);
                else
                    MainActivity.loadContentFragment(fragment, false);
            }

        }

    }

    public static void loadContentFragment(Fragment fragment, boolean flag, Bundle... bundles) {
        String backStateName = fragment.getClass().getName();
        if (bundles.length > 0) {
            // If any data is present
            fragment.setArguments(bundles[0]);
        }
        FragmentTransaction ft = manager.beginTransaction();
        if (flag) {
            ft.add(R.id.content_frame_camera_activity, fragment);
        } else {
            ft.replace(R.id.content_frame_camera_activity, fragment, backStateName);
            ft.addToBackStack(backStateName);
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        manageBack(this, "");
        // super.onBackPressed();
    }


    public static void manageBack(Activity activity, String query) {

        if (manager.getBackStackEntryCount() > 0) {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            fragmentClassName = fragmentClassName.substring(fragmentClassName.lastIndexOf(".") + 1);
            switch (fragmentClassName) {
                case "UseImageFragment":
                    /*if (!query.equalsIgnoreCase("addmore")) {
                        SharedPreferences mPreferences = activity.getSharedPreferences(camera_Pref, 0);
                        SharedPreferences.Editor editor = mPreferences.edit();
                        editor.putString("imagePath", "");
                        editor.apply();
                    }*/
                    manager.popBackStack();  // lock back button
                    break;
                default:
                    manager.popBackStack();
            }
        } else {
            activity.finish();
            activity.overridePendingTransition(R.anim.stay, R.anim.slide_down);
        }

    }

    public static Fragment getVisibleFragment() {
        Fragment f = manager.findFragmentById(R.id.content_frame_camera_activity);
        return f;
    }

}
