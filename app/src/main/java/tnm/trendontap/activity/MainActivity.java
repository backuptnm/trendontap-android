package tnm.trendontap.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.services.drive.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.fragment.ActivityList_Fragment;
import tnm.trendontap.fragment.Buyers_Fragment;
import tnm.trendontap.fragment.Buyers_Love_Fragment;
import tnm.trendontap.fragment.Buyers_Trend_Fragment;
import tnm.trendontap.fragment.Connection_Fragment;
import tnm.trendontap.fragment.Loves_Fragment;
import tnm.trendontap.fragment.More_Fragment;
import tnm.trendontap.fragment.MyReactionFragment;
import tnm.trendontap.fragment.Supplier_Fragment;
import tnm.trendontap.fragment.Trends_Fragment;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.BadgeUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.UniversalImageLoader;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.fragment.CameraFragment.camera_Pref;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetID;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.Login_Pref;

public class MainActivity extends AppCompatActivity {
    public static FragmentManager manager;
    FrameLayout content_frame;
    public static ImageView trends, suppliers, camera, loves, more, activity, buyers;
    public static TextView trendstxt, supplierstxt, cameratxt, lovestxt, moretxt, activitytxt, buyerstxt, activitycnt;
    public static LinearLayout trendslay, supplierslay, cameralay, loveslay, morelay, buyerslay;
    public static CoordinatorLayout mainwrapper;
    public static RelativeLayout activitylay;
    Preference preference;
    Users users = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        manager = getSupportFragmentManager();
        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        mainwrapper = (CoordinatorLayout) findViewById(R.id.mainwrapper);
        content_frame = (FrameLayout) findViewById(R.id.content_frame);
        trends = (ImageView) findViewById(R.id.trends);
        buyers = (ImageView) findViewById(R.id.buyers);
        suppliers = (ImageView) findViewById(R.id.suppliers);
        camera = (ImageView) findViewById(R.id.camera);
        loves = (ImageView) findViewById(R.id.loves);
        more = (ImageView) findViewById(R.id.more);
        activity = (ImageView) findViewById(R.id.activity);
        trendstxt = (TextView) findViewById(R.id.trendstxt);
        trendstxt.setTypeface(RobotoFont.regularFont(this));
        buyerstxt = (TextView) findViewById(R.id.buyerstxt);
        buyerstxt.setTypeface(RobotoFont.regularFont(this));
        activitycnt = (TextView) findViewById(R.id.activitycnt);
        activitycnt.setTypeface(RobotoFont.regularFont(this));
        supplierstxt = (TextView) findViewById(R.id.supplierstxt);
        supplierstxt.setTypeface(RobotoFont.regularFont(this));
        cameratxt = (TextView) findViewById(R.id.cameratxt);
        cameratxt.setTypeface(RobotoFont.regularFont(this));
        lovestxt = (TextView) findViewById(R.id.lovestxt);
        lovestxt.setTypeface(RobotoFont.regularFont(this));
        moretxt = (TextView) findViewById(R.id.moretxt);
        moretxt.setTypeface(RobotoFont.regularFont(this));
        activitytxt = (TextView) findViewById(R.id.activitytxt);
        activitytxt.setTypeface(RobotoFont.regularFont(this));

        trendslay = (LinearLayout) findViewById(R.id.trendslay);
        buyerslay = (LinearLayout) findViewById(R.id.buyerslay);
        supplierslay = (LinearLayout) findViewById(R.id.supplierslay);
        cameralay = (LinearLayout) findViewById(R.id.cameralay);
        loveslay = (LinearLayout) findViewById(R.id.loveslay);
        morelay = (LinearLayout) findViewById(R.id.morelay);
        activitylay = (RelativeLayout) findViewById(R.id.activitylay);

        trendslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("Trends_Fragment"))
                        return;
                } catch (Exception e) {
                }
                resetAll(MainActivity.this);
                trends.setImageResource(R.mipmap.trends_icon_active);
                trendstxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                try {
                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {

                }
                if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
                    loadContentFragment(new Trends_Fragment(), false);
                } else {
                    Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("buyers", false);
                    fragment.setArguments(bundle);
                    loadContentFragment(fragment, false);
                }*/
                getTrendsScreen("");
            }
        });
        buyerslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("Buyers_Fragment"))
                        return;
                } catch (Exception e) {
                }
                resetAll(MainActivity.this);
                buyers.setImageResource(R.mipmap.suppliers_icon_active);
                buyerstxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                try {
                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {

                }
                loadContentFragment(new Buyers_Fragment(), false);
            }
        });
        supplierslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("Supplier_Fragment"))
                        return;
                } catch (Exception e) {
                }

                resetAll(MainActivity.this);
                suppliers.setImageResource(R.mipmap.suppliers_icon_active);
                supplierstxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                try {
                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {

                }
                loadContentFragment(new Supplier_Fragment(), false);
            }
        });
        cameralay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CameraActivity.class).putExtra("buyers", true));
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        activitylay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("ActivityList_Fragment"))
                        return;
                } catch (Exception e) {
                }

                resetAll(MainActivity.this);
                activity.setImageResource(R.mipmap.activity_2);
                activity.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                activitytxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                try {
                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {

                }
                loadContentFragment(new ActivityList_Fragment(), false);
            }
        });

        loveslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("Loves_Fragment"))
                        return;
                } catch (Exception e) {
                }
                resetAll(MainActivity.this);
                loves.setImageResource(R.mipmap.loves_icon_active);
                lovestxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));

                try {
                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {

                }
                if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
                    loadContentFragment(new Loves_Fragment(), false);
                } else {
                    loadContentFragment(new Buyers_Love_Fragment(), false);
                }*/

                getLovesScreen("");
            }
        });
        morelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String fragmentClassName = getVisibleFragment().getClass().getName();
                    if (fragmentClassName.contains("More_Fragment"))
                        return;
                } catch (Exception e) {
                }
                resetAll(MainActivity.this);
                more.setImageResource(R.mipmap.more_icon_active);
                moretxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
                loadContentFragment(new More_Fragment(), false);
            }
        });

        UniversalImageLoader universalImageLoader = new UniversalImageLoader(this);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());

        boolean isnotification = false;
        if (getIntent().getExtras() != null) {
            try {
                if (getIntent().getStringExtra("issentNotification") != null && getIntent().getStringExtra("issentNotification").equalsIgnoreCase("true")) {
                    isnotification = true;
                }
            } catch (Exception e) {

            }
        }
        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            supplierslay.setVisibility(View.VISIBLE);
            cameralay.setVisibility(View.VISIBLE);
            buyerslay.setVisibility(View.GONE);
            activitylay.setVisibility(View.GONE);
            trendstxt.setText("Trends");
            lovestxt.setText("Loves");
            if (isnotification) {
                redirectType(Utils.Buyer);
            } else {
                loadContentFragment(new Trends_Fragment(), true);
            }
        } else {
            buyerslay.setVisibility(View.VISIBLE);
            supplierslay.setVisibility(View.GONE);
            activitylay.setVisibility(View.VISIBLE);
            activity.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textunselected));
            cameralay.setVisibility(View.GONE);
            trendstxt.setText("Buyer Trends");
            lovestxt.setText("Buyer Loves");
            if (isnotification) {
                redirectType(Utils.Supplier);
            } else {
                Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", false);
                fragment.setArguments(bundle);
                loadContentFragment(fragment, true);
            }
        }
    }

    public void getLovesScreen(String postid) {
        try {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            if (fragmentClassName.contains("Loves_Fragment"))
                return;
        } catch (Exception e) {
        }
        resetAll(MainActivity.this);
        loves.setImageResource(R.mipmap.loves_icon_active);
        lovestxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));

        try {
            if (MainActivity.manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }
        Bundle bundle = new Bundle();
        bundle.putString("postid", postid);
        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            loadContentFragment(new Loves_Fragment(), false, bundle);
        } else {
            loadContentFragment(new Buyers_Love_Fragment(), false, bundle);
        }
    }

    public void getTrendsScreen(String postid) {
        try {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            if (fragmentClassName.contains("Trends_Fragment"))
                return;
        } catch (Exception e) {
        }
        resetAll(MainActivity.this);
        trends.setImageResource(R.mipmap.trends_icon_active);
        trendstxt.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textselected));
        try {
            if (MainActivity.manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }
        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            Trends_Fragment fragment = new Trends_Fragment();
            Bundle bundle = new Bundle();
            bundle.putString("postid", postid);
            fragment.setArguments(bundle);
            loadContentFragment(fragment, false, bundle);
        } else {
            Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("buyers", false);
            bundle.putString("postid", postid);
            fragment.setArguments(bundle);
            loadContentFragment(fragment, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*getBadgeCount(activitycnt, MainActivity.this);*/


        /*users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                activitycnt.setText(String.valueOf(users.getBadgeCount()));
                activitycnt.setVisibility(View.VISIBLE);
            } else {
                activitycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            activitycnt.setVisibility(View.GONE);
        }*/


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.onNotificationReceived");
        NotifyReceiver receiver = new NotifyReceiver();
        registerReceiver(receiver, intentFilter);
    }

    public static Fragment getVisibleFragment() {
        Fragment f = manager.findFragmentById(R.id.content_frame);
        return f;
    }

    public static void resetAll(Context context) {
        trends.setImageResource(R.mipmap.trends_icon);
        suppliers.setImageResource(R.mipmap.suppliers_icon);
        buyers.setImageResource(R.mipmap.suppliers_icon);
        camera.setImageResource(R.mipmap.camera_icon);
        loves.setImageResource(R.mipmap.loves_icon);
        more.setImageResource(R.mipmap.more_icon);
        activity.setImageResource(R.mipmap.activity_icon);
        activity.setColorFilter(ContextCompat.getColor(context, R.color.textunselected));
        activitytxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        buyerstxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        trendstxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        supplierstxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        cameratxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        lovestxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
        moretxt.setTextColor(ContextCompat.getColor(context, R.color.textunselected));
    }

    public static void showSnackbarError(String msg) {
        Snackbar snackbar = Snackbar.make(mainwrapper, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    public static void manageBack(Activity activity, String query) {

        if (manager.getBackStackEntryCount() > 0) {
            String fragmentClassName = getVisibleFragment().getClass().getName();
            fragmentClassName = fragmentClassName.substring(fragmentClassName.lastIndexOf(".") + 1);
            switch (fragmentClassName) {
                case "Trends_Fragment":
                case "Supplier_Fragment":
                case "Loves_Fragment":
                case "More_Fragment":
                case "ActivityList_Fragment":
                case "Buyers_Fragment":
                case "Buyers_Trend_Fragment":
                case "Buyers_Love_Fragment":
                    finishApp(activity);
                    break;
                case "UseImageFragment":
                    /*if (!query.equalsIgnoreCase("addmore")) {
                        SharedPreferences mPreferences = activity.getSharedPreferences(camera_Pref, 0);
                        SharedPreferences.Editor editor = mPreferences.edit();
                        editor.putString("imagePath", "");
                        editor.apply();
                    }*/
                    manager.popBackStack();
                    break;
                default:
                    manager.popBackStack();
                    break;
            }
        } else {
            finishApp(activity);
        }

    }

    static boolean doubleBackToExitPressedOnce = false;

    public static void finishApp(Activity activity) {
        if (doubleBackToExitPressedOnce) {
            activity.finishAffinity();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(activity, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    public static void loadContentFragment(Fragment fragment, boolean flag, Bundle... bundles) {
        String backStateName = fragment.getClass().getName();
        if (bundles.length > 0) {
            // If any data is present
            fragment.setArguments(bundles[0]);
        }
        FragmentTransaction ft = manager.beginTransaction();
        if (flag) {
            ft.add(R.id.content_frame, fragment);
        } else {
            ft.replace(R.id.content_frame, fragment, backStateName);
            ft.addToBackStack(backStateName);
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        manageBack(this, "");
    }


    public static void getBadgeCount(final TextView textView, final Context context) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathUsers);
        Query query = databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users user = dataSnapshot.getValue(Users.class);
                int badgeCount = 0;
                try {
                    badgeCount = (int) user.getBadgeCount();
                    if (badgeCount == 0) {
                        textView.setVisibility(View.GONE);
                        BadgeUtils.clearBadge(context);

                    } else {
                        textView.setText(String.valueOf(badgeCount));
                        textView.setVisibility(View.VISIBLE);
                        BadgeUtils.setBadge(context, badgeCount);
                    }
                } catch (Exception e) {
                    textView.setVisibility(View.GONE);
                    BadgeUtils.clearBadge(context);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                BadgeUtils.clearBadge(context);
            }
        });

    }


    public void redirectType(String userrole) {
        String type = getIntent().getStringExtra("type");
        switch (type) {
            case "like_trend":
                like_trend(this, getIntent().getStringExtra("postid"));
                break;
            case "like_love":
                like_love(this, getIntent().getStringExtra("postid"));
                break;
            case "BuyerFollower":
                loadbuyerinvitation(this, users.getUserID());
                break;
            case "trend":
                MainActivity.trendslay.performClick();
                break;
            case "love":
                MainActivity.loveslay.performClick();
                break;
            case "Invitation":
                loadinvitation(this);
                break;
            case "reaction":
                loadContentFragment(new Trends_Fragment(), true);
                Intent intent = new Intent(MainActivity.this, Reactions_Trends_Love_Activity.class);
                intent.putExtra("postid", getIntent().getStringExtra("postid"));
                intent.putExtra("from", getIntent().getStringExtra("from"));
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case "sample":
            case "sample_sent":
                if (userrole.equalsIgnoreCase(Utils.Supplier)) {
                    resetAll(this);

                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    if (getIntent().getStringExtra("from").equalsIgnoreCase("love")) {
                        trends.setImageResource(R.mipmap.trends_icon_active);
                        trendstxt.setTextColor(ContextCompat.getColor(this, R.color.textselected));
                        loadContentFragment(new Buyers_Love_Fragment(), false);
                    } else {
                        loves.setImageResource(R.mipmap.loves_icon_active);
                        lovestxt.setTextColor(ContextCompat.getColor(this, R.color.textselected));
                        Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("buyers", false);
                        fragment.setArguments(bundle);
                        loadContentFragment(fragment, false);
                    }

                    redirectSupplierSample(MainActivity.this, getIntent().getStringExtra("postid"),
                            getIntent().getStringExtra("from"));

                } else {
                    resetAll(this);

                    if (MainActivity.manager.getBackStackEntryCount() > 0)
                        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    if (getIntent().getStringExtra("from").equalsIgnoreCase("love")) {
                        loves.setImageResource(R.mipmap.loves_icon_active);
                        lovestxt.setTextColor(ContextCompat.getColor(this, R.color.textselected));
                        loadContentFragment(new Loves_Fragment(), false);
                    } else {
                        trends.setImageResource(R.mipmap.trends_icon_active);
                        trendstxt.setTextColor(ContextCompat.getColor(this, R.color.textselected));
                        loadContentFragment(new Trends_Fragment(), true);
                    }
                    redirectBuyerSample(MainActivity.this, getIntent().getStringExtra("postid"),
                            getIntent().getStringExtra("from"));
                }
                break;
            case "costing sheet":
            case "costing sheet_sent":
                if (userrole.equalsIgnoreCase(Utils.Supplier)) {
                    Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("buyers", false);
                    fragment.setArguments(bundle);
                    loadContentFragment(fragment, true);
                } else {
                    loadContentFragment(new Trends_Fragment(), true);
                }
                redirectCostingsheet(MainActivity.this, getIntent().getStringExtra("postid"), getIntent().getStringExtra("reactionId"), getIntent().getStringExtra("userId"));
                break;

            case "comment":
            case "PostComment":
                if (userrole.equalsIgnoreCase(Utils.Supplier)) {
                    Buyers_Trend_Fragment fragment = new Buyers_Trend_Fragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("buyers", false);
                    fragment.setArguments(bundle);
                    loadContentFragment(fragment, true);
                } else {
                    loadContentFragment(new Trends_Fragment(), true);
                }

                redirectComment(MainActivity.this, getIntent().getStringExtra("reactionId"), getIntent().getStringExtra("postid"),
                        getIntent().getStringExtra("userId"), getIntent().getStringExtra("from"),
                        getIntent().getStringExtra("post_img"));
                break;
        }
    }


    public static void redirectBuyerSample(Context context, String postid, String from) {
        Intent intent = new Intent(context, Reactions_Trends_Love_Activity.class);
        intent.putExtra("postid", postid);
        intent.putExtra("from", from);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
    }

    public static void redirectSupplierSample(Context context, String postid, String from) {

        Intent intent = new Intent(context, Myreactions_Trends_Love_Activity.class);
        intent.putExtra("postid", postid);
        intent.putExtra("from", from);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);

    }

    public static void redirectComment(Context context, String reactionId, String postid, String userId, String from, String post_img) {
        boolean is_from_post = false;
        try {
            if (reactionId.equalsIgnoreCase("")) {
                is_from_post = true;
            }
        } catch (Exception e) {

        }
        context.startActivity(new Intent(context, CommentActivity.class).
                putExtra("post_id", postid).
                putExtra("reaction_id", reactionId).
                putExtra("is_from_post", is_from_post).
                putExtra("sento", userId).
                putExtra("from", from).
                putExtra("post_img", post_img));
        ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
    }


    public static void redirectCostingsheet(final Context context, final String postid, final String reactionid, final String userid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).child(reactionid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String costsheetid = "";
                if (dataSnapshot.hasChild(CostsheetID))
                    costsheetid = String.valueOf(dataSnapshot.child(CostsheetID).getValue());

                Intent costingsheet = new Intent(context, CostingSheet_Activity.class);

                costingsheet.putExtra("post_id", postid).
                        putExtra("Reaction_Id", reactionid).
                        putExtra("postimg", String.valueOf(dataSnapshot.child(tPostPhotoId).child("0").getValue())).
                        putExtra("sentto", userid).
                        putExtra("costsheet_id", costsheetid);
                context.startActivity(costingsheet);
                ((Activity) context).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public static void like_trend(Context context, String postid) {
        resetAll(context);
        trends.setImageResource(R.mipmap.trends_icon_active);
        trendstxt.setTextColor(ContextCompat.getColor(context, R.color.textselected));
        try {
            if (manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }

        Bundle bundle = new Bundle();
        bundle.putString("postid", postid);
        loadContentFragment(new Trends_Fragment(), false, bundle);
    }

    public static void like_love(Context context, String postid) {
        resetAll(context);
        loves.setImageResource(R.mipmap.loves_icon_active);
        lovestxt.setTextColor(ContextCompat.getColor(context, R.color.textselected));

        try {
            if (manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }
        Bundle bundle = new Bundle();
        bundle.putString("postid", postid);
        loadContentFragment(new Loves_Fragment(), false, bundle);
    }


    public static void loadinvitation(Context context) {
        resetAll(context);
        buyers.setImageResource(R.mipmap.suppliers_icon_active);
        buyerstxt.setTextColor(ContextCompat.getColor(context, R.color.textselected));
        try {
            if (manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }

        Bundle bundle = new Bundle();
        bundle.putString("invitation", "yes");
        loadContentFragment(new Buyers_Fragment(), false, bundle);
    }


    public static void loadbuyerinvitation(Context context, String userid) {
        resetAll(context);
        buyers.setImageResource(R.mipmap.suppliers_icon_active);
        buyerstxt.setTextColor(ContextCompat.getColor(context, R.color.textselected));
        try {
            if (manager.getBackStackEntryCount() > 0)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {

        }
        Bundle bundle = new Bundle();
        bundle.putString("buyerinvitation", "yes");
        bundle.putString("UserID", userid);
        loadContentFragment(new Connection_Fragment(), false, bundle);
    }

    private class NotifyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        /*if (intent.getIntExtra("badgecnt", 0) == 0) {
                            activitycnt.setVisibility(View.GONE);
                        } else {
                            activitycnt.setVisibility(View.VISIBLE);
                            activitycnt.setText(String.valueOf(intent.getIntExtra("badgecnt", 0)));
                        }*/
                        MainActivity.getBadgeCount(MainActivity.activitycnt, context);

                    } catch (Exception e) {

                    }
                }
            });


        }
    }


}
