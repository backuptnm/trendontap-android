package tnm.trendontap.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.ActivityAdapter;
import tnm.trendontap.adapter.CategoryAdapter;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.LovesCategoryItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.R.id.activitylist;
import static tnm.trendontap.R.id.labeltxt;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathLovesCategory;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserLovesCategory;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.Utils.hidekeyboard;

/**
 * Created by Desap on 15-11-2017.
 */

public class CategoriesActivity extends AppCompatActivity {

    TextView labeltxt;
    ListView categorylist;
    ArrayList<LovesCategoryItem> activityItemArrayList;
    CategoryAdapter mCategoryAdapter;
    ImageView categories_icon, addition_icon;
    DatabaseReference databaseReference, databaseReference1;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUserLovesCategory).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(FirebasePathLovesCategory);
        databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathLovesCategory);
        categories_icon = (ImageView) findViewById(R.id.categories_icon);
        addition_icon = (ImageView) findViewById(R.id.addition_icon);
        addition_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCategoryDialog();
            }
        });
        activityItemArrayList = new ArrayList<>();
        if (getIntent().getBooleanExtra("isFilter", false))
            activityItemArrayList.add(new LovesCategoryItem("All", "All", 0, ""));


        categories_icon.setColorFilter(Color.parseColor("#000000"));
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        labeltxt.setText("Categories");
        categorylist = (ListView) findViewById(R.id.categorylist);
        categories_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mCategoryAdapter = new CategoryAdapter(CategoriesActivity.this, activityItemArrayList);
        categorylist.setAdapter(mCategoryAdapter);
        categorylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent();
                intent.putExtra("categoryId", activityItemArrayList.get(i).getId());
                intent.putExtra("categoryName", activityItemArrayList.get(i).getName());

                setResult(RESULT_OK, intent);
                finish();

               /* if (getIntent().getBooleanExtra("isFilter", false)) {

                } else {
                    Intent intent = new Intent();
                    intent.putExtra("categoryId", activityItemArrayList.get(i).getId());
                    intent.putExtra("categoryName", activityItemArrayList.get(i).getName());

                    setResult(RESULT_OK, intent);
                    finish();
                }*/
              /*  if (getIntent().hasExtra("from")) {
                    if (getIntent().getStringExtra("from").equals("UploadLoveFragment")) {

                    }

                }*/

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCategory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    public void setCategory() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        FirebaseDatabase.getInstance().getReference().child(FirebasePathLovesCategory)
                .orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot categorylist : dataSnapshot.getChildren()) {
                                LovesCategoryItem lovesCategoryItem = categorylist.getValue(LovesCategoryItem.class);
                                try {
                                    int pos = matchValue(activityItemArrayList, lovesCategoryItem.getId());
                                    if (pos == -1)
                                        activityItemArrayList.add(lovesCategoryItem);
                                } catch (Exception e) {
                                    activityItemArrayList.add(lovesCategoryItem);
                                }
                            }
                        }
                        mCategoryAdapter.addList(activityItemArrayList);
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }


    public void addCategoryDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_add_category);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(this));

        final EditText edtname = (EditText) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(this));
        // edtname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edtname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) ;
                {
                    saveCatgories(edtname, dialog);

                }
                return false;
            }
        });

        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setText("OK");
        done_action.setTypeface(RobotoFont.boldFont(this));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCatgories(edtname, dialog);
                dialog.dismiss();
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(this));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public int matchValue(ArrayList<LovesCategoryItem> lovesCategoryItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < lovesCategoryItemArrayList.size(); j++) {
            if (lovesCategoryItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    public void saveCatgories(EditText edtname, Dialog dialog) {
        String name = edtname.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Please enter category Name", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean flag = false;
        for (int j = 0; j < activityItemArrayList.size(); j++) {
            if (activityItemArrayList.get(j).getName().trim().equalsIgnoreCase(name)) {
                flag = true;
                break;
            }
        }

        if (flag) {
            Toast.makeText(this, "Category already exist", Toast.LENGTH_LONG).show();
            return;
        }

        String id = databaseReference.push().getKey();
        databaseReference.child(id).setValue(true);
        long tsLong = System.currentTimeMillis();
        LovesCategoryItem lovesCategoryItem = new LovesCategoryItem(id, name, tsLong, FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference1.child(id).setValue(lovesCategoryItem);

        hidekeyboard(this, edtname);
        if (dialog.isShowing())
            dialog.dismiss();


    }


}
