package tnm.trendontap.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.ReactionsTrendAdapter;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.PostID;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 3/30/2018.
 */

public class Reactions_Trends_Love_Activity extends AppCompatActivity {
    TextView labeltxt, name, type, title, category, takelook, notifycnt;
    LinearLayout back_arrow;
    ImageView trendimg;
    RelativeLayout activity2_icon_lay;
    ListView listview_reaction_list;
    ReactionsTrendAdapter reactionsTrendAdapter;
    ArrayList<ReactionTrendItem> reactionItemArrayList;
    Preference preference;
    Users users;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reactions_trends);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        labeltxt.setText("Reactions");

        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(this));

        View headerview = LayoutInflater.from(this).inflate(R.layout.reactions_trend_header, null, false);
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(this));
        name.setText(getName(users.getFirstName(), users.getLastName()));

        trendimg = (ImageView) headerview.findViewById(R.id.trendimg);

        type = (TextView) headerview.findViewById(R.id.type);
        type.setTypeface(RobotoFont.regularFont(this));
        type.setText(users.getJobTitle());

        title = (TextView) headerview.findViewById(R.id.title);
        title.setTypeface(RobotoFont.regularFont(this));


        category = (TextView) headerview.findViewById(R.id.category);
        category.setTypeface(RobotoFont.regularFont(this));


        takelook = (TextView) headerview.findViewById(R.id.takelook);
        takelook.setTypeface(RobotoFont.regularFont(this));

        back_arrow = (LinearLayout) findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        activity2_icon_lay = (RelativeLayout) findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Reactions_Trends_Love_Activity.this, ActivityList.class));
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        listview_reaction_list = (ListView) findViewById(R.id.listview_reaction_list);
        reactionItemArrayList = new ArrayList<>();
        reactionsTrendAdapter = new ReactionsTrendAdapter(this, reactionItemArrayList,
                "", getIntent().getStringExtra("from"));
        // listview_reaction_list.addHeaderView(headerview);
        listview_reaction_list.setAdapter(reactionsTrendAdapter);
        // processPost(getIntent().getStringExtra("postid"));
        //setReactionList();

        getCountReaction(getIntent().getStringExtra("postid"));
    }

   /* @Override
    public void onResume() {
        super.onResume();
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt, this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    public void processPost(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(postid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                title.setText(String.valueOf(dataSnapshot.child(PostTitle).getValue()));
                category.setText(String.valueOf(dataSnapshot.child(CatagoryName).getValue()));
                String postimg = String.valueOf(dataSnapshot.child(tPostPhotoId).child("0").getValue());
                if (!TextUtils.isEmpty(postimg)) {
                    Glide.with(getApplicationContext())
                            .load(postimg)
                            .into(trendimg);
                }
                reactionsTrendAdapter.setPosturl(postimg);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void getCountReaction(final String postid) {

        reactionItemArrayList.clear();

        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        getReactionList(postid);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void getReactionList(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if (Utils.shouldShow(dataSnapshot,users.getUserType())) {
                            ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                            reactionItemArrayList.add(reactionTrendItem1);
                            reactionsTrendAdapter.addList(reactionItemArrayList);
                            setUserData(reactionItemArrayList, String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()), reactionTrendItem1, "add");
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (Utils.shouldShow(dataSnapshot,users.getUserType())) {
                            ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                            int pos = matchValue(reactionItemArrayList, reactionTrendItem1.getId());
                            if (pos == -1) {
                                return;
                            }
                            reactionItemArrayList.set(pos, reactionTrendItem1);
                            reactionsTrendAdapter.notifyDataSetChanged();
                            setUserData(reactionItemArrayList, String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()), reactionTrendItem1, "edit");
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                        int pos = matchValue(reactionItemArrayList, reactionTrendItem1.getId());
                        if (pos == -1) {
                            return;
                        }
                        reactionItemArrayList.remove(pos);
                        reactionsTrendAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void setUserData(final ArrayList<ReactionTrendItem> reactionItemArrayList, String userid, final ReactionTrendItem reactionTrendItem1, String action) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);

                        reactionTrendItem1.setUsername(getName(users.getFirstName(), users.getLastName()));
                        reactionTrendItem1.setUserrole(users.getJobTitle());
                        reactionTrendItem1.setUserphoto(users.getProfilePic());
                        reactionTrendItem1.setUsercompany(users.getCompanyName());

                        int pos = matchValue(reactionItemArrayList, reactionTrendItem1.getId());
                        if (pos == -1)
                            return;
                        reactionItemArrayList.set(pos, reactionTrendItem1);
                        //  reactionItemArrayList.add(reactionTrendItem1);
                        reactionsTrendAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public int matchValue(ArrayList<ReactionTrendItem> reactionItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < reactionItemArrayList.size(); j++) {
            if (reactionItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

}
