package tnm.trendontap.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import tnm.trendontap.R;
import tnm.trendontap.modal.Position;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.utility.RobotoFont;

public class TagItemActivity extends AppCompatActivity implements View.OnDragListener, View.OnTouchListener {

    TextView labeltxt, labelDone, tvTag;
    ImageView back_arrow, imageTag;
    RelativeLayout rlImageTag;
    private Dialog m_dialog;
    ArrayList<Tag_Items> tag_items;
    float dX;
    float dY;
    int lastAction;
    int itemCount = 0;
    String LOGCAT = "dragevent";



    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_item);
        tag_items = new ArrayList<>();
        rlImageTag = (RelativeLayout) findViewById(R.id.rlImageTag);
        imageTag = (ImageView) findViewById(R.id.imageTag);
        rlImageTag.setOnDragListener(this);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labelDone = (TextView) findViewById(R.id.labelDone);
        tvTag = (TextView) findViewById(R.id.tvTag);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        labelDone.setTypeface(RobotoFont.mediumFont(this));
        tvTag.setTypeface(RobotoFont.mediumFont(this));

        boolean isBitmap = getIntent().getBooleanExtra("isBitmap", false);
        String imageUrl = getIntent().getStringExtra("imageUrl");
        if (!isBitmap) {
            RequestOptions requestOptions = new RequestOptions();
            Glide.with(this)
                    .load(imageUrl)
                    .apply(requestOptions)
                    .into(imageTag);
        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
            imageTag.setImageBitmap(bitmap);
        }

        labeltxt.setText("Tag Item");
        labelDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tag_items.size() == 0) {
                    onBackPressed();
                } else {
                    Intent intent = new Intent();
                  /*  Bundle args = new Bundle();
                    args.putSerializable("tag_items", (Serializable) tag_items);
                    intent.putExtra("bundle", args);*/
                    intent.putParcelableArrayListExtra("tag_items", tag_items);
                    setResult(RESULT_OK, intent);
                    onBackPressed();
                }
            }
        });
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        itemCount = 0;
        imageTag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(x, y);
                }
                return true;
            }
        });

        if (getIntent().hasExtra("tag_items_list")) {
            tag_items.clear();
            ArrayList<Tag_Items> tag_items1 = getIntent().getParcelableArrayListExtra("tag_items_list");
            if (tag_items1 == null) {
                tag_items = new ArrayList<>();
                return;
            }

            for (int k = 0; k < tag_items1.size(); k++) {
                Tag_Items tag_itemsval = tag_items1.get(k);
                setTagName(tag_itemsval.getPosition().getxPos(), tag_itemsval.getPosition().getyPos(), tag_itemsval.getname());
            }
        }

       /* rlImageTag.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                return false;
            }
        });*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    public void showDialog(final int x, final int y) {
        m_dialog = new Dialog(TagItemActivity.this, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater m_inflater = LayoutInflater.from(TagItemActivity.this);
        View m_view = m_inflater.inflate(R.layout.dialog_edit_tag, null);
        TextView tvTitle = (TextView) m_view.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.mediumFont(this));
        final EditText etName = (EditText) m_view.findViewById(R.id.etName);
        etName.setTypeface(RobotoFont.regularFont(this));
        Button m_btnOk = (Button) m_view.findViewById(R.id.btnOK);
        m_btnOk.setTypeface(RobotoFont.regularFont(this));
        Button m_btnCancel = (Button) m_view.findViewById(R.id.btnCancel);
        m_btnCancel.setTypeface(RobotoFont.regularFont(this));

        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    addTag(etName, x, y);
                }
                return false;
            }
        });


        etName.requestFocus();
        m_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        View.OnClickListener m_clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTag(etName, x, y);
            }
        };
        m_btnOk.setOnClickListener(m_clickListener);
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });
        m_dialog.setContentView(m_view);
        m_dialog.show();

    }


    public void addTag(EditText etName, int x, final int y) {
        String name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(TagItemActivity.this, "Please enter tag", Toast.LENGTH_LONG).show();
            return;
        }
        m_dialog.dismiss();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(TagItemActivity.this, "Please enter Tag", Toast.LENGTH_SHORT).show();
        } else {
            InputMethodManager imm = (InputMethodManager) TagItemActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
            setTagName(x, y, name);
        }
    }

    private void setTagName(final float x, final float y, final String tag) {
        Log.d("Nzm", "x=" + x + "y=" + y);
        final FrameLayout mFrame = new FrameLayout(TagItemActivity.this);
        RelativeLayout mInnerFrame = new RelativeLayout(TagItemActivity.this);
        mInnerFrame.setOnTouchListener(this);

        if (x != 0 && y != 0) {
            FrameLayout.LayoutParams mParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mFrame.setLayoutParams(mParams);
            mFrame.setBackgroundColor(Color.parseColor("#929292"));
            //  mFrame.setPadding(x, y, 0, 0);
            mFrame.setX(x);
            mFrame.setY(y);
            RelativeLayout.LayoutParams mParams1 = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mInnerFrame.setLayoutParams(mParams1);
            mInnerFrame.setPadding(10, 10, 10, 10);
            //mFrame1.setBackground(getDrawable(R.mipmap.item_tagged_text_container));
            mInnerFrame.setBackgroundColor(Color.parseColor("#000000"));

            TextView tv = new TextView(TagItemActivity.this);
            tv.setLayoutParams(mParams1);
            tv.setText(tag);
            tv.setGravity(Gravity.CENTER);
            tv.setTextSize(16);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setId(R.id.tvId);
            mInnerFrame.addView(tv);

            RelativeLayout.LayoutParams imageParam = new RelativeLayout.LayoutParams(45, 45);
            // imageParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            // imageParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            imageParam.addRule(RelativeLayout.RIGHT_OF, R.id.tvId);
            imageParam.addRule(RelativeLayout.ALIGN_PARENT_TOP, R.id.tvId);
            imageParam.setMargins(0,-10,-10,0);
            ImageView image = new ImageView(TagItemActivity.this);
            image.setId(itemCount);
            image.setTag(itemCount);
            image.setLayoutParams(imageParam);
            image.setPadding(10, 10, 10, 10);
            image.setMaxHeight(50);
            image.setVisibility(View.GONE);
            image.setImageResource(R.mipmap.close);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                image.setForegroundGravity(Gravity.RIGHT);
            }
            image.setMaxWidth(50);

            mInnerFrame.setTag(itemCount);
            mInnerFrame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    ImageView image = (ImageView) view.findViewById(pos);
                    if (image.getVisibility() == View.VISIBLE)
                        image.setVisibility(View.GONE);
                    else
                        image.setVisibility(View.VISIBLE);

                }
            });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    FrameLayout frame = (FrameLayout) ((ViewGroup) view.getParent()).getParent();
                    rlImageTag.removeView(frame);
                    tag_items.remove(pos);
                    // mdataArray.remove(pos);
                    synchronized (rlImageTag) {
                        rlImageTag.notifyAll();
                    }
                }
            });
            mInnerFrame.addView(image);
            mFrame.setTag(itemCount);
            mInnerFrame.setTag(itemCount);
            //  rlImageTag.setTag(itemCount);
            mFrame.addView(mInnerFrame);
            rlImageTag.addView(mFrame);
            mFrame.post(new Runnable() {
                @Override
                public void run() {
                    int h = mFrame.getMeasuredHeight();
                    int w = mFrame.getMeasuredWidth();

                    Position position = new Position(h, w, x, y);
                    tag_items.add(new Tag_Items(tag, position));
                    // mdataArray.put(itemCount, object);
                    itemCount += 1;
                }
            });
        }
    }

    @Override
    public boolean onDrag(View layoutview, DragEvent dragEvent) {
        int action = dragEvent.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                Log.d(LOGCAT, "Drag event started");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                Log.d(LOGCAT, "Drag event entered into " + layoutview.toString());
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                Log.d(LOGCAT, "Drag event exited from " + layoutview.toString());
                break;
            case DragEvent.ACTION_DROP:
                Log.d(LOGCAT, "Dropped");
                View view = (View) dragEvent.getLocalState();
                int pos = 0;
                try {
                    pos = (int) view.getTag();
                } catch (ClassCastException e) {
                }
                ViewGroup owner = (ViewGroup) view.getParent();
                owner.removeView(view);
                RelativeLayout container = (RelativeLayout) layoutview;
                float x = dragEvent.getX() - (view.getWidth() / 2);
                float y = dragEvent.getY() - (view.getHeight() / 2);
                view.setX(x);
                view.setY(y);
                container.addView(view);
                tag_items.get(pos).getPosition().xPos = x;
                tag_items.get(pos).getPosition().yPos = y;
                view.setVisibility(View.VISIBLE);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                Log.d(LOGCAT, "Drag ended");
                View view1 = (View) dragEvent.getLocalState();
                view1.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData clipData = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(clipData, shadowBuilder, view, 0);
            view.setVisibility(View.GONE);
            return true;
        }
        return false;
    }
}
