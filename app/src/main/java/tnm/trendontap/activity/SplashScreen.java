package tnm.trendontap.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import tnm.trendontap.R;
import tnm.trendontap.utility.BadgeUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.Utils;


public class SplashScreen extends Activity {
    // ProgressBar progressbar;
    Preference preference;
    DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private static boolean activityStarted;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Resume last Activity when opening a Notification-*/
        if (activityStarted
                && getIntent() != null
                && (getIntent().getFlags() & Intent.FLAG_ACTIVITY_REORDER_TO_FRONT) != 0) {
            finish();
            return;
        }
        activityStarted = true;

        /*---- end------*/

        setContentView(R.layout.activity_splash);
        auth = FirebaseAuth.getInstance();
        preference = new Preference(this, Utils.Login_Pref);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (auth.getCurrentUser() != null && preference.haskey("userid") && !preference.getString("userid").equalsIgnoreCase("")) {
                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                } else {
                    startActivity(new Intent(SplashScreen.this, HomeActivity.class));
                }
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                finish();
            }
        }, 2000);

    }


}
