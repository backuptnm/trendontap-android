package tnm.trendontap.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import tnm.trendontap.R;
import tnm.trendontap.fragment.TermCondition_Fragment;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.activity.HomeActivity.isPrivacy;
import static tnm.trendontap.activity.HomeActivity.isTerms;

public class TermConditionActivity extends AppCompatActivity {
    WebView webView;
    TextView canceltxt, labeltxt, acceptbtn;
    ProgressDialog progressDialog;
    String url = "http://www.trendontap.com/Terms-Of-Use";
    boolean isLoading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termcondition);

        canceltxt = (TextView) findViewById(R.id.canceltxt);
        canceltxt.setTypeface(RobotoFont.regularFont(this));
        canceltxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        acceptbtn = (TextView) findViewById(R.id.acceptbtn);
        acceptbtn.setTypeface(RobotoFont.mediumFont(this));

        if (isTerms && isPrivacy)
            acceptbtn.setVisibility(View.GONE);
        else
            acceptbtn.setVisibility(View.VISIBLE);

        acceptbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTerms = true;
                if (isPrivacy)
                    onBackPressed();
                else {
                    startActivity(new Intent(TermConditionActivity.this, PrivacyPolicy.class));
                    overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                    finish();
                }
            }
        });
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        //
        progressDialog = new ProgressDialog(TermConditionActivity.this);
        progressDialog.setMessage("Loading...");


        if (!isLoading) {
            isLoading = true;
            new MyAsynTask().execute();

        }
        //  startWebView("http://www.trendontap.com/Terms-Of-Use");
    }


   /* private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {


            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
            }

            public void onPageFinished(WebView view, String url) {
                try {

                    webView.loadUrl("javascript:document.getElementsByTagName(\"hedaer\").style.display = \"none\";");
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();

                    }
                } catch (Exception exception) {
                }
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        *//*
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        *//*

     *//*
         String summary = "<html><body>You scored <b>192</b> points.</body></html>";
         webview.loadData(summary, "text/html", null);
         *//*

        //Load url in webview
        webView.loadUrl(url);


    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    private class MyAsynTask extends AsyncTask<Void, Void, Document> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !progressDialog.isShowing())
                progressDialog.show();
        }

        @Override
        protected Document doInBackground(Void... voids) {

            Document document = null;
            try {
                document = Jsoup.connect(url).get();
                document.getElementsByTag("header").remove();
                document.getElementsByClass("container").attr("style","padding-top: 10px !important");

            } catch (IOException e) {
                e.printStackTrace();
            }
            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            super.onPostExecute(document);
            isLoading = false;
            webView.loadDataWithBaseURL(url, document.toString(), "text/html", "utf-8", "");
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, request);
                }
            });
        }
    }
}
