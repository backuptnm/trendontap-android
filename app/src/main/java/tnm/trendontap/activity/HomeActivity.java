package tnm.trendontap.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.onesignal.OneSignal;

import java.util.HashMap;

import tnm.trendontap.R;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.AppVersion;
import static tnm.trendontap.utility.FireStoreUtils.BadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.Buyers;
import static tnm.trendontap.utility.FireStoreUtils.BuyersOfSupplier;
import static tnm.trendontap.utility.FireStoreUtils.DeviceType;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.InvitedSupplierEmailID;
import static tnm.trendontap.utility.FireStoreUtils.InvitedSuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.IsInviationAccepted;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.OneSingleUserID;
import static tnm.trendontap.utility.FireStoreUtils.SupplierInvitations;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;
import static tnm.trendontap.utility.FireStoreUtils.VerificationCode;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.isValidEmail;


public class HomeActivity extends Activity {
    TextView logintxt, activatecodetxt, trendsinstantlytxt, logintitletxt, nexttxt, registertitle,
            tvforgotPassword, digitstext, activationtxt, activationlogintxt, regtxt, tvTermsAndCondition, activate;
    LinearLayout login_btn, next_btn, rlactivationcode, activation_next_btn, reg_btn, rlLogin,termuselayout;
    Preference preference;
    ImageView backimg, backimgactivation, passwordicon, registerback, reg_pwdicon, reg_confpwdicon;
    RelativeLayout rlMainHome, mainlayout;
    ScrollView registerlayout;
    EditText etEmail, etPassword, passcode, reg_fname, reg_lname, reg_jobtitle, reg_company, reg_pwd, reg_confpwd;
    DatabaseReference databaseReference;
    InputMethodManager imgr;
    ProgressDialog progressDialog;
    CheckBox checktermcond;
    String passcodid = "", passcodemail = "", buyerid = "";
    boolean isPasswordVisible, isregPasswordVisible, isregconfPasswordVisible, isalready = false;
    Animation loginfadeOut, activecodefadeOut, loginfadeIn, activecodeadeIn;
    private FirebaseAuth auth;
    boolean fromLogin = false;

    static boolean isTerms = false, isPrivacy = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // Window w = getWindow();
        // w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        auth = FirebaseAuth.getInstance();
        termuselayout = (LinearLayout) findViewById(R.id.termuselayout);
        checktermcond = (CheckBox) findViewById(R.id.checktermcond);
        termuselayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isTerms) {
                    termsClick();
                } else if (!isPrivacy) {
                    privacyClick();
                } else {
                    checktermcond.setButtonDrawable(getResources().getDrawable(R.mipmap.checkselected, null));
                }
            }
        });
        registerlayout = (ScrollView) findViewById(R.id.registerlayout);
        mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        rlMainHome = (RelativeLayout) findViewById(R.id.rlMainHome);
        rlLogin = (LinearLayout) findViewById(R.id.rlLogin);
        isPasswordVisible = false;
        isregPasswordVisible = false;
        isregconfPasswordVisible = false;
        rlactivationcode = (LinearLayout) findViewById(R.id.rlactivationcode);
        next_btn = (LinearLayout) findViewById(R.id.next_btn);
        reg_btn = (LinearLayout) findViewById(R.id.reg_btn);

        activation_next_btn = (LinearLayout) findViewById(R.id.activation_next_btn);
        passwordicon = (ImageView) findViewById(R.id.passwordicon);
        reg_pwdicon = (ImageView) findViewById(R.id.reg_pwdicon);
        reg_confpwdicon = (ImageView) findViewById(R.id.reg_confpwdicon);

        registerback = (ImageView) findViewById(R.id.registerback);
        registerback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tvTermsAndCondition = (TextView) findViewById(R.id.tvTermsAndCondition);
        logintxt = (TextView) findViewById(R.id.logintxt);
        tvforgotPassword = (TextView) findViewById(R.id.tvforgotPassword);

        nexttxt = (TextView) findViewById(R.id.nexttxt);
        registertitle = (TextView) findViewById(R.id.registertitle);
        regtxt = (TextView) findViewById(R.id.regtxt);

        activatecodetxt = (TextView) findViewById(R.id.activatecodetxt);
        activate = (TextView) findViewById(R.id.activate);
        backimg = (ImageView) findViewById(R.id.backimg);
        backimgactivation = (ImageView) findViewById(R.id.backimgactivation);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        reg_fname = (EditText) findViewById(R.id.reg_fname);
        reg_lname = (EditText) findViewById(R.id.reg_lname);
        reg_jobtitle = (EditText) findViewById(R.id.reg_jobtitle);
        reg_company = (EditText) findViewById(R.id.reg_company);
        reg_pwd = (EditText) findViewById(R.id.reg_pwd);
        reg_confpwd = (EditText) findViewById(R.id.reg_confpwd);
        logintxt.setTypeface(RobotoFont.boldFont(this));
        tvforgotPassword.setTypeface(RobotoFont.regularFont(this));

        nexttxt.setTypeface(RobotoFont.regularFont(this));
        registertitle.setTypeface(RobotoFont.regularFont(this));
        regtxt.setTypeface(RobotoFont.regularFont(this));
        activatecodetxt.setTypeface(RobotoFont.mediumFont(this));
        activate.setTypeface(RobotoFont.mediumFont(this));
        etEmail.setTypeface(RobotoFont.regularFont(this));
        reg_fname.setTypeface(RobotoFont.regularFont(this));
        reg_lname.setTypeface(RobotoFont.regularFont(this));
        reg_jobtitle.setTypeface(RobotoFont.regularFont(this));
        reg_company.setTypeface(RobotoFont.regularFont(this));
        reg_pwd.setTypeface(RobotoFont.regularFont(this));
        reg_confpwd.setTypeface(RobotoFont.regularFont(this));
        reg_confpwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    registerUser();
                }
                return false;
            }
        });

        etPassword.setTypeface(RobotoFont.regularFont(this));

        loginfadeOut = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.fade_out);
        activecodefadeOut = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.fade_out);
        loginfadeIn = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.fade_in);
        activecodeadeIn = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.fade_in);

        preference = new Preference(this, Login_Pref);
        trendsinstantlytxt = (TextView) findViewById(R.id.trendsinstantlytxt);
        trendsinstantlytxt.setTypeface(RobotoFont.mediumFont(this));

        activationlogintxt = (TextView) findViewById(R.id.activationlogintxt);
        logintitletxt = (TextView) findViewById(R.id.logintitletxt);
        activationlogintxt.setTypeface(RobotoFont.boldFont(this));
        logintitletxt.setTypeface(RobotoFont.boldFont(this));

        activatecodetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activecodefadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (rlLogin.getVisibility() == View.VISIBLE) {
                            rlLogin.setVisibility(View.GONE);
                        } else {
                            rlMainHome.setVisibility(View.GONE);
                        }
                    }
                });
                activatecodetxt.setVisibility(View.GONE);
                if (rlLogin.getVisibility() == View.VISIBLE) {
                    fromLogin = true;
                    rlLogin.startAnimation(activecodefadeOut);
                } else {
                    fromLogin = false;
                    rlMainHome.startAnimation(activecodefadeOut);
                }
                rlactivationcode.setVisibility(View.VISIBLE);
                rlactivationcode.startAnimation(activecodeadeIn);

            }
        });

        activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activecodefadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (rlLogin.getVisibility() == View.VISIBLE) {
                            rlLogin.setVisibility(View.GONE);
                        } else {
                            rlMainHome.setVisibility(View.GONE);
                        }
                    }
                });
                activatecodetxt.setVisibility(View.GONE);
                if (rlLogin.getVisibility() == View.VISIBLE) {
                    fromLogin = true;
                    rlLogin.startAnimation(activecodefadeOut);
                } else {
                    fromLogin = false;
                    rlMainHome.startAnimation(activecodefadeOut);
                }
                rlactivationcode.setVisibility(View.VISIBLE);
                rlactivationcode.startAnimation(activecodeadeIn);

            }
        });
        login_btn = (LinearLayout) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginfadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        rlactivationcode.setVisibility(View.GONE);
                        rlMainHome.setVisibility(View.GONE);
                    }
                });
                rlMainHome.startAnimation(loginfadeOut);
                rlLogin.setVisibility(View.VISIBLE);
                activatecodetxt.setVisibility(View.GONE);
                rlLogin.startAnimation(loginfadeIn);

            }
        });


        setShowPassword(isPasswordVisible);
        passwordicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPasswordVisible) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    setShowPassword(false);
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    setShowPassword(true);
                }

            }
        });

        setRegShowPassword(isregPasswordVisible);
        reg_pwdicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isregPasswordVisible) {
                    reg_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    setRegShowPassword(false);
                } else {
                    reg_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    setRegShowPassword(true);
                }

            }
        });

        setRegConfShowPassword(isregconfPasswordVisible);
        reg_confpwdicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isregconfPasswordVisible) {
                    reg_confpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    setRegConfShowPassword(false);
                } else {
                    reg_confpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    setRegConfShowPassword(true);
                }

            }
        });

        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        backimgactivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        passcode = (EditText) findViewById(R.id.passcode);
        passcode.setTypeface(RobotoFont.regularFont(this));

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processLogin();
            }
        });
        activation_next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activationNext();
            }
        });

        activationtxt = (TextView) findViewById(R.id.activationtxt);
        activationtxt.setTypeface(RobotoFont.mediumFont(this));

        digitstext = (TextView) findViewById(R.id.digitstext);
        digitstext.setTypeface(RobotoFont.regularFont(this));


        passcode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    activationNext();
                }
                return false;
            }
        });

        backimgactivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvforgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        reg_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

        setClickableSpanTextview();

    }

    private void registerUser() {
        if (TextUtils.isEmpty(reg_fname.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter first name", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(reg_lname.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter last name", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(reg_jobtitle.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter job title", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(reg_company.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter company", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(reg_pwd.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
        } else if (reg_pwd.getText().toString().trim().length() < 6) {
            Toast.makeText(HomeActivity.this, "Password must be atleast 6 characters", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(reg_confpwd.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Please enter confirm password", Toast.LENGTH_LONG).show();
        } else if (reg_pwd.getText().toString().trim().length() < 6) {
            Toast.makeText(HomeActivity.this, "Confirm Password must be atleast 6 characters", Toast.LENGTH_LONG).show();
        } else if (!reg_pwd.getText().toString().trim().equals(reg_confpwd.getText().toString().trim())) {
            Toast.makeText(HomeActivity.this, "Confirm password and password are not matching", Toast.LENGTH_LONG).show();
        } else if (!isTerms && !isPrivacy) {
            Toast.makeText(HomeActivity.this, "Please agree to our Terms of Use and Privacy Policy to continue", Toast.LENGTH_LONG).show();
        } else if (!isTerms) {
            Toast.makeText(HomeActivity.this, "Please agree to our Terms of Use to continue", Toast.LENGTH_LONG).show();
        } else if (!isPrivacy) {
            Toast.makeText(HomeActivity.this, "Please agree to our Privacy Policy to continue", Toast.LENGTH_LONG).show();
        } else {
            if (progressDialog != null && !progressDialog.isShowing())
                progressDialog.show();
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(passcodemail, reg_pwd.getText().toString().trim())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            try {
                                //checking if success
                                if (task.isSuccessful()) {
                                    String userid = task.getResult().getUser().getUid();
                                    updateUser(userid);
                                } else {
                                    Toast.makeText(HomeActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    if (progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                }
                            } catch (Exception e) {
                                Toast.makeText(HomeActivity.this, "Something wrong", Toast.LENGTH_LONG).show();
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                            }

                        }
                    });
        }
    }

    public void updateUser(final String userid) {

        final DatabaseReference usersref = FirebaseDatabase.getInstance().getReference().
                child(FirebasePathUsers);
        long datecreated = System.currentTimeMillis();
        final Users users = new Users(Utils.getVersion(this), userid,
                reg_company.getText().toString().trim(),
                "", "android", passcodemail.toLowerCase(), reg_fname.getText().toString().trim(),
                reg_lname.getText().toString().trim(),
                reg_jobtitle.getText().toString().trim(), reg_pwd.getText().toString().trim(),
                Utils.Supplier, "", null, 0, "",
                datecreated, datecreated, true, false, true, true);

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                child(SupplierInvitations).child(passcodid);
        databaseReference.child(IsInviationAccepted).setValue(true);
        isalready = true;

       /* if (!buyerid.equalsIgnoreCase("")) {
            DatabaseReference usersuppliersinvitations = FirebaseDatabase.getInstance().getReference().
                    child(InvitedSuppliersOfBuyer);
            usersuppliersinvitations.child(buyerid).child(userid).setValue(true);
        }*/

        DatabaseReference userfollower = FirebaseDatabase.getInstance().getReference().
                child(SuppliersOfBuyer);

        userfollower.child(buyerid).child(Suppliers).child(userid).setValue(true);

        DatabaseReference userfollowing = FirebaseDatabase.getInstance().getReference().
                child(BuyersOfSupplier);

        userfollowing.child(userid).child(Buyers).child(buyerid).setValue(true);

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.d("debug", "User:" + userId);
                users.setOne_single_userId(userId);
                usersref.child(userid).setValue(users);

                SharedPreferences.Editor editor = preference.edit();
                String json = new Gson().toJson(users);
                editor.putString("userid", users.getUserID());
                editor.putString("userrole", users.getUserType());
                editor.putString("userdetail", json);
                editor.apply();
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                finish();
            }
        });

        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(buyerid).child(Loves).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> hashMap = new HashMap<>();
                for (DataSnapshot lovesitem : dataSnapshot.getChildren()) {
                    hashMap.put(lovesitem.getKey(), lovesitem.getValue());
                }

               // DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
              //  userposts.child(userid).child(Loves).updateChildren(hashMap);

                DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
                userpostsfeed.child(userid).child(Loves).updateChildren(hashMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(buyerid).child(Trends).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> hashMap = new HashMap<>();
                for (DataSnapshot trenditem : dataSnapshot.getChildren()) {
                    hashMap.put(trenditem.getKey(), trenditem.getValue());
                }

             //   DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
              //  userposts.child(userid).child(Trends).updateChildren(hashMap);

                DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
                userpostsfeed.child(userid).child(Trends).updateChildren(hashMap);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void activationNext() {

        if (passcode.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(this, "Please enter activation code", Toast.LENGTH_LONG).show();
            return;
        } else if (passcode.getText().toString().trim().length() != 8) {
            Toast.makeText(this, "Activation code should be 8 digits", Toast.LENGTH_LONG).show();
            return;
        }


        verifyPasscode(passcode.getText().toString().trim());

    }

    private Dialog m_dialog;

    public void showDialog() {
        m_dialog = new Dialog(this, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View m_view = m_inflater.inflate(R.layout.dialog_forgot_password, null);

        TextView tvTitle = (TextView) m_view.findViewById(R.id.tvTitle);
        TextView message = (TextView) m_view.findViewById(R.id.message);
        tvTitle.setTypeface(RobotoFont.mediumFont(this));
        message.setTypeface(RobotoFont.mediumFont(this));

        tvTitle.setText("Forgot password ?");
        message.setText("If you forgot your password, we can send you an email to reset it.");

        final EditText etEmail = (EditText) m_view.findViewById(R.id.etEmail);
        TextView m_btnOk = (TextView) m_view.findViewById(R.id.btnSendMail);
        TextView m_btnCancel = (TextView) m_view.findViewById(R.id.btnCancel);

        etEmail.requestFocus();
        etEmail.setHint("Enter Email");
        m_btnOk.setText("Send Mail");

        View.OnClickListener m_clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(HomeActivity.this, "Please enter your email", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(email)) {
                    Toast.makeText(HomeActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) HomeActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(HomeActivity.this, "Email sent successfully, please check your inbox to reset your password", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(HomeActivity.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                }

                m_dialog.dismiss();
            }
        };
        m_btnOk.setOnClickListener(m_clickListener);
        m_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });
        m_dialog.setContentView(m_view);
        m_dialog.show();
    }

    private void setShowPassword(boolean isShow) {
        isPasswordVisible = isShow;
        if (isShow)
            passwordicon.setImageResource(R.mipmap.hidepwd);
        else
            passwordicon.setImageResource(R.mipmap.showpwd);
    }

    private void setRegShowPassword(boolean isShow) {
        isregPasswordVisible = isShow;
        if (isShow)
            reg_pwdicon.setImageResource(R.mipmap.hidepwd);
        else
            reg_pwdicon.setImageResource(R.mipmap.showpwd);
    }

    private void setRegConfShowPassword(boolean isShow) {
        isregconfPasswordVisible = isShow;
        if (isShow)
            reg_confpwdicon.setImageResource(R.mipmap.hidepwd);
        else
            reg_confpwdicon.setImageResource(R.mipmap.showpwd);
    }

    public void processLogin() {
        String emailtxt = etEmail.getText().toString();
        if (TextUtils.isEmpty(emailtxt)) {
            Toast.makeText(HomeActivity.this, "Please enter your email", Toast.LENGTH_LONG).show();
            return;
        } else if (!isValidEmail(emailtxt)) {
            Toast.makeText(HomeActivity.this, "Please enter valid email address", Toast.LENGTH_LONG).show();
            return;
        } else if (etPassword.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(HomeActivity.this, "Please enter your password", Toast.LENGTH_LONG).show();
            return;
        } else if (etPassword.getText().toString().length() < 6) {
            Toast.makeText(HomeActivity.this, "Password must be atleast 6 characters", Toast.LENGTH_LONG).show();
            return;
        }


        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();

        auth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(HomeActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            Toast.makeText(HomeActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(auth.getCurrentUser().getUid());

                            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                                @Override
                                public void idsAvailable(String userId, String registrationId) {
                                    Log.d("debug", "User:" + userId);

                                    databaseReference.child(OneSingleUserID).setValue(userId);
                                    databaseReference.child(DeviceType).setValue("android");
                                    databaseReference.child(AppVersion).setValue(Utils.getVersion(HomeActivity.this));

                                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot dataSnapshot) {
                                            final Users users = dataSnapshot.getValue(Users.class);

                                            Log.i("users", users + "");
                                            SharedPreferences.Editor editor = preference.edit();
                                            String json = new Gson().toJson(users);
                                            editor.putString("userid", users.getUserID());
                                            editor.putString("userrole", users.getUserType());
                                            editor.putString("userdetail", json).apply();
                                            editor.apply();
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            startActivity(new Intent(HomeActivity.this, MainActivity.class));
                                            overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
                                            finish();

                                        }


                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                        }
                                    });

                                }
                            });


                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        Utils.hideSoftKeyboard(this);
        if (registerlayout.getVisibility() == View.VISIBLE) {
            loginfadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    registerlayout.setVisibility(View.GONE);
                }
            });
            registerlayout.startAnimation(loginfadeOut);
            mainlayout.setVisibility(View.VISIBLE);
            mainlayout.startAnimation(loginfadeIn);

        } else if (rlLogin.getVisibility() == View.VISIBLE) {
            loginfadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rlactivationcode.setVisibility(View.GONE);
                    activatecodetxt.setVisibility(View.VISIBLE);
                    rlLogin.setVisibility(View.GONE);
                }
            });
            rlLogin.startAnimation(loginfadeOut);
            rlMainHome.setVisibility(View.VISIBLE);
            rlMainHome.startAnimation(loginfadeIn);
        } else if (rlactivationcode.getVisibility() == View.VISIBLE) {
            activecodefadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    rlactivationcode.setVisibility(View.GONE);
                   /* imgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imgr.showSoftInput(passcode, InputMethodManager.SHOW_IMPLICIT);*/
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            passcode.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                            passcode.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                            Utils.hideSoftKeyboard(HomeActivity.this);
                        }
                    }, 200);

                }
            });


            rlactivationcode.startAnimation(activecodefadeOut);

            if (fromLogin) {
                rlLogin.setVisibility(View.VISIBLE);
                activatecodetxt.setVisibility(View.GONE);
                rlLogin.startAnimation(activecodeadeIn);
            } else {
                activatecodetxt.setVisibility(View.VISIBLE);
                rlMainHome.setVisibility(View.VISIBLE);
                rlMainHome.startAnimation(activecodeadeIn);
            }
        } else {
            super.onBackPressed();
        }

    }

    public void verifyPasscode(String activationcode) {
        passcodid = "";
        passcodemail = "";
        buyerid = "";
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        FirebaseDatabase.getInstance().getReference().child(SupplierInvitations)
                .orderByChild(VerificationCode).equalTo(activationcode).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (isalready)
                            return;
                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                boolean isaccepted = (boolean) dataSnapshot1.child(IsInviationAccepted).getValue();
                                if (isaccepted) {
                                    Toast.makeText(HomeActivity.this, "Activation code not valid", Toast.LENGTH_LONG).show();
                                    if (progressDialog != null && progressDialog.isShowing())
                                        progressDialog.dismiss();
                                } else {
                                    passcodid = dataSnapshot1.child(ID).getValue().toString();
                                    passcodemail = dataSnapshot1.child(InvitedSupplierEmailID).getValue().toString();

                                    buyerid = dataSnapshot1.child(UserID).getValue().toString();
                                    loginfadeOut.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            if (progressDialog != null && progressDialog.isShowing())
                                                progressDialog.dismiss();
                                            passcode.setText("");
                                            mainlayout.setVisibility(View.GONE);

                                        }
                                    });
                                    mainlayout.startAnimation(loginfadeOut);
                                    registerlayout.setVisibility(View.VISIBLE);
                                    registerlayout.startAnimation(loginfadeIn);
                                }
                            }

                        } else {
                            Toast.makeText(HomeActivity.this, "Activation code not valid", Toast.LENGTH_LONG).show();
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    private void termsClick() {
        startActivity(new Intent(HomeActivity.this, TermConditionActivity.class));
        overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
    }

    private void privacyClick() {
        startActivity(new Intent(HomeActivity.this, PrivacyPolicy.class));
        overridePendingTransition(R.anim.right_slide_in, R.anim.right_slide_out);
    }

    private void setClickableSpanTextview() {

        ClickableSpan termsOfServiceClickSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                termsClick();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#FFFFFF")); // specific color for this link
            }
        };

        ClickableSpan privacyPolicyeClickSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                privacyClick();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                //ds.setUnderlineText(false);
                ds.setColor(Color.parseColor("#FFFFFF")); // specific color for this link
            }
        };

        makeLinks(tvTermsAndCondition, new String[]{
                "Terms of Use", "Privacy Policy"
        }, new ClickableSpan[]{
                termsOfServiceClickSpan, privacyPolicyeClickSpan
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isTerms && isPrivacy) {
            checktermcond.setButtonDrawable(getResources().getDrawable(R.mipmap.checkselected, null));
        }
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

}
