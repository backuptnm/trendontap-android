package tnm.trendontap.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.CommentsAdapter;
import tnm.trendontap.adapter.LoveImagesAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.CommentsItem;
import tnm.trendontap.modal.LovesCategoryItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.EventClass.COMMENT;
import static tnm.trendontap.utility.EventClass.COMMENTMSG;
import static tnm.trendontap.utility.EventClass.PostComment;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathcComments;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathcPostComments;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathcReactionComments;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Message;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.FireStoreUtils.tReactionCommentCount;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentBuyer;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentSupplier;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;


public class CommentActivity extends AppCompatActivity {
    TextView labeltxt, cancel;
    EditText postarea;
    TextView postbtn;
    ListView commentslist;
    CommentsAdapter commentsAdapter;
    ArrayList<CommentsItem> commentsItemArrayList;
    String reaction_id = "", sento = "", post_id = "";
    Preference preference;
    Users users;
    boolean isFromPost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_comments);

        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        cancel = (TextView) findViewById(R.id.cancel);
        cancel.setTypeface(RobotoFont.regularFont(this));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        postarea = (EditText) findViewById(R.id.postarea);
        postarea.setTypeface(RobotoFont.regularFont(this));
        postbtn = (TextView) findViewById(R.id.postbtn);
        postbtn.setTypeface(RobotoFont.regularFont(this));

        commentslist = (ListView) findViewById(R.id.commentslist);
        commentsItemArrayList = new ArrayList<>();
        post_id = getIntent().getStringExtra("post_id");
        if (getIntent().hasExtra("is_from_post") && getIntent().getBooleanExtra("is_from_post", false)) {
            isFromPost = true;
            sento = getIntent().getStringExtra("sento");
            commentsAdapter = new CommentsAdapter(this, commentsItemArrayList, "");
        } else {
            isFromPost = false;
            commentsAdapter = new CommentsAdapter(this, commentsItemArrayList, "");
            reaction_id = getIntent().getStringExtra("reaction_id");
            sento = getIntent().getStringExtra("sento");
        }

        commentslist.setAdapter(commentsAdapter);


        postbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFromPost) {
                    addPostComment();
                } else {
                    addComment();
                }
            }
        });

        postbtn.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_ENTER) {
                    if (isFromPost) {
                        addPostComment();
                    } else {
                        addComment();
                    }
                }
                return false;
            }
        });

       // getListCount(isFromPost);
        if (isFromPost)
            setPostDetails();
        else
            setReactiondetails();

        setList(isFromPost);
    }

    public void setPostDetails() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(post_id);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<String> photoUrlsList = (ArrayList<String>) dataSnapshot.child(tPostPhotoId).getValue();
                commentsAdapter.setReactimg(photoUrlsList.get(0));
                commentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setReactiondetails() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).child(reaction_id);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<String> photoUrlsList = (ArrayList<String>) dataSnapshot.child(tPostPhotoId).getValue();
                commentsAdapter.setReactimg(photoUrlsList.get(0));
                commentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.hidekeyboard(this, postarea);
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    public void addComment() {
        String postareatxt = postarea.getText().toString().trim();
        if (TextUtils.isEmpty(postareatxt)) {
            Toast.makeText(this, "Please enter comment", Toast.LENGTH_SHORT).show();
            return;
        }
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcComments);
        String commentid = databaseReference.push().getKey();
        long tsLong = System.currentTimeMillis();
        CommentsItem commentsItem = new CommentsItem(commentid, postareatxt, reaction_id, tsLong, FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(commentid).setValue(commentsItem);

        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reaction_id);
        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long cnt = 0;
                try {
                    cnt = (long) dataSnapshot.child(FireStoreUtils.tReactionCommentCount).getValue();
                } catch (Exception e) {

                }
                databaseReference1.child(tReactionCommentCount).setValue(cnt + 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            databaseReference1.child(tReactionUnreadCommentSupplier).setValue(true); //send comment to supplier
        } else {
            databaseReference1.child(tReactionUnreadCommentBuyer).setValue(true);//send comment to buyer
        }

        DatabaseReference reactioncomment = FirebaseDatabase.getInstance().getReference(FirebasePathcReactionComments).child(reaction_id).child(FirebasePathcComments);
        reactioncomment.child(commentid).setValue(true);
        postarea.setText("");
        ActivityItem activityItem = new ActivityItem();
        activityItem.setProfilePic(users.getProfilePic());
        activityItem.setLongMessage(COMMENTMSG);
        activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + COMMENTMSG);
        activityItem.setPostID(getIntent().getStringExtra("post_id"));
        activityItem.setPostImageURL(getIntent().getStringExtra("postimg"));
        activityItem.setReactionID(reaction_id);
        activityItem.setShortMessage(getIntent().getStringExtra("from"));
        activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
        activityItem.setActivityType(COMMENT);
        activityItem.setTimeStamp(System.currentTimeMillis());
        activityItem.setUserID(users.getUserID());

        // processActivity(activityItem);
        String msg = users.getFirstName() + " " + users.getLastName() + COMMENTMSG + getIntent().getStringExtra("from");
        sendActivityToUser(sento, activityItem, msg);
        Utils.hidekeyboard(this, postarea);
    }

    public void addPostComment() {
        String postareatxt = postarea.getText().toString().trim();
        if (TextUtils.isEmpty(postareatxt)) {
            Toast.makeText(this, "Please enter comment", Toast.LENGTH_SHORT).show();
            return;
        }
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcComments);
        String commentid = databaseReference.push().getKey();
        long tsLong = System.currentTimeMillis();
        CommentsItem commentsItem = new CommentsItem(commentid, postareatxt, post_id, tsLong, FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(commentid).setValue(commentsItem);

        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts).child(post_id);
        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long cnt = 0;
                try {
                    cnt = (long) dataSnapshot.child(FireStoreUtils.tReactionCommentCount).getValue();
                } catch (Exception e) {

                }
                databaseReference1.child(tReactionCommentCount).setValue(cnt + 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        DatabaseReference postcomment = FirebaseDatabase.getInstance().getReference(FirebasePathcPostComments).child(post_id).child(FirebasePathcComments);
        postcomment.child(commentid).setValue(true);

        postarea.setText("");

        ActivityItem activityItem = new ActivityItem();
        activityItem.setProfilePic(users.getProfilePic());
        activityItem.setLongMessage(COMMENTMSG);
        activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + COMMENTMSG);
        activityItem.setPostID(post_id);
        activityItem.setPostImageURL(getIntent().getStringExtra("post_img"));
        activityItem.setReactionID("");
        activityItem.setShortMessage(getIntent().getStringExtra("from"));
        activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
        activityItem.setActivityType(PostComment);
        activityItem.setTimeStamp(System.currentTimeMillis());
        activityItem.setUserID(users.getUserID());

        String msg = users.getFirstName() + " " + users.getLastName() + COMMENTMSG + getIntent().getStringExtra("from");
        sendActivityToUser(sento, activityItem, msg);
        Utils.hidekeyboard(this, postarea);
    }


    public void getListCount(final boolean flag) {
        DatabaseReference databaseReference;
        if (flag)
            databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcPostComments).child(post_id).child(FirebasePathcComments);
        else
            databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcReactionComments).child(reaction_id).child(FirebasePathcComments);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0)
                    return;
                setList(flag);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setList(boolean flag) {
        DatabaseReference databaseReference;
        if (flag)
            databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcPostComments).child(post_id).child(FirebasePathcComments);
        else
            databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathcReactionComments).child(reaction_id).child(FirebasePathcComments);

        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String commentid = dataSnapshot.getKey();
                loadComment(commentid);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void loadComment(String commentid) {
        FirebaseDatabase.getInstance().getReference(FirebasePathcComments).orderByChild(ID).equalTo(commentid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        setUsersDetails(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        setUsersDetails(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        setUsersDetails(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void setUsersDetails(final DataSnapshot dataSnapshot, final String action) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(dataSnapshot.child(UserID).getValue().toString()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshotinner) {
                        Users users = dataSnapshotinner.getValue(Users.class);
                        String postid = "";
                        try {
                            postid = post_id;
                        } catch (Exception e) {

                        }

                        // if(action.equalsIgnoreCase("add")){
                        commentsItemArrayList.add(new CommentsItem(dataSnapshot.child(ID).getValue().toString(),
                                dataSnapshot.child(Message).getValue().toString(),
                                postid,
                                (long) dataSnapshot.child(TimeStamp).getValue(),
                                dataSnapshot.child(FireStoreUtils.UserID).getValue().toString(),
                                getName(users.getFirstName(), users.getLastName()),
                                users.getProfilePic()));

                        commentsAdapter.update(commentsItemArrayList);
                        // }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

}
