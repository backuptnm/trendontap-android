package tnm.trendontap.activity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import tnm.trendontap.R;
import tnm.trendontap.adapter.ActivityAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.BadgeUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;


import static tnm.trendontap.activity.MainActivity.like_love;
import static tnm.trendontap.activity.MainActivity.like_trend;
import static tnm.trendontap.activity.MainActivity.loadbuyerinvitation;
import static tnm.trendontap.activity.MainActivity.redirectBuyerSample;
import static tnm.trendontap.activity.MainActivity.redirectComment;
import static tnm.trendontap.activity.MainActivity.redirectCostingsheet;
import static tnm.trendontap.utility.FireStoreUtils.BadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.tActivityisRead;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class ActivityList extends AppCompatActivity {
    LinearLayout close;
    TextView labeltxt;
    ActivityAdapter activityAdapter;
    ArrayList<ActivityItem> activityItemArrayList;
    ListView activitylist;
    LinearLayout noactivitydata;
    ProgressDialog progressDialog;
    Preference preference;
    Users users;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitylist);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        close = (LinearLayout) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        noactivitydata = (LinearLayout) findViewById(R.id.noactivitydata);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        labeltxt.setText("Activity");
        activitylist = (ListView) findViewById(R.id.activitylist);

        activityItemArrayList = new ArrayList<>();
        activityAdapter = new ActivityAdapter(this, activityItemArrayList);

        activitylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ActivityItem activityItem = activityItemArrayList.get(i);
                ArrayList<String> stringList = null;
                try {
                    stringList = activityItem.getIsReadUsers();
                } catch (Exception e) {

                }
                if (stringList == null) {
                    stringList = new ArrayList<>();
                }

                if (!stringList.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    stringList.add(String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                    activityItem.setIsReadUsers(stringList);
                    activityItemArrayList.set(i, activityItem);
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities).child(activityItem.getId());
                    databaseReference.child(tActivityisRead).setValue(stringList);

                  /*  long badgeCount = 0;
                    try {
                        badgeCount = users.getBadgeCount();
                    } catch (Exception e) {

                    }
                    if (badgeCount > 0) {*/
                    final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long badgeCount = (long) dataSnapshot.child(BadgeCount).getValue();
                            if (badgeCount == 0)
                                return;
                            databaseReference1.child(BadgeCount).setValue(badgeCount - 1);
                            users.setBadgeCount(badgeCount - 1);
                            Log.i("users", users + "");
                            SharedPreferences.Editor editor = preference.edit();
                            String json = new Gson().toJson(users);
                            editor.putString("userid", users.getUserID());
                            editor.putString("userdetail", json);
                            editor.apply();

                            BadgeUtils.setBadge(getApplicationContext(), (int) badgeCount - 1);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    // }

                    activityAdapter.notifyDataSetChanged();
                }

                switch (activityItem.getActivityType()) {
                    case "reaction":
                    case "sample":
                        redirectBuyerSample(ActivityList.this, activityItem.getPostID(), activityItem.getShortMessage());
                        break;
                    case "costing sheet":
                        redirectCostingsheet(ActivityList.this, activityItem.getPostID(), activityItem.getReactionID(), activityItem.getUserID());
                        break;
                    case "like_trend":
                        like_trend(ActivityList.this, activityItem.getPostID());
                        onBackPressed();
                        break;
                    case "like_love":
                        like_love(ActivityList.this, activityItem.getPostID());
                        onBackPressed();
                        break;
                    case "comment":
                        redirectComment(ActivityList.this, activityItem.getReactionID(),
                                activityItem.getPostID(),
                                activityItem.getUserID(), activityItem.getShortMessage(),
                                activityItem.getPostImageURL());
                        break;
                    case "PostComment":
                        redirectComment(ActivityList.this, activityItem.getReactionID(),
                                activityItem.getPostID(),
                                activityItem.getUserID(), activityItem.getShortMessage(),
                                activityItem.getPostImageURL());
                        break;

                    case "BuyerFollower":
                        loadbuyerinvitation(ActivityList.this, activityItem.getUserID());
                        finish();
                        // Toast.makeText(ActivityList.this,"click",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        activitylist.setAdapter(activityAdapter);
        getUserActivityCount();
        //  setLoveslist();
    }


    public void getUserActivityCount() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            activitylist.setVisibility(View.GONE);
                            noactivitydata.setVisibility(View.VISIBLE);
                            return;
                        }
                        activitylist.setVisibility(View.VISIBLE);
                        noactivitydata.setVisibility(View.GONE);
                        getUserActivityList();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getUserActivityList() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.show();
            }
        } catch (Exception e) {

        }
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getActivityList(dataSnapshot.getKey());
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                });
    }


    public void getActivityList(String key) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
        databaseReference.orderByChild(ID).equalTo(key).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                manageData(dataSnapshot, "add");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                manageData(dataSnapshot, "edit");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                manageData(dataSnapshot, "delete");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });

    }


    public void manageData(DataSnapshot activitydata, String action) {
        final ActivityItem activityItem = activitydata.getValue(ActivityItem.class);
        try {
            ArrayList<String> is_read_users = activitydata.child(tActivityisRead).getValue(new GenericTypeIndicator<ArrayList<String>>() {
            });
            activityItem.setIsReadUsers(is_read_users);

        } catch (Exception e) {

        }
        if (action.equalsIgnoreCase("add")) {
            activityItemArrayList.add(activityItem);
            activityAdapter.setList(activityItemArrayList);
        } else if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(activityItemArrayList, activityItem.getId());
            if (pos == -1) {
                return;
            }
            activityItemArrayList.set(pos, activityItem);
            activityAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(activityItemArrayList, activityItem.getId());
            if (pos == -1) {
                return;
            }
            activityItemArrayList.remove(pos);
            activityAdapter.notifyDataSetChanged();
        }
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    public int matchValue(ArrayList<ActivityItem> activityItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < activityItemArrayList.size(); j++) {
            if (activityItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

}
