package tnm.trendontap.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.adapter.LoveImagesAdapter;
import tnm.trendontap.adapter.MyReactionsAdapter;
import tnm.trendontap.adapter.ReactionsTrendAdapter;
import tnm.trendontap.fragment.GalleryFragment;
import tnm.trendontap.modal.MyReaction;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.MainActivity.cameralay;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetID;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetRequest;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetSent;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.PostByUserID;
import static tnm.trendontap.utility.FireStoreUtils.PostID;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.SampleRequest;
import static tnm.trendontap.utility.FireStoreUtils.SampleSent;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.FireStoreUtils.tReactionCommentCount;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentSupplier;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 3/30/2018.
 */

public class Myreactions_Trends_Love_Activity extends AppCompatActivity {
    TextView labeltxt, name, type, title, category, takelook, notifycnt;
    LinearLayout back_arrow;

    RelativeLayout activity2_icon_lay, categorybg;
    ListView myReactionList;
    MyReactionsAdapter myRectionAdapter;
    ArrayList<MyReaction> myReactionItemArrayList;
    Preference preference;
    Users users;
    String from = "";

    LinearLayout reactbtn;

    ClickableViewPager loveimgpager;
    CirclePageIndicator indicator;
    CircleImageView userimg;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_reaction);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        labeltxt.setText("My Reactions");

        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(this));

        if (getIntent().getExtras() != null && getIntent().getStringExtra("from") != null) {
            from = getIntent().getStringExtra("from");
        }

        View headerview = LayoutInflater.from(this).inflate(R.layout.reactions_love_header, null, false);
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(this));
        categorybg = (RelativeLayout) headerview.findViewById(R.id.categorybg);

        if (from.equalsIgnoreCase("love"))
            categorybg.setVisibility(View.GONE);
        else
            categorybg.setVisibility(View.VISIBLE);

        reactbtn = (LinearLayout) headerview.findViewById(R.id.reactbtn);
        reactbtn.setVisibility(View.GONE);
        loveimgpager = (ClickableViewPager) headerview.findViewById(R.id.loveimgpager);
        indicator = (CirclePageIndicator) headerview.findViewById(R.id.indicator);
        userimg = (CircleImageView) headerview.findViewById(R.id.userimg);

        type = (TextView) headerview.findViewById(R.id.type);
        type.setTypeface(RobotoFont.regularFont(this));

        title = (TextView) headerview.findViewById(R.id.title);
        title.setTypeface(RobotoFont.regularFont(this));

        category = (TextView) headerview.findViewById(R.id.category);
        category.setTypeface(RobotoFont.regularFont(this));

        takelook = (TextView) headerview.findViewById(R.id.takelook);
        takelook.setTypeface(RobotoFont.regularFont(this));

        back_arrow = (LinearLayout) findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        activity2_icon_lay = (RelativeLayout) findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Myreactions_Trends_Love_Activity.this, ActivityList.class));
                overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        myReactionList = findViewById(R.id.my_reaction_list);
        myReactionItemArrayList = new ArrayList<>();
        myReactionList.addHeaderView(headerview);
        myRectionAdapter = new MyReactionsAdapter(this, myReactionItemArrayList, "", getIntent().getStringExtra("from"));
        myReactionList.setAdapter(myRectionAdapter);
        String post_id = getIntent().getStringExtra("postid");
        processPost(post_id);
        getCountReaction(post_id);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

   /* @Override
    public void onResume() {
        super.onResume();
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt,this);
    }


    public void processPost(final String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(postid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                title.setText(String.valueOf(dataSnapshot.child(PostTitle).getValue()));
                category.setText(String.valueOf(dataSnapshot.child(CatagoryName).getValue()));
                getPostUser(String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()));
                final ArrayList<String> photoUrlsList = (ArrayList<String>) dataSnapshot.child(tPostPhotoId).getValue();
                LoveImagesAdapter myPagerAdapter = new LoveImagesAdapter(Myreactions_Trends_Love_Activity.this, photoUrlsList);
                loveimgpager.setAdapter(myPagerAdapter);
                indicator.setViewPager(loveimgpager);
                myRectionAdapter.setPosturl(photoUrlsList.get(0));

                reactbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("buyers", false);
                        bundle.putString("from", from);
                        bundle.putString("postimg", photoUrlsList.get(0));
                        bundle.putString("postid", postid);
                        bundle.putString("postuserid", String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()));
                        MainActivity.loadContentFragment(new GalleryFragment(), false, bundle);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getPostUser(String userid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userlist) {
                        Users users = userlist.getValue(Users.class);
                        name.setText(getName(users.getFirstName(), users.getLastName()));
                        type.setText(users.getJobTitle());
                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.mipmap.userplaceholder);
                            requestOptions.error(R.mipmap.userplaceholder);

                            Glide.with(Myreactions_Trends_Love_Activity.this)
                                    .load(users.getProfilePic())
                                    .apply(requestOptions)
                                    .into(userimg);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getCountReaction(final String postid) {
        myReactionItemArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        myReactionItemArrayList.clear();
                        getReactionList(postid);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }


    public void getReactionList(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if (!dataSnapshot.child(FireStoreUtils.UserID).getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            return;
                        }
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void manageData(DataSnapshot postidlist, String action) {
        final MyReaction myReaction = new MyReaction();
        myReaction.setReactionid(postidlist.child(ID).getValue().toString());
        myReaction.setTimeStamp((long) postidlist.child(TimeStamp).getValue());
        myReaction.setCommentcount((long) postidlist.child(tReactionCommentCount).getValue());
        myReaction.setUnread_comment((boolean) postidlist.child(tReactionUnreadCommentSupplier).getValue());
        myReaction.setAbouttrend(postidlist.child(Content).getValue().toString());
        myReaction.setImgurl(postidlist.child(tPostPhotoId).child("0").getValue().toString());
        myReaction.setSample_request((boolean) postidlist.child(SampleRequest).getValue());
        myReaction.setSample_sent((boolean) postidlist.child(SampleSent).getValue());
        myReaction.setBuyer_user_id((String) postidlist.child(PostByUserID).getValue());
        myReaction.setCost_request((boolean) postidlist.child(CostsheetRequest).getValue());
        myReaction.setCost_sent((boolean) postidlist.child(CostsheetSent).getValue());
        myReaction.setPost_id(postidlist.child(PostID).getValue().toString());

        ArrayList<String> yourStringArray = new ArrayList<>();
        int size = (int) postidlist.child(tPostPhotoId).getChildrenCount();
        for (int i = 0; i < size; i++) {
            String url = postidlist.child(tPostPhotoId).child(i + "").getValue().toString();
            yourStringArray.add(url);
        }
        myReaction.setPhotoUrls(yourStringArray);

        if (postidlist.hasChild(CostsheetID))
            myReaction.setCostsheet_id(String.valueOf(postidlist.child(CostsheetID).getValue()));
        else
            myReaction.setCostsheet_id("");

        if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
            if (pos == -1)
                return;
            myReactionItemArrayList.set(pos, myReaction);
            myRectionAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
            if (pos == -1)
                return;
            myReactionItemArrayList.remove(pos);
            myRectionAdapter.notifyDataSetChanged();
        } else {
            myReactionItemArrayList.add(myReaction);
            myRectionAdapter.update(myReactionItemArrayList);
        }
        if (!action.equalsIgnoreCase("delete"))
            setUserData(String.valueOf(postidlist.child(FireStoreUtils.UserID).getValue()), myReaction);
    }


    public void setUserData(String userid, final MyReaction myReaction) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userlist) {
                        Users users = userlist.getValue(Users.class);
                        myReaction.setName(getName(users.getFirstName(), users.getLastName()));
                        myReaction.setType(users.getCompanyName());
                        myReaction.setAvtarurl(users.getProfilePic());
                        int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
                        myReactionItemArrayList.set(pos, myReaction);
                        myRectionAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public int matchValue(ArrayList<MyReaction> myReactionItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < myReactionItemArrayList.size(); j++) {
            if (myReactionItemArrayList.get(j).getReactionid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    //int cnt = 0;

  /*  public void setRectionlist(final String postid) {
        myReactionItemArrayList.clear();
        cnt = 0;
        FirebaseDatabase.getInstance().getReference().child("reaction")
                .orderByChild("post_id").equalTo(postid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {

                        myReactionItemArrayList.clear();
                        if (dataSnapshot.getChildrenCount() == 0) {
                            return;
                        }
                        cnt = 0;
                        for (final DataSnapshot postidlist : dataSnapshot.getChildren()) {
                            cnt++;
                            if (!postidlist.child(UserID).getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                continue;
                            }

                            final MyReaction myReaction = new MyReaction();
                            myReaction.setReactionid(postidlist.child(ID).getValue().toString());
                            myReaction.setTimeStamp((long) postidlist.child(TimeStamp).getValue());
                            myReaction.setCommentcount((long) postidlist.child(tReactionCommentCount).getValue());
                            myReaction.setUnread_comment((boolean) postidlist.child(tReactionUnreadCommentSupplier).getValue());
                            myReaction.setAbouttrend(postidlist.child(Content).getValue().toString());
                            myReaction.setImgurl(postidlist.child("photoUrls").child("0").getValue().toString());
                            myReaction.setSample_request((boolean) postidlist.child("sample_request").getValue());
                            myReaction.setSample_sent((boolean) postidlist.child("sample_sent").getValue());
                            myReaction.setBuyer_user_id((String) postidlist.child("post_by_userId").getValue());
                            myReaction.setCost_request((boolean) postidlist.child("costsheet_request").getValue());
                            myReaction.setCost_sent((boolean) postidlist.child("costsheet_sent").getValue());
                            myReaction.setPost_id(postid);

                            ArrayList<String> yourStringArray = new ArrayList<>();
                            int size = (int) postidlist.child("photoUrls").getChildrenCount();
                            for (int i = 0; i < size; i++) {
                                String url = postidlist.child("photoUrls").child(i + "").getValue().toString();
                                yourStringArray.add(url);
                            }
                            myReaction.setPhotoUrls(yourStringArray);

                            if (postidlist.hasChild("costsheet_id"))
                                myReaction.setCostsheet_id(String.valueOf(postidlist.child("costsheet_id").getValue()));
                            else
                                myReaction.setCostsheet_id("");

                            FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(postidlist.child(UserID).getValue().toString())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot userlist) {
                                            Users users = userlist.getValue(Users.class);
                                            myReaction.setName(getName(users.getFirstName(), users.getLastName()));
                                            myReaction.setType(users.getCompany());
                                            myReaction.setAvtarurl(users.getAvatarUrl());
                                            myReactionItemArrayList.add(myReaction);
                                            if (cnt >= dataSnapshot.getChildrenCount())
                                                myRectionAdapter.update(myReactionItemArrayList);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }*/


}
