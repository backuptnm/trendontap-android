package tnm.trendontap.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.adapter.NearbyAdapter;
import tnm.trendontap.fragment.UploadLoveFragment;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.Place;
import tnm.trendontap.modal.PlacesList;
import tnm.trendontap.utility.DataParser;
import tnm.trendontap.utility.DownloadUrl;
import tnm.trendontap.utility.GPSTracker;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.Utils.googlekey;
import static tnm.trendontap.utility.Utils.milestoMeter;


public class LocationActivity extends Activity implements LocationListener {

    TextView labeltxt, cancel, nolocation, retrybtn;
    ListView activitylist;
    NearbyAdapter mNearbyLocation_Adapter;
    PlacesList mPlacesList;
    ArrayList<LocationItem> mArrayPlaceList = new ArrayList<>();
    ProgressDialog progressDialog;
    LocationManager locationManager;
    LinearLayout nolocationdata;
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    double latitude = 0.0, longitude = 0.0;
    GPSTracker gps;
    ImageView locationimg, imgClear;
    EditText etSearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locationlist);
        gps = new GPSTracker(this);
        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        nolocationdata = (LinearLayout) findViewById(R.id.nolocationdata);
        labeltxt.setText("Locations");
        labeltxt.setTypeface(RobotoFont.regularFont(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Locating...");
        activitylist = (ListView) findViewById(R.id.activitylist);
        activitylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.putExtra("locationval", mArrayPlaceList.get(i));
                setResult(UploadLoveFragment.Location_Flag, intent);
                onBackPressed();
            }
        });

        retrybtn = (TextView) findViewById(R.id.retrybtn);
        retrybtn.setTypeface(RobotoFont.regularFont(this));

        retrybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nolocation.setText("Locating...");
                processLocation();
                etSearch.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
            }
        });

        nolocation = (TextView) findViewById(R.id.nolocation);
        nolocation.setTypeface(RobotoFont.regularFont(this));
        nolocation.setText("Locating...");
        cancel = (TextView) findViewById(R.id.cancel);
        cancel.setTypeface(RobotoFont.regularFont(this));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgClear = (ImageView) findViewById(R.id.imgClear);
        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(this));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mArrayPlaceList.clear();
                if (charSequence.toString().length() > 0) {
                    imgClear.setVisibility(View.VISIBLE);
                    new GetNearbyPlacesData().execute(getUrl(charSequence.toString().trim()));
                } else {
                    imgClear.setVisibility(View.INVISIBLE);
                    processLocation();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                // Some phones disregard the IME setting option in the xml, instead
                // they send IME_ACTION_UNSPECIFIED so we need to catch that
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;
                }

                return handled;
            }
        });

        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });

        locationimg = (ImageView) findViewById(R.id.locationimg);
        locationimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nolocation.setText("Locating...");
                processLocation();
                etSearch.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        processLocation();
    }

    public void processLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE_LOCATION);
        } else {


            if (!gps.isLocationEnabled(this)) {
                gps.showSettingsAlert(this);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation();
                    }
                }, 500);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        } else {
            Toast.makeText(this, "You can not see locations until allow permission", Toast.LENGTH_LONG).show();
        }
    }

    public void getLocation() {
       /* if (!progressDialog.isShowing())
            progressDialog.show();*/
        activitylist.setVisibility(View.GONE);
        nolocationdata.setVisibility(View.VISIBLE);
        nolocation.setText("Locating...");
        Location location = gps.getLocation(this);
        retrybtn.setVisibility(View.GONE);
        if (location != null) {

            latitude = location.getLatitude();
            //double latitude = 53.466558;
            //double longitude = -2.282875;
            longitude = location.getLongitude();
            if (latitude == 0.0 || longitude == 0.0) {
                activitylist.setVisibility(View.GONE);
                nolocationdata.setVisibility(View.VISIBLE);
                nolocation.setText("Couldn't locate your position.");
                retrybtn.setVisibility(View.VISIBLE);
                /*if (progressDialog.isShowing())
                    progressDialog.dismiss();*/
            } else {
                retrybtn.setVisibility(View.GONE);
                mPlacesList = new PlacesList();
                String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=" + milestoMeter() + "&key=" + googlekey;

                RequestQueue mQueue = Volley.newRequestQueue(getApplicationContext());
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                               /* if (progressDialog.isShowing())
                                    progressDialog.dismiss();*/
                                mPlacesList = new Gson().fromJson(response, PlacesList.class);

                                for (Place mplace : mPlacesList.results) {
                                    if (!mplace.getName().equalsIgnoreCase(mplace.getVicinity())) {
                                        LocationItem locationitem = new LocationItem(mplace.getName(), mplace.getVicinity()
                                                , mplace.getGeometry().location.lat, mplace.getGeometry().location.lng);
                                        mArrayPlaceList.add(locationitem);
                                    }
                                }
                                if (mArrayPlaceList.size() == 0) {
                                    activitylist.setVisibility(View.GONE);
                                    nolocationdata.setVisibility(View.VISIBLE);
                                    retrybtn.setVisibility(View.VISIBLE);
                                    nolocation.setText("Couldn't locate your position.");
                                } else {
                                    activitylist.setVisibility(View.VISIBLE);
                                    nolocationdata.setVisibility(View.GONE);
                                    retrybtn.setVisibility(View.GONE);
                                    mNearbyLocation_Adapter = new NearbyAdapter(LocationActivity.this, mArrayPlaceList);
                                    activitylist.setAdapter(mNearbyLocation_Adapter);
                                }
                                Log.e("Response:", "" + mPlacesList.status);

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                       /* if (progressDialog.isShowing())
                            progressDialog.dismiss();*/
                        activitylist.setVisibility(View.GONE);
                        nolocationdata.setVisibility(View.VISIBLE);
                        retrybtn.setVisibility(View.VISIBLE);
                        nolocation.setText("Couldn't locate your position.");
                    }
                });

                mQueue.add(stringRequest);
            }
        } else {
            activitylist.setVisibility(View.GONE);
            nolocationdata.setVisibility(View.VISIBLE);
            retrybtn.setVisibility(View.VISIBLE);
            nolocation.setText("Couldn't locate your position.");
           /* if (progressDialog.isShowing())
                progressDialog.dismiss();*/
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private String getUrl(String search) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        //googlePlacesUrl.append("&radius=" + Utils.milestoMeter());
        //googlePlacesUrl.append("&type=" + nearbyPlace);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + Utils.googlekey);
        googlePlacesUrl.append("&name=" + search);
        Log.d("getUrl", googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

        String googlePlacesData;

        String url;

        @Override
        protected String doInBackground(Object... params) {
            try {
                Log.d("GetNearbyPlacesData", "doInBackground entered");

                url = (String) params[0];
                DownloadUrl downloadUrl = new DownloadUrl();
                googlePlacesData = downloadUrl.readUrl(url);
                Log.d("GooglePlacesReadTask", "doInBackground Exit");
            } catch (Exception e) {
                Log.d("GooglePlacesReadTask", e.toString());
            }
            return googlePlacesData;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("GooglePlacesReadTask", "onPostExecute Entered");
            List<HashMap<String, String>> nearbyPlacesList = null;
            DataParser dataParser = new DataParser();
            nearbyPlacesList = dataParser.parse(result);
            ShowNearbyPlaces(nearbyPlacesList);
            Log.d("GooglePlacesReadTask", "onPostExecute Exit");
        }

        private void ShowNearbyPlaces(List<HashMap<String, String>> nearbyPlacesList) {
            mArrayPlaceList.clear();
            for (int i = 0; i < nearbyPlacesList.size(); i++) {
                Log.d("onPostExecute", "Entered into showing locations");

                HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
                double lat = Double.parseDouble(googlePlace.get("lat"));
                double lng = Double.parseDouble(googlePlace.get("lng"));
                String placeName = googlePlace.get("place_name");
                String vicinity = googlePlace.get("vicinity");

                if (!placeName.equalsIgnoreCase(vicinity)) {
                    LocationItem locationitem = new LocationItem(placeName, vicinity
                            , lat, lng);
                    mArrayPlaceList.add(locationitem);
                }
            }

            if (mArrayPlaceList.size() == 0) {
                activitylist.setVisibility(View.GONE);
                nolocationdata.setVisibility(View.VISIBLE);
                nolocation.setText("");
            } else {
                activitylist.setVisibility(View.VISIBLE);
                nolocationdata.setVisibility(View.GONE);
                mNearbyLocation_Adapter.notifyDataSetChanged();
            }

        }
    }


}
