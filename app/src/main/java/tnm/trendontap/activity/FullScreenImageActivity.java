package tnm.trendontap.activity;

/**
 * Created by admin on 11-Dec-17.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.adapter.ZoomImageAdapter;


public class FullScreenImageActivity extends FragmentActivity{
    ViewPager pager;
    CirclePageIndicator indicator;

    ZoomImageAdapter myPagerAdapter;
    private Context mContext;
    private List<String> imageList;
    LinearLayout back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        mContext = this;
        pager = (ViewPager) findViewById(R.id.loveimgpager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        back_arrow = (LinearLayout) findViewById(R.id.back_arrow);

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imageList = getIntent().getStringArrayListExtra("photoUrlsList");
        if (imageList == null) {
            indicator.setVisibility(View.GONE);
            return;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        myPagerAdapter = new ZoomImageAdapter(mContext, imageList);
        pager.setAdapter(myPagerAdapter);
        indicator.setViewPager(pager);
        if (imageList.size() > 1) {
            indicator.setVisibility(View.VISIBLE);
        } else {
            indicator.setVisibility(View.GONE);
        }
        pager.setCurrentItem(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.gc();
        finish();
    }




}
