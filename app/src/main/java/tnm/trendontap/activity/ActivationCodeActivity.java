package tnm.trendontap.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import tnm.trendontap.R;
import tnm.trendontap.utility.RobotoFont;

/**
 * Created by TNM on 12/20/2017.
 */

public class ActivationCodeActivity extends Activity {
    ImageView backimg;
    InputMethodManager imgr;
    EditText pw1, pw2, pw3, pw4;
    TextView digitstext, activationtxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activationcode);
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        activationtxt = (TextView) findViewById(R.id.activationtxt);
        activationtxt.setTypeface(RobotoFont.mediumFont(this));
        digitstext = (TextView) findViewById(R.id.digitstext);
        digitstext.setTypeface(RobotoFont.regularFont(this));

        pw1 = (EditText) findViewById(R.id.pw1);
        pw1.setTypeface(RobotoFont.regularFont(this));
        pw2 = (EditText) findViewById(R.id.pw2);
        pw2.setTypeface(RobotoFont.regularFont(this));
        pw3 = (EditText) findViewById(R.id.pw3);
        pw3.setTypeface(RobotoFont.regularFont(this));
        pw4 = (EditText) findViewById(R.id.pw4);
        pw4.setTypeface(RobotoFont.regularFont(this));


        pw4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return false;
            }
        });


        pw1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.i("text", i + "->" + i1 + "->" + i2);
                if (charSequence.toString().length() == 2 && i > 0) {
                    pw2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pw2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0 && charSequence.toString().length() == 0) {
                    pw1.requestFocus();
                } else if (charSequence.toString().length() == 2 && i > 0) {
                    pw3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pw3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0 && charSequence.toString().length() == 0) {
                    pw2.requestFocus();
                } else if (charSequence.toString().length() == 2 && i > 0) {
                    pw4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pw4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0 && charSequence.toString().length() == 0) {
                    pw3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        backimg = (ImageView) findViewById(R.id.backimg);
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        imgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.showSoftInput(pw1, InputMethodManager.SHOW_IMPLICIT);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                pw1.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                pw1.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_slide_in, R.anim.left_slide_out);
    }


}
