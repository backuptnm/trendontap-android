package tnm.trendontap.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import tnm.trendontap.R;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.Utils.confiramtionDialog;

/**
 * Created by TNM on 2/16/2018.
 */

public class RecycleImageActivity extends AppCompatActivity {

    LinearLayout back_arrow, bottom_layout;
    TextView labeltxt, delete, restore;
    ImageView largeimage;

    ReactionTrendItem reactionTrendItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycle_image_activity);

        delete = (TextView) findViewById(R.id.delete);
        delete.setTypeface(RobotoFont.regularFont(this));
        restore = (TextView) findViewById(R.id.restore);
        restore.setTypeface(RobotoFont.regularFont(this));
        labeltxt = (TextView) findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(this));
        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        back_arrow = (LinearLayout) findViewById(R.id.back_arrow);
        largeimage = (ImageView) findViewById(R.id.largeimage);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        reactionTrendItem = getIntent().getParcelableExtra("reactionTrendItem");
        if (!TextUtils.isEmpty(reactionTrendItem.getPhotoUrls().get(0))) {
            RequestOptions requestOptions = new RequestOptions();

            Glide.with(this)
                    .load(reactionTrendItem.getPhotoUrls().get(0))
                    .apply(requestOptions)
                    .into(largeimage);
        }


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReaction(reactionTrendItem.getId());
            }
        });


        restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restoreReaction(reactionTrendItem.getId(), reactionTrendItem.getPostID());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    public void deleteReaction(String reactionid) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction);
        databaseReference.child(reactionid).child(isDeleted).setValue(true);
        //databaseReference.child(reactionidlist.get(j).getId()).removeValue();

        final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts).child(reactionid);
        databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                if (reactionsCount > 0)
                    databaseReference1.child(ReactionsCount).setValue(reactionsCount - 1);
                confiramtionDialog(RecycleImageActivity.this, "Deleted");
                onBackPressed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onBackPressed();
            }
        });


    }

    public void restoreReaction(String reactionid, final String postid) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reactionid);
        databaseReference.child(RecycleBin).setValue(false);

       // Toast.makeText(this, "Restored", Toast.LENGTH_LONG).show();
        confiramtionDialog(this, "Restored");

        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).
                child(postid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();

                        DatabaseReference reactioncnt = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);
                        reactioncnt.child(postid).child(ReactionsCount).setValue(reactionsCount + 1);
                        onBackPressed();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        onBackPressed();
                    }
                });

    }
}
