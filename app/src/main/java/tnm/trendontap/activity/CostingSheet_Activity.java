package tnm.trendontap.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import tnm.trendontap.R;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.CostSheet_Model;
import tnm.trendontap.modal.CostsheetShareItem;

import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.DecimalDigitsInputFilter;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;


import static tnm.trendontap.utility.EventClass.COSTINSHEET;
import static tnm.trendontap.utility.EventClass.SENTMSG;

import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetID;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetSent;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathCostSheet;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.confiramtionDialog;
import static tnm.trendontap.utility.Utils.parseDouble;


public class CostingSheet_Activity extends Activity {

    ImageView more;
    TableLayout caseTable;
    private LayoutInflater mViewinflater;
    private TextView close, labeltxt, txt_total, txt_total_amount;
    private TextView txt_styleNumber, txt_supplier_name, txt_packaging_cost, txt_overheads, overheads,
            overheadstxt;
    private TextView txt_fabric_composition, txt_outer_fabric, txt_lining, packagingcost, packagingcosttxt;
    private String reaction_Id;

    ArrayList<HashMap<String, Object>> mCostingSheet;


    boolean isdisable = false;
    Preference preference;
    Users users = null;
    private List<EditText> edit_fabric_composition_list = new ArrayList<EditText>();
    private List<EditText> edit_outer_fabric_list = new ArrayList<EditText>();
    private List<EditText> edit_lining_list = new ArrayList<EditText>();

    private CostSheet_Model mCostSheet_Model, costSheet_model;

    private TextView txt_total_garment_cost, txt_total_garment_cost_txt, txt_supplier_name_txt;
    private EditText edt_style_number, edt_packaging_cost, edt_overheads;

    String costsheetid = "", postimg = "", post_id = "", sentto = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.costing_sheet_layout);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        mViewinflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        txt_total_garment_cost = (TextView) findViewById(R.id.txt_total_garment_cost);
        txt_total_garment_cost.setTypeface(RobotoFont.regularFont(this));
        txt_supplier_name_txt = (TextView) findViewById(R.id.txt_supplier_name_txt);
        txt_supplier_name_txt.setTypeface(RobotoFont.regularFont(this));
        txt_total_garment_cost_txt = (TextView) findViewById(R.id.txt_total_garment_cost_txt);
        txt_total_garment_cost_txt.setTypeface(RobotoFont.regularFont(this));
        edt_style_number = (EditText) findViewById(R.id.edt_style_number);
        edt_style_number.setTypeface(RobotoFont.regularFont(this));
        edt_packaging_cost = (EditText) findViewById(R.id.edt_packaging_cost);
        edt_packaging_cost.setTypeface(RobotoFont.regularFont(this));
        edt_packaging_cost.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(10, 2)});

        edt_overheads = (EditText) findViewById(R.id.edt_overheads);
        edt_overheads.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6, 2)});
        edt_overheads.setTypeface(RobotoFont.regularFont(this));

        edt_packaging_cost.addTextChangedListener(inputTextWatcher);
        edt_overheads.addTextChangedListener(inputTextWatcher);

        reaction_Id = getIntent().getStringExtra("Reaction_Id");
        costsheetid = getIntent().getStringExtra("costsheet_id");
        postimg = getIntent().getStringExtra("postimg");

        post_id = getIntent().getStringExtra("post_id");
        //  isdisable = getIntent().getBooleanExtra("isdisable", false);
        sentto = getIntent().getStringExtra("sentto");


        preference = new Preference(this, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        if (users.getUserType().equalsIgnoreCase(Utils.Buyer))
            isdisable = true;
        else
            isdisable = false;

        if (isdisable) {
            edt_style_number.setKeyListener(null);
            edt_packaging_cost.setKeyListener(null);
            edt_overheads.setKeyListener(null);
        }

        labeltxt = (TextView) findViewById(R.id.labeltxt);
        close = (TextView) findViewById(R.id.close);
        txt_total = (TextView) findViewById(R.id.txt_total);
        txt_total_amount = (TextView) findViewById(R.id.txt_total_amount);

        txt_styleNumber = (TextView) findViewById(R.id.txt_styleNumber);
        txt_supplier_name = (TextView) findViewById(R.id.txt_supplier_name);
        txt_packaging_cost = (TextView) findViewById(R.id.txt_packaging_cost);
        txt_overheads = (TextView) findViewById(R.id.txt_overheads);
        overheads = (TextView) findViewById(R.id.overheads);
        overheads.setTypeface(RobotoFont.regularFont(this));
        overheadstxt = (TextView) findViewById(R.id.overheadstxt);
        overheadstxt.setTypeface(RobotoFont.regularFont(this));

        txt_fabric_composition = (TextView) findViewById(R.id.txt_fabric_composition);
        txt_outer_fabric = (TextView) findViewById(R.id.txt_outer_fabric);
        txt_lining = (TextView) findViewById(R.id.txt_lining);

        txt_fabric_composition.setTypeface(RobotoFont.regularFont(this));
        txt_outer_fabric.setTypeface(RobotoFont.regularFont(this));
        txt_lining.setTypeface(RobotoFont.regularFont(this));

        packagingcost = (TextView) findViewById(R.id.packagingcost);
        packagingcost.setTypeface(RobotoFont.regularFont(this));

        packagingcosttxt = (TextView) findViewById(R.id.packagingcosttxt);
        packagingcosttxt.setTypeface(RobotoFont.regularFont(this));

        labeltxt.setTypeface(RobotoFont.boldFont(this));
        close.setTypeface(RobotoFont.mediumFont(this));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txt_styleNumber.setTypeface(RobotoFont.regularFont(this));
        txt_supplier_name.setTypeface(RobotoFont.regularFont(this));
        txt_packaging_cost.setTypeface(RobotoFont.regularFont(this));
        txt_overheads.setTypeface(RobotoFont.regularFont(this));

        txt_total.setTypeface(RobotoFont.boldFont(this));
        txt_total_amount.setTypeface(RobotoFont.boldFont(this));

        caseTable = (TableLayout) findViewById(R.id.caseTable);
        more = (ImageView) findViewById(R.id.more);

       /* if (reaction_Id.trim().equalsIgnoreCase("")) {
            more.setVisibility(View.GONE);
        } else {
            more.setVisibility(View.VISIBLE);
        }*/

        if (!costsheetid.equalsIgnoreCase("") && !costsheetid.equalsIgnoreCase("null")) {
            FirebaseDatabase.getInstance().getReference(FirebasePathCostSheet).child(costsheetid).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot costsheet) {

                            costSheet_model = costsheet.getValue(CostSheet_Model.class);
                            mCostingSheet = costSheet_model.getFabrics();
                            txt_supplier_name_txt.setText(costSheet_model.getSupplierName());
                            edt_style_number.setText(costSheet_model.getStyleNumber());
                            edt_packaging_cost.setText(String.format("%.2f", costSheet_model.getPackagingCost()));
                            edt_overheads.setText(String.format("%.2f", costSheet_model.getOverhead()));
                            overheadstxt.setText("$" + String.format("%.2f", costSheet_model.getTotalOverhead()));
                            txt_total_garment_cost_txt.setText("$" + String.format("%.2f", costSheet_model.getGarmentCost()));
                            txt_total_amount.setText("$" + String.format("%.2f", costSheet_model.getTotalCost()));
                            processFabrics(mCostingSheet);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        } else {
            mCostingSheet = new ArrayList<>();
            txt_supplier_name_txt.setText(users.getFirstName() + " " + users.getLastName());

            String[] name = {"Fabric/Yarn Cost", "Usage", "Total Fabric/Yarn Cost", "C.M. Cost", "Garment Add Ons", "Product Trim Cost"};


            for (int j = 0; j < name.length; j++) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("FabricComposition", name[j]);
                hashMap.put("OuterFabric", 0);
                hashMap.put("Lining", 0);
                mCostingSheet.add(hashMap);
            }

            processFabrics(mCostingSheet);
        }

        /*if (isdisable) {
            more.setVisibility(View.GONE);
        } else {
            more.setVisibility(View.VISIBLE);
        }*/
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsDialog(CostingSheet_Activity.this);
            }
        });
    }


    TextWatcher inputTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try {
                double txt_total_garment_cost_val = calculateTotalGarment();

                txt_total_garment_cost_txt.setText("$" + String.format("%.2f", txt_total_garment_cost_val));

                double packagingcostval = parseDouble(edt_packaging_cost.getText().toString().trim(), 0);
                double overheadstxtval = parseDouble(edt_overheads.getText().toString().trim(), 0);
                overheadstxtval = txt_total_garment_cost_val * overheadstxtval / 100;
                overheadstxtval = parseDouble(String.format("%.2f", overheadstxtval), 0);

                packagingcosttxt.setText("$" + String.format("%.2f", packagingcostval));
                overheadstxt.setText("$" + String.format("%.2f", overheadstxtval));

                txt_total_amount.setText("$" + String.format("%.2f", packagingcostval + overheadstxtval + txt_total_garment_cost_val));


            } catch (NumberFormatException e) {

            }
        }
    };

    public void processFabrics(ArrayList<HashMap<String, Object>> mCostingSheet) {
        for (int i = 0; i < mCostingSheet.size(); i++) {
            View tr = mViewinflater.inflate(R.layout.table_raw_layout, null, false);
            caseTable.addView(tr);

            EditText edt_fabric_composition = (EditText) tr.findViewById(R.id.edt_fabric_composition);
            edt_fabric_composition.setTypeface(RobotoFont.regularFont(CostingSheet_Activity.this));
            edt_fabric_composition.setSelection(0);

            EditText edt_outer_fabric = (EditText) tr.findViewById(R.id.edt_outer_fabric);
            edt_outer_fabric.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(10, 2)});
            edt_outer_fabric.setTypeface(RobotoFont.regularFont(CostingSheet_Activity.this));
            EditText edt_lining = (EditText) tr.findViewById(R.id.edt_lining);
            edt_lining.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(10, 2)});
            edt_lining.setTypeface(RobotoFont.regularFont(CostingSheet_Activity.this));
            edt_fabric_composition.setText(String.valueOf(mCostingSheet.get(i).get("FabricComposition")));

            try {
                long outerFabric = (long) mCostingSheet.get(i).get("OuterFabric");
                if (outerFabric > 0)
                    edt_outer_fabric.setText(String.valueOf(outerFabric));
                else
                    edt_outer_fabric.setText("");
            } catch (Exception e) {
                try {
                    int outerFabric = (int) mCostingSheet.get(i).get("OuterFabric");
                    if (outerFabric > 0)
                        edt_outer_fabric.setText(String.valueOf(outerFabric));
                    else
                        edt_outer_fabric.setText("");
                } catch (Exception ex) {
                    edt_outer_fabric.setText(mCostingSheet.get(i).get("OuterFabric").toString());
                }
            }

            try {
                long lining = (long) mCostingSheet.get(i).get("Lining");
                if (lining > 0)
                    edt_lining.setText(String.valueOf(lining));
                else
                    edt_lining.setText("");

            } catch (Exception e) {
                try {
                    int lining = (int) mCostingSheet.get(i).get("Lining");
                    if (lining > 0)
                        edt_lining.setText(String.valueOf(lining));
                    else
                        edt_lining.setText("");
                } catch (Exception ex) {
                    edt_lining.setText(mCostingSheet.get(i).get("Lining").toString());
                }
            }


            edt_outer_fabric.addTextChangedListener(inputTextWatcher);
            edt_lining.addTextChangedListener(inputTextWatcher);

            edt_fabric_composition.setSelection(edt_fabric_composition.getText().length());
            edt_outer_fabric.setSelection(edt_outer_fabric.getText().length());
            edt_lining.setSelection(edt_lining.getText().length());
            if (isdisable) {
                edt_fabric_composition.setKeyListener(null);
                edt_outer_fabric.setKeyListener(null);
                edt_lining.setKeyListener(null);
            }
            edit_fabric_composition_list.add(edt_fabric_composition);
            edit_outer_fabric_list.add(edt_outer_fabric);
            edit_lining_list.add(edt_lining);


        }
    }

    public double calculateTotalGarment() {
        double sum = 0;
        int edit_outer_fabric_list_val = edit_outer_fabric_list.size();
        for (int i = 0; i < edit_outer_fabric_list_val; i++) {
            sum += parseDouble(edit_outer_fabric_list.get(i).getText().toString(), 0);
            sum += parseDouble(edit_lining_list.get(i).getText().toString(), 0);
        }
        return sum;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    Dialog optionDialog;

    private void onOptionsDialog(Context activity) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_costsheet_options);
        dialog.setCancelable(false);

        optionDialog = dialog;
        View txt_add_row_line = ((View) dialog.findViewById(R.id.txt_add_row_line));
        View update_line = ((View) dialog.findViewById(R.id.update_line));
        View txt_send_line = ((View) dialog.findViewById(R.id.txt_send_line));

        TextView txt_add_row = ((TextView) dialog.findViewById(R.id.txt_add_row));
        txt_add_row.setTypeface(RobotoFont.regularFont(this));
        txt_add_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRow(dialog);
            }
        });


        TextView update = ((TextView) dialog.findViewById(R.id.update));
        update.setTypeface(RobotoFont.regularFont(this));
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!costsheetid.equalsIgnoreCase("") && !costsheetid.equalsIgnoreCase("null"))
                    validation(dialog, "update");
                else
                    validation(dialog, "save");
            }
        });

        if (!costsheetid.equalsIgnoreCase("") && !costsheetid.equalsIgnoreCase("null")) {
            update.setText("Update");
        } else {
            update.setText("Save");
        }

        TextView txt_send = ((TextView) dialog.findViewById(R.id.txt_send));
        txt_send.setTypeface(RobotoFont.regularFont(this));
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation(dialog, "send");
            }
        });

        TextView txt_share = ((TextView) dialog.findViewById(R.id.txt_share));
        txt_share.setTypeface(RobotoFont.regularFont(this));
        txt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(CostingSheet_Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(CostingSheet_Activity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
                else {
                    shareCostSheet();
                }
            }
        });


        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            txt_add_row.setVisibility(View.GONE);
            txt_add_row_line.setVisibility(View.GONE);
            update.setVisibility(View.GONE);
            update_line.setVisibility(View.GONE);
            txt_send.setVisibility(View.GONE);
            txt_send_line.setVisibility(View.GONE);
        }

        TextView txt_cancel = ((TextView) dialog.findViewById(R.id.txt_cancel));
        txt_cancel.setTypeface(RobotoFont.regularFont(this));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (ActivityCompat.checkSelfPermission(CostingSheet_Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                Toast.makeText(CostingSheet_Activity.this, "You must allow write storage permission", Toast.LENGTH_LONG).show();
            else {
                shareCostSheet();
            }
        }
    }

    public void addRow(Dialog dialog) {
        View tr = mViewinflater.inflate(R.layout.table_raw_layout, null, false);
        caseTable.addView(tr);

        EditText edt_fabric_composition = (EditText) tr.findViewById(R.id.edt_fabric_composition);
        EditText edt_outer_fabric = (EditText) tr.findViewById(R.id.edt_outer_fabric);
        EditText edt_lining = (EditText) tr.findViewById(R.id.edt_lining);
        edt_outer_fabric.addTextChangedListener(inputTextWatcher);
        edt_lining.addTextChangedListener(inputTextWatcher);
        edt_fabric_composition.append("");
        edt_outer_fabric.append("");
        edt_lining.append("");

        edit_fabric_composition_list.add(edt_fabric_composition);
        edit_outer_fabric_list.add(edt_outer_fabric);
        edit_lining_list.add(edt_lining);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("FabricComposition", "");
        hashMap.put("OuterFabric", 0);
        hashMap.put("Lining", 0);
        mCostingSheet.add(hashMap);

        if (dialog.isShowing())
            dialog.dismiss();
    }

    public void validation(Dialog dialog, String action) {
        if (edt_style_number.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(CostingSheet_Activity.this, "Please enter style number", Toast.LENGTH_LONG).show();
            if (dialog.isShowing())
                dialog.dismiss();
        } else if (edt_packaging_cost.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(CostingSheet_Activity.this, "Please enter packaging cost", Toast.LENGTH_LONG).show();
            if (dialog.isShowing())
                dialog.dismiss();
        } else if (edt_overheads.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(CostingSheet_Activity.this, "Please enter overheads", Toast.LENGTH_LONG).show();
            if (dialog.isShowing())
                dialog.dismiss();
        } else if (parseDouble(txt_total_garment_cost_txt.getText().toString().trim().substring(1), 0) <= 0) {
            Toast.makeText(CostingSheet_Activity.this, "Total garment cost should be more than 0", Toast.LENGTH_LONG).show();
            if (dialog.isShowing())
                dialog.dismiss();
        } else if (action.equalsIgnoreCase("update")) {
            updateCostsheet(dialog);
        } else if (action.equalsIgnoreCase("send") || action.equalsIgnoreCase("save")) {
            sendCostsheet(dialog, action);
        }

    }

    public void updateCostsheet(Dialog dialog) {
        mCostingSheet.clear();
        for (int i = 0; i < edit_fabric_composition_list.size(); i++) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("FabricComposition", edit_fabric_composition_list.get(i).getText().toString());
            long outer_fabric_list = 0;
            if (!edit_outer_fabric_list.get(i).getText().toString().equalsIgnoreCase(""))
                outer_fabric_list = Long.parseLong(edit_outer_fabric_list.get(i).getText().toString());
            hashMap.put("OuterFabric", outer_fabric_list);

            long lining_list = 0;
            if (!edit_lining_list.get(i).getText().toString().equalsIgnoreCase(""))
                lining_list = Long.parseLong(edit_lining_list.get(i).getText().toString());
            hashMap.put("Lining", lining_list);
            mCostingSheet.add(hashMap);
        }

        DatabaseReference updatecost = FirebaseDatabase.getInstance().getReference(FirebasePathCostSheet);
        mCostSheet_Model = new CostSheet_Model("" + sentto, mCostingSheet, parseDouble(txt_total_garment_cost_txt.getText().toString().trim().substring(1), 0), costsheetid, parseDouble(edt_overheads.getText().toString().trim(), 0), parseDouble(packagingcosttxt.getText().toString().trim().substring(1), 0), "" + reaction_Id, edt_style_number.getText().toString().trim(), "" + users.getFirstName() + " " + users.getLastName(), System.currentTimeMillis(), parseDouble(txt_total_amount.getText().toString().trim().substring(1), 0), parseDouble(overheadstxt.getText().toString().trim().substring(1), 0), "" + users.getUserID());
        updatecost.child(costsheetid).setValue(mCostSheet_Model);
        Toast.makeText(this, "Updated", Toast.LENGTH_LONG).show();

        if (dialog.isShowing())
            dialog.dismiss();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onBackPressed();
            }
        }, 500);

    }

    public void sendCostsheet(Dialog dialog, String action) {

        mCostingSheet.clear();
        for (int i = 0; i < edit_fabric_composition_list.size(); i++) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("FabricComposition", edit_fabric_composition_list.get(i).getText().toString());
            double outer_fabric_list = 0;
            if (!edit_outer_fabric_list.get(i).getText().toString().equalsIgnoreCase(""))
                outer_fabric_list = Double.parseDouble(edit_outer_fabric_list.get(i).getText().toString());
            hashMap.put("OuterFabric", outer_fabric_list);

            double lining_list = 0;
            if (!edit_lining_list.get(i).getText().toString().equalsIgnoreCase(""))
                lining_list = Double.parseDouble(edit_lining_list.get(i).getText().toString());
            hashMap.put("Lining", lining_list);
            mCostingSheet.add(hashMap);
        }

        DatabaseReference usersref = FirebaseDatabase.getInstance().getReference().child(FirebasePathCostSheet);
        String key = "";
        if (!costsheetid.equalsIgnoreCase("") && !costsheetid.equalsIgnoreCase("null")) {
            key = costsheetid;
        } else {
            key = usersref.push().getKey();
        }
        mCostSheet_Model = new CostSheet_Model("" + sentto, mCostingSheet, parseDouble(txt_total_garment_cost_txt.getText().toString().trim().substring(1), 0), key, parseDouble(edt_overheads.getText().toString().trim(), 0), parseDouble(packagingcosttxt.getText().toString().trim().substring(1), 0), "" + reaction_Id, edt_style_number.getText().toString().trim(), "" + users.getFirstName() + " " + users.getLastName(), System.currentTimeMillis(), parseDouble(txt_total_amount.getText().toString().trim().substring(1), 0), parseDouble(overheadstxt.getText().toString().trim().substring(1), 0), "" + users.getUserID());
        usersref.child(key).setValue(mCostSheet_Model);

        DatabaseReference posts = FirebaseDatabase.getInstance().getReference(FirebasePathPostReaction).child(reaction_Id);
        if (action.equalsIgnoreCase("send")) {
            posts.child(CostsheetSent).setValue(true);
            ActivityItem activityItem = new ActivityItem();
            activityItem.setProfilePic(users.getProfilePic());
            activityItem.setLongMessage(SENTMSG);
            activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + SENTMSG);
            activityItem.setPostID(post_id);
            activityItem.setPostImageURL(postimg);
            activityItem.setReactionID(reaction_Id);
            activityItem.setShortMessage(COSTINSHEET);
            activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
            activityItem.setActivityType(COSTINSHEET);
            activityItem.setTimeStamp(System.currentTimeMillis());
            activityItem.setUserID(users.getUserID());

            String msg = users.getFirstName() + " " + users.getLastName() + SENTMSG + COSTINSHEET;
            sendActivityToUser(sentto, activityItem, msg);
            confiramtionDialog(this, "Sent");
            // Toast.makeText(this, "Sent", Toast.LENGTH_LONG).show();
        } else {
            confiramtionDialog(this, "Saved");
            // Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
        }

        // posts.child(ID).setValue(key);
        posts.child(CostsheetID).setValue(key);

        if (dialog.isShowing())
            dialog.dismiss();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onBackPressed();
            }
        }, 1500);
    }


    public void shareCostSheet() {

        mCostingSheet = costSheet_model.getFabrics();

        ArrayList<CostsheetShareItem> costsheetShareItemArrayList = new ArrayList<>();
        costsheetShareItemArrayList.add(new CostsheetShareItem("Supplier Name:", costSheet_model.getSupplierName(), ""));
        costsheetShareItemArrayList.add(new CostsheetShareItem("Style Number:", costSheet_model.getStyleNumber(), ""));
        costsheetShareItemArrayList.add(new CostsheetShareItem("Packaging Cost:", "$" + String.valueOf(costSheet_model.getPackagingCost()), ""));
        costsheetShareItemArrayList.add(new CostsheetShareItem("Overheads(%):", String.valueOf(costSheet_model.getOverhead()), ""));
        costsheetShareItemArrayList.add(new CostsheetShareItem("Febric Composition", "Outer Fabric", "Lining"));
        for (int f = 0; f < mCostingSheet.size(); f++) {
            costsheetShareItemArrayList.add(new CostsheetShareItem(
                    String.valueOf(mCostingSheet.get(f).get("FabricComposition")),
                    "$" + String.valueOf(mCostingSheet.get(f).get("OuterFabric")),
                    "$" + String.valueOf(mCostingSheet.get(f).get("Lining"))));
        }

        String garmentcost = String.format("%.2f", costSheet_model.getGarmentCost());
        costsheetShareItemArrayList.add(new CostsheetShareItem("Total Garment Cost", "$" + garmentcost, ""));

        String packagingcost = String.format("%.2f", costSheet_model.getPackagingCost());
        costsheetShareItemArrayList.add(new CostsheetShareItem("Packaging Cost", "$" + packagingcost, ""));

        String overheads = String.format("%.2f", costSheet_model.getTotalOverhead());
        costsheetShareItemArrayList.add(new CostsheetShareItem("Overheads", "$" + overheads, ""));

        String totalcost = String.format("%.2f", costSheet_model.getTotalCost());
        costsheetShareItemArrayList.add(new CostsheetShareItem("Total Cost", "$" + totalcost, ""));
        generatexls(costsheetShareItemArrayList, optionDialog);
    }


    public void generatexls(ArrayList<CostsheetShareItem> costsheetShareItemArrayList, Dialog dialog) {


        String Fnamexls = "Reaction-Costingsheet-" + costsheetShareItemArrayList.get(0).getItem2() + ".xls";

        File sdCard = Environment.getExternalStorageDirectory();

        File directory = new File(sdCard.getAbsolutePath() + "/" + getResources().getString(R.string.app_name));
        if (!directory.exists())
            directory.mkdirs();

        File myFilePath = new File(directory, Fnamexls);

        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook;
        try {
            workbook = Workbook.createWorkbook(myFilePath, wbSettings);
            WritableSheet sheet = workbook.createSheet(Fnamexls, 0);
            int margin = 1;
            for (int j = 0; j < costsheetShareItemArrayList.size(); j++) {

                try {
                    if (costsheetShareItemArrayList.get(j).getItem1().equalsIgnoreCase("Total Garment Cost")) {
                        margin = 3;
                    }
                } catch (Exception e) {

                }

                try {
                    if (costsheetShareItemArrayList.get(j).getItem1().equalsIgnoreCase("Febric Composition")) {
                        margin = 2;
                    }
                } catch (Exception e) {

                }

                Label label01 = new Label(0, j + margin, costsheetShareItemArrayList.get(j).getItem1());
                Label label02 = new Label(1, j + margin, costsheetShareItemArrayList.get(j).getItem2());
                Label label03 = new Label(2, j + margin, costsheetShareItemArrayList.get(j).getItem3());

                sheet.addCell(label01);
                sheet.addCell(label02);
                sheet.addCell(label03);


            }
            workbook.write();
            workbook.close();
        } catch (IOException | WriteException e) {
            e.printStackTrace();
        }

        if (myFilePath.exists()) {
            dialog.dismiss();
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            intentShareFile.setType("application/vnd.ms-excel");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    Fnamexls);
            startActivity(intentShareFile);

        } else {
            Toast.makeText(this, "File not exist", Toast.LENGTH_LONG).show();
        }
    }
}
