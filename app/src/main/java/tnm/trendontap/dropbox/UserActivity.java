package tnm.trendontap.dropbox;

import android.os.Bundle;

import tnm.trendontap.R;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dropbox.core.android.Auth;

import com.dropbox.core.v2.users.FullAccount;

import tnm.trendontap.R;


/**
 * Activity that shows information about the currently logged in user
 */
public class UserActivity extends DropboxActivity {

    boolean isloaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        isloaded = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isloaded || hasToken()) {
            if (hasToken()) {
                startActivity(FilesActivity.getIntent(UserActivity.this, ""));
                finish();
            } else {
                isloaded = true;
                Auth.startOAuth2Authentication(UserActivity.this, "5yugz8yjght1jzb");
            }
        } else {
            finish();
        }
    }

    @Override
    protected void loadData() {
        new GetCurrentAccountTask(DropboxClientFactory.getClient(), new GetCurrentAccountTask.Callback() {
            @Override
            public void onComplete(FullAccount result) {
            }

            @Override
            public void onError(Exception e) {
                Log.e(getClass().getName(), "Failed to get account details.", e);
            }
        }).execute();
    }


}
