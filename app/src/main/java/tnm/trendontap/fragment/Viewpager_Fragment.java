package tnm.trendontap.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.PDFViewActivity;
import tnm.trendontap.activity.UploadFromFileManager;
import tnm.trendontap.adapter.TrendAdapter;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.Catagory;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.FirstName;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.PdfURL;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserID;


public class Viewpager_Fragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextView nottrends, taptostart;
    LinearLayout notrendsdata;
    ListView trendlistview;
    TrendAdapter trendAdapter;
    ArrayList<TrendsItem> trendsItemList;
    Context context;
    Activity activity;
    private String mParam1;
    private String mParam2 = "";
    ImageView addtrends;
    DatabaseReference databaseReference;
    private static final String TAG = "PDFTools";
    int selectedPos = -1;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        trendsItemList = new ArrayList<>();
    }

    public static Viewpager_Fragment newInstance(String param1, String param2) {
        Viewpager_Fragment fragment = new Viewpager_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //if (view != null)
        //   return view;

        view = inflater.inflate(R.layout.fragment_viewpager, container, false);
        activity = getActivity();
        context = getActivity();

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
        }
        nottrends = (TextView) view.findViewById(R.id.nottrends);
        nottrends.setTypeface(RobotoFont.mediumFont(activity));
        taptostart = (TextView) view.findViewById(R.id.taptostart);
        taptostart.setTypeface(RobotoFont.mediumFont(activity));
        notrendsdata = (LinearLayout) view.findViewById(R.id.notrendsdata);
        trendlistview = (ListView) view.findViewById(R.id.trendlist);

        trendAdapter = new TrendAdapter(context, trendsItemList);
        trendlistview.setAdapter(trendAdapter);
        trendlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                manageclick(position);
            }
        });

        addtrends = (ImageView) view.findViewById(R.id.addtrends);
        addtrends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadTrendOption(context);
            }
        });
        // setTrendlist();
        getTrendCount();
        //  getPostList();
        return view;
    }

    public void manageclick(int position) {
        if (trendsItemList == null && trendsItemList.size() == 0)
            return;

        selectedPos = position;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 13);
        } else {
            String pdfUrl = trendsItemList.get(position).getPdfUrl();
            downloadPDF(pdfUrl);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 13) {
            int access_write = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_write != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Sorry!!!, you can't open File without granting permission", Toast.LENGTH_LONG).show();
            } else {
                String pdfUrl = trendsItemList.get(selectedPos).getPdfUrl();
                downloadPDF(pdfUrl);
            }
        }

    }

    int count = 0;

    public void getTrendCount() {

        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(Catagory).equalTo(mParam1).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        count = (int) dataSnapshot.getChildrenCount();
                        if (!dataSnapshot.hasChildren()) {
                            notrendsdata.setVisibility(View.VISIBLE);
                            trendlistview.setVisibility(View.GONE);
                            return;
                        }
                        notrendsdata.setVisibility(View.GONE);
                        trendlistview.setVisibility(View.VISIBLE);
                        getPostList();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getPostList() {
        trendsItemList.clear();
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(Catagory).equalTo(mParam1).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        notrendsdata.setVisibility(View.GONE);
                        trendlistview.setVisibility(View.VISIBLE);
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void manageData(DataSnapshot trendlist, String action) {

        if (String.valueOf(trendlist.child(UserID).getValue()).
                equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            long likecnt = (long) trendlist.child(LikesCount).getValue();

            LocationItem location = null;
            if (trendlist.hasChild(Location)) {
                if (trendlist.child(Location).hasChild(Title)) {
                    location = new LocationItem(String.valueOf(trendlist.child(Location).child(Title).getValue()),
                            String.valueOf(trendlist.child(Location).child(Address).getValue()),
                            (double) trendlist.child(Location).child(Latitude).getValue(),
                            (double) trendlist.child(Location).child(Logitude).getValue());
                }
            }

            if (location == null)
                location = new LocationItem();

            final TrendsItem trendsItem = new TrendsItem(String.valueOf(trendlist.child(PostTitle).getValue()),
                    String.valueOf(trendlist.child(CatagoryName).getValue()),
                    String.valueOf(trendlist.child(Content).getValue()),
                    "",
                    String.valueOf(trendlist.child(DeliveryDate).getValue()),
                    (long) trendlist.child(TimeStamp).getValue(),
                    String.valueOf(trendlist.child(ReactionsCount).getValue()),
                    (boolean) trendlist.child(UnreadReactionCount).getValue(),
                    (ArrayList<String>) trendlist.child(FireStoreUtils.tPostPhotoId).getValue(),
                    String.valueOf(trendlist.child(ID).getValue()), "", "",
                    likecnt, location, String.valueOf(trendlist.child(PdfURL).getValue()),
                    String.valueOf(trendlist.child(FireStoreUtils.UserID).getValue()));


            if (action.equalsIgnoreCase("edit")) {
                int pos = matchValue(trendsItemList, trendsItem.getId());
                if (pos == -1)
                    return;
                trendsItemList.set(pos, trendsItem);
                trendAdapter.notifyDataSetChanged();
            } else if (action.equalsIgnoreCase("delete")) {
                int pos = matchValue(trendsItemList, trendsItem.getId());
                if (pos == -1) {
                    if (trendsItemList.size() == 0) {
                        notrendsdata.setVisibility(View.VISIBLE);
                        trendlistview.setVisibility(View.GONE);
                    }
                    return;
                }
                trendsItemList.remove(pos);
                trendAdapter.notifyDataSetChanged();
                if (trendsItemList.size() == 0) {
                    notrendsdata.setVisibility(View.VISIBLE);
                    trendlistview.setVisibility(View.GONE);
                }
            } else {
                trendsItemList.add(trendsItem);

                // if (trendsItemList.size() == count) {
                trendAdapter.addList(trendsItemList);
                notrendsdata.setVisibility(View.GONE);
                trendlistview.setVisibility(View.VISIBLE);

                //}
            }
            if (!action.equalsIgnoreCase("delete"))
                getPostLikeCount(trendsItem);


        }
    }


    public void manageLikeData(final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(trendsItem.getId()).child(Likes).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getUserDetails(dataSnapshot.getKey(), trendsItem);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void getPostLikeCount(final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(trendsItem.getId()).child(Likes).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            int pos = matchValue(trendsItemList, trendsItem.getId());
                            if (pos == -1)
                                return;
                            trendsItem.setLikecnt(0);
                            trendsItemList.set(pos, trendsItem);
                            trendAdapter.notifyDataSetChanged();
                            managePos();
                            return;
                        }
                        trendsItem.setLikecnt((int) dataSnapshot.getChildrenCount());
                        manageLikeData(trendsItem);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void managePos() {
        trendlistview.post(new Runnable() {
            @Override
            public void run() {
                if (!mParam2.equalsIgnoreCase("")) {
                    int pos = matchValue(trendsItemList, mParam2);
                    if (pos == -1) {
                        return;
                    }
                    trendlistview.setSelection(2);
                }

            }
        });
    }

    public void getUserDetails(String userid, final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (trendsItem.getLiker1().equalsIgnoreCase("")) {
                            trendsItem.setLiker1(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        } else if (trendsItem.getLiker2().equalsIgnoreCase("")) {
                            trendsItem.setLiker2(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        }

                        int pos = matchValue(trendsItemList, trendsItem.getId());
                        if (pos == -1)
                            return;
                        trendsItemList.set(pos, trendsItem);
                        trendAdapter.notifyDataSetChanged();
                        managePos();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public int matchValue(ArrayList<TrendsItem> trendsItemList, String value) {
        int pos = -1;
        for (int j = 0; j < trendsItemList.size(); j++) {
            if (trendsItemList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    protected void uploadTrendOption(final Context activity) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_uploadtrend_options);
        dialog.setCancelable(false);
        TextView txt_upload_trends = (TextView) dialog.findViewById(R.id.txt_upload_trends);
        txt_upload_trends.setTypeface(RobotoFont.regularFont(activity));

        TextView txt_dropbox = (TextView) dialog.findViewById(R.id.txt_dropbox);
        txt_dropbox.setTypeface(RobotoFont.regularFont(activity));

        txt_dropbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(activity, UserActivity.class));
                dialog.dismiss();
            }
        });

        TextView textpdf = (TextView) dialog.findViewById(R.id.textpdf);
        textpdf.setTypeface(RobotoFont.regularFont(context));
        TextView txt_google_drive = (TextView) dialog.findViewById(R.id.txt_google_drive);
        txt_google_drive.setTypeface(RobotoFont.regularFont(activity));

        TextView txt_upload_file = (TextView) dialog.findViewById(R.id.txt_upload_file);
        txt_upload_file.setTypeface(RobotoFont.regularFont(activity));

        txt_upload_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, UploadFromFileManager.class));
                ((Activity) activity).overridePendingTransition(R.anim.slide_up, R.anim.stay);
                dialog.dismiss();
            }
        });

        txt_google_drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new GoogleDriveListFragment(), false);
                dialog.dismiss();
                //  startActivity(new Intent(context, GoogleDriveActivity.class));
                //((Activity) activity).overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void downloadPDF(String url) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];
        viewPDF(name);
      /*  if (!viewPDF(name))
            new DownloadFile().execute(url, name);*/
    }

    public boolean viewPDF(String name) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf

        if (!pdfFile.exists()) {
            return false;
        }
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(context, PDFViewActivity.class);
        pdfIntent.putExtra("pdfUrl", path.toString());
        try {
            startActivity(pdfIntent);
        } catch (Exception e) {
        }
        return true;
    }


}
