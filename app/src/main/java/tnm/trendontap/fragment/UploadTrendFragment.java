package tnm.trendontap.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import tnm.trendontap.R;
import tnm.trendontap.activity.CategoriesActivity;
import tnm.trendontap.activity.LocationActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.SeasonActivity;
import tnm.trendontap.activity.TagItemActivity;
import tnm.trendontap.adapter.LoveTrendSupplierAdapter;

import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.PostLovesItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.EventClass;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.WheelView;

import static android.app.Activity.RESULT_OK;
import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.LOVE;
import static tnm.trendontap.utility.EventClass.POSTMSG;
import static tnm.trendontap.utility.EventClass.TREND;
import static tnm.trendontap.utility.EventClass.processActivity;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getResizedBitmap;
import static tnm.trendontap.utility.Utils.hideSoftKeyboard;
import static tnm.trendontap.utility.Utils.uploadpath;

public class UploadTrendFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "UploadLoveFragment";
    LinearLayout lnrDate, lnrTagItem, lnrAddLocation, lnrSelectCategory, back_arrow, labelPost;
    EditText etSayAbouttrend;
    TextView labeltxt, labelPosttxt, tvSelectDelivery, tvDate, tvTagItem, tvAddLocation, tvSelectCategory;
    ImageView imageSelectAll, imgTag, addlocation;
    SquareImageView imagetrend;
    StickyListHeadersListView supplierList;
    LinearLayout lnrgetlocation;
    TextView loc_name, loc_address;
    private Context mContext;
    private FragmentActivity mActivity;
    TextView loc_latitude, loc_longitude;
    DatabaseReference databaseReference;
    LoveTrendSupplierAdapter loveTrendSupplierAdapter;
    ArrayList<Users> supplierItemArrayList;
    ProgressDialog progressDialog;
    // String selectedImage;
    int index = 0;
    Bitmap bitmap;
    LocationItem place;
    public static final int Location_Flag = 101;
    private String mImageurl;
    ArrayList<Tag_Items> tag_items = null;
    LocationItem locationItemslist = null;
    String action = "", uploadpost_id = "";
    String seasonid = "";
    Preference preference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();


        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        //   selectedImage = getArguments().getString("selectedImage", "");
        if (getArguments().getString("action") != null) {
            action = getArguments().getString("action");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_trend, container, false);
        setUI(view);
        getData();
        setData();
        if (!action.equalsIgnoreCase("edit"))
            getImage();
        return view;
    }

    public void getImage() {
        String trendimg = getArguments().getString("trendimg");
        java.io.File pdfFile = new java.io.File(trendimg);
        if (pdfFile.exists()) {
            try {
                PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(pdfFile,
                        ParcelFileDescriptor.MODE_READ_ONLY));
                PdfRenderer.Page page = renderer.openPage(0);

                bitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                canvas.drawColor(Color.WHITE);
                canvas.drawBitmap(bitmap, 0, 0, null);
                page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                imagetrend.setImageBitmap(bitmap);
                page.close();
                renderer.close();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    private void setUI(View view) {
        loc_latitude = view.findViewById(R.id.loc_latitude);
        loc_longitude = view.findViewById(R.id.loc_longitude);
        etSayAbouttrend = view.findViewById(R.id.etSayAbouttrend);
        supplierList = (StickyListHeadersListView) view.findViewById(R.id.supplierList);
        back_arrow = view.findViewById(R.id.back_arrow);
        imagetrend = view.findViewById(R.id.imagetrend);
        imageSelectAll = view.findViewById(R.id.imageSelectAll);
        imgTag = view.findViewById(R.id.imgTag);
        addlocation = view.findViewById(R.id.addlocation);
        lnrgetlocation = view.findViewById(R.id.lnrgetlocation);
        loc_name = view.findViewById(R.id.loc_name);
        loc_address = view.findViewById(R.id.loc_address);
        lnrDate = view.findViewById(R.id.lnrDate);
        lnrTagItem = view.findViewById(R.id.lnrTagItem);
        lnrAddLocation = view.findViewById(R.id.lnrAddLocation);
        lnrSelectCategory = view.findViewById(R.id.lnrSelectCategory);
        labeltxt = view.findViewById(R.id.labeltxt);
        labelPost = view.findViewById(R.id.labelPost);
        labelPosttxt = view.findViewById(R.id.labelPosttxt);
        tvSelectDelivery = view.findViewById(R.id.tvSelectDelivery);
        tvDate = view.findViewById(R.id.tvDate);
        tvTagItem = view.findViewById(R.id.tvTagItem);
        tvAddLocation = view.findViewById(R.id.tvAddLocation);
        tvSelectCategory = view.findViewById(R.id.tvSelectCategory);


        labeltxt.setTypeface(RobotoFont.boldFont(mContext));
        labelPosttxt.setTypeface(RobotoFont.mediumFont(mContext));

        tvSelectDelivery.setTypeface(RobotoFont.mediumFont(mContext));
        tvDate.setTypeface(RobotoFont.mediumFont(mContext));
        tvTagItem.setTypeface(RobotoFont.mediumFont(mContext));
        tvAddLocation.setTypeface(RobotoFont.mediumFont(mContext));
        tvSelectCategory.setTypeface(RobotoFont.mediumFont(mContext));

        labeltxt.setTypeface(RobotoFont.boldFont(mActivity));
        labeltxt.setText("Upload trend");

        lnrSelectCategory.setOnClickListener(this);
        lnrDate.setOnClickListener(this);
        labelPost.setOnClickListener(this);
        back_arrow.setOnClickListener(this);
        //  lnrTagItem.setOnClickListener(this);
        lnrAddLocation.setOnClickListener(this);

    }

    private void setData() {
        supplierItemArrayList = new ArrayList<>();
        loveTrendSupplierAdapter = new LoveTrendSupplierAdapter(mContext, supplierItemArrayList);
        supplierList.setAdapter(loveTrendSupplierAdapter);
        supplierList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        supplierList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                loveTrendSupplierAdapter.toggleSelection(position);
            }
        });
        supplierList();
    }

    public void supplierList() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers);
        index = 0;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                supplierItemArrayList.clear();
                final int childrenCount = (int) dataSnapshot.getChildrenCount();
                if (childrenCount == 0) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    return;
                }
                for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(childDataSnapshot.getKey());
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Users users = dataSnapshot.getValue(Users.class);
                            supplierItemArrayList.add(users);

                            if (index + 1 == childrenCount) {
                                loveTrendSupplierAdapter.addList(supplierItemArrayList);
                                if (progressDialog != null && progressDialog.isShowing())
                                    progressDialog.dismiss();
                            } else
                                index++;
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrSelectCategory:
                startActivityForResult(new Intent(getContext(), SeasonActivity.class), 100);
                mActivity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.lnrDate:
                selectDate();
                break;
            case R.id.back_arrow:
                mActivity.onBackPressed();
                break;
           /* case R.id.lnrTagItem:
                boolean isBitmap = false;
                if (getArguments().containsKey("selectedImage")) {
                    isBitmap = true;
                }

                String imageUrl = "";
                if (isBitmap)
                    imageUrl = selectedImage;
                else
                    imageUrl = mImageurl;

                Intent intent = new Intent(mActivity, TagItemActivity.class);
                intent.putParcelableArrayListExtra("tag_items_list", tag_items);
                intent.putExtra("imageUrl", imageUrl);
                intent.putExtra("isBitmap", isBitmap);
                startActivityForResult(intent, 1001);
                mActivity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;*/
            case R.id.lnrAddLocation:
                startActivityForResult(new Intent(mActivity, LocationActivity.class), Location_Flag);
                mActivity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.labelPost:
                if (action.equalsIgnoreCase("edit")) {
                    editFile();
                } else {
                    if (tvSelectCategory.getText().toString().trim().equalsIgnoreCase("Select Season")) {
                        Toast.makeText(mActivity, "Please select a season", Toast.LENGTH_LONG).show();
                    } else {
                        uploadFile();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == 100) {
            seasonid = data.getStringExtra("seasonid");
            String seasonname = data.getStringExtra("seasonname");
            tvSelectCategory.setText(seasonname.trim());

            if (!seasonname.equalsIgnoreCase(""))
                lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textselected));
            else
                lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textunselected));

        } else if (requestCode == Location_Flag && resultCode == Location_Flag) {
            place = (LocationItem) data.getParcelableExtra("locationval");
            lnrgetlocation.setVisibility(View.VISIBLE);
            tvAddLocation.setVisibility(View.GONE);
            addlocation.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPrimary));
            loc_name.setText(place.getTitle());
            loc_address.setText(place.getAddress());
            loc_latitude.setText(String.valueOf(place.getLatitude()));
            loc_longitude.setText(String.valueOf(place.getLogitude()));
            locationItemslist = place;

        }/* else if (requestCode == 1001) {
            if (resultCode == mActivity.RESULT_OK) {

                tag_items = data.getParcelableArrayListExtra("tag_items");

                int tag_items_cnt = tag_items.size();
                if (tag_items_cnt == 0) {
                    tvTagItem.setText("Tag item");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                } else if (tag_items_cnt == 1) {
                    tvTagItem.setText(tag_items_cnt + " item tagged");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                    imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                } else {
                    tvTagItem.setText(tag_items_cnt + " items tagged");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                    imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                }

            }
        }*/
    }


    private void uploadFile() {
        progressDialog.show();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageRef = storage.getReferenceFromUrl(uploadpath);
        StorageReference pdffileref = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + System.currentTimeMillis() / 1000 + ".pdf");
        String pdfFile = getArguments().getString("trendimg");
        try {
            InputStream inputStream = new FileInputStream(pdfFile);
            pdffileref.putStream(inputStream).
                    addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.i("pdfurl", String.valueOf(taskSnapshot.getDownloadUrl()));
                            uploadImage(storageRef, String.valueOf(taskSnapshot.getDownloadUrl()));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {

        }
    }

    public void uploadImage(StorageReference storageRef, final String uploadpdfurl) {
        progressDialog.setMessage("Please wait");
        StorageReference imagesRef = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + System.currentTimeMillis() / 1000 + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap = getResizedBitmap(bitmap, 1500);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Toast.makeText(mActivity, "posting failed", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl-->", "" + downloadUrl);
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                        child(FirebaseTrendOnTapPosts);
                String post_id = databaseReference.push().getKey();
                String tvSelectCategorytxt = tvSelectCategory.getText().toString().trim();
                ArrayList<String> lovephoto = new ArrayList<>();

                locationItemslist = null;
                if (!loc_latitude.getText().toString().trim().equalsIgnoreCase("0.0")) {
                    locationItemslist = new LocationItem(loc_name.getText().toString().trim(),
                            loc_address.getText().toString().trim(),
                            Double.parseDouble(loc_latitude.getText().toString().trim()),
                            Double.parseDouble(loc_longitude.getText().toString().trim()));
                }

                lovephoto.add(String.valueOf(downloadUrl));
                PostLovesItem lovesItem = new PostLovesItem(lovephoto, seasonid, tvSelectCategorytxt,
                        etSayAbouttrend.getText().toString().trim(), 0, tvDate.getText().toString().trim(),
                        locationItemslist, uploadpdfurl, System.currentTimeMillis(),
                        0, false, "trend", post_id,
                        FirebaseAuth.getInstance().getCurrentUser().getUid(),
                        getArguments().getString("trendname"), tag_items);

                databaseReference.child(post_id).setValue(lovesItem);
                DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
                userposts.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Trends).child(post_id).setValue(true);
                DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
                userpostsfeed.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Trends).child(post_id).setValue(true);

                preference = new Preference(mActivity, Login_Pref);
                Users users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
                ActivityItem activityItem = new ActivityItem();
                activityItem.setProfilePic(users.getProfilePic());
                activityItem.setLongMessage(POSTMSG);
                activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + POSTMSG);
                activityItem.setPostID(post_id);
                activityItem.setPostImageURL(String.valueOf(downloadUrl));
                activityItem.setReactionID("");
                activityItem.setShortMessage(TREND);
                activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                activityItem.setActivityType(TREND);
                activityItem.setTimeStamp(System.currentTimeMillis());
                activityItem.setUserID(users.getUserID());

                if (loveTrendSupplierAdapter.getSupplierItemArrayList().size() == 0) {
                    for (int j = 0; j < supplierItemArrayList.size(); j++) {
                        userpostsfeed.child(supplierItemArrayList.get(j).getUserID()).child(Trends).child(post_id).setValue(true);
                    }
                    processActivity(activityItem);
                    databaseReference.child(post_id).child(TimeStamp).setValue(System.currentTimeMillis());
                } else {
                    for (int j = 0; j < loveTrendSupplierAdapter.getSupplierItemArrayList().size(); j++) {
                        userpostsfeed.child(loveTrendSupplierAdapter.getSupplierItemArrayList().get(j).getUserID()).child(Trends).child(post_id).setValue(true);
                        String msg = users.getFirstName() + " " + users.getLastName() + POSTMSG + TREND;
                        sendActivityToUser(loveTrendSupplierAdapter.getSupplierItemArrayList().get(j).getUserID(), activityItem, msg);
                    }
                    databaseReference.child(post_id).child(TimeStamp).setValue(System.currentTimeMillis());
                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                hideSoftKeyboard(mActivity);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.trendslay.performClick();
                    }
                }, 1000);
            }
        });
    }

    private void editFile() {
        progressDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts)
                .child(uploadpost_id);
        String tvSelectCategorytxt = "";
        if (!tvSelectCategory.getText().toString().trim().equalsIgnoreCase("Select Season")) {
            tvSelectCategorytxt = tvSelectCategory.getText().toString().trim();
        }

        databaseReference.child(CatagoryName).setValue(tvSelectCategorytxt);
        databaseReference.child(Content).setValue(etSayAbouttrend.getText().toString().trim());
        databaseReference.child(DeliveryDate).setValue(tvDate.getText().toString().trim());
        databaseReference.child(TimeStamp).setValue(System.currentTimeMillis());
        //if (tag_items != null)
        // databaseReference.child("tag_items").setValue(tag_items);
        if (locationItemslist != null)
            databaseReference.child(Location).setValue(locationItemslist);

        //Toast.makeText(mActivity, "Successfully saved", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        MainActivity.manageBack(mActivity, "");

    }


    private void getData() {
        supplierItemArrayList = new ArrayList<>();
        Bundle bundle = getArguments();
        String selectCat = "Select Season";
        if (bundle != null) {
            if (bundle.containsKey("buyerTrendsItem")) {
                BuyerTrendsItem buyerTrendsItem = bundle.getParcelable("buyerTrendsItem");
                if (buyerTrendsItem != null) {

                    etSayAbouttrend.setText(buyerTrendsItem.getAbouttrend());
                    if (!TextUtils.isEmpty(buyerTrendsItem.getCategory()))
                        selectCat = buyerTrendsItem.getCategory();

                    tvDate.setText(buyerTrendsItem.getDeliverydate());

                    if (!TextUtils.isEmpty(buyerTrendsItem.getImgurl())) {
                        mImageurl = buyerTrendsItem.getImgurl();
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(buyerTrendsItem.getImgurl())
                                .apply(requestOptions)
                                .into(imagetrend);
                    }

                }
            } else if (bundle.containsKey("trendsItem")) {
                TrendsItem trendsItem = bundle.getParcelable("trendsItem");
                if (trendsItem != null) {

                    etSayAbouttrend.setText(trendsItem.getContent());
                    if (!TextUtils.isEmpty(trendsItem.getCategoryName()))
                        selectCat = trendsItem.getCategoryName();
                    tvDate.setText(trendsItem.getDeliveryDate());
                    uploadpost_id = trendsItem.getId();
                    if (!TextUtils.isEmpty(trendsItem.getPhotoUrlsList().get(0))) {
                        mImageurl = trendsItem.getPhotoUrlsList().get(0);
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(trendsItem.getPhotoUrlsList().get(0))
                                .apply(requestOptions)
                                .into(imagetrend);
                    }

                    if (trendsItem.getLocationItemArrayList() != null && trendsItem.getLocationItemArrayList().getLatitude() != 0.0) {
                        locationItemslist = trendsItem.getLocationItemArrayList();
                        tvAddLocation.setVisibility(View.GONE);
                        lnrgetlocation.setVisibility(View.VISIBLE);
                        loc_name.setText(trendsItem.getLocationItemArrayList().getTitle());
                        loc_address.setText(trendsItem.getLocationItemArrayList().getAddress());
                    } else {
                        tvAddLocation.setVisibility(View.VISIBLE);
                        lnrgetlocation.setVisibility(View.GONE);
                    }

                }

            }
        }
        tvSelectCategory.setText(selectCat);
        if (selectCat.equalsIgnoreCase("Select Season")) {
            lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textunselected));
        } else {
            lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textselected));
        }

    }


    private SimpleDateFormat dateFormatter;
    int monthOfYear;
    int year;
    int dayOfMonth;
    private static final String[] MONTH = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final int[] MONTH_INT = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    private static final List<String> DATES = new ArrayList<>();
    private static final List<String> YEAR = new ArrayList<>();
    String selectedDate;

    private void selectDate() {

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);

        for (int i = 0; i < 30; i++) {
            DATES.add(String.valueOf(i + 1));
        }
        for (int i = 1900; i < 2050; i++) {
            YEAR.add(String.valueOf(i + 1));
        }
        monthOfYear = -1;
        year = -1;
        dayOfMonth = -1;

        final int yearThis, month, date, posYear, posDate;
        if (TextUtils.isEmpty(selectedDate)) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            yearThis = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date));
        } else {
            String[] fulldate = selectedDate.split("/");
            yearThis = Integer.parseInt(fulldate[2]);
            month = Integer.parseInt(fulldate[1]);
            date = Integer.parseInt(fulldate[0]);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date)) + 1;
        }

        View outerView = LayoutInflater.from(mContext).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);

        // monthOfYear = month-1;
        // year = Integer.parseInt(YEAR.get(posYear));
        //  dayOfMonth = posDate;
        wv.setItems(Arrays.asList(MONTH));
        wv.setSeletion(month - 1);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                monthOfYear = selectedIndex;
            }
        });
        WheelView wv1 = (WheelView) outerView.findViewById(R.id.wheel_view_wv1);
        wv1.setItems(DATES);

        wv1.setSeletion(posDate - 1);
        wv1.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                dayOfMonth = Integer.parseInt(item);
            }
        });
        WheelView wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
        wv2.setItems(YEAR);
        wv2.setSeletion(posYear);
        wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                year = Integer.parseInt(item);
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        new AlertDialog.Builder(mActivity, R.style.Theme_AppCompat_Light_Dialog)
                .setView(outerView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (monthOfYear == -1) {
                            monthOfYear = month - 1;
                        } else {
                            monthOfYear -= 1;
                        }
                        if (dayOfMonth == -1) {
                            dayOfMonth = posDate;
                        }
                        if (year == -1) {
                            year = Integer.parseInt(YEAR.get(posYear));
                        }
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Date date = new Date(newDate.getTimeInMillis());
                        tvDate.setText(dateFormatter.format(date.getTime()));
                    }
                })
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt, mActivity);
    }


}
