package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Random;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.modal.ActivityItem;

import tnm.trendontap.modal.SuppliersInvitations;
import tnm.trendontap.modal.Users;

import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Mail;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.INVITEMSG;

import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.EmailID;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.InvitedSupplierEmailID;
import static tnm.trendontap.utility.FireStoreUtils.InvitedSuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.IsInviationAccepted;
import static tnm.trendontap.utility.FireStoreUtils.SupplierInvitations;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;
import static tnm.trendontap.utility.Utils.isValidEmail;
import static tnm.trendontap.utility.Utils.senderemail;
import static tnm.trendontap.utility.Utils.senderpwd;


public class Invite_Supplier_Fragment extends Fragment {
    TextView labeltxt, sendyoursuppliertxt, sendinvitation, notifycnt;
    LinearLayout back_arrow;
    EditText emailaddr;
    RelativeLayout activity2_icon_lay;
    Preference preference;
    private DatabaseReference databaseReference;
    Activity activity;
    private ProgressDialog progressDialog;
    Users users;
    boolean isSendingMail = false, showToast = false;
    String activationCode;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invitesupplier, container, false);
        sendyoursuppliertxt = (TextView) view.findViewById(R.id.sendyoursuppliertxt);
        activity = getActivity();
        showToast = false;

        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Sending invitation...");
        progressDialog.setCancelable(false);
        sendyoursuppliertxt.setTypeface(RobotoFont.regularFont(activity));
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        //labeltxt.setText("Invite Suppliers");
        labeltxt.setText("Invite Profile");
        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.GONE);
        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        emailaddr = (EditText) view.findViewById(R.id.emailaddr);
        emailaddr.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (EditorInfo.IME_ACTION_DONE == i) {

                    sendInvitation();
                }
                return false;
            }
        });

        sendinvitation = (TextView) view.findViewById(R.id.sendinvitation);
        sendinvitation.setTypeface(RobotoFont.mediumFont(activity));
        sendinvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendInvitation();

            }
        });
        return view;
    }


    public void sendInvitation() {
        String emailtxt = emailaddr.getText().toString().trim().toLowerCase();
        if (TextUtils.isEmpty(emailtxt) || !isValidEmail(emailtxt)) {
            MainActivity.showSnackbarError("Please enter a valid email address");
            return;
        }

        isInvitaionSent1(emailtxt);
    }

    public void isInvitaionSent1(final String emailtxt) {
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(SupplierInvitations);
        Query query = databaseReference.orderByChild(UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    // not sending any invitation
                    processInvitation(emailtxt);
                    return;
                }
                boolean isinvited = false, is_inviation_accepted = false;
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    String emailval = String.valueOf(dataSnapshot1.child(InvitedSupplierEmailID).getValue()).trim().toLowerCase();
                    if (emailval.equals(emailtxt.trim().toLowerCase())) {
                        is_inviation_accepted = (boolean) dataSnapshot1.child(IsInviationAccepted).getValue();
                        isinvited = true;
                        break;
                    }
                }

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }


                if (is_inviation_accepted) {
                    Toast.makeText(activity, "Supplier already in your connection", Toast.LENGTH_SHORT).show();
                } else if (isinvited) {
                    Toast.makeText(activity, "You have already invited this supplier.", Toast.LENGTH_SHORT).show();
                } else {
                    processInvitation(emailtxt);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }


    public void processInvitation(final String emailtxt) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(FirebasePathUsers);

        Query queryRef = ref.orderByChild(EmailID).equalTo(emailtxt);

        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (dataSnapshot1.getChildrenCount() == 0) {
                    Random rand = new Random();
                    activationCode = String.valueOf(rand.nextInt(90000000) + 10000000);
                    isSendingMail = true;
                    new SendMail().execute(emailtxt.toLowerCase());
                    return;
                }
                for (DataSnapshot dataSnapshot : dataSnapshot1.getChildren()) {

                    doneInvitation(emailtxt.toLowerCase());
                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(users.getProfilePic());
                    activityItem.setLongMessage(INVITEMSG);
                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + " of " + users.getCompanyName() + INVITEMSG);
                    activityItem.setPostID("");
                    activityItem.setPostImageURL("");
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("");
                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                    activityItem.setActivityType("Invitation");
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(users.getUserID());

                    String usersentid = "";

                    usersentid = String.valueOf(dataSnapshot.child(UserID).getValue());

                    DatabaseReference usersuppliersinvitations = FirebaseDatabase.getInstance().getReference().
                            child(InvitedSuppliersOfBuyer);
                    usersuppliersinvitations.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(usersentid).setValue(true);

                    String msg = users.getFirstName() + " " + users.getLastName() + INVITEMSG;
                    sendActivityToUser(usersentid, activityItem, msg);
                    break;

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });

    }

    /*public void isInvitaionSent(final String emailtxt) {

        FirebaseDatabase.getInstance().getReference().child(SupplierInvitations).
                orderByChild(InvitedSupplierEmailID).equalTo(emailtxt)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (showToast) {
                            return;
                        }
                        if (dataSnapshot.getChildrenCount() == 0) {
                            if (!isSendingMail) {
                                FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                        orderByChild(EmailID).equalTo(emailtxt).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.getChildrenCount() > 0) {
                                                    doneInvitation(emailtxt);
                                                    ActivityItem activityItem = new ActivityItem();
                                                    activityItem.setProfilePic(users.getProfilePic());
                                                    activityItem.setLongMessage(INVITEMSG);
                                                    activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + " of " + users.getCompany() + INVITEMSG);
                                                    activityItem.setPostID("");
                                                    activityItem.setPostImageURL("");
                                                    activityItem.setReactionID("");
                                                    activityItem.setShortMessage("");
                                                    activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                                                    activityItem.setActivityType("");
                                                    activityItem.setTimeStamp(System.currentTimeMillis());
                                                    activityItem.setUserID(users.getUserID());


                                                    String msg = users.getFirstName() + " " + users.getLastName() + INVITEMSG;
                                                    sendActivityToUser(String.valueOf(dataSnapshot.child("userId").getValue()), activityItem, msg);

                                                } else {
                                                    Random rand = new Random();
                                                    activationCode = String.valueOf(rand.nextInt(90000000) + 10000000);
                                                    isSendingMail = true;
                                                    new SendMail().execute(emailtxt);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });

                            }
                            return;
                        }
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            if ((boolean) dataSnapshot1.child(IsInviationAccepted).getValue())
                                Toast.makeText(activity, "Supplier already in your connection", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(activity, "Invitation already sent", Toast.LENGTH_SHORT).show();
                            break;
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }*/

    private class SendMail extends AsyncTask<String, Integer, Boolean> {


        String sentto = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !progressDialog.isShowing())
                progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            isSendingMail = false;
            if (aVoid) {
                doneInvitation(sentto);
            } else {
                progressDialog.dismiss();
                Toast.makeText(activity, "Error sending email, please try again", Toast.LENGTH_SHORT).show();
            }
        }

/*
        public String convertToHtml(String htmlString) {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<![CDATA[");
            stringBuilder.append(htmlString);
            stringBuilder.append("]]>");
            return stringBuilder.toString();
        }*/

        protected Boolean doInBackground(String... params) {

            String fullName = getName(users.getFirstName(), users.getLastName());
            String company = users.getCompanyName();
            sentto = params[0];
            String subject = "Supplier Invitation";
            String body = "Hello, <br><br><br>\" " + fullName + "\" from \" " + company + "\" wants to connect with you on Trend On Tap.</br></br></br><br><br>To join their network, download the app below and enter your 8 digit activation code.</br></b<br><br><br>Activation code: <b>" + activationCode + "</b></br></br></br><br><br><a href=https://app.trendontap.com/downloadApp.aspx>Click here to download app</a></br></br><br><br>Regards</br></br><br><br>Trend On Tap Team</br></br>";
            Mail m = new Mail(senderemail, senderpwd);
            String[] toArr = {sentto};
            m.setTo(toArr);
            m.setFrom("noreply@trendontap.com");
            m.setSubject(subject);
            m.setBody(body);
            try {
                if (m.send()) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                Log.e("MailApp", "Could not send email", e);
                return false;
            }
        }
    }

    private void doneInvitation(String sentto) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SupplierInvitations);

        String key = databaseReference.push().getKey();
        long tsLong = System.currentTimeMillis();
        String company = users.getCompanyName();
        showToast = true;
        String BuyerName = getName(users.getFirstName(),
                users.getLastName());

        if (activationCode == null)
            activationCode = "";

        HashMap<String, Object> invitations = new HashMap<>();
        invitations.put(FireStoreUtils.ProfilePic, "");
        invitations.put(FireStoreUtils.VerificationCode, activationCode);
        invitations.put(FireStoreUtils.BuyerName, BuyerName);
        invitations.put(FireStoreUtils.CompanyName, company);
        invitations.put(FireStoreUtils.ID, key);
        invitations.put(FireStoreUtils.InvitedSupplierEmailID, sentto);
        invitations.put(FireStoreUtils.IsInviationAccepted, false);
        invitations.put(FireStoreUtils.TimeStamp, tsLong);
        invitations.put(FireStoreUtils.UserID, FirebaseAuth.getInstance().getCurrentUser().getUid());

        databaseReference.child(key).setValue(invitations);
        progressDialog.dismiss();
        Toast.makeText(activity, "Invitation sent", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity.manageBack(activity, "");
            }
        }, 500);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }
}
