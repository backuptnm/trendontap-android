package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.BuyerLovesAdapter;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 12/15/2017.
 */

public class Buyers_Love_Fragment extends Fragment {

    ListView buyerlovelist;
    TextView buyerlovestxt, notloves;
    ArrayList<BuyerLovesItem> buyerLovesItemArrayList;
    BuyerLovesAdapter buyerLovesAdapter;
    ProgressDialog progressDialog;
    DatabaseReference databaseReference;
    Activity activity;
    String postidval = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_buyers_loves, container, false);
        if (getArguments() != null && getArguments().getString("postid") != null) {
            postidval = getArguments().getString("postid");
        }
        buyerlovelist = (ListView) view.findViewById(R.id.buyerlovelist);
        buyerlovestxt = (TextView) view.findViewById(R.id.buyerlovestxt);
        buyerlovestxt.setTypeface(RobotoFont.boldFont(activity));
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        notloves = (TextView) view.findViewById(R.id.notloves);
        notloves.setTypeface(RobotoFont.mediumFont(activity));
        buyerLovesItemArrayList = new ArrayList<>();
        buyerLovesAdapter = new BuyerLovesAdapter(activity, buyerLovesItemArrayList);
        buyerlovelist.setAdapter(buyerLovesAdapter);
        getBuyerLoveCount();
        return view;
    }

    int count = 0;

    public void getBuyerLoveCount() {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Loves).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                count = (int) dataSnapshot.getChildrenCount();
                if (!dataSnapshot.hasChildren()) {
                    notloves.setVisibility(View.VISIBLE);
                    buyerlovelist.setVisibility(View.GONE);
                    return;
                }
                setUserPostFeed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void setUserPostFeed() {
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }
        buyerLovesItemArrayList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Loves).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getLovePostData(dataSnapshot.getKey());
                notloves.setVisibility(View.GONE);
                buyerlovelist.setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void getLovePostData(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                        if (buyerLovesItemArrayList.size() == 0) {
                            notloves.setVisibility(View.VISIBLE);
                            buyerlovelist.setVisibility(View.GONE);
                        } else {
                            notloves.setVisibility(View.GONE);
                            buyerlovelist.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    public void managePos() {
        buyerlovelist.post(new Runnable() {
            @Override
            public void run() {
                if (!postidval.equalsIgnoreCase("")) {
                    int pos = matchValue(buyerLovesItemArrayList, postidval);
                    if (pos == -1) {
                        return;
                    }
                    buyerlovelist.setSelection(pos);
                }
            }
        });
    }

    public void manageData(DataSnapshot lovelist, String action) {
        boolean isshown = true;
        if (lovelist.hasChild(HideSuppliers)) {
            if (lovelist.child(HideSuppliers).hasChild(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                isshown = false;
            }
        }
        if (isshown) {
            String userId = "";

            if (!TextUtils.isEmpty(String.valueOf(lovelist.child(FireStoreUtils.UserID).getValue()))) {
                userId = String.valueOf(lovelist.child(FireStoreUtils.UserID).getValue());
            }

            boolean isreactions = false;
            ArrayList<String> suppliers = new ArrayList<>();

            if (lovelist.hasChild(ReactedSuppiers) && lovelist.child(ReactedSuppiers).getValue() != null) {
                suppliers = (ArrayList<String>) lovelist.child(ReactedSuppiers).getValue();
                if (suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    isreactions = true;
                }
            }

            final ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
            if (lovelist.hasChild(TagItems)) {
                for (DataSnapshot messageSnapshot : lovelist.child(TagItems).getChildren()) {
                    Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                    tag_itemsArrayList.add(tag_Items);
                }
            }

            ArrayList<String> yourStringArray = new ArrayList<>();
            int size = (int) lovelist.child(tPostPhotoId).getChildrenCount();
            for (int i = 0; i < size; i++) {
                String url = lovelist.child(tPostPhotoId).child(i + "").getValue().toString();
                yourStringArray.add(url);
            }

            LocationItem location = null;
            if (lovelist.hasChild(Location)) {
                location = new LocationItem(lovelist.child(Location).child(Title).getValue().toString(),
                        lovelist.child(Location).child(Address).getValue().toString(),
                        (double) lovelist.child(Location).child(Latitude).getValue(),
                        (double) lovelist.child(Location).child(Logitude).getValue());
            }

            BuyerLovesItem buyerLovesItem = new BuyerLovesItem(
                    yourStringArray,
                    lovelist.child(PostTitle).getValue().toString(),
                    lovelist.child(CatagoryName).getValue().toString(),
                    lovelist.child(Content).getValue().toString(),
                    lovelist.child(DeliveryDate).getValue().toString(),
                    (long) lovelist.child(TimeStamp).getValue(), "", "",
                    isreactions,
                    lovelist.child(ID).getValue().toString(), "",
                    "", tag_itemsArrayList,
                    (long) lovelist.child(LikesCount).getValue(), "",
                    location);


            if (action.equalsIgnoreCase("edit")) {
                int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                if (pos == -1)
                    return;
                buyerLovesItemArrayList.set(pos, buyerLovesItem);
                buyerLovesAdapter.notifyDataSetChanged();
            } else if (action.equalsIgnoreCase("delete")) {
                int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                if (pos == -1)
                    return;
                buyerLovesItemArrayList.remove(pos);
                buyerLovesAdapter.notifyDataSetChanged();
            } else {
                int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                if (pos == -1)
                    buyerLovesItemArrayList.add(buyerLovesItem);

               // if (buyerLovesItemArrayList.size() == count) {
                    buyerLovesAdapter.addList(buyerLovesItemArrayList);
                    notloves.setVisibility(View.GONE);
                    buyerlovelist.setVisibility(View.VISIBLE);
                    // buyerlovelist.smoothScrollToPositionFromTop(1, 50,1000);
              //  }
            }

            if (!action.equalsIgnoreCase("delete") && buyerLovesItemArrayList.size() > 0)
                getUserDetails(userId, buyerLovesItem, action);
        }

        if (buyerLovesItemArrayList.size() == 0) {
            notloves.setVisibility(View.VISIBLE);
            buyerlovelist.setVisibility(View.GONE);
        }
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt,activity);
    }


    public void getUserDetails(String userid, final BuyerLovesItem buyerLovesItem, final String action) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Users users = dataSnapshot.getValue(Users.class);
                        buyerLovesItem.setUsername(getName(users.getFirstName(), users.getLastName()));
                        buyerLovesItem.setType(users.getJobTitle());
                        buyerLovesItem.setUserimg(users.getProfilePic());
                        buyerLovesItem.setUserpostid(users.getUserID());
                        buyerLovesItem.setUsercompany(users.getCompanyName());

                        int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                        if (pos == -1)
                            return;
                        buyerLovesItemArrayList.set(pos, buyerLovesItem);
                        buyerLovesAdapter.notifyDataSetChanged();

                        managePos();

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }


    public int matchValue(ArrayList<BuyerLovesItem> buyerLovesItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < buyerLovesItemArrayList.size(); j++) {
            if (buyerLovesItemArrayList.get(j).getPostid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }
}
