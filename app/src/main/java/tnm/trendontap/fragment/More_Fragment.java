package tnm.trendontap.fragment;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.RecycleBinActivity;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.Utils.Login_Pref;

public class More_Fragment extends Fragment {
    TextView labeltxt, logout, accounttxt, myprofiletxt, invitesuppliertxt, networksettingtxt, recyclebintxt,
            moreinfo, websitetxt, termconditiontxt, rateapptxt, notifycnt;
    LinearLayout myprofile, invitesupplier, networksetting, recyclebin, website, termcondition, rateapp, buyer_option;
    ImageView categories_icon, filter_list_icon;
    RelativeLayout activity2_icon_lay;
    Preference preference;
    Users users;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        activity = getActivity();
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText(getString(R.string.more));

        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        filter_list_icon = (ImageView) view.findViewById(R.id.filter_list_icon);
        filter_list_icon.setVisibility(View.GONE);
        categories_icon = (ImageView) view.findViewById(R.id.categories_icon);
        categories_icon.setVisibility(View.GONE);
        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        logout = (TextView) view.findViewById(R.id.logout);
        logout.setTypeface(RobotoFont.regularFont(activity));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.logout(activity, "");

            }
        });
        buyer_option = (LinearLayout) view.findViewById(R.id.buyer_option);

        if (users.getUserType().equalsIgnoreCase(Utils.Buyer)) {
            buyer_option.setVisibility(View.VISIBLE);
            activity2_icon_lay.setVisibility(View.GONE);
        } else {
            buyer_option.setVisibility(View.GONE);
            activity2_icon_lay.setVisibility(View.VISIBLE);
        }

        myprofile = (LinearLayout) view.findViewById(R.id.myprofile);
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("loggeduser", true);
                MainActivity.loadContentFragment(new Profile_Fragment(), false, bundle);
            }
        });
        invitesupplier = (LinearLayout) view.findViewById(R.id.invitesupplier);
        invitesupplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Invite_Supplier_Fragment(), false);
            }
        });
        networksetting = (LinearLayout) view.findViewById(R.id.networksetting);
        networksetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Network_Setting_Fragment(), false);
            }
        });
        recyclebin = (LinearLayout) view.findViewById(R.id.recyclebin);
        recyclebin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, RecycleBinActivity.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        website = (LinearLayout) view.findViewById(R.id.website);
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewOurWebsite();
            }
        });
        termcondition = (LinearLayout) view.findViewById(R.id.termcondition);
        termcondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new TermCondition_Fragment(), false);
            }
        });
        rateapp = (LinearLayout) view.findViewById(R.id.rateapp);
        rateapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateApp();
            }
        });
        accounttxt = (TextView) view.findViewById(R.id.accounttxt);
        accounttxt.setTypeface(RobotoFont.regularFont(activity));
        myprofiletxt = (TextView) view.findViewById(R.id.myprofiletxt);
        myprofiletxt.setTypeface(RobotoFont.mediumFont(activity));
        invitesuppliertxt = (TextView) view.findViewById(R.id.invitesuppliertxt);
        invitesuppliertxt.setTypeface(RobotoFont.mediumFont(activity));
        networksettingtxt = (TextView) view.findViewById(R.id.networksettingtxt);
        networksettingtxt.setTypeface(RobotoFont.mediumFont(activity));
        recyclebintxt = (TextView) view.findViewById(R.id.recyclebintxt);
        recyclebintxt.setTypeface(RobotoFont.mediumFont(activity));
        moreinfo = (TextView) view.findViewById(R.id.moreinfo);
        moreinfo.setTypeface(RobotoFont.regularFont(activity));
        websitetxt = (TextView) view.findViewById(R.id.websitetxt);
        websitetxt.setTypeface(RobotoFont.mediumFont(activity));
        termconditiontxt = (TextView) view.findViewById(R.id.termconditiontxt);
        termconditiontxt.setTypeface(RobotoFont.mediumFont(activity));
        rateapptxt = (TextView) view.findViewById(R.id.rateapptxt);
        rateapptxt.setTypeface(RobotoFont.mediumFont(activity));

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();

        if (users.getUserType().equalsIgnoreCase(Utils.Supplier))
            MainActivity.getBadgeCount(MainActivity.activitycnt, activity);
        else
            MainActivity.getBadgeCount(notifycnt, activity);

    }


    /* @Override
    public void onResume() {
        super.onResume();
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    public void rateApp() {
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void viewOurWebsite() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.trendontap.com/"));
        startActivity(browserIntent);
    }


}
