package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.ConnectionLovesAdapter;
import tnm.trendontap.adapter.ConnectionTrendAdapter;
import tnm.trendontap.adapter.LovesAdapter;
import tnm.trendontap.modal.ActivityItem;

import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.PhotoConnectStamp;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendNotification;
import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.BuyerConnections;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.BuyerInvitations;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.FirstName;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.FireStoreUtils.tReactionCommentCount;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;


public class Connection_Loves_Fragment extends Fragment {

    TextView name, city, useremail, userphoneno, connectbtn;
    ImageView profilepic;
    private String userid;
    private Users current_user;
    Preference preference;

    RelativeLayout activity2_icon_lay;
    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ListView connection_loves;
    ConnectionLovesAdapter connectionLovesAdapter;


    ArrayList<LovesItem> lovesItemArrayList = null;
    Activity activity;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection_loves, container, false);
        View header = inflater.inflate(R.layout.header_connect, null);
        activity = getActivity();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        ConnectUser_Fragment.photoTimeList = new ArrayList<>();
        connection_loves = (ListView) view.findViewById(R.id.connection_loves);
        profilepic = (ImageView) header.findViewById(R.id.profilepic);
        name = (TextView) header.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        city = (TextView) header.findViewById(R.id.city);
        city.setTypeface(RobotoFont.regularFont(activity));
        useremail = (TextView) header.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));
        userphoneno = (TextView) header.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));
        connectbtn = (TextView) header.findViewById(R.id.connectbtn);
        connectbtn.setTypeface(RobotoFont.regularFont(activity));

        preference = new Preference(activity, Login_Pref);
        current_user = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        lovesItemArrayList = new ArrayList<>();
        //  lovesAdapter = new LovesAdapter(activity, lovesItemArrayList);
        userid = mParam1;
        connection_loves.addHeaderView(header);
        connectionLovesAdapter = new ConnectionLovesAdapter(activity, lovesItemArrayList, userid);
        connection_loves.setAdapter(connectionLovesAdapter);
        // setListViewHeightBasedOnChildren(connection_loves);
        // setLoveslist();
        getBuyerLoveCount();
        setProfileData();
        //    connectionTrendList();
        return view;
    }

    public static Connection_Loves_Fragment newInstance(String param1, String param2) {
        Connection_Loves_Fragment fragment = new Connection_Loves_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void getBuyerLoveCount() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(userid).child(Loves)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        getPostData();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getPostData() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
        databaseReference.child(userid).child(Loves).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getData(dataSnapshot);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getData(DataSnapshot dataSnapshot) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(dataSnapshot.getKey()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void manageData(DataSnapshot lovelist, final String action) {

        long likecnt = (long) lovelist.child(LikesCount).getValue();
        ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
        if (lovelist.hasChild(TagItems)) {
            for (DataSnapshot messageSnapshot : lovelist.child(TagItems).getChildren()) {
                Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                tag_itemsArrayList.add(tag_Items);
            }
        }

        LocationItem location = null;
        if (lovelist.hasChild(Location)) {
            location = new LocationItem(lovelist.child(Location).child(Title).getValue().toString(),
                    lovelist.child(Location).child(Address).getValue().toString(),
                    (double) lovelist.child(Location).child(Latitude).getValue(),
                    (double) lovelist.child(Location).child(Logitude).getValue());
        }

        long comment_count = 0;
        try {
            if (lovelist.hasChild(tReactionCommentCount)) {
                comment_count = (long) lovelist.child(tReactionCommentCount).getValue();
            }
        } catch (Exception e) {

        }

        LovesItem lovesItem;
        lovesItem = new LovesItem((ArrayList<String>) lovelist.child(FireStoreUtils.tPostPhotoId).getValue(),
                "",
                lovelist.child(CatagoryName).getValue().toString(),
                lovelist.child(Content).getValue().toString(),
                likecnt,
                lovelist.child(DeliveryDate).getValue().toString(),
                (long) lovelist.child(TimeStamp).getValue(),
                lovelist.child(ReactionsCount).getValue().toString(),
                (boolean) lovelist.child(UnreadReactionCount).getValue(),
                false, "", "",
                lovelist.child(ID).getValue().toString(),
                tag_itemsArrayList, location,
                comment_count,
                lovelist.child(FireStoreUtils.UserID).getValue().toString()

        );

        if (action.equalsIgnoreCase("add")) {
            lovesItemArrayList.add(lovesItem);
            connectionLovesAdapter.addList(lovesItemArrayList);
        } else if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
            if (pos == -1)
                return;
            lovesItemArrayList.set(pos, lovesItem);
            connectionLovesAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
            if (pos == -1)
                return;
            lovesItemArrayList.remove(pos);
            connectionLovesAdapter.notifyDataSetChanged();
        }

        if (!action.equalsIgnoreCase("delete"))
            getPostLikeCount(lovesItem);


    }

    public void manageLikeData(final LovesItem lovesItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(lovesItem.getId()).child(Likes).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getUserDetails(dataSnapshot.getKey(), lovesItem);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void getPostLikeCount(final LovesItem lovesItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(lovesItem.getId()).child(Likes).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
                            if (pos == -1)
                                return;
                            lovesItem.setLikecnt(0);
                            lovesItemArrayList.set(pos, lovesItem);
                            connectionLovesAdapter.notifyDataSetChanged();
                            return;
                        }
                        lovesItem.setLikecnt((int) dataSnapshot.getChildrenCount());
                        manageLikeData(lovesItem);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getUserDetails(String userid, final LovesItem lovesItem) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (lovesItem.getLiker1().equalsIgnoreCase("")) {
                            lovesItem.setLiker1(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        } else if (lovesItem.getLiker2().equalsIgnoreCase("")) {
                            lovesItem.setLiker2(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        }
                        int pos = matchValue(lovesItemArrayList, lovesItem.getId());
                        if (pos == -1)
                            return;
                        lovesItemArrayList.set(pos, lovesItem);
                        connectionLovesAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public int matchValue(ArrayList<LovesItem> lovesItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < lovesItemArrayList.size(); j++) {
            if (lovesItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    private void setProfileData() {
        /*******************************************************************/
        DatabaseReference rootInvited = FirebaseDatabase.getInstance().getReference();
        Query query_invited = rootInvited.child(BuyerInvitations).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query_invited.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connectbtn.setText("Requested");
                    connectbtn.setTextColor(Color.parseColor("#000000"));
                    connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
                } else {
                    connectbtn.setText("Connect");

                    checkIfConneced();
                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /******************************************************************/

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers)
                .child(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        ConnectUser_Fragment.labeltxt.setText(getName(users.getFirstName(), users.getLastName()));
                        city.setText(users.getCity());
                        useremail.setText(users.getEmailID());
                        userphoneno.setText(users.getPhone());
                        name.setText(users.getJobTitle());

                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.mipmap.userplaceholder);
                            requestOptions.error(R.mipmap.userplaceholder);

                            Glide.with(activity.getApplicationContext())
                                    .load(users.getProfilePic())
                                    .apply(requestOptions)
                                    .into(profilepic);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        connectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectbtn.getText().toString().equalsIgnoreCase("Connect")) {

                    FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).setValue(true);
                    connectbtn.setText("Requested");

                    FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
                    connectbtn.setText("Requested");

                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(current_user.getProfilePic());
                    activityItem.setLongMessage("started");
                    activityItem.setMessage(current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG);
                    activityItem.setPostID("");
                    activityItem.setPostImageURL("");
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("following you");
                    activityItem.setTiggerBy(current_user.getFirstName() + " " + current_user.getLastName());
                    activityItem.setActivityType(BuyerFollower);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(current_user.getUserID());

                    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                            child(FirebasePathActivities);
                    final String key = databaseReference.push().getKey();
                    activityItem.setId(key);
                    databaseReference.child(key).setValue(activityItem);

                    FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                            child(userid).
                            child(key).setValue(true);

                    sendNotification(userid, current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG, activityItem);

                } else if (connectbtn.getText().toString().equalsIgnoreCase("Requested")) {

                } else {
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).removeValue();
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
                    disconnect();
                }
            }
        });
    }

    private void checkIfConneced() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query query = rootRef.child(BuyerConnections).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connect();
                } else {
                    disconnect();

                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void connect() {
        ConnectUser_Fragment.isconnected = true;
        connectbtn.setText("Disconnect");
        ConnectUser_Fragment.connect.setVisibility(View.VISIBLE);
        connectbtn.setTextColor(Color.parseColor("#000000"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
        ConnectUser_Fragment.disconnect.setVisibility(View.GONE);
    }

    public void disconnect() {
        ConnectUser_Fragment.isconnected = false;
        connectbtn.setText("Connect");
        ConnectUser_Fragment.connect.setVisibility(View.GONE);
        connectbtn.setTextColor(Color.parseColor("#FFFFFF"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_blue_bg));
        ConnectUser_Fragment.disconnect.setVisibility(View.VISIBLE);
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
