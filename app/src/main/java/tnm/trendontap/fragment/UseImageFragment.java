package tnm.trendontap.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import tnm.trendontap.R;
import tnm.trendontap.activity.CameraActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.TopCropImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.CameraActivity.loadContentFragment;
import static tnm.trendontap.utility.Utils.calculateInSampleSize;
import static tnm.trendontap.utility.Utils.confiramtionDialog;
import static tnm.trendontap.utility.Utils.getResizedBitmap;

/**
 * Created by TNM on 11/2/2017.
 */

public class UseImageFragment extends Fragment {

    private static final String TAG = "AndroidCameraApi";
    public static final String camera_Pref = "camera_pref";
    private ImageView recycle, close, imgDownload;
    TopCropImageView imageTop;
    // ImageView imageTop;
    RelativeLayout rl_takepicture_more, rlBtnUse;
    LinearLayout imagesList, lnrAddMore;
    TextView tvAddmore;

    private boolean isFromBuyers;
    boolean isClose = true;
    SharedPreferences mPreferences;

    private FragmentActivity mActivity;
    JSONArray imageCaptureArray;
    String selectedImage;
    String from = "";
    boolean isdeleted;
    String imagestring = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isClose = true;
        isdeleted = false;
        mActivity = getActivity();
        imagestring = "";
        if (getArguments().getString("from") != null) {
            from = getArguments().getString("from");
        }
        isFromBuyers = getArguments().getBoolean("buyers", true);
        //selectedImage = getArguments().getString("selectedImage", "");
        mPreferences = mActivity.getSharedPreferences(camera_Pref, 0);

        try {
            imageCaptureArray = new JSONArray(mPreferences.getString("imagePath", ""));
        } catch (JSONException e) {
            e.printStackTrace();
            imageCaptureArray = new JSONArray();
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_use_image, container, false);
        mActivity = getActivity();
        rl_takepicture_more = (RelativeLayout) view.findViewById(R.id.rl_takepicture_more);
        rlBtnUse = (RelativeLayout) view.findViewById(R.id.rlBtnUse);
        imagesList = (LinearLayout) view.findViewById(R.id.imagesList);
        lnrAddMore = (LinearLayout) view.findViewById(R.id.lnrAddMore);
        imgDownload = (ImageView) view.findViewById(R.id.imgDownload);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confiramtionDialog(mActivity, "Image saved to album.");
            }
        });

        if (imageCaptureArray.length() > 3) {
            lnrAddMore.setVisibility(View.GONE);
        } else {
            lnrAddMore.setVisibility(View.VISIBLE);
        }
        // imageTop = (ImageView) view.findViewById(R.id.imageTop);
        imageTop = (TopCropImageView) view.findViewById(R.id.imageTop);
        close = (ImageView) view.findViewById(R.id.close);

        tvAddmore = (TextView) view.findViewById(R.id.tvAddmore);
        tvAddmore.setTypeface(RobotoFont.regularFont(mActivity));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    for (int j = 0; j < imageCaptureArray.length(); j++) {
                        if (imageCaptureArray.get(j).equals(imagestring)) {
                            imageCaptureArray.remove(j);
                            break;
                        }
                    }
                } catch (Exception e) {

                }

                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", imageCaptureArray.toString());
                editor.apply();

               mActivity.onBackPressed();

            }
        });
        rlBtnUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("selectedImage", selectedImage);
                bundle.putString("imageCaptureArray", imageCaptureArray.toString());

                if (isFromBuyers) {
                    loadContentFragment(new UploadLoveFragment(), false, bundle);
                } else {
                    bundle.putString("postid", getArguments().getString("postid"));
                    bundle.putString("postuserid", getArguments().getString("postuserid"));
                    bundle.putString("postimg", getArguments().getString("postimg"));
                    bundle.putString("from", from);
                    bundle.putBoolean("isEdit", false);
                    MainActivity.loadContentFragment(new UploadReactionFragment(), false, bundle);
                }

            }
        });

        lnrAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFromBuyers) {
                    CameraActivity.manageBack(mActivity, "addmore");
                } else {
                    MainActivity.manageBack(mActivity, "addmore");
                }
            }
        });
        recycle = (ImageView) view.findViewById(R.id.recycle);
        recycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageCaptureArray.length() > 1)
                    imageCaptureArray.remove(selectedImageIndex);

                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", imageCaptureArray.toString());
                editor.apply();
                lnrAddMore.setVisibility(View.VISIBLE);
                try {
                    imageCaptureArray = new JSONArray(mPreferences.getString("imagePath", String.valueOf(new JSONArray())));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setSelected(0);
            }
        });
        close.setImageResource(R.mipmap.back_arrow);

        int size = imageCaptureArray.length();
        try {
            imagestring = (String) imageCaptureArray.get(size - 1);
        } catch (Exception e) {

        }
        setSelected(size - 1);

        return view;
    }


    View.OnClickListener imageClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = (int) view.getTag();
            setSelected(pos);
        }
    };

    int selectedImageIndex = 0;

    Bitmap bitmap = null;

    private void setSelected(final int pos) {
        try {
            selectedImage = (String) imageCaptureArray.get(pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = calculateInSampleSize(options, Utils.getScreenWidth(), Utils.getScreenHeight()); //My device pixel resolution;
                bitmap = null;
                bitmap = BitmapFactory.decodeFile(selectedImage, options);
                bitmap = getResizedBitmap(bitmap, 900);
                imageTop.setImageBitmap(bitmap);

            }
        }, 0);

        selectedImageIndex = pos;
        imagesList.removeAllViews();
        int size = imageCaptureArray.length();
        for (int i = 0; i < size; i++) {
            ImageView imgView = new ImageView(mActivity);
            String path1 = "";
            try {
                path1 = imageCaptureArray.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(BitmapFactory.decodeFile(path1));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(150, 150);
            lp.setMargins(10, 0, 10, 0);
            imgView.setScaleType(ImageView.ScaleType.FIT_XY);
            imgView.setLayoutParams(lp);
            imgView.setTag(i);
            imgView.setOnClickListener(imageClick);
           /* if (i == pos) {
                imgView.setPadding(10, 10, 10, 10);
                imgView.setBackground(mActivity.getDrawable(R.drawable.imageview_red_border));
            }*/
            imagesList.addView(imgView);
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }
    }

}
