package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.SupplierAdapter;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Supplier_Fragment extends Fragment {
    TextView labeltxt, nosupllier, notifycnt;
    ImageView categories_icon, filter_list_icon, imgClear;
    EditText etSearch;
    ListView suplierlist;
    SupplierAdapter supplierAdapter;
    ArrayList<Users> supplierItemArrayList;
    Context context;
    Activity activity;
    public LinearLayout nosupllierdata;
    RelativeLayout activity2_icon_lay;
    TextView add_supplier_icon;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;
    int index = 0;
    Preference preference;
    Users users;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = getActivity();
        context = getActivity();
        View view = inflater.inflate(R.layout.fragment_supplier, container, false);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        nosupllierdata = (LinearLayout) view.findViewById(R.id.nosupllierdata);

        nosupllier = (TextView) view.findViewById(R.id.nosupllier);
        nosupllier.setTypeface(RobotoFont.mediumFont(activity));
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText(getString(R.string.suppliers));
        filter_list_icon = (ImageView) view.findViewById(R.id.filter_list_icon);
        filter_list_icon.setVisibility(View.GONE);
        categories_icon = (ImageView) view.findViewById(R.id.categories_icon);
        categories_icon.setVisibility(View.GONE);
        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        add_supplier_icon = (TextView) view.findViewById(R.id.add_supplier_icon);
        add_supplier_icon.setVisibility(View.VISIBLE);
        add_supplier_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Invite_Supplier_Fragment(), false);
            }
        });
        imgClear = (ImageView) view.findViewById(R.id.imgClear);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(activity));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.INVISIBLE);

                supplierAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;
                }
                return handled;
            }
        });

        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });


        supplierItemArrayList = new ArrayList<>();
        suplierlist = (ListView) view.findViewById(R.id.suplierlist);

        supplierAdapter = new SupplierAdapter(context, supplierItemArrayList, nosupllierdata);
        suplierlist.setAdapter(supplierAdapter);

        suplierlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("loggeduser", false);
                bundle.putString("userid", supplierItemArrayList.get(i).getUserID());
                MainActivity.loadContentFragment(new Profile_Fragment(), false, bundle);
            }
        });

        //supplierList();

        getSuppliersCount();
        return view;
    }

   /* @Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt,activity);

    }



    public void getSuppliersCount() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    suplierlist.setVisibility(View.GONE);
                    nosupllierdata.setVisibility(View.VISIBLE);
                    return;
                }

                getSupplierList();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getSupplierList() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                suplierlist.setVisibility(View.VISIBLE);
                nosupllierdata.setVisibility(View.GONE);
                manageUser(dataSnapshot, "add");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                manageUser(dataSnapshot, "edit");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                manageUser(dataSnapshot, "delete");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public int matchValue(ArrayList<Users> supplierItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < supplierItemArrayList.size(); j++) {
            if (supplierItemArrayList.get(j).getUserID().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    public void manageUser(DataSnapshot dataSnapshot, final String action) {
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers);
        databaseReference1.child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.getValue(Users.class);

                if (action.equalsIgnoreCase("add")) {
                    int pos = matchValue(supplierItemArrayList, users.getUserID());
                    if (pos != -1)
                        return;
                    supplierItemArrayList.add(users);
                    supplierAdapter.addList(supplierItemArrayList);
                } else if (action.equalsIgnoreCase("edit")) {
                    int pos = matchValue(supplierItemArrayList, users.getUserID());
                    if (pos == -1)
                        return;
                    supplierItemArrayList.set(pos, users);
                    supplierAdapter.notifyDataSetChanged();
                } else if (action.equalsIgnoreCase("delete")) {
                    int pos = matchValue(supplierItemArrayList, users.getUserID());
                    if (pos == -1)
                        return;
                    supplierItemArrayList.remove(pos);
                    supplierAdapter.notifyDataSetChanged();

                }
                if (supplierItemArrayList.size() == 0) {
                    suplierlist.setVisibility(View.GONE);
                    nosupllierdata.setVisibility(View.VISIBLE);
                } else {
                    suplierlist.setVisibility(View.VISIBLE);
                    nosupllierdata.setVisibility(View.GONE);
                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                try {
                    if (supplierItemArrayList.size() == 0) {
                        suplierlist.setVisibility(View.GONE);
                        nosupllierdata.setVisibility(View.VISIBLE);
                    } else {
                        suplierlist.setVisibility(View.VISIBLE);
                        nosupllierdata.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


/*    public void supplierList() {
        progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers);
        index = 0;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                index = 0;
                supplierItemArrayList.clear();
                nosupllierdata.setVisibility(View.GONE);
                final int childrenCount = (int) dataSnapshot.getChildrenCount();
                if (childrenCount == 0) {
                    suplierlist.setVisibility(View.GONE);
                    nosupllierdata.setVisibility(View.VISIBLE);
                    progressDialog.hide();
                    return;
                }
                for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                            child(childDataSnapshot.getKey());
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Users users = dataSnapshot.getValue(Users.class);
                            supplierItemArrayList.add(users);

                            if (index + 1 == childrenCount) {
                                Collections.sort(supplierItemArrayList, new Comparator<Users>() {
                                    public int compare(Users o1, Users o2) {
                                        return (o1.getFirstName() + " " + o1.getLastName()).toLowerCase().compareTo((o2.getFirstName() + " " + o2.getLastName()).toLowerCase());
                                    }
                                });
                                supplierAdapter.addList(supplierItemArrayList);
                                if (progressDialog != null)
                                    progressDialog.dismiss();
                            } else
                                index++;

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progressDialog.hide();
                        }
                    });

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.hide();
            }
        });

    }*/

}
