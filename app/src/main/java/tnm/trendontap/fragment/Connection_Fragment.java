package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.ActiveContactAdpater;
import tnm.trendontap.adapter.InviteAdpater;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.BuyerConnections;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.UserType;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Connection_Fragment extends Fragment {
    private RecyclerView horizontal_recycler_view;
    private ArrayList<Users> horizontalList;
    private ArrayList<Users> inviteItemArrayList;
    private ActiveContactAdpater activeContactAdpater;
    private InviteAdpater inviteAdpater;
    ListView invitecontactlist;
    TextView labeltxt, activecontacttxt, invitenewcontacttxt, nosupllier, notifycnt;
    ImageView  imgClear;
    RelativeLayout activity2_icon_lay;
    EditText etSearch;
    Activity activity;
    ProgressDialog progressDialog;
    LinearLayout nosupllierdata,back_arrow;
    private TextView tv_no_buyers, tv_active_no_buyers;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection, container, false);
        activity = getActivity();
        nosupllierdata = (LinearLayout) view.findViewById(R.id.nosupllierdata);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        nosupllier = (TextView) view.findViewById(R.id.nosupllier);
        nosupllier.setTypeface(RobotoFont.regularFont(activity));
        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText("Connections");
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        Preference preference = new Preference(activity, Login_Pref);
        Users users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }

        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        tv_active_no_buyers = (TextView) view.findViewById(R.id.tv_active_no_buyers);
        tv_active_no_buyers.setTypeface(RobotoFont.regularFont(activity));
        tv_no_buyers = (TextView) view.findViewById(R.id.tv_no_buyers);
        tv_no_buyers.setTypeface(RobotoFont.regularFont(activity));

        imgClear = (ImageView) view.findViewById(R.id.imgClear);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(activity));
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                // Some phones disregard the IME setting option in the xml, instead
                // they send IME_ACTION_UNSPECIFIED so we need to catch that
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;
                }
                return handled;
            }
        });


        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });

        horizontal_recycler_view = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);

        horizontalList = new ArrayList<Users>();


        activeContactAdpater = new ActiveContactAdpater(activity, horizontalList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
        horizontal_recycler_view.setAdapter(activeContactAdpater);
        invitecontactlist = (ListView) view.findViewById(R.id.invitecontactlist);

        inviteItemArrayList = new ArrayList<>();
        inviteAdpater = new InviteAdpater(activity, inviteItemArrayList);
        invitecontactlist.setAdapter(inviteAdpater);

        invitecontactlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putString("UserID", inviteItemArrayList.get(i).getUserID());
                MainActivity.loadContentFragment(new ConnectUser_Fragment(), false, bundle);
            }
        });

        try {
            if (getArguments() != null && getArguments().getString("buyerinvitation", "").equalsIgnoreCase("yes")) {
                Bundle bundle = new Bundle();
                bundle.putString("UserID", getArguments().getString("UserID", ""));
                MainActivity.loadContentFragment(new ConnectUser_Fragment(), false, bundle);
            }
        } catch (Exception e) {

        }


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.INVISIBLE);

                inviteAdpater.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        activecontacttxt = (TextView) view.findViewById(R.id.activecontacttxt);
        activecontacttxt.setTypeface(RobotoFont.mediumFont(activity));

        invitenewcontacttxt = (TextView) view.findViewById(R.id.invitenewcontacttxt);
        invitenewcontacttxt.setTypeface(RobotoFont.mediumFont(activity));

        // setList();
        //processList();
        userList();
        return view;
    }

    int cnt = 0;

    public void setList() {
        final ArrayList<String> useridlist = new ArrayList<>();
        FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                child(Suppliers).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() == 0) {
                    activecontacttxt.setVisibility(View.GONE);
                    horizontal_recycler_view.setVisibility(View.GONE);

                } else {
                    activecontacttxt.setVisibility(View.VISIBLE);
                    horizontal_recycler_view.setVisibility(View.VISIBLE);

                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        useridlist.add(dataSnapshot1.getKey());
                    }

                }

                //  processList(useridlist);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void userList() {
        final ArrayList<String> usersArrayList = new ArrayList<>();
        FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        usersArrayList.clear();
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            usersArrayList.add(dataSnapshot1.getKey().toString());
                        }
                        processList(usersArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void processList(final ArrayList<String> usersArrayList) {
        inviteItemArrayList.clear();
        horizontalList.clear();
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                orderByChild(UserType).equalTo("Buyer").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                inviteItemArrayList.clear();
                horizontalList.clear();
                try {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Users users = dataSnapshot1.getValue(Users.class);
                        if (!users.getUserID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            if (usersArrayList.contains(users.getUserID())) {
                                if (!horizontalList.contains(users))
                                    horizontalList.add(users);
                                if (inviteItemArrayList.contains(users)) {
                                    inviteItemArrayList.remove(users);
                                }
                            } else {
                                if (!inviteItemArrayList.contains(users))
                                    inviteItemArrayList.add(users);
                                if (horizontalList.contains(users)) {
                                    horizontalList.remove(users);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    return;
                }
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                inviteAdpater.addList(inviteItemArrayList);
                activeContactAdpater.notifyDataSetChanged();
                setListViewHeightBasedOnChildren(invitecontactlist);
                if (horizontalList.size() == 0) {
                    tv_active_no_buyers.setVisibility(View.VISIBLE);
                } else {
                    tv_active_no_buyers.setVisibility(View.GONE);
                }

                if (inviteItemArrayList.size() == 0) {
                    tv_no_buyers.setVisibility(View.VISIBLE);
                    invitecontactlist.setVisibility(View.GONE);
                } else {
                    tv_no_buyers.setVisibility(View.GONE);
                    invitecontactlist.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
