package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import tnm.trendontap.R;
import tnm.trendontap.activity.CategoriesActivity;
import tnm.trendontap.activity.LocationActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.TagItemActivity;
import tnm.trendontap.adapter.LoveTrendSupplierAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.PostLovesItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.WheelView;

import static android.app.Activity.RESULT_OK;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.LOVE;
import static tnm.trendontap.utility.EventClass.POSTMSG;
import static tnm.trendontap.utility.EventClass.TREND;
import static tnm.trendontap.utility.EventClass.processActivity;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.UserPosts;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getResizedBitmap;
import static tnm.trendontap.utility.Utils.uploadpath;

public class UploadLoveFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "UploadLoveFragment";
    LinearLayout lnrDate, lnrTagItem, lnrAddLocation, lnrSelectCategory, back_arrow, labelPost;
    EditText etSayAboutLove;
    TextView labeltxt, labelPosttxt, tvSelectDelivery, tvDate, tvTagItem, tvAddLocation, tvSelectCategory;
    ImageView imgTag, addlocation;
    SquareImageView imageLove;
    StickyListHeadersListView supplierList;
    LinearLayout lnrgetlocation;
    TextView loc_name, loc_address;
    private Context mContext;
    private FragmentActivity mActivity;
    TextView loc_latitude, loc_longitude;
    DatabaseReference databaseReference;
    LoveTrendSupplierAdapter loveTrendSupplierAdapter;
    ArrayList<Users> supplierItemArrayList;
    ProgressDialog progressDialog;
    String selectedImage;
    int index = 0;
    Bitmap bitmap;
    LocationItem place;
    public static final int Location_Flag = 101;
    private String mImageurl;
    ArrayList<Tag_Items> tag_items = null;
    LocationItem locationItemslist = null;
    String action = "", uploadpost_id = "";
    JSONArray imageCaptureArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        selectedImage = getArguments().getString("selectedImage", "");
        String imageArray = getArguments().getString("imageCaptureArray", "");
        try {
            imageCaptureArray = new JSONArray(imageArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (getArguments().getString("action") != null) {
            action = getArguments().getString("action");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_love, container, false);
        setUI(view);
        getData();
        setData();
        return view;
    }

    private void setUI(View view) {
        loc_latitude = view.findViewById(R.id.loc_latitude);
        loc_longitude = view.findViewById(R.id.loc_longitude);

        etSayAboutLove = view.findViewById(R.id.etSayAboutLove);
        supplierList = (StickyListHeadersListView) view.findViewById(R.id.supplierList);

        back_arrow = view.findViewById(R.id.back_arrow);
        imageLove = view.findViewById(R.id.imageLove);
        imgTag = view.findViewById(R.id.imgTag);
        addlocation = view.findViewById(R.id.addlocation);
        lnrgetlocation = view.findViewById(R.id.lnrgetlocation);
        loc_name = view.findViewById(R.id.loc_name);
        loc_name.setTypeface(RobotoFont.regularFont(mActivity));
        loc_address = view.findViewById(R.id.loc_address);
        loc_address.setTypeface(RobotoFont.regularFont(mActivity));
        lnrDate = view.findViewById(R.id.lnrDate);
        lnrTagItem = view.findViewById(R.id.lnrTagItem);
        lnrAddLocation = view.findViewById(R.id.lnrAddLocation);
        lnrSelectCategory = view.findViewById(R.id.lnrSelectCategory);

        labeltxt = view.findViewById(R.id.labeltxt);
        labelPost = view.findViewById(R.id.labelPost);
        labelPosttxt = view.findViewById(R.id.labelPosttxt);

        tvSelectDelivery = view.findViewById(R.id.tvSelectDelivery);
        tvDate = view.findViewById(R.id.tvDate);
        tvTagItem = view.findViewById(R.id.tvTagItem);
        tvAddLocation = view.findViewById(R.id.tvAddLocation);
        tvSelectCategory = view.findViewById(R.id.tvSelectCategory);


        labeltxt.setTypeface(RobotoFont.boldFont(mContext));
        labelPosttxt.setTypeface(RobotoFont.mediumFont(mContext));

        tvSelectDelivery.setTypeface(RobotoFont.mediumFont(mContext));
        tvDate.setTypeface(RobotoFont.mediumFont(mContext));
        tvTagItem.setTypeface(RobotoFont.mediumFont(mContext));
        tvAddLocation.setTypeface(RobotoFont.mediumFont(mContext));
        tvSelectCategory.setTypeface(RobotoFont.regularFont(mContext));


        labeltxt.setTypeface(RobotoFont.boldFont(mActivity));
        labeltxt.setText("Upload Love");

        lnrSelectCategory.setOnClickListener(this);
        lnrDate.setOnClickListener(this);
        labelPost.setOnClickListener(this);
        back_arrow.setOnClickListener(this);
        lnrTagItem.setOnClickListener(this);
        lnrAddLocation.setOnClickListener(this);

    }

    private void setData() {
        try {
            if (getArguments() != null && !getArguments().getString("action").equalsIgnoreCase("edit")) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeFile(selectedImage, options);
                bitmap = getResizedBitmap(bitmap, 300);
                imageLove.setImageBitmap(bitmap);
              /*
                bitmap = BitmapFactory.decodeFile(selectedImage);
                imageLove.setImageBitmap(bitmap);*/
            }
        } catch (Exception e) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeFile(selectedImage, options);
            bitmap = getResizedBitmap(bitmap, 300);
            imageLove.setImageBitmap(bitmap);

           /* bitmap = BitmapFactory.decodeFile(selectedImage);
            imageLove.setImageBitmap(bitmap);*/
        }

        supplierItemArrayList = new ArrayList<>();
        loveTrendSupplierAdapter = new LoveTrendSupplierAdapter(mContext, supplierItemArrayList);
        supplierList.setAdapter(loveTrendSupplierAdapter);
        supplierList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        supplierList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                loveTrendSupplierAdapter.toggleSelection(position);
            }
        });
        supplierList();
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt, mActivity);
    }


    public void supplierList() {
        progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(SuppliersOfBuyer)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Suppliers);
        index = 0;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                supplierItemArrayList.clear();
                final int childrenCount = (int) dataSnapshot.getChildrenCount();
                if (childrenCount == 0) {
                    // supplierList.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    return;
                }
                for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(childDataSnapshot.getKey());
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Users users = dataSnapshot.getValue(Users.class);
                            supplierItemArrayList.add(users);

                            if (index + 1 == childrenCount) {
                                loveTrendSupplierAdapter.addList(supplierItemArrayList);
                                progressDialog.dismiss();
                            } else
                                index++;
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progressDialog.dismiss();
                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.hide();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrSelectCategory:
                startActivityForResult(new Intent(mActivity, CategoriesActivity.class).putExtra("isFilter", false), 100);
                // overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.lnrDate:
                selectDate();
                break;
            case R.id.back_arrow:
                mActivity.onBackPressed();
                break;
            case R.id.lnrTagItem:
                boolean isBitmap = false;
                if (getArguments().containsKey("selectedImage")) {
                    isBitmap = true;
                }

                String imageUrl = "";
                if (isBitmap)
                    imageUrl = selectedImage;
                else
                    imageUrl = mImageurl;

                Intent intent = new Intent(mActivity, TagItemActivity.class);
                intent.putParcelableArrayListExtra("tag_items_list", tag_items);
                intent.putExtra("imageUrl", imageUrl);
                intent.putExtra("isBitmap", isBitmap);
                startActivityForResult(intent, 1001);
                mActivity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.lnrAddLocation:
                startActivityForResult(new Intent(mActivity, LocationActivity.class), Location_Flag);
                mActivity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                break;
            case R.id.labelPost:
                if (action.equalsIgnoreCase("edit")) {
                    editFile();
                } else
                    uploadFile();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            String categoryId = data.getStringExtra("categoryId");
            String categoryName = data.getStringExtra("categoryName");
            tvSelectCategory.setText(categoryName.trim());
            tvSelectCategory.setTag(categoryId);
            if (!categoryName.equalsIgnoreCase("")) {
                lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textselected));
                tvSelectCategory.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                lnrSelectCategory.setBackgroundColor(Color.parseColor("#D2D2D2"));
                tvSelectCategory.setTextColor(Color.parseColor("#575757"));
            }
        } else if (requestCode == Location_Flag && resultCode == Location_Flag) {
            place = (LocationItem) data.getParcelableExtra("locationval");
            lnrgetlocation.setVisibility(View.VISIBLE);
            tvAddLocation.setVisibility(View.GONE);
            addlocation.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPrimary));
            loc_name.setText(place.getTitle());
            loc_address.setText(place.getAddress());
            loc_latitude.setText(String.valueOf(place.getLatitude()));
            loc_longitude.setText(String.valueOf(place.getLogitude()));

            locationItemslist = place;

        } else if (requestCode == 1001) {
            if (resultCode == mActivity.RESULT_OK) {

                tag_items = data.getParcelableArrayListExtra("tag_items");

                int tag_items_cnt = tag_items.size();
                if (tag_items_cnt == 0) {
                    tvTagItem.setText("Tag item");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                } else if (tag_items_cnt == 1) {
                    tvTagItem.setText(tag_items_cnt + " item tagged");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                    imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                } else {
                    tvTagItem.setText(tag_items_cnt + " items tagged");
                    imgTag.setImageResource(R.mipmap.tag_item_icon);
                    imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                }

            }
        }
    }

    ArrayList<String> lovephoto;

    private void uploadFile() {
        progressDialog.show();

        lovephoto = new ArrayList<>();
        uploadImage(0);
    }

    int uploadImagepos;

    private void uploadImage(int pos) {
        String image = "";
        uploadImagepos = pos;
        try {
            image = (String) imageCaptureArray.get(uploadImagepos);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Uri file = Uri.fromFile(new File(image));
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(uploadpath);
        StorageReference imagesRef = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + System.currentTimeMillis() / 1000 + ".jpg");

        UploadTask uploadTask = imagesRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                progressDialog.dismiss();
                Toast.makeText(mActivity, "posting failed", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl-->", "" + downloadUrl);
                lovephoto.add(String.valueOf(downloadUrl));

                if (lovephoto.size() == imageCaptureArray.length()) {
                    updateData(lovephoto);
                } else {
                    uploadImagepos += 1;
                    uploadImage(uploadImagepos);
                }

            }
        });
    }

    private void updateData(ArrayList<String> photoUrls) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts);
        String post_id = databaseReference.push().getKey();

        String tvSelectCategorytxt = "";
        if (!tvSelectCategory.getText().toString().trim().equalsIgnoreCase("Select Categories")) {
            tvSelectCategorytxt = tvSelectCategory.getText().toString().trim();
        }

        locationItemslist = null;
        if (!loc_latitude.getText().toString().trim().equalsIgnoreCase("0.0")) {
            locationItemslist = new LocationItem(loc_name.getText().toString().trim(),
                    loc_address.getText().toString().trim(),
                    Double.parseDouble(loc_latitude.getText().toString().trim()),
                    Double.parseDouble(loc_longitude.getText().toString().trim()));
        }

        PostLovesItem lovesItem = new PostLovesItem(photoUrls, String.valueOf(tvSelectCategory.getTag()), tvSelectCategorytxt,
                etSayAboutLove.getText().toString().trim(), 0, tvDate.getText().toString().trim(),
                locationItemslist, "", System.currentTimeMillis(),
                0, false, "love", post_id,
                FirebaseAuth.getInstance().getCurrentUser().getUid(), "", tag_items);

        databaseReference.child(post_id).setValue(lovesItem);
        DatabaseReference userposts = FirebaseDatabase.getInstance().getReference().child(UserPosts);
        userposts.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves).child(post_id).setValue(true);
        DatabaseReference userpostsfeed = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds);
        userpostsfeed.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves).child(post_id).setValue(true);

        if (loveTrendSupplierAdapter.getSupplierItemArrayList().size() == 0) {
            for (int j = 0; j < supplierItemArrayList.size(); j++) {
                userpostsfeed.child(supplierItemArrayList.get(j).getUserID()).child(Loves).child(post_id).setValue(true);
            }
        } else {
            for (int j = 0; j < loveTrendSupplierAdapter.getSupplierItemArrayList().size(); j++) {
                userpostsfeed.child(loveTrendSupplierAdapter.getSupplierItemArrayList().get(j).getUserID()).child(Loves).child(post_id).setValue(true);
            }
        }

        Preference preference = new Preference(mActivity, Login_Pref);
        Users users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        ActivityItem activityItem = new ActivityItem();
        activityItem.setProfilePic(users.getProfilePic());
        activityItem.setLongMessage(POSTMSG);
        activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + POSTMSG);
        activityItem.setPostID(post_id);
        activityItem.setPostImageURL(photoUrls.get(0));
        activityItem.setReactionID("");
        activityItem.setShortMessage(LOVE);
        activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
        activityItem.setActivityType(LOVE);
        activityItem.setTimeStamp(System.currentTimeMillis());
        activityItem.setUserID(users.getUserID());

        processActivity(activityItem);
        databaseReference.child(post_id).child(TimeStamp).setValue(System.currentTimeMillis());
        //Toast.makeText(mActivity, "Successfully saved", Toast.LENGTH_SHORT).show();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvSelectCategory.getWindowToken(), 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mActivity.finish();
                MainActivity.loveslay.performClick();
            }
        }, 1000);
    }

    private void editFile() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts).child(uploadpost_id);
        String tvSelectCategorytxt = "";
        if (!tvSelectCategory.getText().toString().trim().equalsIgnoreCase("Select Categories")) {
            tvSelectCategorytxt = tvSelectCategory.getText().toString().trim();
        }

        databaseReference.child(CatagoryName).setValue(tvSelectCategorytxt);
        databaseReference.child(Content).setValue(etSayAboutLove.getText().toString().trim());
        databaseReference.child(DeliveryDate).setValue(tvDate.getText().toString().trim());
        databaseReference.child(TimeStamp).setValue(System.currentTimeMillis());
        if (tag_items != null)
            databaseReference.child(TagItems).setValue(tag_items);
        if (locationItemslist != null)
            databaseReference.child(Location).setValue(locationItemslist);

        //Toast.makeText(mActivity, "Successfully saved", Toast.LENGTH_SHORT).show();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvSelectCategory.getWindowToken(), 0);
        MainActivity.manageBack(mActivity, "");

    }


    private void getData() {
        supplierItemArrayList = new ArrayList<>();
        Bundle bundle = getArguments();
        String selectCat = "Select Categories";
        if (bundle != null) {
            if (bundle.containsKey("lovesItem")) {
                LovesItem lovesItem = bundle.getParcelable("lovesItem");
                if (lovesItem != null) {
                    etSayAboutLove.setText(lovesItem.getAbouttrend());
                    if (!TextUtils.isEmpty(lovesItem.getCategory()))
                        selectCat = lovesItem.getCategory();
                    tvDate.setText(lovesItem.getDeliverydate());
                    uploadpost_id = lovesItem.getId();

                    if (!TextUtils.isEmpty(lovesItem.getPhotoUrls().get(0))) {
                        mImageurl = lovesItem.getPhotoUrls().get(0);
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(lovesItem.getPhotoUrlsList().get(0))
                                .apply(requestOptions)
                                .into(imageLove);
                    }
                    if (lovesItem.getTag_itemsArrayList() != null && lovesItem.getTag_itemsArrayList().size() > 0) {
                        tag_items = lovesItem.getTag_itemsArrayList();
                        if (lovesItem.getTag_itemsArrayList().size() == 1) {
                            tvTagItem.setText(lovesItem.getTag_itemsArrayList().size() + " item tagged");
                            imgTag.setImageResource(R.mipmap.tag_item_icon);
                            imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                        } else if (lovesItem.getTag_itemsArrayList().size() > 1) {
                            tvTagItem.setText(lovesItem.getTag_itemsArrayList().size() + " items tagged");
                            imgTag.setImageResource(R.mipmap.tag_item_icon);
                            imgTag.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                        }
                    }

                    if (lovesItem.getLocationItemArrayList() != null) {
                        locationItemslist = lovesItem.getLocationItemArrayList();
                        tvAddLocation.setVisibility(View.GONE);
                        lnrgetlocation.setVisibility(View.VISIBLE);
                        loc_name.setText(lovesItem.getLocationItemArrayList().getTitle());
                        loc_address.setText(lovesItem.getLocationItemArrayList().getAddress());
                    } else {
                        tvAddLocation.setVisibility(View.VISIBLE);
                        lnrgetlocation.setVisibility(View.GONE);
                    }

                }
            } else if (bundle.containsKey("buyerLovesItem")) {
                BuyerLovesItem buyerLovesItem = bundle.getParcelable("buyerLovesItem");
                if (buyerLovesItem != null) {

                    etSayAboutLove.setText(buyerLovesItem.getAbouttrend());
                    if (!TextUtils.isEmpty(buyerLovesItem.getCategory()))
                        selectCat = buyerLovesItem.getCategory();
                    tvDate.setText(buyerLovesItem.getDeliverydate());


                    if (!TextUtils.isEmpty(buyerLovesItem.getImgurl())) {
                        mImageurl = buyerLovesItem.getImgurl();
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(buyerLovesItem.getImgurl())
                                .apply(requestOptions)
                                .into(imageLove);
                    }

                }
            } else if (bundle.containsKey("buyerTrendsItem")) {
                BuyerTrendsItem buyerTrendsItem = bundle.getParcelable("buyerTrendsItem");
                if (buyerTrendsItem != null) {

                    etSayAboutLove.setText(buyerTrendsItem.getAbouttrend());
                    if (!TextUtils.isEmpty(buyerTrendsItem.getCategory()))
                        selectCat = buyerTrendsItem.getCategory();
                    tvDate.setText(buyerTrendsItem.getDeliverydate());


                    if (!TextUtils.isEmpty(buyerTrendsItem.getImgurl())) {
                        mImageurl = buyerTrendsItem.getImgurl();
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(buyerTrendsItem.getImgurl())
                                .apply(requestOptions)
                                .into(imageLove);
                    }

                }
            } else if (bundle.containsKey("trendsItem")) {
                TrendsItem trendsItem = bundle.getParcelable("trendsItem");
                if (trendsItem != null) {

                    etSayAboutLove.setText(trendsItem.getContent());
                    if (!TextUtils.isEmpty(trendsItem.getCategoryName()))
                        selectCat = trendsItem.getCategoryName();
                    tvDate.setText(trendsItem.getDeliveryDate());

                    if (!TextUtils.isEmpty(trendsItem.getPhotoUrlsList().get(0))) {
                        mImageurl = trendsItem.getPhotoUrlsList().get(0);
                        RequestOptions requestOptions = new RequestOptions();
                        Glide.with(mContext)
                                .load(trendsItem.getPhotoUrlsList().get(0))
                                .apply(requestOptions)
                                .into(imageLove);
                    }
                }
            } else if (getArguments().containsKey("selectedImage")) {
                selectedImage = getArguments().getString("selectedImage", "");
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeFile(selectedImage, options);
                bitmap = getResizedBitmap(bitmap, 300);

                imageLove.setImageBitmap(bitmap);

            }
        }
        tvSelectCategory.setText(selectCat);
      /*  if (selectCat.equalsIgnoreCase("Select Categories")) {
            lnrSelectCategory.setBackgroundColor(Color.parseColor("#D2D2D2"));
        } else {
            lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textselected));
        }*/

        if (selectCat.equalsIgnoreCase("Select Categories")) {
            lnrSelectCategory.setBackgroundColor(Color.parseColor("#D2D2D2"));
            tvSelectCategory.setTextColor(Color.parseColor("#575757"));
        } else {
            lnrSelectCategory.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.textselected));
            tvSelectCategory.setTextColor(Color.parseColor("#FFFFFF"));
        }

    }

    private SimpleDateFormat dateFormatter;
    int monthOfYear;
    int year;
    int dayOfMonth;
    private static final String[] MONTH = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final int[] MONTH_INT = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    private static final List<String> DATES = new ArrayList<>();
    private static final List<String> YEAR = new ArrayList<>();
    String selectedDate;

    private void selectDate() {

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);

        for (int i = 0; i < 30; i++) {
            DATES.add(String.valueOf(i + 1));
        }
        for (int i = 1900; i < 2050; i++) {
            YEAR.add(String.valueOf(i + 1));
        }
        monthOfYear = -1;
        year = -1;
        dayOfMonth = -1;

        final int yearThis, month, date, posYear, posDate;
        if (TextUtils.isEmpty(selectedDate)) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            yearThis = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date));
        } else {
            String[] fulldate = selectedDate.split("/");
            yearThis = Integer.parseInt(fulldate[2]);
            month = Integer.parseInt(fulldate[1]);
            date = Integer.parseInt(fulldate[0]);

            posYear = YEAR.indexOf(String.valueOf(yearThis));
            posDate = DATES.indexOf(String.valueOf(date)) + 1;
        }

        View outerView = LayoutInflater.from(mContext).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);

        // monthOfYear = month-1;
        // year = Integer.parseInt(YEAR.get(posYear));
        //  dayOfMonth = posDate;
        wv.setItems(Arrays.asList(MONTH));
        wv.setSeletion(month - 1);
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                monthOfYear = selectedIndex;
            }
        });
        WheelView wv1 = (WheelView) outerView.findViewById(R.id.wheel_view_wv1);
        wv1.setItems(DATES);

        wv1.setSeletion(posDate - 1);
        wv1.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
                dayOfMonth = Integer.parseInt(item);
            }
        });
        WheelView wv2 = (WheelView) outerView.findViewById(R.id.wheel_view_wv2);
        wv2.setItems(YEAR);
        wv2.setSeletion(posYear);
        wv2.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                year = Integer.parseInt(item);
                Log.d(TAG, "[Dialog]selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });

        new AlertDialog.Builder(mActivity, R.style.Theme_AppCompat_Light_Dialog)
                .setView(outerView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (monthOfYear == -1) {
                            monthOfYear = month - 1;
                        } else {
                            monthOfYear -= 1;
                        }
                        if (dayOfMonth == -1) {
                            dayOfMonth = posDate;
                        }
                        if (year == -1) {
                            year = Integer.parseInt(YEAR.get(posYear));
                        }
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        Date date = new Date(newDate.getTimeInMillis());

                        tvDate.setText(dateFormatter.format(date.getTime()));
                    }
                })
                .show();
    }


}
