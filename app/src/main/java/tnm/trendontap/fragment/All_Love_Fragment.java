package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.CameraActivity;
import tnm.trendontap.activity.FullScreenImageActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.LovesAdapter;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.FirstName;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;

/**
 * Created by TNM on 3/20/2018.
 */

public class All_Love_Fragment extends Fragment {
    ProgressDialog progressDialog;
    LinearLayout nolovesdata;
    ListView loveslistview;
    private ArrayList<LovesItem> lovesItemArrayList;
    LovesAdapter lovesAdapter;
    TextView noloves, taptostart;
    DatabaseReference databaseReference;
    ImageView addloves;
    Activity activity;
    View view;

    String postidval = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lovesItemArrayList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_all_love, container, false);
        activity = getActivity();

        try {
            if (getArguments().getString("postid") != null && !getArguments().getString("postid").equalsIgnoreCase("")) {
                postidval = getArguments().getString("postid");
            }
        } catch (Exception e) {

        }

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");

        noloves = (TextView) view.findViewById(R.id.noloves);
        noloves.setTypeface(RobotoFont.mediumFont(activity));

        addloves = (ImageView) view.findViewById(R.id.addloves);
        addloves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, CameraActivity.class).putExtra("buyers", true));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        taptostart = (TextView) view.findViewById(R.id.taptostart);
        taptostart.setTypeface(RobotoFont.mediumFont(activity));

        nolovesdata = (LinearLayout) view.findViewById(R.id.nolovesdata);
        loveslistview = (ListView) view.findViewById(R.id.loveslist);

        lovesAdapter = new LovesAdapter(activity, lovesItemArrayList);
        loveslistview.setAdapter(lovesAdapter);

        getLoveCount();

        return view;
    }


    int count = 0;

    public void getLoveCount() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Loves).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                count = (int) dataSnapshot.getChildrenCount();
                if (!dataSnapshot.hasChildren()) {
                    nolovesdata.setVisibility(View.VISIBLE);
                    loveslistview.setVisibility(View.GONE);
                    return;
                }
                nolovesdata.setVisibility(View.GONE);
                loveslistview.setVisibility(View.VISIBLE);
                if (progressDialog != null && !progressDialog.isShowing()) {
                    progressDialog.show();
                }
                setUserPostFeed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUserPostFeed() {

        lovesItemArrayList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Loves)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        nolovesdata.setVisibility(View.GONE);
                        loveslistview.setVisibility(View.VISIBLE);
                        getLovePostData(dataSnapshot.getKey());

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    public void managePos() {
        loveslistview.post(new Runnable() {
            @Override
            public void run() {
                if (!postidval.equalsIgnoreCase("")) {
                    int pos = matchValue(lovesItemArrayList, postidval);
                    if (pos == -1) {
                        return;
                    }
                    loveslistview.setSelection(pos);
                }
            }
        });
    }

    public void getLovePostData(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        nolovesdata.setVisibility(View.GONE);
                        loveslistview.setVisibility(View.VISIBLE);
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        manageData(dataSnapshot, "update");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }


    public void manageData(final DataSnapshot lovelist, String action) {
        long likecnt = (long) lovelist.child(LikesCount).getValue();
        ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
        if (lovelist.hasChild(TagItems)) {
            for (DataSnapshot messageSnapshot : lovelist.child(TagItems).getChildren()) {
                Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                tag_itemsArrayList.add(tag_Items);
            }
        }

        LocationItem location = null;
        if (lovelist.hasChild(Location)) {
            location = new LocationItem(lovelist.child(Location).child(Title).getValue().toString(),
                    lovelist.child(Location).child(Address).getValue().toString(),
                    (double) lovelist.child(Location).child(Latitude).getValue(),
                    (double) lovelist.child(Location).child(Logitude).getValue());
        }
        /*ArrayList<String> yourStringArray = new ArrayList<>();
        int size = (int) lovelist.child(tPostPhotoId).getChildrenCount();
        for (int i = 0; i < size; i++) {
            String url = String.valueOf(lovelist.child(tPostPhotoId).child(i + "").getValue());
            yourStringArray.add(url);
        }*/

        ArrayList<String> yourStringArray = (ArrayList<String>) lovelist.child(FireStoreUtils.tPostPhotoId).getValue();

        final LovesItem lovesItem = new LovesItem(yourStringArray,
                "",
                lovelist.child(CatagoryName).getValue().toString(),
                lovelist.child(Content).getValue().toString(),
                likecnt,
                lovelist.child(DeliveryDate).getValue().toString(),
                (long) lovelist.child(TimeStamp).getValue(),
                lovelist.child(ReactionsCount).getValue().toString(),
                (boolean) lovelist.child(UnreadReactionCount).getValue(),
                false, "", "",
                lovelist.child(ID).getValue().toString(),
                tag_itemsArrayList,
                location,
                lovelist.child(FireStoreUtils.UserID).getValue().toString()
        );

        if (action.equalsIgnoreCase("update")) {
            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
            if (pos == -1)
                return;
            lovesItemArrayList.set(pos, lovesItem);
            lovesAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
            if (pos == -1) {
                if (lovesItemArrayList.size() == 0) {
                    nolovesdata.setVisibility(View.VISIBLE);
                    loveslistview.setVisibility(View.GONE);
                }
                return;
            }
            lovesItemArrayList.remove(pos);
            lovesAdapter.notifyDataSetChanged();
            if (lovesItemArrayList.size() == 0) {
                nolovesdata.setVisibility(View.VISIBLE);
                loveslistview.setVisibility(View.GONE);
            }
        } else {
            /*int pos = matchValue(lovesItemArrayList, lovesItem.getId());
            if (pos == -1)*/
            if (getArguments().getString("categoryName") != null && !getArguments().getString("categoryName").equalsIgnoreCase("All")) {
                if (getArguments().getString("categoryName").equalsIgnoreCase(lovesItem.getCategory()))
                    lovesItemArrayList.add(lovesItem);
            } else
                lovesItemArrayList.add(lovesItem);

            //  if (lovesItemArrayList.size() == count) {
            lovesAdapter.addList(lovesItemArrayList);
            nolovesdata.setVisibility(View.GONE);
            loveslistview.setVisibility(View.VISIBLE);

            // }
        }

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        if (lovesItemArrayList.size() == 0) {
            nolovesdata.setVisibility(View.VISIBLE);
            loveslistview.setVisibility(View.GONE);
            return;
        }

        if (!action.equalsIgnoreCase("delete"))
            getPostLikeCount(lovesItem, action);

    }


    public void getPostLikeCount(final LovesItem lovesItem, final String action) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(lovesItem.getId()).child(Likes).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            int pos = matchValue(lovesItemArrayList, lovesItem.getId());
                            if (pos == -1)
                                return;
                            lovesItem.setLikecnt(0);
                            lovesItemArrayList.set(pos, lovesItem);
                            lovesAdapter.notifyDataSetChanged();
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            managePos();
                            return;
                        }
                        lovesItem.setLikecnt((int) dataSnapshot.getChildrenCount());
                        getPostLikers(lovesItem);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                });
    }


    public void getPostLikers(final LovesItem lovesItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(lovesItem.getId()).child(Likes).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getUserDetails(dataSnapshot.getKey(), lovesItem);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.i("getPostLikers", "called");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void getUserDetails(String userid, final LovesItem lovesItem) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (lovesItem.getLiker1().equalsIgnoreCase("")) {
                            lovesItem.setLiker1(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        } else if (lovesItem.getLiker2().equalsIgnoreCase("")) {
                            lovesItem.setLiker2(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        }
                        int pos = matchValue(lovesItemArrayList, lovesItem.getId());
                        if (pos == -1)
                            return;
                        lovesItemArrayList.set(pos, lovesItem);
                        lovesAdapter.notifyDataSetChanged();
                        managePos();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }


    public int matchValue(ArrayList<LovesItem> lovesItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < lovesItemArrayList.size(); j++) {
            if (lovesItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


}
