package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.SampleRequestAdapter;
import tnm.trendontap.modal.SampleRequest;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.BuyerUserID;
import static tnm.trendontap.utility.FireStoreUtils.Sample;
import static tnm.trendontap.utility.FireStoreUtils.StyleNumber;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserID;


public class SampleRequest_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Activity activity;
    ListView sampleRequestlist;
    EditText etSearch;
    ImageView back, imgClear;
    SampleRequestAdapter sampleRequestAdapter;
    ArrayList<SampleRequest> sampleRequestArrayList;
    LinearLayout nosamplerequestdata;
    TextView nosamplerequest;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static SampleRequest_Fragment newInstance(String param1, String param2) {
        SampleRequest_Fragment fragment = new SampleRequest_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample_request, container, false);
        activity = getActivity();
        sampleRequestlist = (ListView) view.findViewById(R.id.samplerequestlist);
        nosamplerequest = (TextView) view.findViewById(R.id.nosamplerequest);
        nosamplerequest.setTypeface(RobotoFont.mediumFont(activity));
        nosamplerequestdata = (LinearLayout) view.findViewById(R.id.nosamplerequestdata);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(activity));

        back = (ImageView) view.findViewById(R.id.back);
        imgClear = (ImageView) view.findViewById(R.id.imgClear);
        sampleRequestArrayList = new ArrayList<>();

        sampleRequestAdapter = new SampleRequestAdapter(activity, sampleRequestArrayList, nosamplerequestdata);
        sampleRequestlist.setAdapter(sampleRequestAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.INVISIBLE);

                sampleRequestAdapter.filter(charSequence.toString(), sampleRequestlist);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                // Some phones disregard the IME setting option in the xml, instead
                // they send IME_ACTION_UNSPECIFIED so we need to catch that
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;

                }
                return handled;
            }
        });

        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });
        if (mParam2.equalsIgnoreCase(Utils.Supplier))
            setList();
        else
            setListBuyer();
        return view;
    }

    public void setList() {
        sampleRequestArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child(Sample)
                .orderByChild(FireStoreUtils.UserID).equalTo(mParam1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        sampleRequestArrayList.clear();
                        try {
                            Profile_Fragment.profiletabLayout.getTabAt(2).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Samples Requested");
                        } catch (Exception e) {

                        }

                        if (dataSnapshot.getChildrenCount() == 0) {
                            nosamplerequestdata.setVisibility(View.VISIBLE);
                            sampleRequestlist.setVisibility(View.GONE);
                            return;
                        } else {
                            nosamplerequestdata.setVisibility(View.GONE);
                            sampleRequestlist.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            sampleRequestArrayList.add(new SampleRequest((long) dataSnapshot1.child(TimeStamp).getValue(),
                                    dataSnapshot1.child(StyleNumber).getValue().toString()));

                        }
                        sampleRequestAdapter.addList(sampleRequestArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }

    public void setListBuyer() {
        sampleRequestArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child(Sample)
                .orderByChild(BuyerUserID).equalTo(mParam1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        sampleRequestArrayList.clear();
                        try {
                            Profile_Fragment.profiletabLayout.getTabAt(2).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Samples Requested");
                        } catch (Exception e) {

                        }
                        if (dataSnapshot.getChildrenCount() == 0) {
                            nosamplerequestdata.setVisibility(View.VISIBLE);
                            sampleRequestlist.setVisibility(View.GONE);
                            return;
                        } else {
                            nosamplerequestdata.setVisibility(View.GONE);
                            sampleRequestlist.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            sampleRequestArrayList.add(new SampleRequest((long) dataSnapshot1.child(TimeStamp).getValue(),
                                    dataSnapshot1.child(StyleNumber).getValue().toString()));

                        }
                        sampleRequestAdapter.addList(sampleRequestArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }

   /* public void setListBuyer() {
        sampleRequestArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child("sample")
                .orderByChild("post_by_userId").equalTo(mParam1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile_Fragment.profiletabLayout.getTabAt(2).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Samples Requested");
                        if (dataSnapshot.getChildrenCount() == 0) {
                            nosamplerequestdata.setVisibility(View.VISIBLE);
                            sampleRequestlist.setVisibility(View.GONE);
                            return;
                        } else {
                            nosamplerequestdata.setVisibility(View.GONE);
                            sampleRequestlist.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            sampleRequestArrayList.add(new SampleRequest((long) dataSnapshot1.child("timeStamp").getValue(),
                                    dataSnapshot1.child("style_number").getValue().toString()));

                        }
                        sampleRequestAdapter.addList(sampleRequestArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }*/
}
