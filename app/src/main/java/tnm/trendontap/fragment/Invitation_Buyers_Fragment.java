package tnm.trendontap.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.InvitationAdapter;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.modal.SuppliersInvitations;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.InvitedSupplierEmailID;
import static tnm.trendontap.utility.FireStoreUtils.SupplierInvitations;
import static tnm.trendontap.utility.Utils.Login_Pref;

/**
 * A simple {@link Fragment} subclass.
 */
public class Invitation_Buyers_Fragment extends Fragment {

    TextView labeltxt, noinvitations;
    ImageView addbtn, back_arrow;
    LinearLayout nonoinvitationsdata;
    ListView invitationlist;

    Context context;
    Activity activity;
    ProgressDialog progressDialog;
    private InvitationAdapter invitationsAdapter;
    ArrayList<SuppliersInvitations> invitationsItemArrayList;
    Preference preference;
    private Users users;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invitation_buyers, container, false);
        context = getActivity();
        activity = getActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");

        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        nonoinvitationsdata = (LinearLayout) view.findViewById(R.id.nonoinvitationsdata);
        noinvitations = (TextView) view.findViewById(R.id.noinvitations);
        noinvitations.setTypeface(RobotoFont.mediumFont(activity));

        back_arrow = (ImageView) view.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        invitationlist = (ListView) view.findViewById(R.id.invitationlist);

        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(context));
        labeltxt.setText("Invitations");

        invitationsItemArrayList = new ArrayList<>();

        invitationsAdapter = new InvitationAdapter(activity, context, invitationsItemArrayList, nonoinvitationsdata,invitationlist);
        invitationlist.setAdapter(invitationsAdapter);

        // invitationsList();
        getInviteCount();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt,activity);
    }

    public void getInviteCount() {
        FirebaseDatabase.getInstance().getReference().child(SupplierInvitations)
                .orderByChild(InvitedSupplierEmailID).equalTo(users.getEmailID())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            invitationlist.setVisibility(View.GONE);
                            nonoinvitationsdata.setVisibility(View.VISIBLE);
                            return;
                        }
                        invitationlist.setVisibility(View.VISIBLE);
                        nonoinvitationsdata.setVisibility(View.GONE);
                        invitationsItemArrayList.clear();
                        getInviteUser();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getInviteUser() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        FirebaseDatabase.getInstance().getReference().child(SupplierInvitations)
                .orderByChild(InvitedSupplierEmailID).equalTo(users.getEmailID())
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }


    public void manageData(DataSnapshot dataSnapshot, String action) {

        SuppliersInvitations suppliersInvitations = dataSnapshot.getValue(SuppliersInvitations.class);

        if (action.equalsIgnoreCase("add")) {
            try {
                if (!suppliersInvitations.isIsInviationAccepted())
                    invitationsItemArrayList.add(suppliersInvitations);
            } catch (Exception e) {

            }
            invitationsAdapter.addList(invitationsItemArrayList);
        } else if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(invitationsItemArrayList, suppliersInvitations.getId());
            if (pos == -1)
                return;
            invitationsItemArrayList.set(pos, suppliersInvitations);
            invitationsAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(invitationsItemArrayList, suppliersInvitations.getId());
            if (pos == -1)
                return;
            invitationsItemArrayList.remove(pos);
            invitationsAdapter.notifyDataSetChanged();
        }

        if (invitationsItemArrayList.size() == 0) {
            invitationlist.setVisibility(View.GONE);
            nonoinvitationsdata.setVisibility(View.VISIBLE);
        }

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    public int matchValue(ArrayList<SuppliersInvitations> invitationsItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < invitationsItemArrayList.size(); j++) {
            if (invitationsItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

   /* public void invitationsList() {
        progressDialog.show();
        FirebaseDatabase.getInstance().getReference().child(SupplierInvitations)
                .orderByChild(InvitedSupplierEmailID).equalTo(users.getEmail())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        invitationsItemArrayList.clear();
                        nonoinvitationsdata.setVisibility(View.GONE);
                        final int childrenCount = (int) dataSnapshot.getChildrenCount();
                        if (childrenCount == 0) {
                            invitationlist.setVisibility(View.GONE);
                            nonoinvitationsdata.setVisibility(View.VISIBLE);
                            progressDialog.hide();
                            return;
                        }

                        for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                            SuppliersInvitations suppliersInvitations = childDataSnapshot.getValue(SuppliersInvitations.class);
                            if (!suppliersInvitations.getIs_inviation_accepted())
                                invitationsItemArrayList.add(suppliersInvitations);
                        }
                        invitationsAdapter.addList(invitationsItemArrayList);
                        progressDialog.hide();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }*/

}
