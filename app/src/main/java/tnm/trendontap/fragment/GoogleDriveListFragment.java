package tnm.trendontap.fragment;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import tnm.trendontap.R;

import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.SeasonActivity;
import tnm.trendontap.adapter.GoogleDriveAdapter;
import tnm.trendontap.modal.GoogleDriveModel;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static android.app.Activity.RESULT_OK;
import static tnm.trendontap.utility.Utils.isLessEqualFifty;

/**
 * Created by TNM on 2/22/2018.
 */

public class GoogleDriveListFragment extends Fragment {

    Activity activity;
    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;
    ListView mListView;
    GoogleDriveAdapter googleDriveAdapter;
    ArrayList<GoogleDriveModel> googleDriveModelArrayList;
    TextView message, labeltxt;
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    static final int REQUEST_PERMISSION_WRITE_STORAGE = 1008;
    private static final String PREF_ACCOUNT_NAME = "gdname";
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA, DriveScopes.DRIVE_READONLY, DriveScopes.DRIVE_FILE};
    TextView close;
    Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_googledrivelist, container, false);
        activity = getActivity();
        mContext = getContext();
        close = (TextView) view.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
                // onBackPressed();
            }
        });

        googleDriveModelArrayList = new ArrayList<>();
        mProgress = new ProgressDialog(activity);
        mProgress.setMessage("Calling Drive API ...");

        message = (TextView) view.findViewById(R.id.message);
        message.setTypeface(RobotoFont.regularFont(activity));
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.regularFont(activity));
        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                activity, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        mListView = (ListView) view.findViewById(R.id.mListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (!isLessEqualFifty(googleDriveModelArrayList.get(i).getSize())) {
                    Toast.makeText(activity, "Your Trend exceeds our maximum size limit of 50MB, please reduce the size and try again", Toast.LENGTH_SHORT).show();
                    return;
                }
                addTrendNameDialog(googleDriveModelArrayList.get(i));
                // put permission here write external storgage
               /* new DownloadTask(mCredential).execute(googleDriveModelArrayList.get(i).getId(),
                        googleDriveModelArrayList.get(i).getName());*/
            }
        });
        googleDriveAdapter = new GoogleDriveAdapter(activity, googleDriveModelArrayList);
        mListView.setAdapter(googleDriveAdapter);


        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
        } else {
            getResultsFromApi();
        }
        return view;
    }


    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            message.setText("No network connection available.");
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                activity, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = activity.getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

  /*  @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }*/

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     *
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode  code indicating the result of the incoming
     *                    activity result.
     * @param data        Intent (containing result data) returned by incoming
     *                    activity result.
     */
    @Override
    public void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    message.setText(
                            "This app requires Google Play Services. Please install " +
                                    "Google Play Services on your device and relaunch this app.");
                    message.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.GONE);
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                activity.getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     *
     * @param requestCode  The request code passed in
     *                     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_WRITE_STORAGE) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
            } else {
                getResultsFromApi();
            }
        } else
            EasyPermissions.onRequestPermissionsResult(
                    requestCode, permissions, grantResults, this);
    }

    /**
     * Callback for when a permission is granted using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     *//*
    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }*/

    /**
     * Callback for when a permission is denied using the EasyPermissions
     * library.
     *
     * @param requestCode The request code associated with the requested
     *                    permission
     * @param list        The requested permission list. Never null.
     */
   /* @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }
*/

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(activity);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(activity);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                activity,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    /**
     * An asynchronous task that handles the Drive API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, ArrayList<GoogleDriveModel>> {
        private com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName(getResources().getString(R.string.app_name))
                    .build();
        }

        /**
         * Background task to call Drive API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected ArrayList<GoogleDriveModel> doInBackground(Void... params) {
            try {
                googleDriveModelArrayList.clear();
                FileList result = mService.files().list()
                        .setPageSize(1000)
                        .setQ("mimeType='application/pdf'")
                        .setFields("files(id,name,size)")
                        .execute();

                List<File> files = result.getFiles();
                if (files != null) {
                    for (File file : files) {
                        //if (isLessEqualFifty(file.getSize()))
                        googleDriveModelArrayList.add(new GoogleDriveModel(file.getName(), file.getId(), file.getSize()));
                    }
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            googleDriveAdapter.notifyDataSetChanged();

                        }
                    });

                }
                return googleDriveModelArrayList;
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            message.setText("");
            mProgress.setMessage("Calling Drive API ...");
            mProgress.show();
        }

        @Override
        protected void onPostExecute(ArrayList<GoogleDriveModel> output) {
            if (mProgress.isShowing())
                mProgress.hide();
            if (output == null || output.size() == 0) {
                message.setText("No supported files");
                message.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            } else {
                message.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            if (mProgress.isShowing())
                mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            REQUEST_AUTHORIZATION);
                } else {
                    message.setText("The following error occurred:\n"
                            + mLastError.getMessage());
                }
            } else {
                message.setText("Request cancelled.");
            }
            message.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {
        private com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;
        GoogleDriveModel googleDriveModel;
        String trendname;

        DownloadTask(GoogleAccountCredential credential, GoogleDriveModel googleDriveModel, String trendname) {
            this.googleDriveModel = googleDriveModel;
            this.trendname = trendname;
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName(getResources().getString(R.string.app_name))
                    .build();
        }

        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            mProgress.hide();
            Bundle bundle = new Bundle();
            bundle.putString("trendimg", string);
            bundle.putString("trendname", trendname);
            MainActivity.loadContentFragment(new UploadTrendFragment(), false, bundle);
        }


        @Override
        protected String doInBackground(String... params) {
            String pdfPath = "";
            try {

                java.io.File main_file = new java.io.File(
                        Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name));
                if (!main_file.exists()) {
                    main_file.mkdir();
                }
                pdfPath = String.format(
                        Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name)
                                + "/%s.pdf", params[1]);

                java.io.File pdfFile = new java.io.File(pdfPath);

                FileOutputStream fileOutput = new FileOutputStream(pdfFile);
                mService.files().get(params[0])
                        .executeMediaAndDownloadTo(fileOutput);

            } catch (Exception e) {
                Log.i("error", e.getMessage().toLowerCase());
            }
            return pdfPath;
        }

        @Override
        protected void onPreExecute() {
            mProgress.setMessage("Downloading...");
            mProgress.show();
        }

    }

    public void addTrendNameDialog(final GoogleDriveModel googleDriveModel) {
        final Dialog dialog = new Dialog(activity);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_add_season);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(activity));
        tvTitle.setText("Give your Trend a Title");
        final EditText edtname = (EditText) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(activity));
        edtname.setHint("Title");
        edtname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) ;
                {
                    if (edtname.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(activity, "Please enter title", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    new DownloadTask(mCredential, googleDriveModel, edtname.getText().toString().trim()).execute(googleDriveModel.getId(),
                            googleDriveModel.getName());
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                    dialog.dismiss();
                }
                return false;
            }
        });


        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setText("OK");
        done_action.setTypeface(RobotoFont.boldFont(activity));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtname.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "Please enter title", Toast.LENGTH_LONG).show();
                    return;
                }
                new DownloadTask(mCredential, googleDriveModel, edtname.getText().toString().trim()).execute(googleDriveModel.getId(),
                        googleDriveModel.getName());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                dialog.dismiss();
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(activity));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);

    }


}
