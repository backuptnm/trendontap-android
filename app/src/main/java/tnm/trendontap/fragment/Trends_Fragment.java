package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.UploadFromFileManager;
import tnm.trendontap.adapter.PagerAdapter;
import tnm.trendontap.dropbox.UserActivity;
import tnm.trendontap.modal.SeasonItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.TrendsSeason;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Trends_Fragment extends Fragment {
    TextView mytrendtxt, noseasontxt, taptostart, notifycnt;
    ViewPager viewpager;
    TabLayout tabLayout;
    PagerAdapter adapter;
    ImageView addtrend, profilepic, connection, listseason, addseason;
    Context context;
    RelativeLayout activitylisticon;
    ProgressDialog progressDialog;
    ArrayList<String> stringArrayList;
    Preference preference;
    Users users;
    LinearLayout noseasondata, mainwrapper;
    Activity activity = null;
    final int REQUEST_CODE_DOC = 101;

    String postid = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trends, container, false);
        activity = getActivity();
        preference = new Preference(activity, Login_Pref);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        profilepic = (ImageView) view.findViewById(R.id.profilepic);
        profilepic.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mainwrapper = (LinearLayout) view.findViewById(R.id.mainwrapper);
        noseasondata = (LinearLayout) view.findViewById(R.id.noseasondata);
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("loggeduser", true);
                MainActivity.loadContentFragment(new Profile_Fragment(), false, bundle);
            }
        });
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        if (getArguments() != null && getArguments().getString("postid") != null) {
            postid = getArguments().getString("postid");
        }

        if (!TextUtils.isEmpty(users.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);
            Glide.with(this)
                    .load(users.getProfilePic())
                    .apply(requestOptions)
                    .into(profilepic);
        }


        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        activitylisticon = (RelativeLayout) view.findViewById(R.id.activitylisticon);
        activitylisticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        connection = (ImageView) view.findViewById(R.id.connection);
        connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Connection_Fragment(), false);
            }
        });

        listseason = (ImageView) view.findViewById(R.id.listseason);
        listseason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Season_Fragment(), false);
            }
        });

        addtrend = (ImageView) view.findViewById(R.id.addtrend);
        addtrend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //addTrenddialog();
                selectOptionsDialog(activity);

            }
        });

        addseason = (ImageView) view.findViewById(R.id.addseason);
        addseason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOptionsDialog(activity);//uploadTrendOption(activity);
            }
        });

        noseasontxt = (TextView) view.findViewById(R.id.noseasontxt);
        noseasontxt.setTypeface(RobotoFont.regularFont(activity));
        taptostart = (TextView) view.findViewById(R.id.taptostart);
        taptostart.setTypeface(RobotoFont.mediumFont(activity));
        mytrendtxt = (TextView) view.findViewById(R.id.mytrendtxt);
        mytrendtxt.setTypeface(RobotoFont.regularFont(activity));
        mytrendtxt.setText(getString(R.string.mytrend));
        context = activity;
        stringArrayList = new ArrayList<>();

        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
        adapter = new PagerAdapter(getChildFragmentManager());

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        FirebaseDatabase.getInstance().getReference().child(TrendsSeason).orderByChild(UserID).
                equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getChildrenCount() == 0) {
                            progressDialog.hide();
                            mainwrapper.setVisibility(View.GONE);
                            noseasondata.setVisibility(View.VISIBLE);
                            return;
                        }
                        mainwrapper.setVisibility(View.VISIBLE);
                        noseasondata.setVisibility(View.GONE);
                        for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                            SeasonItem seasonItem = childDataSnapshot.getValue(SeasonItem.class);
                            if (!stringArrayList.contains(seasonItem.getName()))
                                adapter.addFrag(Viewpager_Fragment.newInstance(seasonItem.getId(), postid),
                                        seasonItem.getName());
                        }
               /* if (dataSnapshot.getChildrenCount() > 0)
                    viewpager.setOffscreenPageLimit((int) dataSnapshot.getChildrenCount() - 1); //before setAdapter*/
                        viewpager.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        tabLayout.setupWithViewPager(viewpager);
                        // adapter.notifyDataSetChanged();

                        //setTabMode();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });


        return view;
    }


    /*@Override
    public void onResume() {
        super.onResume();
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt,activity);

    }



    public void setTabMode() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        //For some reason, setting minWidth in xml and then accessing it here doesn't work, returns 0
        int minWidth = 100;
        tabLayout.setMinimumWidth(minWidth);

        if (tabLayout.getTabCount() < dpWidth / minWidth) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        }
    }

    /*public void uploadTrandDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_layout);
        TextView selectoption = (TextView) dialog.findViewById(R.id.selectoption);
        selectoption.setTypeface(RobotoFont.boldFont(activity));
        selectoption.setText("Upload Trend Using");
        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setText("OK");
        done_action.setTypeface(RobotoFont.boldFont(context));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(context));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        final RadioButton radiobtn1 = (RadioButton) dialog.findViewById(R.id.radiobtn1);
        radiobtn1.setTypeface(RobotoFont.regularFont(context));
        radiobtn1.setText("Dropbox");
        final RadioButton radiobtn2 = (RadioButton) dialog.findViewById(R.id.radiobtn2);
        radiobtn2.setTypeface(RobotoFont.regularFont(context));
        radiobtn2.setText("Google Drive");
        radiobtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radiobtn1.setChecked(true);
                radiobtn2.setChecked(false);
                radiobtn1.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.textselected)));
                radiobtn2.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.textunselected)));
            }
        });

        radiobtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radiobtn2.setChecked(true);
                radiobtn1.setChecked(false);
                radiobtn1.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.textunselected)));
                radiobtn2.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.textselected)));

            }
        });

        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

        dialog.getWindow().setAttributes(lp);
    }*/

    protected void selectOptionsDialog(final Context activity) {
        final Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_select_options);
        dialog.setCancelable(false);

        TextView select_option = (TextView) dialog.findViewById(R.id.select_option);
        select_option.setTypeface(RobotoFont.regularFont(activity));

        TextView manageseason = (TextView) dialog.findViewById(R.id.manageseason);
        manageseason.setTypeface(RobotoFont.regularFont(activity));

        manageseason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Season_Fragment(), false);
                dialog.dismiss();
            }
        });

        TextView upload_trend = (TextView) dialog.findViewById(R.id.upload_trend);
        upload_trend.setTypeface(RobotoFont.regularFont(activity));

        upload_trend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                uploadTrendOption(activity);
            }
        });

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(activity));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    protected void uploadTrendOption(final Context context) {
        final Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWindow.setAttributes(lp);
        dialog.setContentView(R.layout.dialog_uploadtrend_options);
        dialog.setCancelable(false);
        TextView txt_upload_trends = (TextView) dialog.findViewById(R.id.txt_upload_trends);
        txt_upload_trends.setTypeface(RobotoFont.regularFont(context));
        TextView txt_dropbox = (TextView) dialog.findViewById(R.id.txt_dropbox);
        txt_dropbox.setTypeface(RobotoFont.regularFont(context));

        txt_dropbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, UserActivity.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                dialog.dismiss();
            }
        });


        TextView textpdf = (TextView) dialog.findViewById(R.id.textpdf);
        textpdf.setTypeface(RobotoFont.regularFont(context));

        TextView txt_google_drive = (TextView) dialog.findViewById(R.id.txt_google_drive);
        txt_google_drive.setTypeface(RobotoFont.regularFont(context));
        TextView txt_upload_file = (TextView) dialog.findViewById(R.id.txt_upload_file);
        txt_upload_file.setTypeface(RobotoFont.regularFont(context));
        txt_upload_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, UploadFromFileManager.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                dialog.dismiss();
            }
        });

        txt_google_drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new GoogleDriveListFragment(), false);
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
                dialog.dismiss();
            }
        });

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_cancel.setTypeface(RobotoFont.mediumFont(context));
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_DOC) {

        }
    }
}
