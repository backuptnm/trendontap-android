package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.LoveImagesAdapter;
import tnm.trendontap.adapter.ReactionsTrendAdapter;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.ScaledImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.PostID;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;


public class Reactions_Trends_Fragment extends Fragment {

    TextView labeltxt, name, type, title, category, takelook, notifycnt;
    LinearLayout back_arrow;
    ImageView trendimg;
    RelativeLayout activity2_icon_lay;
    ListView listview_reaction_list;
    ReactionsTrendAdapter reactionsTrendAdapter;
    ArrayList<ReactionTrendItem> reactionTrendItemArrayList;
    Preference preference;
    Users users;
    String from = "", postid = "";
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_reactions_trends, container, false);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText("Reactions");
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (getArguments() != null && getArguments().getString("from") != null)
                from = getArguments().getString("from");
        } catch (Exception e) {

        }

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        View headerview = LayoutInflater.from(activity).inflate(R.layout.reactions_trend_header, null, false);
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        name.setText(getName(users.getFirstName(), users.getLastName()));

        trendimg = (ImageView) headerview.findViewById(R.id.trendimg);

        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("postimg", ""))) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerInside();
            requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());

            Glide.with(activity)
                    .load(getArguments().getString("postimg", ""))
                    .apply(requestOptions)
                    .into(trendimg);
        }

        type = (TextView) headerview.findViewById(R.id.type);
        type.setTypeface(RobotoFont.regularFont(activity));
        type.setText(users.getJobTitle());

        title = (TextView) headerview.findViewById(R.id.title);
        title.setTypeface(RobotoFont.regularFont(activity));
        title.setText(getArguments().getString("title", ""));

        category = (TextView) headerview.findViewById(R.id.category);
        category.setTypeface(RobotoFont.regularFont(activity));
        category.setText(getArguments().getString("category", ""));

        takelook = (TextView) headerview.findViewById(R.id.takelook);
        takelook.setTypeface(RobotoFont.regularFont(activity));

        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        listview_reaction_list = (ListView) view.findViewById(R.id.listview_reaction_list);
        reactionTrendItemArrayList = new ArrayList<>();
        reactionsTrendAdapter = new ReactionsTrendAdapter(activity, reactionTrendItemArrayList,
                getArguments().getString("postimg", ""), from);
        // listview_reaction_list.addHeaderView(headerview);
        listview_reaction_list.setAdapter(reactionsTrendAdapter);
        if (getArguments() != null && getArguments().getString("postid", "") != null)
            postid = getArguments().getString("postid", "");
        getCountReaction(postid);
        //  setReactionList();
        return view;
    }

    /*@Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt, activity);

    }


    public void getCountReaction(final String postid) {

        reactionTrendItemArrayList.clear();

        if (getArguments() == null || TextUtils.isEmpty(postid)) {
            return;
        }

        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        getReactionList(postid);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void getReactionList(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                        if (Utils.shouldShow(dataSnapshot,users.getUserType())) {
                            ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                            reactionTrendItemArrayList.add(reactionTrendItem1);
                            reactionsTrendAdapter.update(reactionTrendItemArrayList);

                            setUserData(reactionTrendItemArrayList,
                                    String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()),
                                    reactionTrendItem1, "add");
                        }


                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (Utils.shouldShow(dataSnapshot,users.getUserType())) {
                            ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                            int pos = matchValue(reactionTrendItemArrayList, reactionTrendItem1.getId());
                            if (pos == -1)
                                return;

                            reactionTrendItemArrayList.set(pos, reactionTrendItem1);
                            reactionsTrendAdapter.notifyDataSetChanged();

                            setUserData(reactionTrendItemArrayList, String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()), reactionTrendItem1, "edit");

                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        ReactionTrendItem reactionTrendItem1 = dataSnapshot.getValue(ReactionTrendItem.class);
                        int pos = matchValue(reactionTrendItemArrayList, reactionTrendItem1.getId());
                        if (pos == -1)
                            return;
                        reactionTrendItemArrayList.remove(pos);
                        reactionsTrendAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void setUserData(final ArrayList<ReactionTrendItem> reactionTrendItemArrayList, String userid, final ReactionTrendItem reactionTrendItem1, String action) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);

                        reactionTrendItem1.setUsername(getName(users.getFirstName(), users.getLastName()));
                        reactionTrendItem1.setUserrole(users.getJobTitle());
                        reactionTrendItem1.setUserphoto(users.getProfilePic());
                        reactionTrendItem1.setUsercompany(users.getCompanyName());

                        int pos = matchValue(reactionTrendItemArrayList, reactionTrendItem1.getId());
                        if (pos == -1)
                            return;
                        reactionTrendItemArrayList.set(pos, reactionTrendItem1);
                        //  reactionTrendItemArrayList.add(reactionTrendItem1);
                        reactionsTrendAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public int matchValue(ArrayList<ReactionTrendItem> reactionTrendItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < reactionTrendItemArrayList.size(); j++) {
            if (reactionTrendItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

/*
    public void setReactionList() {
        reactionTrendItemArrayList.clear();
        if (getArguments() == null || TextUtils.isEmpty(getArguments().getString("postid"))) {
            return;
        }
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(getArguments().getString("postid")).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        reactionTrendItemArrayList.clear();
                        for (final DataSnapshot reactionschild : dataSnapshot.getChildren()) {
                            if ((boolean) reactionschild.child(RecycleBin).getValue())
                                continue;

                            FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(reactionschild.child(UserID).getValue().toString()).
                                    addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Users users = dataSnapshot.getValue(Users.class);
                                            ReactionTrendItem reactionTrendItem1 = reactionschild.getValue(ReactionTrendItem.class);
                                            reactionTrendItem1.setUsername(getName(users.getFirstName(), users.getLastName()));
                                            reactionTrendItem1.setUserrole(users.getJobTitle());
                                            reactionTrendItem1.setUserphoto(users.getAvatarUrl());
                                            reactionTrendItem1.setUsercompany(users.getCompany());
                                            reactionTrendItemArrayList.add(reactionTrendItem1);
                                            reactionsTrendAdapter.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }*/
}
