package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.CommentActivity;
import tnm.trendontap.activity.CostingSheet_Activity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.Reactions_Trends_Love_Activity;
import tnm.trendontap.adapter.ActivityAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.BadgeUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.MainActivity.activitycnt;
import static tnm.trendontap.activity.MainActivity.loadContentFragment;
import static tnm.trendontap.activity.MainActivity.loadinvitation;
import static tnm.trendontap.activity.MainActivity.manager;
import static tnm.trendontap.activity.MainActivity.redirectBuyerSample;
import static tnm.trendontap.activity.MainActivity.redirectComment;
import static tnm.trendontap.activity.MainActivity.redirectCostingsheet;

import static tnm.trendontap.activity.MainActivity.redirectSupplierSample;
import static tnm.trendontap.activity.MainActivity.resetAll;
import static tnm.trendontap.activity.MainActivity.trends;
import static tnm.trendontap.activity.MainActivity.trendstxt;
import static tnm.trendontap.utility.FireStoreUtils.BadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.tActivityisRead;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class ActivityList_Fragment extends Fragment {
    TextView labeltxt;
    ActivityAdapter activityAdapter;
    ArrayList<ActivityItem> activityItemArrayList;
    ListView activitylist;
    Activity activity;
    LinearLayout noactivitydata;
    Preference preference;
    Users users;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activitylist, container, false);
        activity = getActivity();
        noactivitydata = (LinearLayout) view.findViewById(R.id.noactivitydata);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText("Activity");
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        activitylist = (ListView) view.findViewById(R.id.activitylist);
        activityItemArrayList = new ArrayList<>();
        activityAdapter = new ActivityAdapter(activity, activityItemArrayList);

        activitylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ActivityItem activityItem = activityItemArrayList.get(i);

                ArrayList<String> stringList = null;
                try {
                    stringList = activityItem.getIsReadUsers();
                } catch (Exception e) {

                }
                if (stringList == null) {
                    stringList = new ArrayList<>();
                }

                if (!stringList.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    stringList.add(String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getUid()));
                    activityItem.setIsReadUsers(stringList);
                    activityItemArrayList.set(i, activityItem);
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference()
                            .child(FirebasePathActivities).child(activityItem.getId());
                    databaseReference.child(tActivityisRead).setValue(stringList);

                   /* long badgeCount = 0;
                    try {
                        badgeCount = users.getBadgeCount();
                    } catch (Exception e) {

                    }*/
                    //  if (badgeCount > 0) {
                    final DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().
                            getReference().child(FirebasePathUsers).child(FirebaseAuth.getInstance().getCurrentUser().getUid());

                    databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long badgeCount = (long) dataSnapshot.child(BadgeCount).getValue();
                            if (badgeCount == 0)
                                return;
                            databaseReference1.child(BadgeCount).setValue(badgeCount - 1);
                            users.setBadgeCount(badgeCount - 1);
                            Log.i("users", users + "");
                            SharedPreferences.Editor editor = preference.edit();
                            String json = new Gson().toJson(users);
                            editor.putString("userid", users.getUserID());
                            editor.putString("userdetail", json).apply();
                            editor.apply();
                            activitycnt.setText(String.valueOf(badgeCount - 1));
                            BadgeUtils.setBadge(activity.getApplicationContext(), (int) badgeCount - 1);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                    // }

                    activityAdapter.notifyDataSetChanged();
                }

                switch (activityItem.getActivityType()) {
                    case "trend":
                        MainActivity.trendslay.performClick();
                        break;
                    case "love":
                        MainActivity.loveslay.performClick();
                        break;
                    case "sample":
                        redirectSupplierSample(activity, activityItem.getPostID(), activityItem.getShortMessage());
                        break;
                    case "costing sheet":
                        redirectCostingsheet(activity, activityItem.getPostID(), activityItem.getReactionID(), activityItem.getUserID());
                        break;
                    case "Invitation":
                        loadinvitation(activity);
                        break;
                    case "comment":
                        redirectComment(activity, activityItem.getReactionID(),
                                activityItem.getPostID(),
                                activityItem.getUserID(), activityItem.getShortMessage(),
                                activityItem.getPostImageURL());
                        break;
                }
            }
        });
        activitylist.setAdapter(activityAdapter);

        getUserActivityCount();
        // setLoveslist();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt, activity);
    }

    public void getUserActivityCount() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            activitylist.setVisibility(View.GONE);
                            noactivitydata.setVisibility(View.VISIBLE);
                            return;
                        }
                        activitylist.setVisibility(View.VISIBLE);
                        noactivitydata.setVisibility(View.GONE);
                        getUserActivityList();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void getUserActivityList() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getActivityList(dataSnapshot.getKey());
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getActivityList(String key) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
        databaseReference.orderByChild(ID).equalTo(key).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                manageData(dataSnapshot, "add");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                manageData(dataSnapshot, "edit");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                manageData(dataSnapshot, "delete");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void manageData(DataSnapshot activitydata, String action) {
        final ActivityItem activityItem = activitydata.getValue(ActivityItem.class);
        try {
            ArrayList<String> is_read_users = activitydata.child(tActivityisRead).getValue(new GenericTypeIndicator<ArrayList<String>>() {
            });
            activityItem.setIsReadUsers(is_read_users);

        } catch (Exception e) {

        }
        if (action.equalsIgnoreCase("add")) {
            activityItemArrayList.add(activityItem);
            activityAdapter.setList(activityItemArrayList);
        } else if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(activityItemArrayList, activityItem.getId());
            if (pos == -1) {
                return;
            }
            activityItemArrayList.set(pos, activityItem);
            activityAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(activityItemArrayList, activityItem.getId());
            if (pos == -1) {
                return;
            }
            activityItemArrayList.remove(pos);
            activityAdapter.notifyDataSetChanged();
        }

    }

    public int matchValue(ArrayList<ActivityItem> activityItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < activityItemArrayList.size(); j++) {
            if (activityItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    int j = 0;

    public void setLoveslist() {
        j = 0;
        activityItemArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                       /* if (activityItemArrayList.size() >= dataSnapshot.getChildrenCount())
                            activityItemArrayList.clear();*/

                        if (dataSnapshot.getChildrenCount() == 0) {
                            activitylist.setVisibility(View.GONE);
                            noactivitydata.setVisibility(View.VISIBLE);
                            return;
                        }
                        activitylist.setVisibility(View.VISIBLE);
                        noactivitydata.setVisibility(View.GONE);

                        for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                            FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities).
                                    child(childDataSnapshot.getKey()).
                                    addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot activitydata) {
                                            final ActivityItem activityItem = activitydata.getValue(ActivityItem.class);
                                            try {
                                                ArrayList<String> is_read_users = activitydata.child(tActivityisRead).getValue(new GenericTypeIndicator<ArrayList<String>>() {
                                                });
                                                activityItem.setIsReadUsers(is_read_users);

                                            } catch (Exception e) {

                                            }

                                            activity.runOnUiThread(new Runnable() {
                                                public void run() {
                                                    activityItemArrayList.add(activityItem);
                                                }

                                            });
                                            // activityItemArrayList.add(activityItem);
                                            if (j + 1 == dataSnapshot.getChildrenCount()) {
                                                if (activityItemArrayList.size() > 1) {

                                                    Collections.sort(activityItemArrayList, new Comparator<ActivityItem>() {
                                                        @Override
                                                        public int compare(ActivityItem o1, ActivityItem o2) {
                                                            return Long.valueOf(o2.getTimeStamp()).compareTo(o1.getTimeStamp());
                                                        }
                                                    });
                                                }

                                                activity.runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        activityAdapter.notifyDataSetChanged();
                                                    }

                                                });

                                            }
                                            j++;
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        activitylist.setVisibility(View.GONE);
                        noactivitydata.setVisibility(View.VISIBLE);
                    }
                });

    }


}
