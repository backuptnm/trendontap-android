package tnm.trendontap.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.TermConditionActivity;
import tnm.trendontap.utility.RobotoFont;

/**
 * Created by TNM on 1/22/2018.
 */

public class TermCondition_Fragment extends Fragment {

    WebView mWebview;
    ImageView back_arrow;
    ProgressDialog progressDialog;
    TextView labeltxt;
    Activity activity;
    String url = "http://www.trendontap.com/Terms-Of-Use";
    boolean isLoading = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();

        View view = inflater.inflate(R.layout.fragment_termcondition, container, false);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");


        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));

        back_arrow = (ImageView) view.findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });
        mWebview = (WebView) view.findViewById(R.id.webView);
        mWebview.getSettings().setJavaScriptEnabled(true);


        if (!isLoading) {
            isLoading = true;
            new MyAsynTask().execute();

        }


        return view;
    }


    private class MyAsynTask extends AsyncTask<Void, Void, Document> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !progressDialog.isShowing())
                progressDialog.show();
        }

        @Override
        protected Document doInBackground(Void... voids) {

            Document document = null;
            try {
                document = Jsoup.connect(url).get();
                document.getElementsByTag("header").remove();
                document.getElementsByClass("container").attr("style","padding-top: 10px !important");

            } catch (IOException e) {
                e.printStackTrace();
            }
            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            super.onPostExecute(document);
            mWebview.loadDataWithBaseURL(url, document.toString(), "text/html", "utf-8", "");
            mWebview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            isLoading = false;
            mWebview.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, request);
                }
            });
        }
    }
}
