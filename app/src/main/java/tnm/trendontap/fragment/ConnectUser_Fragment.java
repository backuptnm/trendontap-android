package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.ConnectGridItemAdapter;
import tnm.trendontap.adapter.ConnectionUserAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.PhotoConnectStamp;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ExpandableHeightGridView;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendNotification;
import static tnm.trendontap.utility.FireStoreUtils.BuyerConnections;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.BuyerInvitations;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

public class ConnectUser_Fragment extends Fragment {
    public static TextView name, city, useremail, userphoneno, connectbtn, connecttxt;
    public static TextView labeltxt;
    public static ImageView  dotsicon, profilepic;
    LinearLayout back_arrow;
    RelativeLayout activity2_icon_lay;
    public static LinearLayout connect, disconnect;
    TabLayout trends_loves_tab;
    ViewPager trends_loves_pager;
    ExpandableHeightGridView data_grid;
    ConnectionUserAdapter connectionUserAdapter;
    public static boolean isconnected = false;
    private String userid;
    Activity activity;
    Context context;
    private Users current_user;
    Preference preference;
    TextView notifycnt;

    public static ConnectGridItemAdapter connectGridItemAdapter;

    boolean isGridViewVisible = false;
    public static ArrayList<PhotoConnectStamp> photoTimeList;
    public static ArrayList<PhotoConnectStamp> photoTimeListLove;
    public static int currentTabPos = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connectuser, container, false);
        currentTabPos = 0;
        activity = getActivity();
        context = getContext();
        photoTimeList = new ArrayList<>();
        photoTimeListLove = new ArrayList<>();
        data_grid = (ExpandableHeightGridView) view.findViewById(R.id.data_grid);
        preference = new Preference(activity, Login_Pref);
        current_user = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        connectGridItemAdapter = new ConnectGridItemAdapter(context, photoTimeList);

        trends_loves_pager = (ViewPager) view.findViewById(R.id.trends_loves_pager);
        trends_loves_tab = (TabLayout) view.findViewById(R.id.trends_loves_tab);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));


        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));

        profilepic = (ImageView) view.findViewById(R.id.profilepic);
        dotsicon = (ImageView) view.findViewById(R.id.dotsicon);

        if (getArguments() != null)
            userid = getArguments().getString("UserID");



        isGridViewVisible = false;
        trends_loves_pager.setVisibility(View.VISIBLE);
        data_grid.setVisibility(View.GONE);
        dotsicon.setColorFilter(Color.parseColor("#C4C4C4"));

        dotsicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGridViewVisible) {
                    dotsicon.setColorFilter(Color.parseColor("#C4C4C4"));
                    trends_loves_pager.setVisibility(View.VISIBLE);
                    data_grid.setVisibility(View.GONE);
                    isGridViewVisible = false;
                } else {
                    dotsicon.setColorFilter(ContextCompat.getColor(activity, R.color.colorPrimary));
                    trends_loves_pager.setVisibility(View.GONE);
                    data_grid.setVisibility(View.VISIBLE);
                    isGridViewVisible = true;
                    if (trends_loves_pager.getCurrentItem() == 0){
                        connectGridItemAdapter = new ConnectGridItemAdapter(context, photoTimeList);
                        connectGridItemAdapter.notifyDataSetChanged();
                    }else {
                        connectGridItemAdapter = new ConnectGridItemAdapter(context, photoTimeListLove);
                        connectGridItemAdapter.notifyDataSetChanged();
                    }
                    data_grid.setAdapter(connectGridItemAdapter);
                }

            }
        });

        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        name = (TextView) view.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        city = (TextView) view.findViewById(R.id.city);
        city.setTypeface(RobotoFont.regularFont(activity));
        useremail = (TextView) view.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));
        userphoneno = (TextView) view.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));
        connectbtn = (TextView) view.findViewById(R.id.connectbtn);
        connectbtn.setTypeface(RobotoFont.regularFont(activity));
        connecttxt = (TextView) view.findViewById(R.id.connecttxt);
        connecttxt.setTypeface(RobotoFont.regularFont(activity));
        connect = (LinearLayout) view.findViewById(R.id.connect);
        disconnect = (LinearLayout) view.findViewById(R.id.disconnect);

        connectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectbtn.getText().toString().equalsIgnoreCase("Connect")) {
                    // connect();

                    FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).setValue(true);
                    connectbtn.setText("Requested");

                   /* FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
                    connectbtn.setText("Requested");*/

                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(current_user.getProfilePic());
                    activityItem.setLongMessage("started");
                    activityItem.setMessage(current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG);
                    activityItem.setPostID("");
                    activityItem.setPostImageURL("");
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("following you");
                    activityItem.setTiggerBy(current_user.getFirstName() + " " + current_user.getLastName());
                    activityItem.setActivityType(BuyerFollower);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(current_user.getUserID());


                    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathActivities);
                    final String key = databaseReference.push().getKey();
                    activityItem.setId(key);
                    databaseReference.child(key).setValue(activityItem);

                    FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                            child(userid).
                            child(key).setValue(true);

                    sendNotification(userid, current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG,activityItem);

                } else if (connectbtn.getText().toString().equalsIgnoreCase("Requested")) {

                } else {
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).removeValue();
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
                    disconnect();
                }
            }
        });


        /*******************************************************************/
        DatabaseReference rootInvited = FirebaseDatabase.getInstance().getReference();
        Query query_invited = rootInvited.child(BuyerInvitations).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query_invited.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connectbtn.setText("Requested");
                    connectbtn.setTextColor(Color.parseColor("#000000"));
                    connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
                } else {
                    connectbtn.setText("Connect");

                    checkIfConneced();
                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /******************************************************************/

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers)
                .child(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        labeltxt.setText(getName(users.getFirstName(), users.getLastName()));
                        city.setText(users.getCity());
                        useremail.setText(users.getEmailID());
                        userphoneno.setText(users.getPhone());
                        name.setText(users.getJobTitle());

                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            Picasso.with(activity)
                                    .load(users.getProfilePic())
                                    .placeholder(R.mipmap.userplaceholder)
                                    .error(R.mipmap.userplaceholder)
                                    .into(profilepic);
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        createViewPager();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        current_user = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (current_user.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(current_user.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }


    private void checkIfConneced() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query query = rootRef.child(BuyerConnections).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connect();
                } else {
                    disconnect();

                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void createViewPager() {
        trends_loves_tab.addTab(trends_loves_tab.newTab().setText("Trends"));
        trends_loves_tab.addTab(trends_loves_tab.newTab().setText("Loves"));
        trends_loves_tab.setTabGravity(TabLayout.GRAVITY_FILL);

        trends_loves_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(trends_loves_tab));
        trends_loves_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                trends_loves_pager.setCurrentItem(tab.getPosition());
                /*if (tab.getPosition() == 0){
                    connectGridItemAdapter = new ConnectGridItemAdapter(context, photoTimeList);
                    connectGridItemAdapter.notifyDataSetChanged();
                }else {
                    connectGridItemAdapter = new ConnectGridItemAdapter(context, photoTimeListLove);
                    connectGridItemAdapter.notifyDataSetChanged();
                }
                data_grid.setAdapter(connectGridItemAdapter);*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        connectionUserAdapter = new ConnectionUserAdapter(getChildFragmentManager(), trends_loves_tab.getTabCount(), userid);
        trends_loves_pager.setAdapter(connectionUserAdapter);
        trends_loves_pager.setCurrentItem(0);

        trends_loves_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                currentTabPos = i;
                connectionUserAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }



    public void connect() {
        isconnected = true;
        connectbtn.setText("Disconnect");
        connect.setVisibility(View.VISIBLE);
        connectbtn.setTextColor(Color.parseColor("#000000"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
        disconnect.setVisibility(View.GONE);
    }

    public void disconnect() {
        isconnected = false;
        connectbtn.setText("Connect");
        connect.setVisibility(View.GONE);
        connectbtn.setTextColor(Color.parseColor("#FFFFFF"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_blue_bg));
        disconnect.setVisibility(View.VISIBLE);
    }
}
