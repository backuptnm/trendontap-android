package tnm.trendontap.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.PDFViewActivity;
import tnm.trendontap.adapter.BuyerTrendAdapter;
import tnm.trendontap.adapter.BuyerTrend_LoveAdapter;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.PdfURL;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 12/19/2017.
 */

public class Buyer_Trend_Pager_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    // private static final String ARG_PARAM2 = "param2";
    ArrayList<BuyerTrendsItem> buyerTrendLoveItemArrayList;
    ArrayList<String> ids;
    // BuyerTrend_LoveAdapter buyerTrendLoveAdapter;
    BuyerTrendAdapter buyerTrendAdapter;
    ListView buyer_trend_love_list;
    TextView nodata;
    LayoutInflater layoutInflater;
    Users users;
    CircleImageView profilepic;
    TextView name, useremail, userphoneno, jobtitle;
    Activity activity;
    View headerview;
    int selectedPos = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            // mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static Buyer_Trend_Pager_Fragment newInstance(String param1) {//, ArrayList<BuyerTrendLoveItem> buyerTrendLoveItemlist) {
        Buyer_Trend_Pager_Fragment fragment = new Buyer_Trend_Pager_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        // buyerTrendLoveItemArrayList = buyerTrendLoveItemlist;
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyer_trend_love_pager, container, false);
        activity = getActivity();
        ids = new ArrayList<>();
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        users = new Gson().fromJson(mParam1, Users.class);
        headerview = layoutInflater.inflate(R.layout.header_profile, null, false);
        profilepic = (CircleImageView) headerview.findViewById(R.id.profilepic);
        jobtitle = (TextView) headerview.findViewById(R.id.jobtitle);
        jobtitle.setTypeface(RobotoFont.regularFont(activity));
        jobtitle.setText(users.getJobTitle());
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        name.setText(users.getCompanyName());
        useremail = (TextView) headerview.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));
        useremail.setText(users.getEmailID());
        userphoneno = (TextView) headerview.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));
        if (!String.valueOf(users.getPhone()).equalsIgnoreCase("null"))
            userphoneno.setText(String.valueOf(users.getPhone()));

        buyer_trend_love_list = (ListView) view.findViewById(R.id.buyer_trend_love_list);
        buyer_trend_love_list.addHeaderView(headerview);
        nodata = (TextView) view.findViewById(R.id.nodata);
        nodata.setTypeface(RobotoFont.mediumFont(activity));

        nodata.setText("There are currently no buyer trends in your network");
        buyerTrendLoveItemArrayList = new ArrayList<>();
        //  buyerTrendLoveAdapter = new BuyerTrend_LoveAdapter(activity, buyerTrendLoveItemArrayList, "trends");
        buyerTrendAdapter = new BuyerTrendAdapter(activity, buyerTrendLoveItemArrayList);
        buyer_trend_love_list.setAdapter(buyerTrendAdapter);

        buyerTrendAdapter.setIsBuyerList(true);

        buyer_trend_love_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (buyerTrendLoveItemArrayList == null && buyerTrendLoveItemArrayList.size() == 0)
                    return;

                selectedPos = position - 1;

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 13);
                } else {
                    String pdfUrl = buyerTrendLoveItemArrayList.get(selectedPos).getPdfUrl();
                    downloadPDF(pdfUrl);
                }
            }
        });

        if (!TextUtils.isEmpty(users.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(activity)
                    .load(users.getProfilePic())
                    .apply(requestOptions)
                    .into(profilepic);
        }
        getBuyerTrendCount();
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 13) {
            int access_write = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_write != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "Sorry!!!, you can't open File without granting permission", Toast.LENGTH_LONG).show();
            } else {
                String pdfUrl = buyerTrendLoveItemArrayList.get(selectedPos).getPdfUrl();
                downloadPDF(pdfUrl);
            }
        }

    }

    public void downloadPDF(String url) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];
        viewPDF(name);
    }

    public boolean viewPDF(String name) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf

        if (!pdfFile.exists()) {
            return false;
        }
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(activity, PDFViewActivity.class);
        pdfIntent.putExtra("pdfUrl", path.toString());
        try {
            startActivity(pdfIntent);
        } catch (Exception e) {
        }
        return true;
    }


   /* @Override
    public void onResume() {
        super.onResume();
        setList();
    }*/

    public void getBuyerTrendCount() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(users.getUserID()).child(Trends)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            nodata.setVisibility(View.VISIBLE);
                         //   buyer_trend_love_list.setVisibility(View.GONE);
                            return;
                        }
                        nodata.setVisibility(View.GONE);
                       // buyer_trend_love_list.setVisibility(View.VISIBLE);
                        getPostData();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getPostData() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(users.getUserID()).child(Trends).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getData(dataSnapshot);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getData(DataSnapshot dataSnapshot) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(dataSnapshot.getKey()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void manageData(DataSnapshot trends, final String action) {
        boolean isshown = true;
        if (trends.hasChild(HideSuppliers)) {
            if (trends.child(HideSuppliers).hasChild(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                isshown = false;
            }
        }

        if (isshown) {
            String userId = "";

            if (!TextUtils.isEmpty(String.valueOf(trends.child(FireStoreUtils.UserID).getValue()))) {
                userId = String.valueOf(trends.child(FireStoreUtils.UserID).getValue());
            }

            boolean isreactions = false;
            ArrayList<String> suppliers = new ArrayList<>();

            if (trends.hasChild(ReactedSuppiers) && trends.child(ReactedSuppiers).getValue() != null) {
                suppliers = (ArrayList<String>) trends.child(ReactedSuppiers).getValue();
                if (suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    isreactions = true;
                }
            }
            LocationItem location = null;
            if (trends.hasChild(Location)) {
                location = new LocationItem(trends.child(Location).child(Title).getValue().toString(),
                        trends.child(Location).child(Address).getValue().toString(),
                        (double) trends.child(Location).child(Latitude).getValue(),
                        (double) trends.child(Location).child(Logitude).getValue());
            }

            BuyerTrendsItem buyerTrendsItem = new BuyerTrendsItem(
                    trends.child(tPostPhotoId).child("0").getValue().toString(),
                    trends.child(PostTitle).getValue().toString(),
                    trends.child(CatagoryName).getValue().toString(),
                    trends.child(Content).getValue().toString(),
                    trends.child(DeliveryDate).getValue().toString(),
                    trends.child(TimeStamp).getValue().toString(),
                    "", "",
                    isreactions,
                    trends.child(ID).getValue().toString(),
                    "", "", (long) trends.child(LikesCount).getValue(),
                    trends.child(PdfURL).getValue().toString(), "",
                    location);


            if (action.equalsIgnoreCase("add")) {
                buyerTrendLoveItemArrayList.add(buyerTrendsItem);
                buyerTrendAdapter.addList(buyerTrendLoveItemArrayList);
                nodata.setVisibility(View.GONE);
             //   buyer_trend_love_list.setVisibility(View.VISIBLE);
            } else if (action.equalsIgnoreCase("edit")) {
                int pos = matchValue(buyerTrendLoveItemArrayList, buyerTrendsItem.getPostid());
                if (pos == -1)
                    return;
                buyerTrendLoveItemArrayList.set(pos, buyerTrendsItem);
                buyerTrendAdapter.notifyDataSetChanged();
                nodata.setVisibility(View.GONE);
            } else if (action.equalsIgnoreCase("delete")) {
                int pos = matchValue(buyerTrendLoveItemArrayList, buyerTrendsItem.getPostid());
                if (pos == -1)
                    return;
                buyerTrendLoveItemArrayList.remove(pos);
                buyerTrendAdapter.notifyDataSetChanged();
                if (buyerTrendLoveItemArrayList.size() == 0) {
                    nodata.setVisibility(View.VISIBLE);
                   // buyer_trend_love_list.setVisibility(View.GONE);
                }
            }

            if (!action.equalsIgnoreCase("delete"))
                getUserDetails(userId, buyerTrendsItem);
        } else {
            if (buyerTrendLoveItemArrayList.size() == 0) {
                nodata.setVisibility(View.VISIBLE);
               // buyer_trend_love_list.setVisibility(View.GONE);
            } else {
                nodata.setVisibility(View.GONE);
              //  buyer_trend_love_list.setVisibility(View.VISIBLE);
            }
        }

    }


    public int matchValue(ArrayList<BuyerTrendsItem> buyerTrendLoveItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < buyerTrendLoveItemArrayList.size(); j++) {
            if (buyerTrendLoveItemArrayList.get(j).getPostid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    public void getUserDetails(String userid, final BuyerTrendsItem buyerTrendsItem) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        buyerTrendsItem.setUsername(getName(users.getFirstName(), users.getLastName()));
                        buyerTrendsItem.setType(users.getJobTitle());
                        buyerTrendsItem.setUsercompany(users.getCompanyName());
                        buyerTrendsItem.setUserimg(users.getProfilePic());
                        buyerTrendsItem.setUserpostid(users.getUserID());

                        int pos = matchValue(buyerTrendLoveItemArrayList, buyerTrendsItem.getPostid());

                        if (pos == -1)
                            return;
                        buyerTrendLoveItemArrayList.set(pos, buyerTrendsItem);
                        buyerTrendAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


}
