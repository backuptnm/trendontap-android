package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.adapter.ReactionpageAdapter;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.activity.MainActivity.redirectBuyerSample;
import static tnm.trendontap.activity.MainActivity.redirectSupplierSample;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.PostByUserID;
import static tnm.trendontap.utility.FireStoreUtils.PostID;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Reaction_Page_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    GridView reactiongrid;
    ArrayList<String> reactionItemArray;
    ArrayList<ReactionTrendItem> reactionItempostArray;
    ReactionpageAdapter reactionAdapter;
    TextView noreactiontxt;
    Activity activity;
    Context context;
    Preference preference;
    Users users;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reactions, container, false);

        activity = getActivity();
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        reactiongrid = (GridView) view.findViewById(R.id.reactiongrid);

        noreactiontxt = (TextView) view.findViewById(R.id.noreactiontxt);
        reactionItemArray = new ArrayList<>();
        reactionItempostArray = new ArrayList<>();
        context = getActivity();
        reactionAdapter = new ReactionpageAdapter(context, reactionItemArray);
        reactiongrid.setAdapter(reactionAdapter);

        reactiongrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (users.getUserType().equalsIgnoreCase(Utils.Supplier)) {
                    redirectSupplierSample(activity, reactionItempostArray.get(i).getPostID(), "");
                } else {
                    redirectBuyerSample(activity, reactionItempostArray.get(i).getPostID(), "");
                }
            }
        });

        if (mParam2.equalsIgnoreCase(Utils.Supplier))
            getReactionCount(FireStoreUtils.UserID);
        else
            getReactionCount(PostByUserID);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static Reaction_Page_Fragment newInstance(String param1, String param2) {
        Reaction_Page_Fragment fragment = new Reaction_Page_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public int matchValue(ArrayList<ReactionTrendItem> reactionTrendItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < reactionTrendItemArrayList.size(); j++) {
            if (reactionTrendItemArrayList.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    int cnt = 0;

    public void getReactionCount(final String UserIDkey) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).orderByChild(UserIDkey).equalTo(mParam1).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        cnt = (int) dataSnapshot.getChildrenCount();
                        if (cnt == 0) {
                            noreactiontxt.setVisibility(View.VISIBLE);
                            reactiongrid.setVisibility(View.GONE);
                            return;
                        }

                        noreactiontxt.setVisibility(View.GONE);
                        reactiongrid.setVisibility(View.VISIBLE);
                        reactionItempostArray.clear();
                        reactionItemArray.clear();
                        setReactionCount(UserIDkey);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void setReactionCount(String UserIDkey) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).orderByChild(UserIDkey).equalTo(mParam1)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        ReactionTrendItem reactionTrendItem = dataSnapshot.getValue(ReactionTrendItem.class);

                        if (Utils.shouldShow(dataSnapshot, users.getUserType())) {
                            reactionItemArray.add(String.valueOf(dataSnapshot.child(tPostPhotoId).child("0").getValue()));
                            reactionItempostArray.add(reactionTrendItem);
                            reactionAdapter.notifyDataSetChanged();
                            noreactiontxt.setVisibility(View.GONE);
                            reactiongrid.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        ReactionTrendItem reactionTrendItem = dataSnapshot.getValue(ReactionTrendItem.class);
                        if (Utils.shouldShow(dataSnapshot, users.getUserType())) {
                            int pos = matchValue(reactionItempostArray, reactionTrendItem.getPostID());
                            if (pos == -1)
                                return;
                            reactionItemArray.set(pos, reactionTrendItem.getPhotoUrls().get(0));
                            reactionItempostArray.set(pos, reactionTrendItem);
                            reactionAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        ReactionTrendItem reactionTrendItem = dataSnapshot.getValue(ReactionTrendItem.class);
                        int pos = matchValue(reactionItempostArray, reactionTrendItem.getPostID());
                        if (pos == -1)
                            return;
                        reactionItemArray.remove(pos);
                        reactionItempostArray.remove(pos);
                        reactionAdapter.notifyDataSetChanged();
                        if (reactionItempostArray.size() == 0) {
                            noreactiontxt.setVisibility(View.VISIBLE);
                            reactiongrid.setVisibility(View.GONE);
                        } else {
                            noreactiontxt.setVisibility(View.GONE);
                            reactiongrid.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (reactionItempostArray.size() == 0) {
                            noreactiontxt.setVisibility(View.VISIBLE);
                            reactiongrid.setVisibility(View.GONE);

                        } else {
                            noreactiontxt.setVisibility(View.GONE);
                            reactiongrid.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }


}
