package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.SupplierAdapter;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Buyers;
import static tnm.trendontap.utility.FireStoreUtils.BuyersOfSupplier;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.Suppliers;
import static tnm.trendontap.utility.FireStoreUtils.SuppliersOfBuyer;


public class Buyers_Fragment extends Fragment {
    TextView labeltxt, nobuyer;
    ImageView imgClear;
    EditText etSearch;
    ListView suplierlist;
    private ImageView invite;

    SupplierAdapter supplierAdapter;
    ArrayList<Users> buyerItemArrayList;
    Context context;
    public LinearLayout nobuyerdata;

    DatabaseReference databaseReference;
    ProgressDialog progressDialog;
    int index = 0;
    Activity activity;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyers, container, false);
        activity = getActivity();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");

        nobuyerdata = (LinearLayout) view.findViewById(R.id.nobuyerdata);
        nobuyer = (TextView) view.findViewById(R.id.nobuyer);
        nobuyer.setTypeface(RobotoFont.mediumFont(activity));
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText("Buyers");

        invite = (ImageView) view.findViewById(R.id.invite);
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.loadContentFragment(new Invitation_Buyers_Fragment(), false);
            }
        });

        try {
            if (getArguments() != null && getArguments().getString("invitation", "").equalsIgnoreCase("yes")) {
                Bundle bundle = getArguments();
                bundle.clear();
                MainActivity.loadContentFragment(new Invitation_Buyers_Fragment(), false, bundle);
            }
        } catch (Exception e) {

        }

        imgClear = (ImageView) view.findViewById(R.id.imgClear);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(activity));
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.INVISIBLE);

                supplierAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                // Some phones disregard the IME setting option in the xml, instead
                // they send IME_ACTION_UNSPECIFIED so we need to catch that
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;
                }

                return handled;
            }
        });

        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });


        context = getActivity();
        buyerItemArrayList = new ArrayList<>();
        suplierlist = (ListView) view.findViewById(R.id.suplierlist);
        suplierlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                String json = new Gson().toJson(buyerItemArrayList.get(i));
                bundle.putString("userdetail", json);
                bundle.putString("from", "supplierlist");
                MainActivity.loadContentFragment(new Buyers_Trend_Love_Fragment(), false, bundle);
            }
        });
        supplierAdapter = new SupplierAdapter(context, buyerItemArrayList, nobuyerdata);
        suplierlist.setAdapter(supplierAdapter);

        // supplierList();
        getSuppliersCount();
        return view;

    }



    public void getSuppliersCount() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(BuyersOfSupplier)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Buyers);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    suplierlist.setVisibility(View.GONE);
                    nobuyerdata.setVisibility(View.VISIBLE);
                    return;
                }

                getBuyerList();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getBuyerList() {

        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(BuyersOfSupplier)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Buyers);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                suplierlist.setVisibility(View.VISIBLE);
                nobuyerdata.setVisibility(View.GONE);
                manageUser(dataSnapshot, "add");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                manageUser(dataSnapshot, "edit");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                manageUser(dataSnapshot, "delete");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt,activity);
    }

    public int matchValue(ArrayList<Users> supplierItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < supplierItemArrayList.size(); j++) {
            if (supplierItemArrayList.get(j).getUserID().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    public void manageUser(DataSnapshot dataSnapshot, final String action) {
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers);
        databaseReference1.child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.getValue(Users.class);

                if (action.equalsIgnoreCase("add")) {
                    int pos = matchValue(buyerItemArrayList, users.getUserID());
                    if (pos != -1)
                        return;
                    buyerItemArrayList.add(users);
                    supplierAdapter.addList(buyerItemArrayList);
                } else if (action.equalsIgnoreCase("edit")) {
                    int pos = matchValue(buyerItemArrayList, users.getUserID());
                    if (pos == -1)
                        return;
                    buyerItemArrayList.set(pos, users);
                    supplierAdapter.notifyDataSetChanged();
                } else if (action.equalsIgnoreCase("delete")) {
                    int pos = matchValue(buyerItemArrayList, users.getUserID());
                    if (pos == -1)
                        return;
                    buyerItemArrayList.remove(pos);
                    supplierAdapter.notifyDataSetChanged();

                }
                if (buyerItemArrayList.size() == 0) {
                    suplierlist.setVisibility(View.GONE);
                    nobuyerdata.setVisibility(View.VISIBLE);
                } else {
                    suplierlist.setVisibility(View.VISIBLE);
                    nobuyerdata.setVisibility(View.GONE);
                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                try {
                    if (buyerItemArrayList.size() == 0) {
                        suplierlist.setVisibility(View.GONE);
                        nobuyerdata.setVisibility(View.VISIBLE);
                    } else {
                        suplierlist.setVisibility(View.VISIBLE);
                        nobuyerdata.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }


    public void supplierList() {
        progressDialog.show();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(BuyersOfSupplier).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Buyers);
        index = 0;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                index = 0;
                buyerItemArrayList.clear();
                nobuyerdata.setVisibility(View.GONE);
                final int childrenCount = (int) dataSnapshot.getChildrenCount();
                if (childrenCount == 0) {
                    suplierlist.setVisibility(View.GONE);
                    nobuyerdata.setVisibility(View.VISIBLE);
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    return;
                }
                for (final DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                            child(childDataSnapshot.getKey());
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Users users = dataSnapshot.getValue(Users.class);
                            buyerItemArrayList.add(users);

                            if (index + 1 == childrenCount) {
                                Collections.sort(buyerItemArrayList, new Comparator<Users>() {
                                    public int compare(Users o1, Users o2) {
                                        return (o1.getFirstName() + " " + o1.getLastName()).toLowerCase().compareTo((o2.getFirstName() + " " + o2.getLastName()).toLowerCase());
                                    }
                                });
                                supplierAdapter.addList(buyerItemArrayList);
                                if (progressDialog != null)
                                    progressDialog.dismiss();
                            } else
                                index++;

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (progressDialog != null)
                                progressDialog.dismiss();
                        }
                    });

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.hide();
            }
        });

    }
}
