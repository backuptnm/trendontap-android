package tnm.trendontap.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.BuyerTrendAdapter;
import tnm.trendontap.adapter.LoveImagesAdapter;
import tnm.trendontap.adapter.MyReactionsAdapter;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.MyReaction;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.ClickableViewPager;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.SquareImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.fragment.CameraFragment.camera_Pref;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetID;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetRequest;
import static tnm.trendontap.utility.FireStoreUtils.CostsheetSent;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.PostByUserID;
import static tnm.trendontap.utility.FireStoreUtils.PostID;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.SampleRequest;
import static tnm.trendontap.utility.FireStoreUtils.SampleSent;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.FireStoreUtils.tReactionCommentCount;
import static tnm.trendontap.utility.FireStoreUtils.tReactionUnreadCommentSupplier;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyReactionFragment extends Fragment {

    CircleImageView userimg;
    TextView labeltxt, username, type, name, category, reacttxt, tvReaction, notifycnt, title;
    LinearLayout back_arrow;
    ImageView trendimg;
    LinearLayout reactbtn;
    ListView myReactionList;
    Users users;
    Context context;
    Activity mActivity;
    ArrayList<MyReaction> myReactionItemArrayList;
    MyReactionsAdapter myRectionAdapter;
    String from = "", postid = "";
    Preference preference;
    RelativeLayout categorybg;

    ClickableViewPager loveimgpager;
    CirclePageIndicator indicator;

    @SuppressLint("CheckResult")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getContext();
        mActivity = getActivity();
        View view = inflater.inflate(R.layout.fragment_my_reaction, container, false);
        preference = new Preference(mActivity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(mActivity));
        notifycnt.setVisibility(View.GONE);
        if (getArguments() != null && getArguments().getString("from") != null) {
            from = getArguments().getString("from");
        }

        View headerview = LayoutInflater.from(mActivity).inflate(R.layout.reactions_love_header, null, false);
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(mActivity));
        title = (TextView) headerview.findViewById(R.id.title);
        title.setTypeface(RobotoFont.regularFont(mActivity));
        categorybg = (RelativeLayout) headerview.findViewById(R.id.categorybg);

        if (from.equalsIgnoreCase("love"))
            categorybg.setVisibility(View.GONE);
        else
            categorybg.setVisibility(View.VISIBLE);
        category = (TextView) headerview.findViewById(R.id.category);
        category.setTypeface(RobotoFont.regularFont(mActivity));

        loveimgpager = (ClickableViewPager) headerview.findViewById(R.id.loveimgpager);
        indicator = (CirclePageIndicator) headerview.findViewById(R.id.indicator);
        userimg = (CircleImageView) headerview.findViewById(R.id.userimg);

        type = (TextView) headerview.findViewById(R.id.type);
        type.setTypeface(RobotoFont.regularFont(mActivity));

        reacttxt = headerview.findViewById(R.id.reacttxt);
        reacttxt.setTypeface(RobotoFont.regularFont(mActivity));

        TextView takelook = (TextView) headerview.findViewById(R.id.takelook);
        takelook.setTypeface(RobotoFont.regularFont(mActivity));

        reactbtn = (LinearLayout) headerview.findViewById(R.id.reactbtn);
        reactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences mPreferences = mActivity.getSharedPreferences(camera_Pref, 0);
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", "");
                editor.apply();

                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", false);
                bundle.putString("from", from);
                bundle.putString("postimg", getArguments().getString("imgurl", ""));
                bundle.putString("postid", getArguments().getString("postid"));
                bundle.putString("postuserid", getArguments().getString("postuserid"));
                MainActivity.loadContentFragment(new GalleryFragment(), false, bundle);
            }
        });

        myReactionList = (ListView) view.findViewById(R.id.my_reaction_list);
        myReactionList.addHeaderView(headerview);
        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(mActivity));
        labeltxt.setText("My Reactions");

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(mActivity, "");
            }
        });

        myReactionItemArrayList = new ArrayList<>();
        myRectionAdapter = new MyReactionsAdapter(mActivity, myReactionItemArrayList, getArguments().getString("imgurl"), from);
        myReactionList.setAdapter(myRectionAdapter);

        if (getArguments() != null && getArguments().getString("postid", "") != null)
            postid = getArguments().getString("postid", "");

        setPostDetails();
        getCountReaction(postid);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt, mActivity);
        // MainActivity.getBadgeCount(notifycnt,mActivity);

    }

   /* @Override
    public void onResume() {
        super.onResume();
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    public void setPostDetails() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(postid);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                category.setText(String.valueOf(dataSnapshot.child(CatagoryName).getValue()));
                title.setText(String.valueOf(dataSnapshot.child(PostTitle).getValue()));
                getPostUser(String.valueOf(dataSnapshot.child(FireStoreUtils.UserID).getValue()));
                ArrayList<String> photoUrlsList = (ArrayList<String>) dataSnapshot.child(tPostPhotoId).getValue();
                LoveImagesAdapter myPagerAdapter = new LoveImagesAdapter(mActivity, photoUrlsList);
                loveimgpager.setAdapter(myPagerAdapter);
                indicator.setViewPager(loveimgpager);
                if (photoUrlsList.size() > 1) {
                    indicator.setVisibility(View.VISIBLE);
                } else {
                    indicator.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getPostUser(String userid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userlist) {
                        Users users = userlist.getValue(Users.class);
                        name.setText(getName(users.getFirstName(), users.getLastName()));
                        type.setText(users.getJobTitle());
                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.mipmap.userplaceholder);
                            requestOptions.error(R.mipmap.userplaceholder);

                            Glide.with(mActivity)
                                    .load(users.getProfilePic())
                                    .apply(requestOptions)
                                    .into(userimg);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getCountReaction(final String postid) {

        myReactionItemArrayList.clear();

        if (getArguments() == null || TextUtils.isEmpty(postid)) {
            return;
        }

        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        myReactionItemArrayList.clear();
                        getReactionList(postid);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void getReactionList(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).
                orderByChild(PostID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if (!dataSnapshot.child(FireStoreUtils.UserID).getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            return;
                        }

                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }


    public void manageData(DataSnapshot postidlist, String action) {
        final MyReaction myReaction = new MyReaction();
        myReaction.setReactionid(postidlist.child(ID).getValue().toString());
        myReaction.setTimeStamp((long) postidlist.child(TimeStamp).getValue());
        myReaction.setCommentcount((long) postidlist.child(tReactionCommentCount).getValue());
        myReaction.setUnread_comment((boolean) postidlist.child(tReactionUnreadCommentSupplier).getValue());
        myReaction.setAbouttrend(postidlist.child(Content).getValue().toString());
        myReaction.setImgurl(postidlist.child(tPostPhotoId).child("0").getValue().toString());
        myReaction.setSample_request((boolean) postidlist.child(SampleRequest).getValue());
        myReaction.setSample_sent((boolean) postidlist.child(SampleSent).getValue());
        myReaction.setBuyer_user_id((String) postidlist.child(PostByUserID).getValue());
        myReaction.setCost_request((boolean) postidlist.child(CostsheetRequest).getValue());
        myReaction.setCost_sent((boolean) postidlist.child(CostsheetSent).getValue());
        myReaction.setPost_id(postidlist.child(PostID).getValue().toString());

        ArrayList<String> yourStringArray = new ArrayList<>();
        int size = (int) postidlist.child(tPostPhotoId).getChildrenCount();
        for (int i = 0; i < size; i++) {
            String url = postidlist.child(tPostPhotoId).child(i + "").getValue().toString();
            yourStringArray.add(url);
        }
        myReaction.setPhotoUrls(yourStringArray);

        if (postidlist.hasChild(CostsheetID))
            myReaction.setCostsheet_id(String.valueOf(postidlist.child(CostsheetID).getValue()));
        else
            myReaction.setCostsheet_id("");

        if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
            if (pos == -1)
                return;
            myReactionItemArrayList.set(pos, myReaction);
            myRectionAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
            if (pos == -1)
                return;
            myReactionItemArrayList.remove(pos);
            myRectionAdapter.notifyDataSetChanged();
        } else {
            myReactionItemArrayList.add(myReaction);
            myRectionAdapter.update(myReactionItemArrayList);
        }
        if (!action.equalsIgnoreCase("delete"))
            setUserData(String.valueOf(postidlist.child(FireStoreUtils.UserID).getValue()), myReaction);
    }

    public void setUserData(String userid, final MyReaction myReaction) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userlist) {
                        Users users = userlist.getValue(Users.class);
                        myReaction.setName(getName(users.getFirstName(), users.getLastName()));
                        myReaction.setType(users.getCompanyName());
                        myReaction.setAvtarurl(users.getProfilePic());
                        int pos = matchValue(myReactionItemArrayList, myReaction.getReactionid());
                        myReactionItemArrayList.set(pos, myReaction);
                        myRectionAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public int matchValue(ArrayList<MyReaction> myReactionItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < myReactionItemArrayList.size(); j++) {
            if (myReactionItemArrayList.get(j).getReactionid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


   /* public void setRectionlist() {
        myReactionItemArrayList.clear();

        FirebaseDatabase.getInstance().getReference().child("reaction")
                .orderByChild("post_id").equalTo(getArguments().getString("postid"))
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {

                        myReactionItemArrayList.clear();
                        if (dataSnapshot.getChildrenCount() == 0) {
                            return;
                        }
                        cnt = 0;
                        for (final DataSnapshot postidlist : dataSnapshot.getChildren()) {
                            cnt++;
                            if (!postidlist.child("userId").getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                continue;
                            }

                            final MyReaction myReaction = new MyReaction();
                            myReaction.setReactionid(postidlist.child("id").getValue().toString());
                            myReaction.setTimeStamp((long) postidlist.child("timeStamp").getValue());
                            myReaction.setCommentcount((long) postidlist.child("comment_count").getValue());
                            myReaction.setUnread_comment((boolean) postidlist.child("unread_comment_supplier").getValue());
                            myReaction.setAbouttrend(postidlist.child("content").getValue().toString());
                            myReaction.setImgurl(postidlist.child("photoUrls").child("0").getValue().toString());
                            myReaction.setSample_request((boolean) postidlist.child("sample_request").getValue());
                            myReaction.setSample_sent((boolean) postidlist.child("sample_sent").getValue());
                            myReaction.setBuyer_user_id((String) postidlist.child("post_by_userId").getValue());
                            myReaction.setCost_request((boolean) postidlist.child("costsheet_request").getValue());
                            myReaction.setCost_sent((boolean) postidlist.child("costsheet_sent").getValue());
                            myReaction.setPost_id(getArguments().getString("postid"));

                            ArrayList<String> yourStringArray = new ArrayList<>();
                            int size = (int) postidlist.child("photoUrls").getChildrenCount();
                            for (int i = 0; i < size; i++) {
                                String url = postidlist.child("photoUrls").child(i + "").getValue().toString();
                                yourStringArray.add(url);
                            }
                            myReaction.setPhotoUrls(yourStringArray);

                            if (postidlist.hasChild("costsheet_id"))
                                myReaction.setCostsheet_id(String.valueOf(postidlist.child("costsheet_id").getValue()));
                            else
                                myReaction.setCostsheet_id("");

                            FirebaseDatabase.getInstance().getReference().child("users").child(postidlist.child("userId").getValue().toString())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot userlist) {
                                            Users users = userlist.getValue(Users.class);
                                            myReaction.setName(getName(users.getFirstName(), users.getLastName()));
                                            myReaction.setType(users.getCompany());
                                            myReaction.setAvtarurl(users.getAvatarUrl());
                                            myReactionItemArrayList.add(myReaction);
                                            if (cnt >= dataSnapshot.getChildrenCount())
                                                myRectionAdapter.update(myReactionItemArrayList);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }*/

}
