package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;


import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.CategoriesActivity;

import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static android.app.Activity.RESULT_OK;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Loves_Fragment extends Fragment {
    TextView labeltxt, alltxt, weektxt, monthstxt, yearstxt, notifycnt;
    ImageView filter_list_icon, categories_icon, alldoneicon, weekdoneicon, monthsdoneicon, yearsdoneicon;
    RelativeLayout activity2_icon_lay;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout filter_option, yearlayout, monthlayout, weeklayout, alllayout, filter_option_inner, back_arrow;

    private String mParam1;
    private String mParam2;
    ProgressDialog progressDialog;
    public static FragmentManager manager;
    Activity activity;
    Preference preference;
    //  All_Love_Fragment all_love_fragment;
    String postid = "";

    Users users;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_love, container, false);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        try {
            manager = getChildFragmentManager();
        } catch (Exception e) {
        }

        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);


        if (getArguments() != null && getArguments().getString("postid") != null) {
            postid = getArguments().getString("postid");
        }


        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));
        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        alldoneicon = (ImageView) view.findViewById(R.id.alldoneicon);
        weekdoneicon = (ImageView) view.findViewById(R.id.weekdoneicon);
        monthsdoneicon = (ImageView) view.findViewById(R.id.monthsdoneicon);
        yearsdoneicon = (ImageView) view.findViewById(R.id.yearsdoneicon);
        filter_option_inner = (LinearLayout) view.findViewById(R.id.filter_option_inner);
        yearlayout = (LinearLayout) view.findViewById(R.id.yearlayout);
        yearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAll();
                yearsdoneicon.setVisibility(View.VISIBLE);
                filter_option.setVisibility(View.GONE);
                if (manager.getBackStackEntryCount() > 0)
                    manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Bundle bundle = new Bundle();
                bundle.putString("from", "year");
                loadContentFragment(new Filter_Love_Fragment(), false, bundle);
            }
        });
        monthlayout = (LinearLayout) view.findViewById(R.id.monthlayout);
        monthlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAll();
                monthsdoneicon.setVisibility(View.VISIBLE);
                filter_option.setVisibility(View.GONE);
                if (manager.getBackStackEntryCount() > 0)
                    manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Bundle bundle = new Bundle();
                bundle.putString("from", "month");
                loadContentFragment(new Filter_Love_Fragment(), false, bundle);
            }
        });
        weeklayout = (LinearLayout) view.findViewById(R.id.weeklayout);
        weeklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAll();
                weekdoneicon.setVisibility(View.VISIBLE);
                filter_option.setVisibility(View.GONE);
                if (manager.getBackStackEntryCount() > 0)
                    manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Bundle bundle = new Bundle();
                bundle.putString("from", "week");
                loadContentFragment(new Filter_Love_Fragment(), false, bundle);
            }
        });
        alllayout = (LinearLayout) view.findViewById(R.id.alllayout);
        alllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAll();
                alldoneicon.setVisibility(View.VISIBLE);
                filter_option.setVisibility(View.GONE);
                if (manager.getBackStackEntryCount() > 0)
                    manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                loadContentFragment(new All_Love_Fragment(), false);
            }
        });

        alltxt = (TextView) view.findViewById(R.id.alltxt);
        alltxt.setTypeface(RobotoFont.regularFont(activity));
        weektxt = (TextView) view.findViewById(R.id.weektxt);
        weektxt.setTypeface(RobotoFont.regularFont(activity));
        monthstxt = (TextView) view.findViewById(R.id.monthstxt);
        monthstxt.setTypeface(RobotoFont.regularFont(activity));
        yearstxt = (TextView) view.findViewById(R.id.yearstxt);
        yearstxt.setTypeface(RobotoFont.regularFont(activity));
        filter_option = (LinearLayout) view.findViewById(R.id.filter_option);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText(getString(R.string.loves));
        filter_list_icon = (ImageView) view.findViewById(R.id.filter_list_icon);
        filter_list_icon.setVisibility(View.VISIBLE);
        filter_list_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (filter_option.getVisibility() == View.VISIBLE) {
                    filter_option.setVisibility(View.GONE);
                } else {
                    filter_option.setVisibility(View.VISIBLE);
                }
            }
        });
        categories_icon = (ImageView) view.findViewById(R.id.categories_icon);
        categories_icon.setVisibility(View.VISIBLE);
        categories_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(activity, CategoriesActivity.class).putExtra("isFilter", true), 200);
                // startActivity(new Intent(activity, CategoriesActivity.class).putExtra("isFilter",true));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });


        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);

        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        resetAll();
        alldoneicon.setVisibility(View.VISIBLE);
        filter_option.setVisibility(View.GONE);
        Bundle bundle = new Bundle();
        bundle.putString("postid", postid);
        loadContentFragment(new All_Love_Fragment(), false, bundle);
        // setLoveslist();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200 && resultCode == RESULT_OK) {
            String categoryId = data.getStringExtra("categoryId");
            String categoryName = data.getStringExtra("categoryName");
            Bundle bundle = new Bundle();
            bundle.putString("postid", postid);
            bundle.putString("categoryId", categoryId);
            bundle.putString("categoryName", categoryName);
            loadContentFragment(new All_Love_Fragment(), false, bundle);
        }
    }

    /* @Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt, activity);

    }


    public void resetAll() {
        alldoneicon.setVisibility(View.GONE);
        weekdoneicon.setVisibility(View.GONE);
        monthsdoneicon.setVisibility(View.GONE);
        yearsdoneicon.setVisibility(View.GONE);
    }


    public static void loadContentFragment(Fragment fragment, boolean flag, Bundle... bundles) {
        String backStateName = fragment.getClass().getName();

        if (bundles.length > 0) {
            // If any data is present
            fragment.setArguments(bundles[0]);
        }

        FragmentTransaction ft = manager.beginTransaction();
        if (flag) {
            ft.add(R.id.frame, fragment);
        } else {
            ft.replace(R.id.frame, fragment, backStateName);
            ft.addToBackStack(backStateName);
        }
        ft.commitAllowingStateLoss();
    }


}
