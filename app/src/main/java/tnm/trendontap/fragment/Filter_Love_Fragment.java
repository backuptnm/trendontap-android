package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import tnm.trendontap.R;
import tnm.trendontap.adapter.CustomAdapter;
import tnm.trendontap.adapter.SectionedGridViewAdapter;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.PhotoTimeStamp;
import tnm.trendontap.modal.Tag_Items;

import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;

/**
 * Created by TNM on 3/20/2018.
 */

public class Filter_Love_Fragment extends Fragment {
    ProgressDialog progressDialog;

    protected static final String TAG = "MainActivity";
    private ListView listView;
    private SectionedGridViewAdapter adapter = null;
    ArrayList<PhotoTimeStamp> photoTimeStampArrayList;

    Activity activity;
    private CustomAdapter mAdapter;
    ArrayList<String> stringArrayList;
    String from = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month_love, container, false);
        activity = getActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");

        photoTimeStampArrayList = new ArrayList<>();

        stringArrayList = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.listview);
        if (getArguments() != null) {
            from = getArguments().getString("from");
        }

        mAdapter = new CustomAdapter(activity, stringArrayList);

        listView.setAdapter(mAdapter);
        // setLoveslist();
        getLoveCount();
        return view;
    }


    public void getLoveCount() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        setUserPostFeed();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void setUserPostFeed() {
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }
        photoTimeStampArrayList.clear();
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Loves)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getLovePostData(dataSnapshot.getKey());

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getLovePostData(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    public void manageData(DataSnapshot lovelist, String action) {


        long timestamp = (long) lovelist.child(TimeStamp).getValue();
        PhotoTimeStamp photoTimeStamp = new PhotoTimeStamp(
                String.valueOf(lovelist.child(tPostPhotoId).child("0").getValue()),
                timestamp,
                String.valueOf(lovelist.child(ID).getValue()),
                (long) lovelist.child(ReactionsCount).getValue());
        if (action.equalsIgnoreCase("add")) {
            photoTimeStampArrayList.add(photoTimeStamp);
            mAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(photoTimeStampArrayList, photoTimeStamp.getPostid());
            if (pos == -1)
                return;
            photoTimeStampArrayList.remove(pos);
            mAdapter.notifyDataSetChanged();
        }

        setView();

    }


    public int matchValue(ArrayList<PhotoTimeStamp> photoTimeStampArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < photoTimeStampArrayList.size(); j++) {
            if (photoTimeStampArrayList.get(j).getPostid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    public void setView() {

        ArrayList<String> stringArrayList = new ArrayList<>();
        Map<String, ArrayList<PhotoTimeStamp>> imagesArray = new HashMap<>();
        // ArrayList<String> imageList = new ArrayList<>();
        ArrayList<PhotoTimeStamp> imageList = new ArrayList<>();

        for (int j = 0; j < photoTimeStampArrayList.size(); j++) {
            String str = "";
            if (from.equalsIgnoreCase("month"))
                str = getMonthYear(photoTimeStampArrayList.get(j).getTimestamp());
            else if (from.equalsIgnoreCase("year"))
                str = getYear(photoTimeStampArrayList.get(j).getTimestamp());
            else
                str = getDate(photoTimeStampArrayList.get(j).getTimestamp());

            if (!stringArrayList.contains(str)) {
                stringArrayList.add(str);
                imageList = new ArrayList<>();
                imageList.add(photoTimeStampArrayList.get(j));
            } else {
                imageList.add(photoTimeStampArrayList.get(j));
            }
            imagesArray.put(str, imageList);

        }

        mAdapter.addList(stringArrayList, imagesArray);

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    public String getYear(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        return String.valueOf(cal.get(Calendar.YEAR));
    }

    public String getMonthYear(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM yyyy");
        String str = month_date.format(cal.getTime());
        return str;
    }

    private String getDate(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        return sdf.format(d);
    }


}
