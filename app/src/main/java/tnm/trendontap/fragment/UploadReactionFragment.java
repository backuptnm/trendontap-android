package tnm.trendontap.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.PostLovesItem;
import tnm.trendontap.modal.PostReaction;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.TopCropImageView;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.LOVE;
import static tnm.trendontap.utility.EventClass.POSTMSG;
import static tnm.trendontap.utility.EventClass.REACTION;
import static tnm.trendontap.utility.EventClass.REACTMSG;

import static tnm.trendontap.utility.EventClass.processActivity;
import static tnm.trendontap.utility.EventClass.sendActivityToUser;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getResizedBitmap;
import static tnm.trendontap.utility.Utils.getScreenHeight;
import static tnm.trendontap.utility.Utils.uploadpath;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadReactionFragment extends Fragment {

    TextView labeltxt, labelPosttxt;
    // ImageView reactionimg;

    TopCropImageView reactionimg;
    EditText etTag;
    Activity activity;
    ProgressDialog progressDialog;
    Bitmap bitmap;
    String postid, postuserid, postimg = "", from = "", selectedImage;
    int height;
    LinearLayout back_arrow, labelPost;

    JSONArray imageCaptureArray;

    public UploadReactionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_reaction, container, false);
        activity = getActivity();

        height = getScreenHeight();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        etTag = (EditText) view.findViewById(R.id.tvTag);
        selectedImage = getArguments().getString("selectedImage", "");
        reactionimg = (TopCropImageView) view.findViewById(R.id.reactionimg);
        // int updateheight = (int) (height / 2.5);
        //reactionimg.getLayoutParams().height = updateheight;
        if (!getArguments().getBoolean("isEdit", false)) {
            if (getArguments().getString("from") != null) {
                from = getArguments().getString("from");
            }
            String imageArray = getArguments().getString("imageCaptureArray", "");
            try {
                imageCaptureArray = new JSONArray(imageArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bitmap = BitmapFactory.decodeFile(selectedImage);
            reactionimg.setImageBitmap(bitmap);
        } else {
            try {
                etTag.setText(getArguments().getString("content", ""));
                etTag.requestFocus();
            } catch (Exception e) {

            }

            if (!TextUtils.isEmpty(selectedImage)) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.centerInside();
                requestOptions.override(Utils.getScreenWidth(), Utils.getScreenWidth());
                Glide.with(activity)
                        .load(selectedImage)
                        .apply(requestOptions)
                        .into(reactionimg);
            }
        }


        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labelPost = (LinearLayout) view.findViewById(R.id.labelPost);
        labelPosttxt = (TextView) view.findViewById(R.id.labelPosttxt);


        postid = getArguments().getString("postid");
        postimg = getArguments().getString("postimg");
        postuserid = getArguments().getString("postuserid");

        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labelPosttxt.setTypeface(RobotoFont.mediumFont(activity));
        etTag.setTypeface(RobotoFont.mediumFont(activity));
        labeltxt.setText("Upload Reaction");
        labelPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                        Context.INPUT_METHOD_SERVICE);

                imm.hideSoftInputFromWindow(etTag.getWindowToken(), 0);
                if (getArguments().getBoolean("isEdit", false)) {
                    editData(etTag.getText().toString(), getArguments().getString("reactionid"));
                } else
                    uploadFile();
            }
        });
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        return view;
    }

    public void editData(String reactiontxt, String reactionid) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).child(reactionid);
        databaseReference.child(Content).setValue(reactiontxt);
        MainActivity.manageBack(activity, "");
    }

    ArrayList<String> lovephoto;

    private void uploadFile() {
        progressDialog.show();
        lovephoto = new ArrayList<>();
        uploadImage(0);
    }

    int uploadImagepos;

    private void uploadImage(int pos) {
        String image = "";
        uploadImagepos = pos;
        try {
            image = (String) imageCaptureArray.get(uploadImagepos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Uri file = Uri.fromFile(new File(image));
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(uploadpath);
        StorageReference imagesRef = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + System.currentTimeMillis() / 1000 + ".jpg");
      /*  ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap = getResizedBitmap(bitmap, 1500);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();*/

        UploadTask uploadTask = imagesRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                progressDialog.dismiss();
                Toast.makeText(activity, "Something wrong, Please try again", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                final Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl-->", "" + downloadUrl);
                lovephoto.add(String.valueOf(downloadUrl));
                if (lovephoto.size() == imageCaptureArray.length()) {
                    updateData(lovephoto);
                } else {
                    uploadImagepos += 1;
                    uploadImage(uploadImagepos);
                }

            }
        });
    }

    private void updateData(ArrayList<String> photoUrls) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().
                getReference().child(FirebasePathPostReaction);
        final String reaction_id = databaseReference.push().getKey();

        PostReaction postReaction = new PostReaction(0,
                etTag.getText().toString().trim(), false, false, reaction_id,
                postuserid, postid,
                false, false, false, System.currentTimeMillis(),
                false, false,
                FirebaseAuth.getInstance().getCurrentUser().getUid(), photoUrls, false);
        databaseReference.child(reaction_id).setValue(postReaction);

        final DatabaseReference postref = FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts)
                .child(postid);
        postref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long reactionsCount = (long) dataSnapshot.child(ReactionsCount).getValue();
                DatabaseReference reactioncnt = FirebaseDatabase.getInstance().getReference(FirebaseTrendOnTapPosts);
                reactioncnt.child(postid).child(ReactionsCount).setValue(reactionsCount + 1);
                reactioncnt.child(postid).child(UnreadReactionCount).setValue(true);
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                ArrayList<String> suppliers = new ArrayList<>();
                if (dataSnapshot.hasChild(ReactedSuppiers) && dataSnapshot.child(ReactedSuppiers).getValue() != null) {
                    suppliers = (ArrayList<String>) dataSnapshot.child(ReactedSuppiers).getValue();
                    if (!suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        suppliers.add(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        postref.child(ReactedSuppiers).setValue(suppliers);
                    }
                } else {
                    suppliers.add(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    postref.child(ReactedSuppiers).setValue(suppliers);
                }

                Preference preference = new Preference(activity, Login_Pref);
                Users users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
                ActivityItem activityItem = new ActivityItem();
                activityItem.setProfilePic(users.getProfilePic());
                activityItem.setLongMessage(REACTMSG);
                activityItem.setMessage(users.getFirstName() + " " + users.getLastName() + REACTMSG);
                activityItem.setPostID(postid);
                activityItem.setPostImageURL(postimg);
                activityItem.setReactionID(reaction_id);
                activityItem.setShortMessage(from);
                activityItem.setTiggerBy(users.getFirstName() + " " + users.getLastName());
                activityItem.setActivityType(REACTION);
                activityItem.setTimeStamp(System.currentTimeMillis());
                activityItem.setUserID(users.getUserID());

                String msg = users.getFirstName() + " " + users.getLastName() + REACTMSG + from;
                sendActivityToUser(postuserid, activityItem, msg);
                databaseReference.child(reaction_id).child(TimeStamp).setValue(System.currentTimeMillis());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FragmentManager fm = MainActivity.manager;
                        for (int i = 0; i < fm.getBackStackEntryCount() - 1; ++i) {
                            fm.popBackStack();
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("from", from);
                        //bundle.putString("imgurl", selectedImage);
                        bundle.putString("postid", postid);
                        bundle.putString("postuserid", postuserid);
                        bundle.putString("imgurl", getArguments().getString("postimg"));
                        MainActivity.loadContentFragment(new MyReactionFragment(), false, bundle);
                    }
                }, 500);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(activity, "Something wrong, Please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
