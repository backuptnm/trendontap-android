package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.Utils.Login_Pref;


public class Network_Setting_Fragment extends Fragment {
    TextView labeltxt, selecttxt, callingsheettxt, lovestxt, trenstxt, suppliersdevtxt, notifycnt;
    LinearLayout back_arrow;
    RelativeLayout activity2_icon_lay;
    Activity activity;
    Preference preference;
    Users users;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_setting, container, false);
        activity = getActivity();
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        labeltxt.setText(getString(R.string.networksetting));

        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });
        selecttxt = (TextView) view.findViewById(R.id.selecttxt);
        selecttxt.setTypeface(RobotoFont.regularFont(activity));

        callingsheettxt = (TextView) view.findViewById(R.id.callingsheettxt);
        callingsheettxt.setTypeface(RobotoFont.mediumFont(activity));

        lovestxt = (TextView) view.findViewById(R.id.lovestxt);
        lovestxt.setTypeface(RobotoFont.mediumFont(activity));
        trenstxt = (TextView) view.findViewById(R.id.trenstxt);
        trenstxt.setTypeface(RobotoFont.mediumFont(activity));
        suppliersdevtxt = (TextView) view.findViewById(R.id.suppliersdevtxt);
        suppliersdevtxt.setTypeface(RobotoFont.mediumFont(activity));
        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);
        activity2_icon_lay.setVisibility(View.VISIBLE);
        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        return view;
    }

    /*@Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (users.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(users.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(notifycnt,activity);
    }

}
