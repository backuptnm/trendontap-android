package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;

import tnm.trendontap.adapter.SeasonAdapter;

import tnm.trendontap.modal.SeasonItem;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.DateUpdated;
import static tnm.trendontap.utility.FireStoreUtils.Name;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.TrendsSeason;
import static tnm.trendontap.utility.FireStoreUtils.TrendsUserSeason;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.Utils.hidekeyboard;


public class Season_Fragment extends Fragment {
    ArrayList<SeasonItem> seasonItemArrayList;
    SeasonAdapter seasonAdapter;
    SwipeMenuListView seasonlist;

    TextView labeltxt, noseason;
    ImageView addbtn, back_arrow;
    Context context;
    LinearLayout noseasondata;
    DatabaseReference mainDatabsereference;

    ProgressDialog progressDialog;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_season, container, false);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        context = getActivity();

        noseasondata = (LinearLayout) view.findViewById(R.id.noseasondata);
        noseason = (TextView) view.findViewById(R.id.noseason);
        noseason.setTypeface(RobotoFont.mediumFont(activity));

        back_arrow = (ImageView) view.findViewById(R.id.back_arrow);

        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(context));
        labeltxt.setText("Seasons");
        addbtn = (ImageView) view.findViewById(R.id.addbtn);
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSeasonDialog("", false, "", 0);
            }
        });
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });
        seasonItemArrayList = new ArrayList<>();
        seasonAdapter = new SeasonAdapter(context, seasonItemArrayList, noseasondata);
        seasonlist = (SwipeMenuListView) view.findViewById(R.id.seasonlist);
        seasonlist.setAdapter(seasonAdapter);

        if (Utils.isConnected(context)) {
            setSeasonList();
        }

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                createMenu1(menu);
            }
        };
        seasonlist.setMenuCreator(creator);
        seasonlist.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                SeasonItem seasonItem = seasonItemArrayList.get(position);
                switch (index) {
                    case 0:
                        // Edit
                        addSeasonDialog(seasonItem.getId(), true, seasonItem.getName(), position);
                        break;
                    case 1:
                        seasonlist.smoothCloseMenu();
                        removeSeason(seasonItem, position);

                       /* seasonItemArrayList.remove(position);
                        seasonAdapter.notifyDataSetChanged();*/
                        break;
                }
                return false;
            }
        });

        return view;
    }

    private void createMenu1(SwipeMenu menu) {
        SwipeMenuItem item1 = new SwipeMenuItem(
                context);
        item1.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                0xCE)));
        item1.setWidth(dp2px(90));
        item1.setTitle("Edit");
        item1.setTitleColor(Color.parseColor("#FFFFFF"));
        item1.setTitleSize(14);
        menu.addMenuItem(item1);
        SwipeMenuItem item2 = new SwipeMenuItem(
                context);
        item2.setBackground(new ColorDrawable(Color.parseColor("#FF0000")));//0xE5, 0x18,
        //0x5E)));
        item2.setWidth(dp2px(90));
        item2.setTitle("Delete");
        item2.setTitleColor(Color.parseColor("#FFFFFF"));
        item2.setTitleSize(14);
        menu.addMenuItem(item2);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public void setSeasonList() {
        progressDialog.show();
        seasonItemArrayList.clear();
        mainDatabsereference = FirebaseDatabase.getInstance().getReference().child(TrendsSeason);
        mainDatabsereference.orderByChild(FireStoreUtils.UserID).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        seasonItemArrayList.clear();
                        seasonlist.setVisibility(View.GONE);
                        noseasondata.setVisibility(View.GONE);
                        if (!dataSnapshot.hasChildren()) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            noseasondata.setVisibility(View.VISIBLE);
                            return;
                        }
                        seasonlist.setVisibility(View.VISIBLE);
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            SeasonItem seasonItem = dataSnapshot1.getValue(SeasonItem.class);
                            seasonItemArrayList.add(seasonItem);
                        }
                        seasonAdapter.addList(seasonItemArrayList);
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                        seasonItemArrayList.clear();
                        seasonlist.setVisibility(View.GONE);
                        noseasondata.setVisibility(View.VISIBLE);
                    }
                });

    }


    public void addSeasonDialog(final String seasonid, final boolean isEdit, final String value, final int pos) {
        final Dialog dialog = new Dialog(activity);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = Utils.getScreenWidth() - (Utils.getScreenWidth() / 5);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.dialog_add_season);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(RobotoFont.boldFont(activity));

        final EditText edtname = (EditText) dialog.findViewById(R.id.edtname);
        edtname.setTypeface(RobotoFont.regularFont(activity));
        // edtname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edtname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    saveBtn(value, isEdit, edtname, seasonid, pos, dialog);
                }
                return false;
            }
        });

        if (isEdit) {
            tvTitle.setText("Edit Season");
            edtname.setText(value);
        } else {
            tvTitle.setText("Add New Season");
            edtname.setText("");
        }

        TextView done_action = (TextView) dialog.findViewById(R.id.done_action);
        done_action.setText("OK");
        done_action.setTypeface(RobotoFont.boldFont(activity));
        done_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveBtn(value, isEdit, edtname, seasonid, pos, dialog);
            }
        });
        TextView cancel_action = (TextView) dialog.findViewById(R.id.cancel_action);
        cancel_action.setTypeface(RobotoFont.boldFont(activity));
        cancel_action.setText("CANCEL");
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);

    }

    public void saveBtn(String savevalue, boolean isEdit, EditText edtname, String seasonid, int pos, Dialog dialog) {
        if (TextUtils.isEmpty(edtname.getText().toString().trim())) {
            Toast.makeText(activity, "Please enter season", Toast.LENGTH_SHORT).show();
            return;
        }
        boolean flag = false;
        for (int j = 0; j < seasonItemArrayList.size(); j++) {
            if (seasonItemArrayList.get(j).getName().trim().equalsIgnoreCase(edtname.getText().toString().trim())) {
                flag = true;
                if (isEdit && seasonItemArrayList.get(j).getName().trim().equalsIgnoreCase(savevalue)) {
                    flag = false;
                }
                break;
            }
        }

        if (flag) {
            Toast.makeText(activity, "Season already exist", Toast.LENGTH_LONG).show();
            return;
        } else if (isEdit) {
            editSeason(seasonid, edtname, pos);
        } else
            saveSeason(edtname.getText().toString().trim(), edtname);

        hidekeyboard(activity, edtname);
        if (dialog.isShowing())
            dialog.dismiss();

    }


   /* public void saveBtn(boolean isEdit, EditText edtname, String seasonid, int pos, Dialog dialog) {
        if (isEdit) {
            if (TextUtils.isEmpty(edtname.getText().toString().trim())) {
                Toast.makeText(activity, "Please enter season", Toast.LENGTH_SHORT).show();
                return;
            }
            editSeason(seasonid,edtname, pos);
        } else {
            if (TextUtils.isEmpty(edtname.getText().toString().trim())) {
                Toast.makeText(activity, "Please enter season", Toast.LENGTH_SHORT).show();
                return;
            }
            boolean flag = false;
            for (int j = 0; j < seasonItemArrayList.size(); j++) {
                if (seasonItemArrayList.get(j).getName().equalsIgnoreCase(edtname.getText().toString().trim())) {
                    flag = true;
                    break;
                }
            }

            if (flag) {
                Toast.makeText(activity, "Season already exist", Toast.LENGTH_LONG).show();
                return;
            } else
                saveSeason(edtname.getText().toString().trim(), edtname);

        }
        hidekeyboard(activity, edtname);
        if (dialog.isShowing())
            dialog.dismiss();

    }*/


    public void saveSeason(String seasonname, EditText edtname) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(TrendsUserSeason).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(TrendsSeason);
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(TrendsSeason);
        String id = databaseReference.push().getKey();
        databaseReference.child(id).setValue(true);
        long timeStamp = System.currentTimeMillis();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
       // SeasonItem seasonItem = new SeasonItem(id, seasonname, timeStamp, FirebaseAuth.getInstance().getCurrentUser().getUid());
        SeasonItem seasonItem = new SeasonItem(id, seasonname, timeStamp, FirebaseAuth.getInstance().getCurrentUser().getUid(), timeStamp, timeStamp, true, false);
        databaseReference1.child(id).setValue(seasonItem);
        noseasondata.setVisibility(View.GONE);
        seasonlist.setVisibility(View.VISIBLE);

    }


    public void removeSeason(SeasonItem seasonItem, int position) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(TrendsUserSeason).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(TrendsSeason);
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(TrendsSeason);

        databaseReference.child(seasonItem.getId()).removeValue();
        databaseReference1.child(seasonItem.getId()).removeValue();

        seasonItemArrayList.remove(position);
        seasonAdapter.notifyDataSetChanged();

        if (seasonItemArrayList.size() == 0) {
            noseasondata.setVisibility(View.VISIBLE);
            seasonlist.setVisibility(View.GONE);
        } else {
            noseasondata.setVisibility(View.GONE);
            seasonlist.setVisibility(View.VISIBLE);
        }

    }


    public void editSeason(String id, EditText edtname, int position) {
        String seasonname = edtname.getText().toString().trim();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().
                child(TrendsSeason).
                child(id);
        databaseReference.child(Name).setValue(seasonname);
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtname.getWindowToken(), 0);
        long timestamp = System.currentTimeMillis();
        SeasonItem seasonItem = new SeasonItem(id, seasonname, timestamp, FirebaseAuth.getInstance().getCurrentUser().getUid());
        seasonItem.setDateUpdated(timestamp);
        databaseReference.child(TimeStamp).setValue(timestamp);
        databaseReference.child(DateUpdated).setValue(timestamp);
        seasonItemArrayList.set(position, seasonItem);
        seasonAdapter.notifyDataSetChanged();
    }


}
