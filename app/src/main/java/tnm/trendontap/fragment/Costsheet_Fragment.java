package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.CostingSheet_Activity;
import tnm.trendontap.adapter.CostsheetAdapter;
import tnm.trendontap.modal.CostSheet_Model;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.BuyerUserID;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathCostSheet;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.Utils.Login_Pref;


public class Costsheet_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView costsheetlist;
    EditText etSearch;
    ImageView back, imgClear;
    CostsheetAdapter costsheetAdapter;
    ArrayList<CostSheet_Model> costsheetItemArrayList;
    RelativeLayout nocostsheetdata;
    TextView nocostsheet;
    Activity activity;
    Preference preference;
    Users users;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static Costsheet_Fragment newInstance(String param1, String param2) {
        Costsheet_Fragment fragment = new Costsheet_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_costsheet, container, false);
        activity = getActivity();
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        costsheetlist = (ListView) view.findViewById(R.id.costsheetlist);
        nocostsheet = (TextView) view.findViewById(R.id.nocostsheet);
        nocostsheet.setTypeface(RobotoFont.mediumFont(activity));
        nocostsheetdata = (RelativeLayout) view.findViewById(R.id.nocostsheetdata);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        etSearch.setTypeface(RobotoFont.regularFont(activity));

        back = (ImageView) view.findViewById(R.id.back);
        imgClear = (ImageView) view.findViewById(R.id.imgClear);
        costsheetItemArrayList = new ArrayList<>();

        costsheetAdapter = new CostsheetAdapter(activity, costsheetItemArrayList, nocostsheetdata);
        costsheetlist.setAdapter(costsheetAdapter);
        costsheetlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                boolean isdisable;
                if (users.getUserType().equalsIgnoreCase(Utils.Supplier)) {
                    isdisable = false;
                } else {
                    isdisable = true;
                }
                CostSheet_Model costsheetItem = costsheetItemArrayList.get(i);
                Intent costingsheet = new Intent(activity, CostingSheet_Activity.class);
                costingsheet.putExtra("post_id", "").
                        putExtra("Reaction_Id", costsheetItem.getReactionID()).
                        putExtra("postimg", "").
                        putExtra("sentto", costsheetItem.getBuyerUserID()).
                        putExtra("isdisable", isdisable).
                        putExtra("costsheet_id", costsheetItem.getId());


                startActivity(costingsheet);
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.INVISIBLE);

                costsheetAdapter.filter(charSequence.toString(), costsheetlist);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                boolean handled = false;

                // Some phones disregard the IME setting option in the xml, instead
                // they send IME_ACTION_UNSPECIFIED so we need to catch that
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    handled = true;
                }
                return handled;
            }
        });


        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
            }
        });

        if (mParam2.equalsIgnoreCase(Utils.Supplier))
            setList();
        else
            setListBuyer();
        return view;
    }

    public void setList() {
        costsheetItemArrayList.clear();

        FirebaseDatabase.getInstance().getReference().child(FirebasePathCostSheet)
                .orderByChild(FireStoreUtils.UserID).equalTo(mParam1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        costsheetItemArrayList.clear();
                        try {
                            Profile_Fragment.profiletabLayout.getTabAt(1).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Costing Sheets");
                        } catch (Exception e) {

                        }
                        if (dataSnapshot.getChildrenCount() == 0) {
                            nocostsheetdata.setVisibility(View.VISIBLE);
                            costsheetlist.setVisibility(View.GONE);
                            return;
                        } else {
                            nocostsheetdata.setVisibility(View.GONE);
                            costsheetlist.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            CostSheet_Model costSheet_model = dataSnapshot1.getValue(CostSheet_Model.class);
                            costsheetItemArrayList.add(costSheet_model);
                        }
                        costsheetAdapter.addList(costsheetItemArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void setListBuyer() {
        costsheetItemArrayList.clear();

        FirebaseDatabase.getInstance().getReference().child(FirebasePathCostSheet).orderByChild(BuyerUserID).equalTo(mParam1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        costsheetItemArrayList.clear();
                        try {
                            Profile_Fragment.profiletabLayout.getTabAt(1).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Costing Sheets");
                        } catch (Exception e) {

                        }
                        if (dataSnapshot.getChildrenCount() == 0) {
                            nocostsheetdata.setVisibility(View.VISIBLE);
                            costsheetlist.setVisibility(View.GONE);
                            return;
                        } else {
                            nocostsheetdata.setVisibility(View.GONE);
                            costsheetlist.setVisibility(View.VISIBLE);
                        }

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            CostSheet_Model costSheet_model = dataSnapshot1.getValue(CostSheet_Model.class);
                            costsheetItemArrayList.add(costSheet_model);

                        }
                        costsheetAdapter.addList(costsheetItemArrayList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }
}
