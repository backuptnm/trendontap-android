package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;

import tnm.trendontap.imagecrop.view.ImageCropView;
import tnm.trendontap.utility.FilePaths;
import tnm.trendontap.utility.FileSearch;
import tnm.trendontap.adapter.GridImageAdapter;
import tnm.trendontap.utility.RobotoFont;

import static com.google.android.gms.internal.zzahn.runOnUiThread;
import static tnm.trendontap.activity.CameraActivity.loadContentFragment;
import static tnm.trendontap.fragment.CameraFragment.camera_Pref;

/**
 * Created by admin on 13-Dec-17.
 */

public class GalleryFragment extends Fragment {

    private static final String TAG = "GalleryFragment";

    //constants
    private static final int NUM_GRID_COLUMNS = 4;

    boolean isFromBuyers;
    //widgets
    private GridView gridView;
    private ImageCropView galleryImage;

    AppBarLayout Appbar;

    Toolbar toolbar;

    boolean ExpandedActionBar = true;

    private Spinner directorySpinner;

    //vars
    private ArrayList<String> directories;
    private String mAppend = "file:/";
    private String mSelectedImage;
    JSONArray imageCaptureArray;
    LinearLayout shareClose, nextScreen;
    SharedPreferences mPreferences;

    // String getmSelectedImage = "";
    Activity activity;
    String from = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        activity = getActivity();
        mPreferences = activity.getSharedPreferences(camera_Pref, 0);
        /*SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("imagePath", "");
        editor.apply();*/
        galleryImage = (ImageCropView) view.findViewById(R.id.galleryImageView);
        gridView = (GridView) view.findViewById(R.id.gridView);
        directorySpinner = (Spinner) view.findViewById(R.id.spinnerDirectory);
        Appbar = (AppBarLayout) view.findViewById(R.id.appbar);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        ArrayList<View> views = new ArrayList<View>();
        views.add(galleryImage);
        Appbar.addTouchables(views);
        Appbar.setExpanded(true);


        directories = new ArrayList<>();
        Log.d(TAG, "onCreateView: started.");

        if (getArguments().getString("from") != null) {
            from = getArguments().getString("from");
        }

        isFromBuyers = getArguments().getBoolean("buyers", true);

        shareClose = (LinearLayout) view.findViewById(R.id.ivCloseShare);
        shareClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing the gallery fragment.");
                if (isFromBuyers)
                    activity.onBackPressed();
                else
                    MainActivity.manageBack(activity, "");

            }
        });


        nextScreen = (LinearLayout) view.findViewById(R.id.tvNext);
        nextScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageCaptureArray = new JSONArray();
                try {
                    imageCaptureArray = new JSONArray(mPreferences.getString("imagePath", ""));
                } catch (JSONException e) {
                    imageCaptureArray = new JSONArray();
                }

                String path = "";
                if (!galleryImage.isChangingScale()) {
                    Bitmap b = galleryImage.getCroppedImage();
                    if (b != null) {
                        path = bitmapConvertToFile(b).getAbsolutePath();
                    } else {
                        //Toast.makeText(activity, R.string.fail_to_crop, Toast.LENGTH_SHORT).show();
                    }
                }

                // String path = mSelectedImage;
                if (imageCaptureArray.length() < 4)
                    imageCaptureArray.put(path);
                else
                    try {
                        imageCaptureArray.put(3, path);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString("imagePath", imageCaptureArray.toString());
                editor.apply();

                UseImageFragment fragment = new UseImageFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", isFromBuyers);
                bundle.putString("selectedImage", mSelectedImage);
                bundle.putString("from", from);
                bundle.putString("postid", getArguments().getString("postid",""));

                if (isFromBuyers) {
                    loadContentFragment(fragment, false, bundle);
                } else {

                    bundle.putString("postuserid", getArguments().getString("postuserid"));
                    bundle.putString("postimg", getArguments().getString("postimg"));

                    MainActivity.loadContentFragment(fragment, false, bundle);
                }
            }
        });

        init();


        return view;
    }


    public File bitmapConvertToFile(Bitmap bitmap) {
        FileOutputStream fileOutputStream = null;
        File bitmapFile = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory("image_crop_sample"), "");
            if (!file.exists()) {
                file.mkdir();
            }

            bitmapFile = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
            fileOutputStream = new FileOutputStream(bitmapFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            MediaScannerConnection.scanFile(activity, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() {

                }

                @Override
                public void onScanCompleted(String path, Uri uri) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(activity, "file saved", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) {
                }
            }
        }

        return bitmapFile;
    }


    private void init() {
        mSelectedImage = "";
        FilePaths filePaths = new FilePaths();

        //check for other folders indide "/storage/emulated/0/pictures"
        if (FileSearch.getDirectoryPaths(filePaths.ROOT_DIR) != null) {
            directories = FileSearch.getDirectoryPaths(filePaths.ROOT_DIR);
        }
        if (!directories.contains(filePaths.CAMERA))
            directories.add(filePaths.CAMERA);


        ArrayList<String> directoryNames = new ArrayList<>();
        for (int i = 0; i < directories.size(); i++) {
            Log.d(TAG, "init: directory: " + directories.get(i));
            int index = directories.get(i).lastIndexOf("/");
            String string = directories.get(i).substring(index + 1);
            directoryNames.add(string);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                R.layout.spinner_item, directoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        directorySpinner.setAdapter(adapter);

        directorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick: selected: " + directories.get(position));

                //setup our image grid for the directory chosen
                setupGridView(directories.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void setupGridView(String selectedDirectory) {

        final ArrayList<String> imgURLs;
        if (selectedDirectory.contains("All Photos")) {
            ArrayList<String> directoryNames = new ArrayList<>();
            directoryNames.addAll(directories);
            directoryNames.remove(0);
            imgURLs = FileSearch.getAllFilePaths(directoryNames);

        } else {
            Log.d(TAG, "setupGridView: directory chosen: " + selectedDirectory);
            imgURLs = FileSearch.getFilePaths(selectedDirectory);
        }
        //set the grid column width
        int gridWidth = getResources().getDisplayMetrics().widthPixels;
        int imageWidth = gridWidth / NUM_GRID_COLUMNS;
        gridView.setColumnWidth(imageWidth);

        //use the grid adapter to adapter the images to gridview
        GridImageAdapter adapter = new GridImageAdapter(activity, R.layout.layout_grid_imageview, mAppend, imgURLs);
        gridView.setAdapter(adapter);

        //set the first image to be displayed when the activity fragment view is inflated

        if (imgURLs.size() > 1) {
            try {
                //  galleryImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mSelectedImage = imgURLs.get(1);
                // setImage(imgURLs.get(1), galleryImage, mAppend);
                galleryImage.setImageFilePath(mSelectedImage);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, "setupGridView: ArrayIndexOutOfBoundsException: " + e.getMessage());

            }
        } else {
            galleryImage.setImageDrawable(null);
        }

        galleryImage.setAspectRatio(1, 1);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick: selected an image: " + imgURLs.get(position));

                if (position == 0) {
                    CameraFragment fragment = new CameraFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("buyers", isFromBuyers);
                    fragment.setArguments(bundle);
                    if (isFromBuyers)
                        loadContentFragment(fragment, false);
                    else {
                        bundle.putString("postid", getArguments().getString("postid"));
                        bundle.putString("postuserid", getArguments().getString("postuserid"));
                        bundle.putString("postimg", getArguments().getString("postimg"));
                        bundle.putString("from", from);
                        MainActivity.loadContentFragment(fragment, false);
                    }
                    return;
                }

                Appbar.setExpanded(true);
                mSelectedImage = imgURLs.get(position);
                galleryImage.setImageDrawable(null);
                //setImage(mSelectedImage, galleryImage, mAppend);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        galleryImage.setImageFilePath(mSelectedImage);
                    }
                }, 500);


            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            gridView.setNestedScrollingEnabled(true);
        }

       /* String path = mSelectedImage;
        if (imageCaptureArray.length() < 4)
            imageCaptureArray.put(path);
        else
            try {
                imageCaptureArray.put(3, path);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("imagePath", imageCaptureArray.toString());
        editor.apply();*/
    }


    private void setImage(String imgURL, ImageView image, String append) {


        Log.d(TAG, "setImage: setting image");

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(append + imgURL, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                // mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                // mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                // mProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }


}
