package tnm.trendontap.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.activity.PDFViewActivity;
import tnm.trendontap.adapter.BuyerTrendAdapter;
import tnm.trendontap.adapter.TrendAdapter;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.activity.MainActivity.getBadgeCount;
import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.PdfURL;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 12/15/2017.
 */

public class Buyers_Trend_Fragment extends Fragment {

    ListView buyertrendlist;
    TextView buyertrendtxt, nottrends;
    ArrayList<BuyerTrendsItem> buyerTrendsItemArrayList;
    BuyerTrendAdapter buyerTrendAdapter;
    ProgressDialog progressDialog;
    ImageView profilepic;
    Preference preference;
    Users users;
    Activity activity;
    int selectedPos = -1;
    String postid = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyers_trend, container, false);
        activity = getActivity();

        if (getArguments() != null && getArguments().getString("postid") != null) {
            postid = getArguments().getString("postid");
        }


        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
        }
        buyertrendlist = (ListView) view.findViewById(R.id.buyertrendlist);
        buyertrendtxt = (TextView) view.findViewById(R.id.buyertrendtxt);
        buyertrendtxt.setTypeface(RobotoFont.boldFont(activity));
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        nottrends = (TextView) view.findViewById(R.id.nottrends);
        nottrends.setTypeface(RobotoFont.mediumFont(activity));
        preference = new Preference(activity, Login_Pref);
        users = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        profilepic = (ImageView) view.findViewById(R.id.profilepic);
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("loggeduser", true);
                MainActivity.loadContentFragment(new Profile_Fragment(), false, bundle);
            }
        });
        if (!TextUtils.isEmpty(users.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(this)
                    .load(users.getProfilePic())
                    .apply(requestOptions)
                    .into(profilepic);
        }

        buyerTrendsItemArrayList = new ArrayList<>();
        buyerTrendAdapter = new BuyerTrendAdapter(activity, buyerTrendsItemArrayList);
        buyertrendlist.setAdapter(buyerTrendAdapter);
        buyertrendlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (buyerTrendsItemArrayList == null && buyerTrendsItemArrayList.size() == 0)
                    return;

                selectedPos = position;

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 13);
                } else {
                    String pdfUrl = buyerTrendsItemArrayList.get(position).getPdfUrl();
                    downloadPDF(pdfUrl);
                }
            }
        });

        getBuyerTrendCount();

        return view;
    }
    public void managePos() {
        buyertrendlist.post(new Runnable() {
            @Override
            public void run() {
                if (!postid.equalsIgnoreCase("")) {
                    int pos = matchValue(buyerTrendsItemArrayList, postid);
                    if (pos == -1) {
                        return;
                    }
                    buyertrendlist.setSelection(pos);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 13) {
            int access_write = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_write != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "Sorry!!!, you can't open File without granting permission", Toast.LENGTH_LONG).show();
            } else {
                String pdfUrl = buyerTrendsItemArrayList.get(selectedPos).getPdfUrl();
                downloadPDF(pdfUrl);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt,activity);
    }

    int count = 0;

    public void getBuyerTrendCount() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Trends).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                count = (int) dataSnapshot.getChildrenCount();
                if (!dataSnapshot.hasChildren()) {
                    nottrends.setVisibility(View.VISIBLE);
                    buyertrendlist.setVisibility(View.GONE);
                    return;
                }
                if (progressDialog != null && !progressDialog.isShowing()) {
                    progressDialog.show();
                }
                setUserPostFeed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUserPostFeed() {

        buyerTrendsItemArrayList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).
                child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(Trends).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getTrendPostData(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void getTrendPostData(String postid) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(postid).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                        manageData(dataSnapshot, "delete");

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    public void manageData(final DataSnapshot trends, final String action) {
        boolean isshown = true;
        if (trends.hasChild(HideSuppliers)) {
            if (trends.child(HideSuppliers).hasChild(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                isshown = false;
            }
        }

        if (isshown) {
            String userId = "";

            if (!TextUtils.isEmpty(String.valueOf(trends.child(FireStoreUtils.UserID).getValue()))) {
                userId = String.valueOf(trends.child(FireStoreUtils.UserID).getValue());
            }

            boolean isreactions = false;
            ArrayList<String> suppliers = new ArrayList<>();

            if (trends.hasChild(ReactedSuppiers) && trends.child(ReactedSuppiers).getValue() != null) {
                suppliers = (ArrayList<String>) trends.child(ReactedSuppiers).getValue();
                if (suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    isreactions = true;
                }
            }
            LocationItem location = null;
            if (trends.hasChild(Location)) {
                location = new LocationItem(trends.child(Location).child(Title).getValue().toString(),
                        trends.child(Location).child(Address).getValue().toString(),
                        (double) trends.child(Location).child(Latitude).getValue(),
                        (double) trends.child(Location).child(Logitude).getValue());
            }
            BuyerTrendsItem buyerTrendsItem = new BuyerTrendsItem(
                    trends.child(tPostPhotoId).child("0").getValue().toString(),
                    trends.child(PostTitle).getValue().toString(),
                    trends.child(CatagoryName).getValue().toString(),
                    trends.child(Content).getValue().toString(),
                    trends.child(DeliveryDate).getValue().toString(),
                    trends.child(TimeStamp).getValue().toString(),
                    "", "",
                    isreactions,
                    trends.child(ID).getValue().toString(),
                    "", "", (long) trends.child(LikesCount).getValue(),
                    trends.child(PdfURL).getValue().toString(), "",
                    location);

            if (action.equalsIgnoreCase("edit")) {
                int pos = matchValue(buyerTrendsItemArrayList, buyerTrendsItem.getPostid());
                if (pos == -1)
                    return;
                buyerTrendsItemArrayList.set(pos, buyerTrendsItem);
                buyerTrendAdapter.notifyDataSetChanged();
            } else if (action.equalsIgnoreCase("delete")) {
                int pos = matchValue(buyerTrendsItemArrayList, buyerTrendsItem.getPostid());
                if (pos == -1)
                    return;
                buyerTrendsItemArrayList.remove(pos);
                buyerTrendAdapter.notifyDataSetChanged();
                if (buyerTrendsItemArrayList.size() == 0) {
                    nottrends.setVisibility(View.VISIBLE);
                    buyertrendlist.setVisibility(View.GONE);
                }
            } else {
                int pos = matchValue(buyerTrendsItemArrayList, buyerTrendsItem.getPostid());
                if (pos == -1)
                    buyerTrendsItemArrayList.add(buyerTrendsItem);
               // if (buyerTrendsItemArrayList.size() == count) {
                    buyerTrendAdapter.addList(buyerTrendsItemArrayList);
                    nottrends.setVisibility(View.GONE);
                    buyertrendlist.setVisibility(View.VISIBLE);
               // }
            }
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if (!action.equalsIgnoreCase("delete"))
                getUserDetails(userId, buyerTrendsItem, action);
        } else {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            if (buyerTrendsItemArrayList.size() == 0) {
                nottrends.setVisibility(View.VISIBLE);
                buyertrendlist.setVisibility(View.GONE);
            } else {
                nottrends.setVisibility(View.GONE);
                buyertrendlist.setVisibility(View.VISIBLE);
            }
        }


    }

    public void getUserDetails(String userid, final BuyerTrendsItem buyerTrendsItem, final String action) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            return;
                        }
                        Users users = dataSnapshot.getValue(Users.class);
                        buyerTrendsItem.setUsername(getName(users.getFirstName(), users.getLastName()));
                        buyerTrendsItem.setType(users.getJobTitle());
                        buyerTrendsItem.setUsercompany(users.getCompanyName());
                        buyerTrendsItem.setUserimg(users.getProfilePic());
                        buyerTrendsItem.setUserpostid(users.getUserID());

                        int pos = matchValue(buyerTrendsItemArrayList, buyerTrendsItem.getPostid());
                        if (pos == -1)
                            return;
                        buyerTrendsItemArrayList.set(pos, buyerTrendsItem);
                        buyerTrendAdapter.notifyDataSetChanged();
                        managePos();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public int matchValue(ArrayList<BuyerTrendsItem> buyerTrendsItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < buyerTrendsItemArrayList.size(); j++) {
            if (buyerTrendsItemArrayList.get(j).getPostid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }


    public void downloadPDF(String url) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];
        viewPDF(name);
    }

    public boolean viewPDF(String name) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf

        if (!pdfFile.exists()) {
            return false;
        }
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(activity, PDFViewActivity.class);
        pdfIntent.putExtra("pdfUrl", path.toString());
        try {
            startActivity(pdfIntent);
        } catch (Exception e) {
        }
        return true;
    }

}
