package tnm.trendontap.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import tnm.trendontap.R;
import tnm.trendontap.activity.CameraActivity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.utility.CustomCameraView;
import tnm.trendontap.utility.Preference;

import static tnm.trendontap.activity.CameraActivity.loadContentFragment;
import static tnm.trendontap.utility.Utils.Login_Pref;

/**
 * Created by TNM on 11/2/2017.
 */

public class CameraFragment extends Fragment {

    private static final String TAG = "AndroidCameraApi";
    public static final String camera_Pref = "camera_pref";
    private ImageView btn_takepicture, close, flash, flipcamera, gallery;
    private TextureView textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    RelativeLayout takepicture_lay, rl_takepicture_main;

    Preference preference;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    private CameraCharacteristics cameraCharacteristics;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;


    public static final String CAMERA_FRONT = "1";
    public static final String CAMERA_BACK = "0";

    private String cameraId = CAMERA_BACK;
    private Boolean isTorchOn;
    boolean isClose = true;
    SharedPreferences mPreferences;

    private Context mContext;
    private FragmentActivity mActivity;
    private boolean isFromBuyers;
    String postid = "", postuserid = "", postimg = "", from = "";
    CustomCameraView cv;
    private FrameLayout preview;

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTorchOn = false;
        isClose = true;

        mContext = getContext();
        mActivity = getActivity();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            int access_camera = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);
            int access_write = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_camera != PackageManager.PERMISSION_GRANTED || access_write != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
            } else {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        cv.startCamera();
                        preview.removeAllViews();
                        preview.addView(cv);
                    }
                }, 100);
            }
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        mActivity = getActivity();
        preference = new Preference(mActivity, Login_Pref);
        isFromBuyers = getArguments().getBoolean("buyers", true);
        preview = (FrameLayout) view.findViewById(R.id.camera_preview);

        try {
            postid = getArguments().getString("postid");
            postuserid = getArguments().getString("postuserid");
            postimg = getArguments().getString("postimg");
            from = getArguments().getString("from");
        } catch (Exception e) {

        }


        cv = new CustomCameraView(mActivity, isFromBuyers, "CameraFragment", postid, postuserid, postimg, from);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    cv.startCamera();
                    preview.removeAllViews();
                    preview.addView(cv);
                }
            }, 100);

        }

        textureView = (TextureView) view.findViewById(R.id.texture);
        takepicture_lay = (RelativeLayout) view.findViewById(R.id.takepicture_lay);
        rl_takepicture_main = (RelativeLayout) view.findViewById(R.id.rl_takepicture_main);
        takepicture_lay.setVisibility(View.GONE);

        textureView.setSurfaceTextureListener(textureListener);
        btn_takepicture = (ImageView) view.findViewById(R.id.btn_takepicture);

        close = (ImageView) view.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });

        flash = (ImageView) view.findViewById(R.id.flash);
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hasFlash = mActivity.getApplicationContext().getPackageManager()
                        .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

                if (!hasFlash) {
                    AlertDialog alert = new AlertDialog.Builder(mActivity)
                            .create();
                    alert.setTitle("Error");
                    alert.setMessage("Sorry, your device doesn't support flash light!");
                    alert.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                } else {
                    onOffFlash();
                }
            }
        });
        gallery = (ImageView) view.findViewById(R.id.gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mActivity.onBackPressed();
                GalleryFragment galleryFragment = new GalleryFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("buyers", isFromBuyers);
                galleryFragment.setArguments(bundle);
                if (isFromBuyers)
                    loadContentFragment(galleryFragment, false);
                else {
                    bundle.putString("postimg", getArguments().getString("postimg"));
                    MainActivity.loadContentFragment(galleryFragment, false);
                }
            }
        });


        btn_takepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        flipcamera = (ImageView) view.findViewById(R.id.flipcamera);
        flipcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // switchCamera();

                cv.flipCamera();
            }
        });

        Boolean isFlashAvailable = mActivity.getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!isFlashAvailable || cv.camaraval != 0) {
            flash.setVisibility(View.GONE);
        } else if (preference.getBoolean("isflash", false)) {
            flash.setImageResource(R.mipmap.ic_flash_on_white);
        } else {
            flash.setImageResource(R.mipmap.ic_flash_off_white);

        }


        return view;
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open  camera here
            takepicture_lay.setVisibility(View.VISIBLE);
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private Camera camera;
    private boolean isFlashOn;
    private boolean hasFlash;
    Camera.Parameters params;
    CameraManager mCameraManager;

    @SuppressLint("LongLogTag")
    private void getCamera() {
        if (camera == null) {
            try {
                camera = Camera.open();
                params = camera.getParameters();
                params.setPictureFormat(PixelFormat.JPEG);
            } catch (RuntimeException e) {
                Log.e("Camera Error. Failed to Open. Error: ", e.getMessage());
            }
        }
    }

    /*
     * Toggle switch button images
     * changing image states to on / off
     * */
    private void toggleButtonImage() {
        if (isFlashOn) {
            flash.setImageResource(R.mipmap.ic_flash_off_white);
        } else {
            flash.setImageResource(R.mipmap.ic_flash_on_white);
        }
    }


    protected void takePicture() {

        cv.takePictureFocus();

    }

    public void onOffFlash() {

        if (cv.camaraval == 0) {
            boolean isLighOn = preference.getBoolean("isflash", false);
            if (isLighOn) {
                isLighOn = false;
                flash.setImageResource(R.mipmap.ic_flash_off_white);
            } else {
                isLighOn = true;
                flash.setImageResource(R.mipmap.ic_flash_on_white);
            }

            SharedPreferences.Editor editor = preference.edit();
            editor.putBoolean("isflash", isLighOn);
            editor.apply();
        }

    }


    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(mActivity, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        Log.e(TAG, "is camera open");
        try {
            // cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            mCameraManager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");
    }

    protected void updatePreview() {
        if (null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
      /*  startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }*/


    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        //closeCamera();

        super.onPause();


        // turnOffFlash();
    }

    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }

        }
    };
    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            // Toast.makeText(mActivity, "Saved:" + file, Toast.LENGTH_SHORT).show();
            createCameraPreview();
        }
    };

    public void switchCamera() {
        if (cameraId.equals(CAMERA_FRONT)) {
            cameraId = CAMERA_BACK;
            closeCamera();
            reopenCamera();

        } else if (cameraId.equals(CAMERA_BACK)) {
            cameraId = CAMERA_FRONT;
            closeCamera();
            reopenCamera();
        }
    }

    public void reopenCamera() {
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }
}
