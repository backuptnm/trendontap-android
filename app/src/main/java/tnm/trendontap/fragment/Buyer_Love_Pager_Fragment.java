package tnm.trendontap.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.adapter.BuyerLovesAdapter;
import tnm.trendontap.adapter.BuyerTrend_LoveAdapter;
import tnm.trendontap.modal.BuyerLovesItem;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.BuyerTrendsItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.CatagoryName;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.HideSuppliers;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.Loves;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactedSuppiers;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.Utils.getName;

/**
 * Created by TNM on 12/19/2017.
 */

public class Buyer_Love_Pager_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    static ArrayList<BuyerLovesItem> buyerLovesItemArrayList;
    BuyerLovesAdapter buyerLovesAdapter;
    ListView buyer_trend_love_list;
    TextView nodata;
    LayoutInflater layoutInflater;
    Users users;
    CircleImageView profilepic;
    TextView name, useremail, userphoneno, jobtitle;
    Activity activity;
    View headerview;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            // mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static Buyer_Love_Pager_Fragment newInstance(String param1) {//, ArrayList<BuyerTrendLoveItem> buyerTrendLoveItemlist) {
        Buyer_Love_Pager_Fragment fragment = new Buyer_Love_Pager_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        // buyerTrendLoveItemArrayList = buyerTrendLoveItemlist;
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyer_trend_love_pager, container, false);
        activity = getActivity();
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        users = new Gson().fromJson(mParam1, Users.class);
        headerview = layoutInflater.inflate(R.layout.header_profile, null, false);
        profilepic = (CircleImageView) headerview.findViewById(R.id.profilepic);
        jobtitle = (TextView) headerview.findViewById(R.id.jobtitle);
        jobtitle.setTypeface(RobotoFont.regularFont(activity));
        jobtitle.setText(users.getJobTitle());
        name = (TextView) headerview.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        name.setText(users.getCompanyName());
        useremail = (TextView) headerview.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));
        useremail.setText(users.getEmailID());
        userphoneno = (TextView) headerview.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));
        if (!String.valueOf(users.getPhone()).equalsIgnoreCase("null"))
            userphoneno.setText(String.valueOf(users.getPhone()));

        buyer_trend_love_list = (ListView) view.findViewById(R.id.buyer_trend_love_list);
        buyer_trend_love_list.addHeaderView(headerview);
        nodata = (TextView) view.findViewById(R.id.nodata);
        nodata.setTypeface(RobotoFont.mediumFont(activity));
        nodata.setText("There are currently no buyer loves in your network");
        buyerLovesItemArrayList = new ArrayList<>();
        buyerLovesAdapter = new BuyerLovesAdapter(activity, buyerLovesItemArrayList);
        buyer_trend_love_list.setAdapter(buyerLovesAdapter);

        buyerLovesAdapter.setIsBuyerList(true);

        if (!TextUtils.isEmpty(users.getProfilePic())) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.userplaceholder);
            requestOptions.error(R.mipmap.userplaceholder);

            Glide.with(activity)
                    .load(users.getProfilePic())
                    .apply(requestOptions)
                    .into(profilepic);
        }
        getBuyerTrendCount();
        return view;
    }


    public void getBuyerTrendCount() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(users.getUserID()).child(Loves)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            nodata.setVisibility(View.VISIBLE);
                            //  headerview.setVisibility(View.GONE);
                            // buyer_trend_love_list.setVisibility(View.GONE);
                            return;
                        }
                        nodata.setVisibility(View.GONE);
                        // headerview.setVisibility(View.VISIBLE);
                        //    buyer_trend_love_list.setVisibility(View.VISIBLE);
                        getPostData();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getPostData() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(users.getUserID()).child(Loves).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getData(dataSnapshot);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getData(DataSnapshot dataSnapshot) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(dataSnapshot.getKey()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void manageData(DataSnapshot lovelist, final String action) {
        boolean isshown = true;
        if (lovelist.hasChild(HideSuppliers)) {
            if (lovelist.child(HideSuppliers).hasChild(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                isshown = false;
            }
        }

        if (isshown) {
            String userId = "";

            if (!TextUtils.isEmpty(String.valueOf(lovelist.child(FireStoreUtils.UserID).getValue()))) {
                userId = String.valueOf(lovelist.child(FireStoreUtils.UserID).getValue());
            }

            boolean isreactions = false;
            ArrayList<String> suppliers = new ArrayList<>();

            if (lovelist.hasChild(ReactedSuppiers) && lovelist.child(ReactedSuppiers).getValue() != null) {
                suppliers = (ArrayList<String>) lovelist.child(ReactedSuppiers).getValue();
                if (suppliers.contains(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    isreactions = true;
                }
            }

            final ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
            if (lovelist.hasChild(TagItems)) {
                for (DataSnapshot messageSnapshot : lovelist.child(TagItems).getChildren()) {
                    Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                    tag_itemsArrayList.add(tag_Items);
                }
            }

            ArrayList<String> yourStringArray = new ArrayList<>();
            int size = (int) lovelist.child(tPostPhotoId).getChildrenCount();
            for (int i = 0; i < size; i++) {
                String url = lovelist.child(tPostPhotoId).child(i + "").getValue().toString();
                yourStringArray.add(url);
            }

            LocationItem location = null;
            if (lovelist.hasChild(Location)) {
                location = new LocationItem(lovelist.child(Location).child(Title).getValue().toString(),
                        lovelist.child(Location).child(Address).getValue().toString(),
                        (double) lovelist.child(Location).child(Latitude).getValue(),
                        (double) lovelist.child(Location).child(Logitude).getValue());
            }

            BuyerLovesItem buyerLovesItem = new BuyerLovesItem(
                    yourStringArray,
                    lovelist.child(PostTitle).getValue().toString(),
                    lovelist.child(CatagoryName).getValue().toString(),
                    lovelist.child(Content).getValue().toString(),
                    lovelist.child(DeliveryDate).getValue().toString(),
                    (long) lovelist.child(TimeStamp).getValue(), "", "",
                    isreactions,
                    lovelist.child(ID).getValue().toString(), "",
                    "", tag_itemsArrayList,
                    (long) lovelist.child(LikesCount).getValue(), "",
                    location);


            if (action.equalsIgnoreCase("edit")) {
                int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                buyerLovesItemArrayList.set(pos, buyerLovesItem);
                buyerLovesAdapter.notifyDataSetChanged();
                nodata.setVisibility(View.GONE);
            } else if (action.equalsIgnoreCase("delete")) {
                int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                buyerLovesItemArrayList.remove(pos);
                buyerLovesAdapter.notifyDataSetChanged();
                if (buyerLovesItemArrayList.size() == 0) {
                    nodata.setVisibility(View.VISIBLE);
                    // headerview.setVisibility(View.GONE);
                    //  buyer_trend_love_list.setVisibility(View.GONE);
                }
            } else {
                buyerLovesItemArrayList.add(buyerLovesItem);
                buyerLovesAdapter.addList(buyerLovesItemArrayList);
                nodata.setVisibility(View.GONE);
                //  headerview.setVisibility(View.VISIBLE);
                //  buyer_trend_love_list.setVisibility(View.VISIBLE);
            }

            if (!action.equalsIgnoreCase("delete"))
                getUserDetails(userId, buyerLovesItem);
        } else {
            if (buyerLovesItemArrayList.size() == 0) {
                nodata.setVisibility(View.VISIBLE);
                // buyer_trend_love_list.setVisibility(View.GONE);
            } else {
                nodata.setVisibility(View.GONE);
                //  buyer_trend_love_list.setVisibility(View.VISIBLE);
            }
        }
    }


    public int matchValue(ArrayList<BuyerLovesItem> buyerTrendLoveItemArrayList, String value) {
        int pos = -1;
        for (int j = 0; j < buyerTrendLoveItemArrayList.size(); j++) {
            if (buyerTrendLoveItemArrayList.get(j).getPostid().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    public void getUserDetails(String userid, final BuyerLovesItem buyerLovesItem) {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        buyerLovesItem.setUsername(getName(users.getFirstName(), users.getLastName()));
                        buyerLovesItem.setType(users.getJobTitle());
                        buyerLovesItem.setUsercompany(users.getCompanyName());
                        buyerLovesItem.setUserimg(users.getProfilePic());
                        buyerLovesItem.setUserpostid(users.getUserID());

                        int pos = matchValue(buyerLovesItemArrayList, buyerLovesItem.getPostid());
                        if (pos == -1)
                            return;
                        buyerLovesItemArrayList.set(pos, buyerLovesItem);
                        buyerLovesAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

}
