package tnm.trendontap.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.BuyerPagerAdapter;
import tnm.trendontap.adapter.PagerAdapter;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.Utils.getName;


public class Buyers_Trend_Love_Fragment extends Fragment {
    public static ViewPager viewpager;
    TabLayout tabLayout;
    BuyerPagerAdapter pagerAdapter;
    ImageView back_img;
    TextView buyerlovestxt;
    Users users;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyer_trend_loves, container, false);
        activity = getActivity();
        users = new Gson().fromJson(getArguments().getString("userdetail"), Users.class);
        buyerlovestxt = (TextView) view.findViewById(R.id.buyerlovestxt);
        buyerlovestxt.setTypeface(RobotoFont.boldFont(activity));
        buyerlovestxt.setText(getName(users.getFirstName(), users.getLastName()));
        back_img = (ImageView) view.findViewById(R.id.back_img);
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });

        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
        createViewPager(viewpager);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                pagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        viewpager.setCurrentItem(0);
        //viewpager.getAdapter().notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewpager);
        return view;
    }

    private void createViewPager(final ViewPager viewPager) {
        // buyerTrendItemArrayList.clear();
        //buyerLoveItemArrayList.clear();
        pagerAdapter = new BuyerPagerAdapter(getChildFragmentManager());

        if (!pagerAdapter.isExist(new Buyer_Trend_Pager_Fragment())) {
            pagerAdapter.addFrag(Buyer_Trend_Pager_Fragment.
                    newInstance(getArguments().getString("userdetail")), "Trends");


        }

        if (!pagerAdapter.isExist(new Buyer_Love_Pager_Fragment())) {
            pagerAdapter.addFrag(Buyer_Love_Pager_Fragment.
                    newInstance(getArguments().getString("userdetail")), "Loves");
        }


        viewPager.setAdapter(pagerAdapter);

        /*FirebaseDatabase.getInstance().getReference().child("posts").orderByChild("userId").equalTo(users.getUserId()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot trendlovelist : dataSnapshot.getChildren()) {
                            if (trendlovelist.child("postType").getValue().toString().equalsIgnoreCase("Trend")) {
                                buyerTrendItemArrayList.add(new BuyerTrendLoveItem(
                                        trendlovelist.child("photoUrls").child("0").getValue().toString(),
                                        trendlovelist.child("postTitle").getValue().toString(),
                                        trendlovelist.child("catagoryName").getValue().toString(),
                                        trendlovelist.child("content").getValue().toString(),
                                        trendlovelist.child("deliveryDate").getValue().toString(),
                                        (long) trendlovelist.child("timeStamp").getValue(),
                                        (long) trendlovelist.child("reactionsCount").getValue(),
                                        trendlovelist.child("id").getValue().toString(), users.getUserId()));
                            } else {
                                buyerLoveItemArrayList.add(new BuyerTrendLoveItem(
                                        trendlovelist.child("photoUrls").child("0").getValue().toString(),
                                        trendlovelist.child("postTitle").getValue().toString(),
                                        trendlovelist.child("catagoryName").getValue().toString(),
                                        trendlovelist.child("content").getValue().toString(),
                                        trendlovelist.child("deliveryDate").getValue().toString(),
                                        (long) trendlovelist.child("timeStamp").getValue(),
                                        (long) trendlovelist.child("reactionsCount").getValue(),
                                        trendlovelist.child("id").getValue().toString(), users.getUserId()));
                            }

                        }
                        pagerAdapter.addFrag(Buyer_Trend_Pager_Fragment.
                                newInstance(getArguments().getString("userdetail")), "Trends");

                        pagerAdapter.addFrag(Buyer_Love_Pager_Fragment.
                                newInstance(getArguments().getString("userdetail")), "Loves");


                        viewPager.setAdapter(pagerAdapter);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.getBadgeCount(MainActivity.activitycnt,activity);
    }
}
