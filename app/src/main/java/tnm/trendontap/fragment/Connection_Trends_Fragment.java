package tnm.trendontap.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import tnm.trendontap.R;
import tnm.trendontap.activity.PDFViewActivity;
import tnm.trendontap.adapter.ConnectionTrendAdapter;
import tnm.trendontap.modal.ActivityItem;
import tnm.trendontap.modal.BuyerTrendLoveItem;
import tnm.trendontap.modal.LocationItem;
import tnm.trendontap.modal.LovesItem;
import tnm.trendontap.modal.PhotoConnectStamp;
import tnm.trendontap.modal.Tag_Items;
import tnm.trendontap.modal.TrendsItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;

import static tnm.trendontap.utility.EventClass.BUYER_INVITE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_MSG;
import static tnm.trendontap.utility.EventClass.LIKE_TREND;
import static tnm.trendontap.utility.EventClass.sendNotification;
import static tnm.trendontap.utility.FireStoreUtils.Address;
import static tnm.trendontap.utility.FireStoreUtils.BuyerConnections;
import static tnm.trendontap.utility.FireStoreUtils.BuyerFollower;
import static tnm.trendontap.utility.FireStoreUtils.BuyerInvitations;
import static tnm.trendontap.utility.FireStoreUtils.Content;
import static tnm.trendontap.utility.FireStoreUtils.DeliveryDate;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUserActivities;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.FirebaseTrendOnTapPosts;
import static tnm.trendontap.utility.FireStoreUtils.FirstName;
import static tnm.trendontap.utility.FireStoreUtils.ID;
import static tnm.trendontap.utility.FireStoreUtils.Latitude;
import static tnm.trendontap.utility.FireStoreUtils.Likes;
import static tnm.trendontap.utility.FireStoreUtils.LikesCount;
import static tnm.trendontap.utility.FireStoreUtils.Location;
import static tnm.trendontap.utility.FireStoreUtils.Logitude;
import static tnm.trendontap.utility.FireStoreUtils.PdfURL;
import static tnm.trendontap.utility.FireStoreUtils.PostLikes;
import static tnm.trendontap.utility.FireStoreUtils.PostTitle;
import static tnm.trendontap.utility.FireStoreUtils.ReactionsCount;
import static tnm.trendontap.utility.FireStoreUtils.TagItems;
import static tnm.trendontap.utility.FireStoreUtils.TimeStamp;
import static tnm.trendontap.utility.FireStoreUtils.Title;
import static tnm.trendontap.utility.FireStoreUtils.Trends;
import static tnm.trendontap.utility.FireStoreUtils.UnreadReactionCount;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.UserPostFeeds;
import static tnm.trendontap.utility.FireStoreUtils.tPostPhotoId;
import static tnm.trendontap.utility.FireStoreUtils.tReactionCommentCount;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;


public class Connection_Trends_Fragment extends Fragment {

    private String mParam1;
    private String mParam2;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView name, city, useremail, userphoneno, connectbtn;
    ImageView profilepic;
    private String userid;
    private Users current_user;
    Preference preference;

    ListView connection_trend;
    ConnectionTrendAdapter connectionTrendAdapter;
    ArrayList<TrendsItem> connectionTrendsItems = null;


    Activity activity;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog;

    int selectedPos = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        connectionTrendsItems = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection_trends, container, false);
        activity = getActivity();

        View header = inflater.inflate(R.layout.header_connect, null);
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");

        connection_trend = (ListView) view.findViewById(R.id.connection_trend);

        profilepic = (ImageView) header.findViewById(R.id.profilepic);
        name = (TextView) header.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        city = (TextView) header.findViewById(R.id.city);
        city.setTypeface(RobotoFont.regularFont(activity));
        useremail = (TextView) header.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));
        userphoneno = (TextView) header.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));
        connectbtn = (TextView) header.findViewById(R.id.connectbtn);
        connectbtn.setTypeface(RobotoFont.regularFont(activity));

        userid = mParam1;
        preference = new Preference(activity, Login_Pref);
        current_user = new Gson().fromJson(preference.getString("userdetail"), Users.class);

        connection_trend.addHeaderView(header);
        connectionTrendAdapter = new ConnectionTrendAdapter(activity, connectionTrendsItems, mParam1);
        connection_trend.setAdapter(connectionTrendAdapter);

        connection_trend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (connectionTrendsItems == null && connectionTrendsItems.size() == 0)
                    return;

                selectedPos = position - 1;

                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 13);
                } else {
                    String pdfUrl = connectionTrendsItems.get(selectedPos).getPdfUrl();
                    downloadPDF(pdfUrl);
                }
            }
        });

        getBuyerTrendCount();
        setProfileData();

        return view;
    }

    public static Connection_Trends_Fragment newInstance(String param1, String param2) {
        Connection_Trends_Fragment fragment = new Connection_Trends_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void getBuyerTrendCount() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(userid).child(Trends)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            return;
                        }
                        getPostData();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void getPostData() {
        FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(userid).child(Trends).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getData(dataSnapshot);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getData(DataSnapshot dataSnapshot) {
        FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).orderByChild(ID).equalTo(dataSnapshot.getKey()).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "add");
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        manageData(dataSnapshot, "edit");
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        manageData(dataSnapshot, "delete");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void manageData(DataSnapshot trendlist, final String action) {

        long likecnt = (long) trendlist.child(LikesCount).getValue();
        ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
        if (trendlist.hasChild(TagItems)) {
            for (DataSnapshot messageSnapshot : trendlist.child(TagItems).getChildren()) {
                Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                tag_itemsArrayList.add(tag_Items);
            }
        }

        LocationItem location = null;
        if (trendlist.hasChild(Location)) {
            location = new LocationItem(trendlist.child(Location).child(Title).getValue().toString(),
                    trendlist.child(Location).child(Address).getValue().toString(),
                    (double) trendlist.child(Location).child(Latitude).getValue(),
                    (double) trendlist.child(Location).child(Logitude).getValue());
        }

        long comment_count = 0;
        try {
            if (trendlist.hasChild(tReactionCommentCount)) {
                comment_count = (long) trendlist.child(tReactionCommentCount).getValue();
            }
        } catch (Exception e) {

        }

        final TrendsItem trendsItem = new TrendsItem(trendlist.child(PostTitle).getValue().toString(),
                mParam1,
                trendlist.child(Content).getValue().toString(),
                "",
                trendlist.child(DeliveryDate).getValue().toString(),
                (long) trendlist.child(TimeStamp).getValue(),
                trendlist.child(ReactionsCount).getValue().toString(),
                (boolean) trendlist.child(UnreadReactionCount).getValue(),
                (ArrayList<String>) trendlist.child(tPostPhotoId).getValue(),
                trendlist.child(ID).getValue().toString(), "", "",
                likecnt, location,
                trendlist.child(PdfURL).getValue().toString(),
                trendlist.child(FireStoreUtils.UserID).getValue().toString(),
                comment_count
        );

        if (action.equalsIgnoreCase("add")) {
            connectionTrendsItems.add(trendsItem);
            connectionTrendAdapter.addList(connectionTrendsItems);
        } else if (action.equalsIgnoreCase("edit")) {
            int pos = matchValue(connectionTrendsItems, trendsItem.getId());
            if (pos == -1)
                return;
            connectionTrendsItems.set(pos, trendsItem);
            connectionTrendAdapter.notifyDataSetChanged();
        } else if (action.equalsIgnoreCase("delete")) {
            int pos = matchValue(connectionTrendsItems, trendsItem.getId());
            if (pos == -1)
                return;
            connectionTrendsItems.remove(pos);
            connectionTrendAdapter.notifyDataSetChanged();
        }

        if (!action.equalsIgnoreCase("delete"))
            getPostLikeCount(trendsItem);


    }

    public void manageLikeData(final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(trendsItem.getId()).child(Likes).
                addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        getUserDetails(dataSnapshot.getKey(), trendsItem);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    public void getPostLikeCount(final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(PostLikes).
                child(trendsItem.getId()).child(Likes).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.hasChildren()) {
                            int pos = matchValue(connectionTrendsItems, trendsItem.getId());
                            if (pos == -1)
                                return;
                            trendsItem.setLikecnt(0);
                            connectionTrendsItems.set(pos, trendsItem);
                            connectionTrendAdapter.notifyDataSetChanged();
                            return;
                        }
                        trendsItem.setLikecnt((int) dataSnapshot.getChildrenCount());
                        manageLikeData(trendsItem);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getUserDetails(String userid, final TrendsItem trendsItem) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).child(userid).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (trendsItem.getLiker1().equalsIgnoreCase("")) {
                            trendsItem.setLiker1(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        } else if (trendsItem.getLiker2().equalsIgnoreCase("")) {
                            trendsItem.setLiker2(String.valueOf(dataSnapshot.child(FirstName).getValue()));
                        }
                        int pos = matchValue(connectionTrendsItems, trendsItem.getId());
                        if (pos == -1)
                            return;
                        connectionTrendsItems.set(pos, trendsItem);
                        connectionTrendAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 13) {
            int access_write = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (access_write != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "Sorry!!!, you can't open File without granting permission", Toast.LENGTH_LONG).show();
            } else {
                String pdfUrl = connectionTrendsItems.get(selectedPos).getPdfUrl();
                downloadPDF(pdfUrl);
            }
        }
    }

    public int matchValue(ArrayList<TrendsItem> connectionTrendsItems, String value) {
        int pos = -1;
        for (int j = 0; j < connectionTrendsItems.size(); j++) {
            if (connectionTrendsItems.get(j).getId().equalsIgnoreCase(value)) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    public void downloadPDF(String url) {
        String[] urls = url.split("\\?");
        String[] urls1 = urls[0].split("/");

        String name = urls1[urls1.length - 1];
        viewPDF(name);
    }

    public boolean viewPDF(String name) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/TOT DOWNLOAD/" + name);  // -> filename = maven.pdf

        if (!pdfFile.exists()) {
            return false;
        }
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(activity, PDFViewActivity.class);
        pdfIntent.putExtra("pdfUrl", path.toString());
        try {
            startActivity(pdfIntent);
        } catch (Exception e) {
        }
        return true;
    }

    public void setTrendslist() {
        progressDialog.show();
        connectionTrendsItems.clear();
        ConnectUser_Fragment.photoTimeList.clear();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(UserPostFeeds).child(mParam1)
                .child(Trends);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                connectionTrendsItems.clear();
                if (dataSnapshot.getChildrenCount() == 0) {

                    progressDialog.dismiss();
                    return;
                }

                for (final DataSnapshot trenduser : dataSnapshot.getChildren()) {

                    FirebaseDatabase.getInstance().getReference().child(FirebaseTrendOnTapPosts).child(trenduser.getKey())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot trendlist) {
                                    connectionTrendsItems.clear();
                                    if (!trendlist.hasChildren())
                                        return;
                                    long likecnt = (long) trendlist.child(LikesCount).getValue();
                                    ArrayList<Tag_Items> tag_itemsArrayList = new ArrayList<>();
                                    if (trendlist.hasChild(TagItems)) {
                                        for (DataSnapshot messageSnapshot : trendlist.child(TagItems).getChildren()) {
                                            Tag_Items tag_Items = messageSnapshot.getValue(Tag_Items.class);
                                            tag_itemsArrayList.add(tag_Items);
                                        }
                                    }

                                    LocationItem location = null;
                                    if (trendlist.hasChild(Location)) {
                                        location = new LocationItem(trendlist.child(Location).child(Title).getValue().toString(),
                                                trendlist.child(Location).child(Address).getValue().toString(),
                                                (double) trendlist.child(Location).child(Latitude).getValue(),
                                                (double) trendlist.child(Location).child(Logitude).getValue());
                                    }

                                    long comment_count = 0;
                                    try {
                                        if (trendlist.hasChild(tReactionCommentCount)) {
                                            comment_count = (long) trendlist.child(tReactionCommentCount).getValue();
                                        }
                                    } catch (Exception e) {

                                    }

                                    final TrendsItem trendsItem = new TrendsItem(trendlist.child(PostTitle).getValue().toString(),
                                            mParam1,
                                            trendlist.child(Content).getValue().toString(),
                                            "",
                                            trendlist.child(DeliveryDate).getValue().toString(),
                                            (long) trendlist.child(TimeStamp).getValue(),
                                            trendlist.child(ReactionsCount).getValue().toString(),
                                            (boolean) trendlist.child(UnreadReactionCount).getValue(),
                                            (ArrayList<String>) trendlist.child(FireStoreUtils.tPostPhotoId).getValue(),
                                            trendlist.child(ID).getValue().toString(), "", "",
                                            likecnt, location,
                                            trendlist.child(PdfURL).getValue().toString(),
                                            trendlist.child(FireStoreUtils.UserID).getValue().toString(),
                                            comment_count
                                    );


                                    FirebaseDatabase.getInstance().getReference().child(PostLikes).
                                            child(trendlist.child(ID).getValue().toString()).child(Likes).
                                            addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(final DataSnapshot likerdataSnapshot) {
                                                    if (likerdataSnapshot.getChildrenCount() == 0) {

                                                        ArrayList<String> arrayList = new ArrayList<>();
                                                        arrayList.add(trendsItem.getPhotoUrlsList().get(0));
                                                        ConnectUser_Fragment.photoTimeList.add(new PhotoConnectStamp(trendsItem.getPostTitle(), trendsItem.getCategoryName(), trendsItem.getPhotoUrlsList().get(0), trendsItem.getId(), "trend", arrayList));
                                                        if (ConnectUser_Fragment.currentTabPos == 0) {
                                                            ConnectUser_Fragment.connectGridItemAdapter.addList(ConnectUser_Fragment.photoTimeList);
                                                            //ConnectUser_Fragment.connectGridItemAdapter.notifyDataSetChanged();
                                                        }
                                                        connectionTrendsItems.add(trendsItem);
                                                        connectionTrendAdapter.addList(connectionTrendsItems);
                                                        // connectionTrendAdapter.notifyDataSetChanged();
                                                        progressDialog.dismiss();
                                                    }

                                                    for (DataSnapshot likers : likerdataSnapshot.getChildren()) {
                                                        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers).
                                                                child(likers.getKey()).
                                                                addValueEventListener(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                                        for (int j = 0; j < dataSnapshot.getChildrenCount(); j++) {
                                                                            if (j == 0) {
                                                                                trendsItem.setLiker1(dataSnapshot.child(FirstName).getValue().toString());
                                                                            } else if (j == 1) {
                                                                                trendsItem.setLiker2(dataSnapshot.child(FirstName).getValue().toString());
                                                                            } else {
                                                                                break;
                                                                            }
                                                                        }
                                                                        ArrayList<String> arrayList = new ArrayList<>();
                                                                        arrayList.add(trendsItem.getPhotoUrlsList().get(0));

                                                                        ConnectUser_Fragment.photoTimeList.add(new PhotoConnectStamp(trendsItem.getPostTitle(), trendsItem.getCategoryName(), trendsItem.getPhotoUrlsList().get(0), trendsItem.getId(), "trend", arrayList));
                                                                        if (ConnectUser_Fragment.currentTabPos == 0) {
                                                                            ConnectUser_Fragment.connectGridItemAdapter.addList(ConnectUser_Fragment.photoTimeList);
                                                                            // ConnectUser_Fragment.connectGridItemAdapter.notifyDataSetChanged();
                                                                        }
                                                                        connectionTrendsItems.add(trendsItem);
                                                                        connectionTrendAdapter.addList(connectionTrendsItems);
                                                                        //  setListViewHeightBasedOnChildren(connection_trend);
                                                                        progressDialog.dismiss();
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {

                                                                    }
                                                                });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    progressDialog.dismiss();
                                                }
                                            });

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    progressDialog.dismiss();
                                }
                            });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });


    }

    private void setProfileData() {
        /*******************************************************************/
        DatabaseReference rootInvited = FirebaseDatabase.getInstance().getReference();
        Query query_invited = rootInvited.child(BuyerInvitations).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query_invited.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connectbtn.setText("Requested");
                    connectbtn.setTextColor(Color.parseColor("#000000"));
                    connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
                } else {
                    connectbtn.setText("Connect");

                    checkIfConneced();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /******************************************************************/

        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers)
                .child(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        ConnectUser_Fragment.labeltxt.setText(getName(users.getFirstName(), users.getLastName()));
                        city.setText(users.getCity());
                        useremail.setText(users.getEmailID());
                        userphoneno.setText(users.getPhone());
                        name.setText(users.getJobTitle());

                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.mipmap.userplaceholder);
                            requestOptions.error(R.mipmap.userplaceholder);

                            Glide.with(activity.getApplicationContext())
                                    .load(users.getProfilePic())
                                    .apply(requestOptions)
                                    .into(profilepic);
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        connectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectbtn.getText().toString().equalsIgnoreCase("Connect")) {
                    // connect();

                    FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).setValue(true);
                    connectbtn.setText("Requested");

                    FirebaseDatabase.getInstance().getReference().child(BuyerInvitations).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(true);
                    connectbtn.setText("Requested");

                    ActivityItem activityItem = new ActivityItem();
                    activityItem.setProfilePic(current_user.getProfilePic());
                    activityItem.setLongMessage("started");
                    activityItem.setMessage(current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG);
                    activityItem.setPostID("");
                    activityItem.setPostImageURL("");
                    activityItem.setReactionID("");
                    activityItem.setShortMessage("following you");
                    activityItem.setTiggerBy(current_user.getFirstName() + " " + current_user.getLastName());
                    activityItem.setActivityType(BuyerFollower);
                    activityItem.setTimeStamp(System.currentTimeMillis());
                    activityItem.setUserID(current_user.getUserID());


                    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("activities");
                    final String key = databaseReference.push().getKey();
                    activityItem.setId(key);
                    databaseReference.child(key).setValue(activityItem);

                    FirebaseDatabase.getInstance().getReference().child(FirebasePathUserActivities).
                            child(userid).
                            child(key).setValue(true);

                    sendNotification(userid, current_user.getFirstName() + " " + current_user.getLastName() + BUYER_INVITE_MSG, activityItem);

                } else if (connectbtn.getText().toString().equalsIgnoreCase("Requested")) {

                } else {
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).
                            child(userid).removeValue();
                    FirebaseDatabase.getInstance().getReference().child(BuyerConnections).
                            child(userid).
                            child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
                    disconnect();
                }
            }
        });
    }

    private void checkIfConneced() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query query = rootRef.child(BuyerConnections).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userid)) {
                    connect();
                } else {
                    disconnect();

                }
               /* for (DataSnapshot requested : dataSnapshot.getChildren()){
                    if (users.getUserId().equals(requested.getKey())){
                        holder.connect.setText("Requested");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void connect() {
        ConnectUser_Fragment.isconnected = true;
        connectbtn.setText("Disconnect");
        ConnectUser_Fragment.connect.setVisibility(View.VISIBLE);
        connectbtn.setTextColor(Color.parseColor("#000000"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_transgrey_bg));
        ConnectUser_Fragment.disconnect.setVisibility(View.GONE);
    }

    public void disconnect() {
        ConnectUser_Fragment.isconnected = false;
        connectbtn.setText("Connect");
        ConnectUser_Fragment.connect.setVisibility(View.GONE);
        connectbtn.setTextColor(Color.parseColor("#FFFFFF"));
        connectbtn.setBackground(activity.getDrawable(R.drawable.rectangle_blue_bg));
        ConnectUser_Fragment.disconnect.setVisibility(View.VISIBLE);
    }


    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
