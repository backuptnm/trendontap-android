package tnm.trendontap.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import de.hdodenhof.circleimageview.CircleImageView;
import tnm.trendontap.R;
import tnm.trendontap.activity.ActivityList;
import tnm.trendontap.activity.EditProfile_Activity;
import tnm.trendontap.activity.MainActivity;
import tnm.trendontap.adapter.ProfilePagerAdapter;
import tnm.trendontap.modal.ReactionTrendItem;
import tnm.trendontap.modal.Users;
import tnm.trendontap.utility.FireStoreUtils;
import tnm.trendontap.utility.Preference;
import tnm.trendontap.utility.RobotoFont;
import tnm.trendontap.utility.Utils;

import static tnm.trendontap.utility.FireStoreUtils.BuyerUserID;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathCostSheet;
import static tnm.trendontap.utility.FireStoreUtils.FirebasePathPostReaction;

import static tnm.trendontap.utility.FireStoreUtils.FirebasePathUsers;
import static tnm.trendontap.utility.FireStoreUtils.Phone;
import static tnm.trendontap.utility.FireStoreUtils.PostByUserID;
import static tnm.trendontap.utility.FireStoreUtils.RecycleBin;
import static tnm.trendontap.utility.FireStoreUtils.Sample;
import static tnm.trendontap.utility.FireStoreUtils.UserID;
import static tnm.trendontap.utility.FireStoreUtils.isDeleted;
import static tnm.trendontap.utility.Utils.Login_Pref;
import static tnm.trendontap.utility.Utils.getName;
import static tnm.trendontap.utility.Utils.isDestroy;


public class Profile_Fragment extends Fragment {

    TextView labeltxt, name, company, useremail, userphoneno, notifycnt;
    ImageView edit_profile_btn;
    static TabLayout profiletabLayout;
    CircleImageView profilepic;
    ProfilePagerAdapter profilePagerAdapter;
    ViewPager profileviewpager;
    RelativeLayout activity2_icon_lay;
    ProgressDialog progressDialog;
    Preference preference;
    LinearLayout back_arrow;
    String userid = "", userrole = "", usertype = "";
    Activity activity;
    Users usersval;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        activity = getActivity();
        labeltxt = (TextView) view.findViewById(R.id.labeltxt);
        labeltxt.setTypeface(RobotoFont.boldFont(activity));
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        preference = new Preference(activity, Login_Pref);
        usersval = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        edit_profile_btn = (ImageView) view.findViewById(R.id.edit_profile_btn);
        activity2_icon_lay = (RelativeLayout) view.findViewById(R.id.activity2_icon_lay);

        if (getArguments() != null && getArguments().getBoolean("loggeduser")) {
            edit_profile_btn.setVisibility(View.VISIBLE);
            userid = usersval.getUserID();
            userrole = usersval.getUserType();
        } else {
            edit_profile_btn.setVisibility(View.GONE);
            userid = getArguments().getString("userid");
            userrole = Utils.Supplier;
        }

        if (userrole.equalsIgnoreCase(Utils.Supplier))
            activity2_icon_lay.setVisibility(View.GONE);
        else
            activity2_icon_lay.setVisibility(View.VISIBLE);

        notifycnt = (TextView) view.findViewById(R.id.notifycnt);
        notifycnt.setTypeface(RobotoFont.regularFont(activity));

        back_arrow = (LinearLayout) view.findViewById(R.id.back_arrow);
        back_arrow.setVisibility(View.VISIBLE);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.manageBack(activity, "");
            }
        });
        profilepic = (CircleImageView) view.findViewById(R.id.profilepic);

        activity2_icon_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, ActivityList.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });

        name = (TextView) view.findViewById(R.id.name);
        name.setTypeface(RobotoFont.regularFont(activity));
        company = (TextView) view.findViewById(R.id.company);
        company.setTypeface(RobotoFont.regularFont(activity));

        useremail = (TextView) view.findViewById(R.id.useremail);
        useremail.setTypeface(RobotoFont.regularFont(activity));


        edit_profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, EditProfile_Activity.class));
                activity.overridePendingTransition(R.anim.slide_up, R.anim.stay);
            }
        });
        userphoneno = (TextView) view.findViewById(R.id.userphoneno);
        userphoneno.setTypeface(RobotoFont.regularFont(activity));

        profileviewpager = (ViewPager) view.findViewById(R.id.profileviewpager);
        profiletabLayout = (TabLayout) view.findViewById(R.id.profiletabLayout);

        getUserDetails(userid);
        createViewPager(profileviewpager);
        return view;
    }

   /* @Override
    public void onResume() {
        super.onResume();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        usersval = new Gson().fromJson(preference.getString("userdetail"), Users.class);
        try {
            if (usersval.getBadgeCount() > 0) {
                notifycnt.setText(String.valueOf(usersval.getBadgeCount()));
                notifycnt.setVisibility(View.VISIBLE);
            } else {
                notifycnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            notifycnt.setVisibility(View.GONE);
        }
    }*/


    @Override
    public void onResume() {
        super.onResume();

        if (userrole.equalsIgnoreCase(Utils.Supplier)) {
            reactioncount();
            samplecount();
            costcount();
        } else {
            reactioncountBuyer();
            samplecountbuyer();
            costcountBuyer();
        }

        if (usersval.getUserType().equalsIgnoreCase(Utils.Supplier))
            MainActivity.getBadgeCount(MainActivity.activitycnt, activity);
        else
            MainActivity.getBadgeCount(notifycnt, activity);
    }


    private void createViewPager(final ViewPager viewPager) {
        profiletabLayout.addTab(profiletabLayout.newTab().setText("()\n" + "Reactions"));
        profiletabLayout.addTab(profiletabLayout.newTab().setText("()\n" + "Costing Sheets"));
        profiletabLayout.addTab(profiletabLayout.newTab().setText("()\n" + "Samples Requested"));
        profiletabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        profileviewpager.setCurrentItem(0);
        profilePagerAdapter = new ProfilePagerAdapter(getFragmentManager(), profiletabLayout.getTabCount(), userid, userrole);

        viewPager.setAdapter(profilePagerAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(profiletabLayout));

        profiletabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }



    public void reactioncount() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction).orderByChild(FireStoreUtils.UserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        String reactionval = "(" + dataSnapshot.getChildrenCount() + ")\n Reactions";
                        if (dataSnapshot.getChildrenCount() == 1) {
                            reactionval = "(" + dataSnapshot.getChildrenCount() + ")\n Reaction";
                        }
                        Profile_Fragment.profiletabLayout.getTabAt(0).setText(reactionval);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(0).setText("(0)\n" + "Reactions");
                    }
                });
    }

    public void reactioncountBuyer() {

        FirebaseDatabase.getInstance().getReference().child(FirebasePathPostReaction)
                .orderByChild(PostByUserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        int cnt = 0;
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            if (Utils.shouldShow(dataSnapshot1, Utils.Buyer)) {
                                cnt++;
                            }
                        }

                        String reactionval = "(" + cnt + ")\n Reactions";
                        if (cnt == 1) {
                            reactionval = "(" + cnt + ")\n Reaction";
                        }
                        Profile_Fragment.profiletabLayout.getTabAt(0).setText(reactionval);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(0).setText("(0)\n" + "Reactions");
                    }
                });
    }

    public void getUserDetails(String userid) {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathUsers)
                .child(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        users.setPhone(dataSnapshot.child(Phone).getValue().toString());
                        labeltxt.setText(getName(users.getFirstName(), users.getLastName()));
                        company.setText(users.getCompanyName());
                        useremail.setText(users.getEmailID());
                        if (users.getPhone() != null) {
                            userphoneno.setVisibility(View.VISIBLE);
                            userphoneno.setText(dataSnapshot.child(Phone).getValue().toString());
                        } else
                            userphoneno.setVisibility(View.GONE);

                        name.setText(users.getJobTitle());

                        if (!TextUtils.isEmpty(users.getProfilePic())) {
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.mipmap.userplaceholder);
                            requestOptions.error(R.mipmap.userplaceholder);

                            if (!isDestroy(activity)) {
                                Glide.with(activity.getApplicationContext())
                                        .load(users.getProfilePic())
                                        .apply(requestOptions)
                                        .into(profilepic);
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    public void samplecount() {
        FirebaseDatabase.getInstance().getReference().child(Sample).orderByChild(FireStoreUtils.UserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile_Fragment.profiletabLayout.getTabAt(2).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Samples Requested");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(2).setText("(0)\n" + "Samples Requested");
                    }
                });
    }

    public void samplecountbuyer() {
        FirebaseDatabase.getInstance().getReference().child(Sample)
                .orderByChild(BuyerUserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile_Fragment.profiletabLayout.getTabAt(2).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Samples Requested");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(2).setText("(0)\n" + "Samples Requested");
                    }
                });
    }

    public void costcount() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathCostSheet)
                .orderByChild(FireStoreUtils.UserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile_Fragment.profiletabLayout.getTabAt(1).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Costing Sheets");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(1).setText("(0)\n" + "Costing Sheets");
                    }
                });
    }

    public void costcountBuyer() {
        FirebaseDatabase.getInstance().getReference().child(FirebasePathCostSheet)
                .orderByChild(BuyerUserID).equalTo(userid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Profile_Fragment.profiletabLayout.getTabAt(1).setText("(" + dataSnapshot.getChildrenCount() + ")\n" + "Costing Sheets");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Profile_Fragment.profiletabLayout.getTabAt(1).setText("(0)\n" + "Costing Sheets");
                    }
                });
    }

}
